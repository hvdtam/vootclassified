$(document).ready(function()
    {

        var WebUrl = $('#webUrl').val()+'/';
        //var CKEDITOR_BASEPATH = '/assets/ckeditor/';
//=========================================================//
//              DEFAULT SETTINGS
//=========================================================//
        var dflag = 'dflag flag-icon flag-icon-';

        var defaultSettingsCity = $("#dCityBtn").val();
        var defaultSettingsFlag = $("#dFlagBtn").val();
        var defaultSettingsTheme = $("#dthemeBtn").val();
        var defaultbg = $("#dBgBtn").val();
        var UserIsLogin = $("#IsLogin").val();

        var bgPath = $("#dBgBtn").attr('data-url')+defaultbg;
        $('body').css("background-image","url("+bgPath+")");

        // // initialize with defaults
        // $("#input-upload-img1").fileinput();
        // $("#input-upload-img2").fileinput();
        // $("#input-upload-img3").fileinput();
        // $("#input-upload-img4").fileinput();
        // $("#input-upload-img5").fileinput();

        $("#dCity").text(defaultSettingsCity);
        $("#dCity2").text(defaultSettingsCity);
        $("#dflag1").removeClass();
        $("#dflag1").addClass(dflag+defaultSettingsFlag);
        $("#dflag2").removeClass();
        $("#dflag2").addClass(dflag+defaultSettingsFlag);
      //  $("#themeCss").attr('href',defaultSettingsTheme);
        if (UserIsLogin == 'login')
        {
            $('.loginFalse').hide();
            $('.loginTrue').show();

        }
        else
        {
            $('.loginFalse').show();
            $('.loginTrue').hide();
        }

//document.getElementById("theme").setAttribute("href", WebUrl+'/themes/assets/css/'+defaultSettingsTheme+'.css');
        Popper.Defaults.modifiers.preventOverflow = { enabled: false };



        //=========================================================//
        //       MASTER FORM AJAX FUNCTION FOR FRONTEND
        //=========================================================//
        $(".frontend-master-form").submit(function(event) {
            event.preventDefault(); // stopping submitting
            event.stopImmediatePropagation();
            var dataVar = $(this).serializeArray();
            var url = $(this).attr('action');
            var msg = '';
            var containBody = $(this).attr('id')+"-body";
            var containHeader = $(this).attr('id')+"-header";
            $.ajax({
                url: url,
                type: 'POST',
                dataType: 'json',
                data: dataVar
            }).done(function(response) {
                if (response.data.success == true) {
                    $('#'+containHeader).text(response.data.title);
                    $('#'+containBody).html("<h3>"+response.data.message+"</h3>");
                    $('.close_modal_v').hide();
                    //$('.frontend-master-form').modal('hide');
                    //=========================================================//
                    //              NOTIFICATION SETTINGS
                    //=========================================================//
                    var template = 5;
                    var title = response.data.title;
                    var icon = 'fa fa-success';
                    var message = response.data.message;
                    var alert_type = 'success';
                    notification(template,title,message,icon,false,false,alert_type);
                    //=========================================================//
                    //              NOTIFICATION SETTINGS END
                    //=========================================================//
                }
                else{


                    $('.frontend-master-form').modal('hide');
                    var msg = response.data.message;
                    $('#'+containBody).html("<h3>"+msg+"</h3>");

                    //=========================================================//
                    //              NOTIFICATION SETTINGS
                    //=========================================================//
                    var template = 5;
                    var title = response.data.title;
                    var message = response.data.message;
                    var icon = 'fa fa-warning';
                    var alert_type = 'danger';
                    notification(template,title,message,icon,false,false,alert_type);
                    console.log(response.data.success);
                    //console.log(JSON.stringify(response));
                }
            }).fail(function(response) {
                console.log("error:");
                //alert(response.data.message);
                return;
                //$('.frontend-master-form').modal('hide');
                //var msg = response.data.message;
                //$('#'+containBody).html("<h3>"+msg+"</h3>");
                ////=========================================================//
                ////              NOTIFICATION SETTINGS
                ////=========================================================//
                //var template = 5;
                //var title = response.data.title;
                //var message = response.data.message;
                //var icon = 'fa fa-warning';
                //var alert_type = 'danger';
                //notification(template,title,message,icon,false,false,alert_type);
                //console.log(response.data.success);
                ////console.log(JSON.stringify(response));
            });
            return false;

        });
//=========================================================//
//              DEFAULT SETTINGS
//=========================================================//

        $('.saveAdsBtn').click(function() {
            var value = $(this).attr('ads');
            var url = $(this).attr('data-ref');
            var del_url = $(this).attr('data-del');


            $.get(url, function (data)
            {
                //var $toastContent = $('<span><i class="fa fa-check-circle-o"></i> &nbsp;' + data + '</span>');
                // Materialize.toast($toastContent, 3000, 'toost-unfriend yellow darken-3 white-text');
            }).done(function (data) {
                if(data == "liked")
                {
                    var confrm = confirm("Already saved! Do you want to delete from save list?");
                    if(confrm == true)
                    {
                        $.get(del_url, function (data)
                        {
                            var dil = $('#dil'+value);

                            $('#saved'+value).text('Save');
                            dil.removeClass('fa-check');
                            dil.addClass('fa-heart');
                            return false;
                        }).fail(function () {
                            alert("fail! Some problem with remove property from save list. please try letter");
                            //

                        }).error(function (data) {
                            console.log(data);

                        });
                    }
                    else
                    {
                        var dil = $('#dil'+value);
                        $('#saved'+value).text('Saved');
                        dil.removeClass('fa-check');
                        dil.addClass('fa-heart');

                        return false;
                    }
                }
                else
                {
                    var dil = $('#dil'+value);
                    $('#saved'+value).text('Saved');
                    dil.removeClass('fa-heart');
                    dil.addClass('fa-check');
                    //=========================================================//
                    //              NOTIFICATION SETTINGS
                    //=========================================================//
                    var template = 5;
                    var title = ' Saved Successfully ';
                    var message = 'This ad save in your saved ads list.';
                    var icon = 'fa fa-check';
                    var alert_type = 'success';
                    notification(template,title,message,icon,false,false,alert_type);
                    //=========================================================//
                    //              NOTIFICATION SETTINGS END
                    //=========================================================//
                }
                // var $toastContent = $('<span><i class="fa fa-check-circle-o"></i> &nbsp;' + data + '</span>');
                //  Materialize.toast($toastContent, 3000, 'toost-unfriend yellow darken-3 white-text');

            }).fail(function () {
                alert("Please Login or Signup");
                $('.zmdi-favorite-outline').show();
                $('.zmdi-favorite').hide();

                //

            }).error(function (data) {
                console.log(data);

            });

        });
        $('input[agents]').click(function() {
            var value = $(this).attr('agents');
            var url = $(this).attr('data-ref');
            var del_url = $(this).attr('data-del');


            $.get(url, function (data)
            {
                //var $toastContent = $('<span><i class="fa fa-check-circle-o"></i> &nbsp;' + data + '</span>');
                // Materialize.toast($toastContent, 3000, 'toost-unfriend yellow darken-3 white-text');
            }).done(function (data) {
                if(data == "liked")
                {
                    var confrm = confirm("Already saved! Do you want to delete from save list?");
                    if(confrm == true)
                    {
                        $.get(del_url, function (data)
                        {
                            $('.zmdi-favorite-outline').show();
                            $('.zmdi-favorite').hide();
                            return false;
                        }).fail(function () {
                            alert("fail! Some problem with remove agents from save list. please try letter");


                            //

                        }).error(function (data) {
                            console.log(data);

                        });
                    }
                    else
                    {
                        $('.zmdi-favorite-outline').hide();
                        $('.zmdi-favorite').show();
                        return false;
                    }
                }
                // var $toastContent = $('<span><i class="fa fa-check-circle-o"></i> &nbsp;' + data + '</span>');
                //  Materialize.toast($toastContent, 3000, 'toost-unfriend yellow darken-3 white-text');

            }).fail(function () {
                alert("Please Login or Signup");
                $('.zmdi-favorite-outline').show();
                $('.zmdi-favorite').hide();

                //

            }).error(function (data) {
                console.log(data);

            });

        });
        $('#reviewbtn').click(function(){

            $('#reviewblc').removeClass('hidden');
            $('#reviewList').addClass('hidden');

            $('#reviewSubmit').removeClass('hidden');
            $('#reviewbtn').addClass('hidden');

        });
        $('.remSaveProp').click(function()
        {

            var del_url = $(this).attr('data-del');
            var id = $(this).attr('data-val');



            $.get(del_url, function (data)
            {
                //var $toastContent = $('<span><i class="fa fa-check-circle-o"></i> &nbsp;' + data + '</span>');
                // Materialize.toast($toastContent, 3000, 'toost-unfriend yellow darken-3 white-text');
            }).done(function (data) {

                $('#savedList'+id).hide();
            }).fail(function () {
                alert("Fail. Sorry please try after some time");


            }).error(function (data) {
                console.log(data);

            });

        });
        $('.remSaveAge').click(function()
        {

            var del_url = $(this).attr('data-del');
            var id = $(this).attr('data-val');



            $.get(del_url, function (data)
            {
                //var $toastContent = $('<span><i class="fa fa-check-circle-o"></i> &nbsp;' + data + '</span>');
                // Materialize.toast($toastContent, 3000, 'toost-unfriend yellow darken-3 white-text');
            }).done(function (data) {

                $('#savedListAgent'+id).hide();
            }).fail(function () {
                alert("Fail. Sorry please try after some time");


            }).error(function (data) {
                console.log(data);

            });

        });
        $('.leads').click(function() {
            var leads = $(this).attr('leads');
            var data_url = $(this).attr('data-ref');

            $.get(data_url, function (data)
            {
                //var $toastContent = $('<span><i class="fa fa-check-circle-o"></i> &nbsp;' + data + '</span>');
                // Materialize.toast($toastContent, 3000, 'toost-unfriend yellow darken-3 white-text');
            }).done(function (data) {

                alert("done")
            }).fail(function () {
                alert("Fail. Sorry please try after some time");


            }).error(function (data) {
                console.log(data);

            });
        });
        //leads_del
        $('.leads_del').click(function() {
            var leads = "#leads"+$(this).attr('data-val');
            var data_url = $(this).attr('data-del');

            $.get(data_url, function (data)
            {
                //var $toastContent = $('<span><i class="fa fa-check-circle-o"></i> &nbsp;' + data + '</span>');
                // Materialize.toast($toastContent, 3000, 'toost-unfriend yellow darken-3 white-text');
            }).done(function (data) {
                var old = $('.leads-total').text();
                $('.leads-total').text(old-1);
                $(leads).animate({left:800,display:"none"},500,function () {
                    $(leads).hide();
                });
            }).fail(function () {
                alert("Fail. Sorry please try after some time");

            }).error(function (data) {
                console.log(data);

            });
        });
        //task done
        $('.taskDone').click(function() {
            var taskId = $(this).attr('task');
            var data_url = $(this).attr('data-ref')+"/"+taskId;
            // alert(data_url);

            $.get(data_url, function (data)
            {
                //var $toastContent = $('<span><i class="fa fa-check-circle-o"></i> &nbsp;' + data + '</span>');
                // Materialize.toast($toastContent, 3000, 'toost-unfriend yellow darken-3 white-text');
            }).done(function (data) {
                // var old = $('.leads-total').text();
                // $('.taskId-total').text(old-1);
                // $(taskId).animate({left:800,display:"none"},500,function () {
                //     $(leads).hide();
                // });
            }).fail(function () {
                alert("Fail. Sorry please try after some time");

            }).error(function (data) {
                console.log(data);

            });
        });
        //task delete
        $('.taskDelete').click(function() {
            var confrm = confirm('Are You Sure Delete this Task???');
            if(confrm == true)
            {
                var taskId = $(this).attr('task');
                var data_url = $(this).attr('data-ref')+"/"+taskId;

                $.get(data_url, function (data)
                {
                    //var $toastContent = $('<span><i class="fa fa-check-circle-o"></i> &nbsp;' + data + '</span>');
                    // Materialize.toast($toastContent, 3000, 'toost-unfriend yellow darken-3 white-text');
                }).done(function (data) {
                    var old = $('.task-total').text();
                    $('.task-total').text(old-1);
                    $(".task"+taskId).animate({left:800,display:"none"},500,function () {
                        $(".task"+taskId).hide();
                    });
                }).fail(function () {
                    alert("Fail. Sorry please try after some time");

                }).error(function (data) {
                    console.log(data);

                });
            }

        });

        $('.contactDel').click(function() {
            var div_id = $(this).attr('data-val');
            var URL_f = $(this).attr('data-ref');

//contact
            var data_url =URL_f;

            $.get(data_url, function (data)
            {
                //var $toastContent = $('<span><i class="fa fa-check-circle-o"></i> &nbsp;' + data + '</span>');
                // Materialize.toast($toastContent, 3000, 'toost-unfriend yellow darken-3 white-text');
            }).done(function (data) {
                var old = $('#totalContacts').text();
                $('#totalContacts').text(old-1);
                $(".contact"+div_id).animate({left:600,display:"none"},500,function () {
                    $(".contact"+div_id).hide();
                });
            }).fail(function () {
                alert("Fail. Sorry please try after some time");

            }).error(function (data) {
                console.log(data);

            });

        });

        $('.noteDel').click(function()
        {

            var cnfrm = confirm("Are you sure for delete this note?");
            if(cnfrm === true)
            {
                var id = $(this).attr('data-val');

                var del_url = $(this).attr('data-ref')+"/"+id;;


                $.get(del_url, function (data)
                {
                    //var $toastContent = $('<span><i class="fa fa-check-circle-o"></i> &nbsp;' + data + '</span>');
                    // Materialize.toast($toastContent, 3000, 'toost-unfriend yellow darken-3 white-text');
                }).done(function (data) {

                    $("#noteblock"+id).animate({left:600,display:"none"},500,function () {
                        $("#noteblock"+id).hide();
                    });
                }).fail(function () {
                    alert("Fail. Sorry please try after some time");


                }).error(function (data) {
                    console.log(data);

                });
            }

        });

        $('.property_wrap').click(function(){
            var value = $(this);
            $('.property_wrap').removeClass("property_wrap_active");
            value.addClass("property_wrap_active");
            $('#ad-category').val(value.attr('data_value'));

            //alert(value.attr('data_value'));
            //$('#total').text('Product price: $1000');
        });

        $('.property_features').click(function(){
            var value = $(this);
            if(value.attr('class') == "col-lg-4 property_features")
            {
                // $('.property_features').removeClass("property_features_active");
                value.addClass("property_features_active");
                var inId = "#int"+value.attr('data_id');
                $(inId).val(value.attr('data_value'));
            }
            else
            {
                $(this).removeClass("property_features_active");
                // value.removeClass("property_features_active");
                var inId = "#int"+value.attr('data_id');
                $(inId).val("");
            }


            //alert(inId);
            //$('#total').text('Product price: $1000');
        });

        $('.zmdi').click(function(){
            var type = $(this).attr('data-tag');
            if(type == "listview")
            {
                $('.zmdi').removeClass('active');
                $(this).addClass('active');
                $('#content_wrap').removeClass('listings-grid');
                $('#content_block').removeClass('col-sm-12');
                $('#content_block').addClass('col-sm-8');
                $('#content_block').addClass('listings-list');
                $('.content_list').removeClass('col-sm-6');
                $('.content_list').removeClass('col-md-3');
                $('.listings-grid__main').addClass('pull-left ');
                $('').show();
                $('#show_in_list').removeClass('hidden');

            }
            else
            {
                $('.zmdi').removeClass('active');
                $(this).addClass('active');

                $('#content_wrap').addClass('listings-grid');
                $('#content_block').addClass('col-sm-12');
                $('#content_block').removeClass('col-sm-8');
                $('#content_block').removeClass('listings-list');
                $('.content_list').addClass('col-sm-6');
                $('.content_list').addClass('col-md-3');
                $('.listings-grid__main').removeClass('pull-left ');
                $('#show_in_list').addClass('hidden');
            }
            //  alert(type);
//        $('.property_wrap').removeClass("property_wrap_active");
//        value.addClass("property_wrap_active");
//        $('#ad-category').val(value.attr('data_value'));

            //alert(value.attr('data_value'));
            //$('#total').text('Product price: $1000');
        });

        $('.formCatSelect').click(function(){
            var data_cat = $(this).attr('data-cat');
            var data_sub_cat = $(this).attr('data-sub-cat');
            var search_type = $(this).attr('data-search-type');
            if (search_type == 'cat')
            {
                var text_inline = "All in " + data_cat;
                $('#autocomplete-category-ajax').val(data_cat);
                $('#autocomplete-type-ajax').val('cat');
                $('.autocomplete-category-ajax').val(data_cat);
                $('.autocomplete-type-ajax').val('cat');

            }
            else
            {
                var text_inline = data_cat + " in " + data_sub_cat;
                $('#autocomplete-category-ajax').val(data_sub_cat);
                $('#autocomplete-type-ajax').val('sub-cat');
                $('.autocomplete-category-ajax').val(data_sub_cat);
                $('.autocomplete-type-ajax').val('sub-cat');
            }

            $('.change-text').text(text_inline);
            //alert('hello');

        });

        //=========================================================//
        //              SEARCH AJAX FUNCTION
        //=========================================================//
        $('#dosearch7458').keyup(function(){
            $("#searchresult784259").show();
            $.ajax(
                {
                    type: "GET",
                    url: $("#searchUrl").val(),
                    //data: {Type: "ajax", _csrf : csrfToken},
                    data: {
                        q: $('#dosearch7458').val()
                        //_csrf : csrfToken


                    },
                    dataType: "html",
                    beforeSend : function ()
                    {
                        $("#searchresult784259").html("<h3>Searching</h3>");
                    },
                    success: function (response)
                    {
                        $("#searchresult784259").html(response);

                        //  console.log("success "+JSON.stringify(response));
                    },error:function(response)
                {
                    $("#searchresult784259").html("<h3>Not found</h3>");
                    console.log("error "+JSON.stringify(response));
                },complete:function (response) {
                    // alert(response)
                }

                });
        });

        $($).click(function(){
            $('#dosearch7458').val('');
            $('#searchresult784259').hide();
        });
        //=========================================================//
        //              LOGIN AJAX FUNCTION
        //=========================================================//
        $('.loginSubmit').click(function () {
            var username = $('#login_username').val();
            var password = $('#login_password').val();
            var Loginpath = $('#loginPath').val();
            var csrfToken = username;
            //loginBoxForm
            $("#loginBoxForm").addClass('hidden');
            $("#loginBox").removeClass('hidden');

            $.ajax(
            {
                type: "POST",
                url: Loginpath,
                //data: {Type: "ajax", _csrf : csrfToken},
                data: {
                    login_username: username,
                    login_password: password,
                    Type: 'ajax',
                    _csrf : csrfToken


                },
                beforeSend : function () {
                    $('.loginSubmit').text('Loging...');
                },
                success: function(response) {
                    if(response)
                    {
                        //console.log("data text: "+response);
                        //bg-l-green
                        $("#loginBox").removeClass('bg-light');
                        $("#loginBox").addClass('bg-l-green');
                        $("#login-icon").css('color','#fff');
                        $("#login-text").css('color','#fff');

                        $("#login-icon").removeClass('fa fa-circle-o-notch fa-spin');
                        $("#login-icon").addClass('fa fa-check');
                        $("#login-text").html('<h1>Login Success<h1>');
                        $('.loginSubmit').text('Logged in');

                        window.location.href = $("#HomeUrl").val();
                    }
                    else
                    {
                        //loginFormError
                        $("#loginBox").addClass('hidden');
                        $("#loginBoxForm").removeClass('hidden');
                        $("#loginFormError").removeClass('hidden');
                        $('.loginSubmit').text('submit');
                       // alert("fail");
                    }
                },error:function(response)
                {
                    console.log(JSON.stringify(response));
                    //alert(JSON.stringify(response));
                },complete:function (response) {
               // alert(response)
                }

           })
        });

        //=========================================================//
        //              POST FORM CATEGORY SELECT
        //=========================================================//
        $('.postFormCat_list').click(function ()
        {
            $('#post-from-modal-cat').addClass('hidden');
            $('#post-from-modal-sub').removeClass('hidden');

            var id = $(this).attr('data-postForm-cat');
            var cat_name = $(this).attr('data-postForm-cat-name');
            var cat_icon = $(this).attr('data-postForm-cat-icon');
            var csrfToken = id;
            var del_url = $('#subcatUrl').val()+id;

            $('.catChipsW').removeClass('hidden');
            $('.catChips').text(cat_name);
            $('#category45983').val(cat_name);
            $('.CmaainI > i').removeClass();
            $('.CmaainI > i').addClass(cat_icon);

//('#subcatUrl').val()+id;//
            $.ajax(
                {
                    type: "GET",
                    url: $("#subcatUrl").val(),
                    //data: {Type: "ajax", _csrf : csrfToken},
                    data: {
                        id: id,
                        _csrf : csrfToken


                    },
                    dataType: "html",
                    beforeSend : function ()
                    {
                        //$('.loginSubmit').text('Loging...');
                    },
                    success: function (response)
                    {
                        $("#post-modal-data").removeClass('hidden');
                        $("#post-modal-waiting").addClass('hidden');
                        $("#post-modal-data-list").html(response);

                      //  console.log("success "+JSON.stringify(response));
                    },error:function(response)
                    {
                        $("#waiting-icon").removeClass('fa fa-circle-o-notch fa-spin');
                        $("#waiting-icon").addClass('fa fa-check');
                        $("#waiting-text").html('<h1>Fail to Load<h1>');
                        console.log("error "+JSON.stringify(response));
                    },complete:function (response) {
                        // alert(response)
                    }

                });
        });

        //=========================================================//
        //              POST FORM VALIDATION SELECT
        //=========================================================//
      //  $('#post1489').submit();
        $('#button1id74568').click(function (){
            var cat = $("#category_input").val();
            var error = '';
            if(cat == '')
            {
                //=========================================================//
                //              NOTIFICATION SETTINGS
                //=========================================================//
                var template = 5;
                var title = ' Category Cannot be Blank ';
                var message = 'Please Choose Some Category';
                var icon = 'fa fa-warning';
                var alert_type = 'danger';
                notification(template,title,message,icon,false,false,alert_type);
                //=========================================================//
                //              NOTIFICATION SETTINGS END
                //=========================================================//
                error = true;
            };
            var sub = $("#subCategory_input").val();
            if(sub == '')
            {
                //=========================================================//
                //              NOTIFICATION SETTINGS
                //=========================================================//
                var template = 5;
                var title = ' Sub Category Cannot be Blank ';
                var message = 'Please Choose Some Sub Category';
                var icon = 'fa fa-warning';
                var alert_type = 'danger';
                notification(template,title,message,icon,false,false,alert_type);
                //=========================================================//
                //              NOTIFICATION SETTINGS END
                //=========================================================//
                error = true;
            }
            var country = $("#hiddenInputCountry").val();
            if(country == '')
            {
                //=========================================================//
                //              NOTIFICATION SETTINGS
                //=========================================================//
                var template = 4;
                var title = ' Country Cannot be Blank ';
                var message = 'Every good advertisement should have proper detail of location. please Choose Country first';
                var icon = WebUrl+'themes/assets/img/mapMarker.png';
                var alert_type = 'minimalist';
                notification(template,title,message,icon,false,false,alert_type);
                //=========================================================//
                //              NOTIFICATION SETTINGS END
                //=========================================================//
                error = true;
            }
            var state = $("#hiddenInputState").val();
            if(state == '')
            {
                //=========================================================//
                //              NOTIFICATION SETTINGS
                //=========================================================//
                var template = 4;
                var title = ' State Of Country Cannot be Blank ';
                var message = 'Every good advertisement should have proper detail of location. please Choose state first';
                var icon = WebUrl+'themes/assets/img/mapMarker.png';
                var alert_type = 'minimalist';
                notification(template,title,message,icon,false,false,alert_type);
                //=========================================================//
                //              NOTIFICATION SETTINGS END
                //=========================================================//
                error = true;
            }
            var city = $("#hiddenInputCity").val();
            if(city == '')
            {
                //=========================================================//
                //              NOTIFICATION SETTINGS
                //=========================================================//
                var template = 4;
                var title = ' Are You Kidding? City Cannot be Blank ';
                var message = 'Every good advertisement should have proper detail of location. please Choose state first';
                var icon = WebUrl+'themes/assets/img/mapMarker.png';
                var alert_type = 'minimalist';
                notification(template,title,message,icon,false,false,alert_type);
                //=========================================================//
                //              NOTIFICATION SETTINGS END
                //=========================================================//
                error = true;
            }
            var lat = $("#adsform-latitude").val();
            var long = $("#adsform-longitude").val();

            if(long == '' || lat == '')
            {
                $('.blocInput').show();
                error = true;
            }

            if (error == true){
                //alert('true');
                return true;
            }else {
               $('#post1489').submit();
            };

        });

        //=========================================================//
        //              DELET ADS FROM PROFILE AJAX FUNCTION
        //=========================================================//
        $('#allDel_btn').click(function (){
            var checkBox = document.querySelectorAll('input[name="ads-delete"]:checked'), values = [];
            for(var i = 0; i <= checkBox.length ; i ++)
            {
                var ads_id = checkBox[i].value;
                var list_id = "list_"+ads_id;
             //console.log(checkBox[i].value);
                adsDelete(ads_id,list_id,'allDel_btn');
            }
               // Array.prototype.forEach.call(checkBox,function (el) {
               //    //values.push(el.value);
               //    // alert(el.value);
               //     //console.log(el.value);
               //     adsDelete(el.value,'list_'+el.value,'allDel_btn');
               //    //alert("hello");
               // });
            //console.log(values);

        });

        //=========================================================//
        //              list show less js
        //=========================================================//


        $('.mobilevVerification').click(function(){


            var value = $("#adsform-mobile").val();
            var name = $("#adsform-name").val();
            var lenght = value.length;
            if(lenght <= 0)
            {
                alert("Enter The Number");
                return false;
            }
            else
            {
                var regex = new RegExp(/^\+?[0-9(),.-]+$/);
                if($.isNumeric(value))
                {
                    var baseUrl = $("#mobileurl124").val();

                    var url = baseUrl+"?name="+name+"&number="+value
                  //  alert(url);

                    $.get(url, function (data)
                    {
                        
                    }).done(function (data) {
                        $('.mobilevVerification').html("Send Again");
                        $('.otpHintS').text(data.message);
                        $('.field-enterotp').show();

                    }).fail(function () {
                        $('.otpHintS').text("Fail. Sorry please try after some time");

                    }).error(function (data) {
                        console.log(data);

                    });


                }
                else
                {
                    alert("Enter Valid Number");

                }
            };

        });
        $('.otpvalidate').click(function(){
            var otp = $("#enterotp").val();

            var url = $("#otpurl").val()+"?otp="+otp;
            $.get(url, function (data)
            {

            }).done(function (data) {
                if(data.code == 1)
                {
                    $('#otpmsg').text(data.message);
                    $('#otpalert').show();
                    $('#optvirifyresult').hide();
                    $('#button1id74568').show();

                }
                else
                {
                    $('.otpHintS').html(data.message);
                    //$('.field-enterotp').hide();
                }

            }).fail(function () {
                $('#optvirifyresult').text("Fail. Sorry please try after some time");

            }).error(function (data) {
                console.log(data);

            });
        });
    }

);
document.addEventListener("DOMContentLoaded", function() {
    let lazyImages = [].slice.call(document.querySelectorAll("img.lazy"));
    let active = false;

    const lazyLoad = function() {
        if (active === false) {
            active = true;

            setTimeout(function() {
                lazyImages.forEach(function(lazyImage) {
                    if ((lazyImage.getBoundingClientRect().top <= window.innerHeight && lazyImage.getBoundingClientRect().bottom >= 0) && getComputedStyle(lazyImage).display !== "none") {
                        lazyImage.src = lazyImage.dataset.src;
                        lazyImage.srcset = lazyImage.dataset.srcset;
                        lazyImage.classList.remove("lazy");

                        lazyImages = lazyImages.filter(function(image) {
                            return image !== lazyImage;
                        });

                        if (lazyImages.length === 0) {
                            document.removeEventListener("scroll", lazyLoad);
                            window.removeEventListener("resize", lazyLoad);
                            window.removeEventListener("orientationchange", lazyLoad);
                        }
                    }
                });

                active = false;
            }, 200);
        }
    };

    document.addEventListener("scroll", lazyLoad);
    window.addEventListener("resize", lazyLoad);
    window.addEventListener("orientationchange", lazyLoad);
});


var WebUrl = $('#webUrl').val()+'/';
function PostAd() {

    var cat = $("#category45983").val();
    var error = '';
    if(cat == '')
    {
        //=========================================================//
        //              NOTIFICATION SETTINGS
        //=========================================================//
        var template = 5;
        var title = ' Category Cannot be Blank ';
        var message = 'Please Choose Some Category';
        var icon = 'fa fa-warning';
        var alert_type = 'danger';
        notification(template,title,message,icon,false,false,alert_type);
        //=========================================================//
        //              NOTIFICATION SETTINGS END
        //=========================================================//
        error = true;
    };
    var sub = $("#Subcategory45983").val();
    if(sub == '')
    {
        //=========================================================//
        //              NOTIFICATION SETTINGS
        //=========================================================//
        var template = 5;
        var title = ' Sub Category Cannot be Blank ';
        var message = 'Please Choose Some Sub Category';
        var icon = 'fa fa-warning';
        var alert_type = 'danger';
        notification(template,title,message,icon,false,false,alert_type);
        //=========================================================//
        //              NOTIFICATION SETTINGS END
        //=========================================================//
        error = true;
    }
    var country = $("#hiddenInputCountry").val();
    if(country == '')
    {
        //=========================================================//
        //              NOTIFICATION SETTINGS
        //=========================================================//
        var template = 4;
        var title = ' Country Cannot be Blank ';
        var message = 'Every good advertisement should have proper detail of location. please Choose Country first';
        var icon = WebUrl+'themes/assets/img/mapMarker.png';
        var alert_type = 'minimalist';
        notification(template,title,message,icon,false,false,alert_type);
        //=========================================================//
        //              NOTIFICATION SETTINGS END
        //=========================================================//
        error = true;
    }
    var state = $("#hiddenInputState").val();
    if(state == '')
    {
        //=========================================================//
        //              NOTIFICATION SETTINGS
        //=========================================================//
        var template = 4;
        var title = ' State Of Country Cannot be Blank ';
        var message = 'Every good advertisement should have proper detail of location. please Choose state first';
        var icon = WebUrl+'themes/assets/img/mapMarker.png';
        var alert_type = 'minimalist';
        notification(template,title,message,icon,false,false,alert_type);
        //=========================================================//
        //              NOTIFICATION SETTINGS END
        //=========================================================//
        error = true;
    }
    var city = $("#hiddenInputCity").val();
    if(city == '')
    {
        //=========================================================//
        //              NOTIFICATION SETTINGS
        //=========================================================//
        var template = 4;
        var title = ' Are You Kidding? City Cannot be Blank ';
        var message = 'Every good advertisement should have proper detail of location. please Choose state first';
        var icon = WebUrl+'themes/assets/img/mapMarker.png';
        var alert_type = 'minimalist';
        notification(template,title,message,icon,false,false,alert_type);
        //=========================================================//
        //              NOTIFICATION SETTINGS END
        //=========================================================//
        error = true;
    }

    var lat = $("#adsform-latitude").val();
    var long = $("#adsform-longitude").val();

    if(long == '' || lat == '')
    {
        //=========================================================//
        //              NOTIFICATION SETTINGS
        //=========================================================//
        var template = 4;
        var title = 'Error: Choose Location Properly ';
        var message = 'It`s seems like you don`t choose location property. Please move marker to set your location 587 ';
        var icon = WebUrl+'themes/assets/img/mapMarker.png';
        var alert_type = 'minimalist';
        notification(template,title,message,icon,false,false,alert_type);
        //=========================================================//
        //              NOTIFICATION SETTINGS END
        //=========================================================//
        error = true;
    }
    if (error == true){
        return true;
    }
    else {
        return false;
    };


}
function popSubCat_a77(id,cat) {
    var TyUrl = $('#PostType7842').val()+'/'+id;
    $('.cat-model > i').addClass('fa fa-circle-o-notch fa-spin');
    $('.cat-model > span').text('Processing...');
    $('#Subcategory45983').val(cat);
    $('#subChipTxt').text(cat);
    $('.subChip > i').removeClass();
    $('.subChip > i').addClass("fa fa-angle-right");
    //=========================================================//
    //           AJAX FUNCTION FOR LOAD SUB CATEGORY TYPE
    //=========================================================//
    $.get(TyUrl, function (data)
    {
    }).done(function (data) {
        $("#adsform-type").html(data);
        $('#select-Category').modal('hide');
        $('.cat-model > i').removeClass('fa fa-circle-o-notch fa-spin');
        $('.cat-model > span').text('Close');
    }).fail(function (data) {
        $('#select-Category').modal('hide');
        alert("Some Error occure fetching detail about category. Please Check your internet connection ");
        //console.log(JSON.stringify(data));
        $('#selectRegion').modal('hide');

    }).error(function (data) {
        console.log(JSON.stringify(data));
    });
    //=========================================================//
    //           AJAX FUNCTION FOR LOAD SUB CATEGORY TYPE END :-
    //=========================================================//

};
function typeWriter(txt,speed,id)
{
    var i = 0;
   // alert('working');
    if (i < txt.length) {
        document.getElementById(id).innerHTML += txt.charAt(i);
        i++;
        setTimeout(typeWriter, speed);
    }
}
function demo_button()
{
    alert('Edit/Delete action not allow in demo version.');
}

//adsDelete();
function adsDelete(ads_id,hide_id,loadingClass)
{
    var del_url = $('#ads-delete-url').val()+'/'+ads_id;
    $.ajax({
        url: del_url,
        beforeSend: function( xhr ) {
            $('#'+hide_id).html("<td colspan='5' style='padding: 15px 20px;'><i class='fa fa-circle-o-notch fa-spin'></i> Deleting..</td>");
        }
    }).done(function( data )
    {
           // $('#'+hide_id).hide(1000,'swing');
        $('#'+hide_id).remove();

    }).fail(function ()
    {

        $('#'+hide_id).html("<td colspan='5' style='padding: 15px 20px;'><i class='fa fa-trash'></i> Fail to delete.</td>");

    });

}

function citySearch() {
    var city = $('#citySearchInput').val();
    var URL_f = $('#citySearchInput').attr('data-ref');

    var data_url = URL_f+'/'+city;
    $("#cityWaiting").removeClass('hidden');
    $("#cityWaiting").addClass('show');
    $("#citySearch").html('<i class="fa fa-circle-o-notch fa-spin"></i>');

    //c-12
    $("#c-12").removeClass('hidden');
    $("#c-12").addClass('show');
    $("#c-11").removeClass('show');
    $("#c-11").addClass('hidden');
    $.get(data_url, function (data)
    {
        // alert('done');
        // $("#cityWaitingResult").html(data);
        //
        // $("#cityWaiting").removeClass('hidden');
        // $("#cityWaiting").addClass('show');
        // <i class="fa fa-spinner fa-spin"></i><strong>Choose</strong>
        $("#citySearch").html('<i class="fa fa-circle-o-notch fa-spin"></i>');
        if(data == '0')
        {
            $("#c-11").text('Nothing Found. Search Again');
        }else{
            $("#c-11").text('Choose Your Location');

        }
    }).done(function (data) {

        $("#cityWaiting").html(data);
        $("#citySearch").html('<i class="icon-search"></i><strong> Find Again</strong>');
        $("#c-11").removeClass('hidden');
        $("#c-11").addClass('show');
        $("#c-12").removeClass('show');
        $("#c-12").addClass('hidden');

    }).fail(function (data) {
        $("#cityWaiting").text('Fail: to load');
        $("#citySearch").html('<i class="fa fa-warning"></i><strong> Find Again </strong>');
        $("#c-11").removeClass('hidden');
        $("#c-11").addClass('show');
        $("#c-12").removeClass('show');
        $("#c-12").addClass('hidden');


    }).error(function (data) {
        console.log(data);
        $("#cityWaiting").text('Error: '+data);
        $("#citySearch").html('<i class="fa fa-warning"></i><strong> Error </strong>');
        $("#c-11").removeClass('hidden');
        $("#c-11").addClass('show');
        $("#c-12").removeClass('show');
        $("#c-12").addClass('hidden');

    });

}
function addToGroup(id) {
    //alert("hello");
    var GroupId = $('#addMemberToGroup'+id).val();
    var UserId = $('#gData'+id).attr('data-uId');
    var URL_f = $('#gData'+id).attr('data-ref');

    var data_url = URL_f+"/"+GroupId+"/"+UserId;
    //  alert(data_url);

    $.get(data_url, function (data)
    {
        $('#AddMemberSuccessText').text(data);
        $("#addToGroup1").modal('hide');
        $("#AddMemberSuccess").modal('show');
    }).done(function (data) {

        $('#AddMemberSuccessText').text(data);
        $("#addToGroup1").modal('hide');
        $("#AddMemberSuccess").modal('show');
    }).fail(function (data) {
        alert('fail');
        $('#AddMemberSuccessText').text(data);
        $("#addToGroup1").modal('hide');
        $("#AddMemberSuccess").modal('show');

    }).error(function (data) {
        console.log(data);

    });


}
function selstar(val,sel)
{
    for(var x=1;x<=val;x++)
    {
        $('#'+sel+x).removeClass('mdc-text-grey-300');
        $('#'+sel+x).addClass('mdc-text-yellow-800');

    }

}

function remstar(val,sel)
{
    for(var x=1;x<=val;x++)
    {
        var rated = $('#'+sel+x).attr('rated');
        if(rated != "1")
        {
            $('#'+sel+x).addClass('mdc-text-grey-300');
            $('#'+sel+x).removeClass('mdc-text-yellow-800');
        }

    }
}
function setrate(val,sel)
{
    for(var x=1;x<=val;x++)
    {
        $('#'+sel+x).removeClass('mdc-text-grey-300');
        $('#'+sel+x).addClass('mdc-text-yellow-800');
        $('#'+sel+x).attr('rated','1');
    }
    var corect = 1 + val;
    for(var x=corect;x<=5;x++)
    {
        $('#'+sel+x).addClass('mdc-text-grey-300');
        $('#'+sel+x).removeClass('mdc-text-yellow-800');
        $('#'+sel+x).attr('rated','0');
    }
    $('#'+sel).val(val);
}
function demoss(jsonData)
{

    for(var key in jsonData)
    {
        var jsd = jsonData[key];
        alert(jsd.date);

    }


}
function setCity(country,state,city,type,flag) {
    //var city = city;
    var type = type;
    var flag = flag;
    var URL_f = $('#citySearchInput').attr('data-ref-set');
    var data_url = URL_f+'/'+country+'/'+state+'/'+city+'/'+type+'/'+flag;
    $('#CountryCip745').text(country);
    $('#StateCip754').text(state);
    $('#CityCip754').text(city);

    $('#hiddenInputCountry').val(country);
    $('#hiddenInputState').val(state);
    $('#hiddenInputCity').val(city);
    $("#"+city ).removeClass($("#"+city ).attr('class'));
    $("#"+city ).addClass('fa fa-spinner fa-spin');
    $.get(data_url, function (data)
    {
        // alert('done');
        // $("#cityWaitingResult").html(data);
        //
        // $("#cityWaiting").removeClass('hidden');
        // $("#cityWaiting").addClass('show');

    }).done(function (data) {
        $('.zmdi-check').removeClass('fa fa-check');
        $("#"+city ).removeClass('fa fa-spinner fa-spin');
        $("#"+city ).addClass('fa fa-check');
        $("#dCity").text(data.data.city);
        $("#dCity2").text(data.data.city);
        $(".CityText").text(city);
        $(".CityValue").val(city);


        $("#dflag1").removeClass();
        $("#dflag1").addClass(flag+data.data.flag.toLowerCase());
        $("#dflag2").removeClass();
        $("#dflag2").addClass(flag+data.data.flag.toLowerCase());
        $('#selectRegion').modal('hide');


        //=========================================================//
        //              NOTIFICATION SETTINGS
        //=========================================================//
        var template = 4;
        var title = data.data.city + ' is set as default '+ type;
        var message = data.data.city +' now set as your default '+type+'. Now we showing all ads in '+data.data.city+' or nearby';
        var icon = WebUrl+'themes/assets/img/mapMarker.png';
        var alert_type = 'minimalist';
        notification(template,title,message,icon,false,false,alert_type);
        //=========================================================//
        //              NOTIFICATION SETTINGS END
        //=========================================================//

    }).fail(function (data) {
        $('.dropdown').removeClass('open');
        $("#c-11").text('Fail: to set');
        $('#selectRegion').modal('hide');
        //=========================================================//
        //              NOTIFICATION SETTINGS
        //=========================================================//
        var template = 5;
        var title = city + ' is faild to set as default '+ type;
        var message = 'try again twice, your location is not default';
        var icon = 'fa fa-warning';
        var alert_type = 'danger';
        notification(template,title,message,icon,false,false,alert_type);
        //=========================================================//
        //              NOTIFICATION SETTINGS END
        //=========================================================//
    }).error(function (data) {
        console.log(data);
        $(".cityForm").text('Error: '+data);

    });




}

function notification(temp,title,message,icon,img,url,type)
{

    if (temp == 0)
    {
        $.notify({
            // options
            icon: 'glyphicon glyphicon-warning-sign',
            title: 'Bootstrap notify',
            message: 'Turning standard Bootstrap alerts into "notify" like notifications',
            url: 'https://github.com/mouse0270/bootstrap-notify',
            target: '_blank'
        }, {
            // settings
            element: 'body',
            position: null,
            type: "info",
            allow_dismiss: true,
            newest_on_top: false,
            showProgressbar: false,
            placement:
                {
                from: "top",
                align: "right"
            },
            offset:  {
                x: 50,
                y: 100
            },
            spacing: 10,
            z_index: 1031,
            delay: 5000,
            timer: 1000,
            url_target: '_blank',
            mouse_over: null,
            animate: {
                enter: 'animated fadeInDown',
                exit: 'animated fadeOutUp'
            },
            onShow: null,
            onShown: null,
            onClose: null,
            onClosed: null,
            icon_type: 'class',
            template: '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0}" role="alert">' +
            '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
            '<span data-notify="icon"></span> ' +
            '<span data-notify="title">{1}</span> ' +
            '<span data-notify="message">{2}</span>' +
            '<div class="progress" data-notify="progressbar">' +
            '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
            '</div>' +
            '<a href="{3}" target="{4}" data-notify="url"></a>' +
            '</div>'
        });
    };
    if(temp == 1)
    {
        $.notify(message, {
            offset: {
                x: 50,
                y: 100
            }
        });
    };
    if (temp == 2)
    {
        $.notify({
            title: '<strong>'+title+'</strong>',
            message: message
        },{
            type: type
        });
    };
    if(temp == 3)
    {
        // alert(type);
        // $.notify(message, {
        //     animate: {
        //         enter: 'animated fadeInRight',
        //         exit: 'animated fadeOutRight'
        //     }
        // },{
        //     type: type
        // });
        $.notify({
            icon: 'https://randomuser.me/api/portraits/med/men/77.jpg',
            title: 'Byron Morgan',
            message: 'Momentum reduce child mortality effectiveness incubation empowerment connect.'
        },{
            type: 'minimalist',
            delay: 5000,
            icon_type: 'image',
            template: '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0}" role="alert">' +
            '<img data-notify="icon" class="img-circle pull-left">' +
            '<span data-notify="title">{1}</span>' +
            '<span data-notify="message">{2}</span>' +
            '</div>'
        });

        //for url

        $.notify({
            message: "Check out my twitter account by clicking on this notification!",
            url: "https://twitter.com/Mouse0270",
            target: "_self"
        });

        //for icon
        $.notify({
            icon: 'fa fa-paw',
            message: "You're not limited to just Bootstrap Font Icons"
        });

        //Using Images Instead Of Font Icons
        $.notify({
            icon: "img/growl_64x.png",
            message: " I am using an image."
        },{
            icon_type: 'image'
        })
    }

    if(temp == 4)
    {

        $.notify({
            icon: icon,
            title: title,
            message: message,
            animate: {
                enter: 'animated fadeInRight',
                exit: 'animated fadeOutRight'
            }

        },{
            type: type,
            delay: 5000,
            offset: {
                x: 5,
                y: 100
            },
            icon_type: 'image',
            template: '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0}" role="alert">' +
            '<img data-notify="icon" class="img-circle pull-left">' +
            '<span data-notify="title">{1}</span>' +
            '<span data-notify="message">{2}</span>' +
            '</div>'
        });



    };
    if(temp == 5)
    {

        $.notify({
            icon: icon,
            title: title,
            message: message,
            animate: {
                enter: 'animated fadeInRight',
                exit: 'animated fadeOutRight'
            }

        },{
            type: type,
            delay: 5000,
            offset: {
                x: 5,
                y: 100
            },
            template: '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0}" role="alert">' +
            '<i data-notify="icon" class="img-circle pull-left"></i>' +
            '<span data-notify="title"><b>{1}</b></span><br>' +
            '<span data-notify="message">{2}</span>' +
            '</div>'
        });



    }
}


