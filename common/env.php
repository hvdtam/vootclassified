<?php
/**
 * Require helpers
 */
require_once(__DIR__ . '/helpers.php');

/**
 * Load application environment from .env file
 */
//$dotenv = new \Dotenv\Dotenv(dirname(__DIR__));
$dotenv = new \Dotenv\Dotenv(dirname(__DIR__),'.env');
$dotenv->load();

/**
 * Init application constants
 */
defined('YII_DEBUG') or define('YII_DEBUG', env_value('YII_DEBUG'));
defined('YII_ENV') or define('YII_ENV', env_value('YII_ENV', 'prod'));

