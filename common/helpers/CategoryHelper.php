<?php

namespace common\helpers;

use common\models\Article;
use common\models\ArticleDetail;
use common\models\Category;
use Faker\Provider\Text;
use frontend\helpers\TextHelper;
use Yii;

/*
 * Class ArticleHelper
 * Danh sách Function
 * 1. Chi tiết bài viết
 * 2. Số lượng bài viết theo danh mục
 * */

class CategoryHelper
{
    public static function getDetail($id, $update = false)
    {
        $update = true;

        $cacheKey = [
            Category::class,
            CACHE_CATEGORY_DETAIL . $id
        ];

        $data = dataCache()->get($cacheKey);

        $update = true;

        if ($data === false or $update) {
            /** @var Category $model */
            $model = Category::find()
                ->where(['id' => $id])
                ->one();
            $data = [];
            if ($model) {
                $data['id'] = $model['id'];
                $data['name'] = $model['name'];
                $data['icon'] = $model['fa_icon'];
                $data['slug'] = TextHelper::getCategoryText($model['name']);
            }
            $cacheKeySlug = [
                Category::class,
                CACHE_CATEGORY_DETAIL . $data['slug']
            ];
            /*Set cache*/
            dataCache()->set($cacheKey, $data, TimeHelper::SECONDS_IN_A_MINUTE);
            dataCache()->set($cacheKeySlug, $data, TimeHelper::SECONDS_IN_A_MINUTE);
        }

        return $data;
    }

    public static function getCatNameBySlug($id, $update = false){

        $cacheKey = [
            Category::class,
            CACHE_CATEGORY_DETAIL . $id
        ];

        $data = dataCache()->get($cacheKey);

        return $data;
    }
}