<?php
/**
 * Created by PhpStorm.
 * User: dungpx
 * Date: 12/20/2017
 * Time: 11:44 AM
 */

return [
    'a' => 'a',
    'Enter City, State, Zip' => 'Nhập Địa Điểm',
    'FIND' => 'Tìm Kiếm',
    'Choose Your Location' => 'Chọn Địa Điểm',
    'CURRENT LOCATION' => 'Địa Điểm Hiện Tại',
    'Find Anything Here' => 'Rao vặt miễn phí',

    'SEARCH IN ' => 'Tìm kiếm tại',
    //'Find Anything Here' => 'Tìm kiếm mọi thứ trong đây',
    'I am looking for a' => 'Tìm kiếm bản tin',
    'Popular Categories' => 'Danh mục xem nhiều',
    'Browse by' => 'Xem bởi',
    'Load More' => 'Xem thêm',
    'Show Less' => 'Ẩn lại',
    'Categories' => 'Danh mục',
    'View more' => 'Xem thêm',

];