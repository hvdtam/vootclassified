<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "seo_tools".
 *
 * @property int $id
 * @property string $google
 * @property string $alexa
 * @property string $yandex
 * @property string $bing
 * @property string $pherma_link
 * @property string $pherma_extension
 */
class SeoTools extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'seo_tools';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['google', 'alexa', 'yandex', 'bing', 'pherma_link', 'pherma_extension'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'google' => 'Google',
            'alexa' => 'Alexa',
            'yandex' => 'Yandex',
            'bing' => 'Yandex',
            'pherma_link' => 'Pherma Link',
            'pherma_extension' => 'Pherma Extension',
        ];
    }
}
