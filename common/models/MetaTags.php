<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "meta_tags".
 *
 * @property int $id
 * @property string $page
 * @property string $title
 * @property string $description
 * @property string $keyword
 * @property string $status
 */
class MetaTags extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'meta_tags';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['page', 'description', 'status'], 'string'],
            [['title', 'keyword'], 'string', 'max' => 225],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'page' => 'Page',
            'title' => 'Title',
            'description' => 'Description',
            'keyword' => 'Keyword',
            'status' => 'Status',
        ];
    }
}
