<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "sms_twilio".
 *
 * @property int $id
 * @property string $sid
 * @property string $msg_from
 * @property string $token
 * @property string $verification
 * @property string $otp_length
 * @property string $status
 */
class SmsTwilio extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sms_twilio';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['token', 'status', 'verification', 'otp_length'], 'string'],
            [['sid'], 'string', 'max' => 220],
            [['msg_from'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sid' => 'Twilio Account SID',
            'msg_from' => 'Twilio From',
            'token' => 'Twilio Auth Token',
            'status' => 'Status',
        ];
    }

//    public static function details()
//    {
//        $model = SmsTwilio::find()->one();
//        return [
//            'sid'=>$model->sid,
//            'msg_from'=>$model->msg_from,
//            'token'=>$model->token,
//            'status'=>$model->status,
//        ];
//    };

    public static function setup()
    {

        $model = self::find()->one();

        $sid = $model['sid'];
        $msg_from = $model['msg_from'];
        $token = $model['token'];
        if ($model['status'] == 1) {
            $sms_status = false;
        } else {
            $sms_status = true;
        }
        \Yii::$app->sms->useFileTransport = $sms_status;
        \Yii::$app->sms->messageConfig['from'] = $sms_status;
        \Yii::$app->sms->sid = $sid;
        \Yii::$app->sms->token = $token;


        return $model['otp_length'];

    }
}
