<?php

namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * Category model
 *
 * @property integer $id
 * @property string $name
 * @property string $name_vi
 * @property string $fa_icon
 * @property string $item
 *

 */

define('IMG_ICON_DIR', str_replace('frontend', '', \yii::getAlias('@frontend')) . 'images/icon/');

class Category extends ActiveRecord
{


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%category}}';
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['name', 'required'],
            [['name_en'], 'string', 'max' => 64],
            ['fa_icon', 'safe'],
            [['fa_icon'], 'file', 'extensions' => ['jpg', 'jpeg', 'png'], 'checkExtensionByMimeType' => false],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('app', 'Category Name'),
            'name_vi' => Yii::t('app', 'Category Name VN'),
            'fa_icon' => Yii::t('app', 'Category Icon'),
            'item' => Yii::t('app', 'Item')
        ];
    }

    public function uploadIcon()
    {
        $name = rand(137, 999) . time();
        $this->fa_icon->saveAs(IMG_ICON_DIR . $name . '.' . $this->fa_icon->extension);
        return $name . '.' . $this->fa_icon->extension;
    }

    public static function findId($name)
    {
        $model = static::find()->where(['name' => $name])->one();
        return ($model) ? $model['id'] : 0;
    }

    public static function findNameById($id)
    {
        $model = static::find()->where(['id' => $id])->one();
        return ($model) ? $model['name'] : 'Category Not Set, Set Category';
    }

    public static function addCounter($name)
    {
        $model = static::find()->where(['name' => $name])->one();
        $model->item = $model['item'] + '1';
        $model->save(false);
    }

    public static function removeCounter($name)
    {
        $model = static::find()->where(['name' => $name])->one();
        $model->item = $model['item'] - '1';
        $model->save(false);
    }

    public static function IconByName($name)
    {
        $model = static::find()->where(['name' => $name])->one();
        return $model['fa_icon'];
    }

    public function getTextForUrl(){
        return $this->name;
    }
    public function getName(){
        return $this->name;
    }
}
