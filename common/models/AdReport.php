<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "ad_report".
 *
 * @property int $id
 * @property int $ad_id
 * @property string $reason
 * @property string $message
 * @property string $email
 * @property string $url
 * @property string $seen
 * @property string $status
 * @property int $created_at
 */
class AdReport extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ad_report';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['seen', 'status'], 'safe'],
            [['ad_id', 'reason', 'message', 'email', 'url', 'created_at'], 'required'],
            [['ad_id', 'created_at'], 'integer'],
            [['reason', 'message', 'status'], 'string'],
            [['email'], 'string', 'max' => 225],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'ad_id' => Yii::t('app', 'Ad ID'),
            'reason' => Yii::t('app', 'Reason'),
            'message' => Yii::t('app', 'Message'),
            'email' => Yii::t('app', 'Email'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
        ];
    }

    public static function changeStatus($id, $status)
    {
        $report = AdReport::find()->where(['id' => $id])->one();
        $report->status = $status;
        $report->save(false);
    }
}
