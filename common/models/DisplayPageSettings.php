<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "display_page_settings".
 *
 * @property int $id
 * @property int $widget
 * @property int $priority
 * @property string $status
 */
class DisplayPageSettings extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'display_page_settings';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'widget', 'priority', 'status'], 'required'],
            [['id', 'widget', 'priority'], 'integer'],
            [['status'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'widget' => Yii::t('app', 'Widget'),
            'priority' => Yii::t('app', 'Priority'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    public static function changeStatus($id, $value)
    {
        $view = static::find()->where(['id' => $id,])->one();
        $view->status = "enable";
        if ($view->save(false)) {
            return true;
        } else {
            return false;
        };


    }
}
