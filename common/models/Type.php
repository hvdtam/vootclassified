<?php

namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * TYpe model
 *
 * @property integer $id
 * @property string $name
 * @property string $parent
 * @property string $item
 */
class Type extends ActiveRecord
{


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%type}}';
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['name', 'required'],
            ['parent', 'required'],
            ['item', 'safe']

        ];
    }

    public static function FindIdByP($parent)
    {
        $parentId = SubCategory::findId($parent);
        $model = static::find()->where(['parent' => $parentId])->asArray()->all();
        return $model->id;
    }

    public static function addCounter($name, $Subcat)
    {
        $parentId = SubCategory::findId($Subcat);
        $model = static::find()->where(['name' => $name])->andWhere(['parent' => $parentId])->one();

        $model->item = $model->item + '1';
        $model->save(false);
    }

    public static function addCounteFakerr($name, $Subcat)
    {
        $parentId = SubCategory::findId($Subcat);
        $model = static::find()->where(['name' => $name])->andWhere(['parent' => $parentId])->one();
        $model->item = $model->item + '1';
        $model->save(false);
    }
}
