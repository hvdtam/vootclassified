<?php

namespace common\models;
//Exception
use Yii;
use yii\db\ActiveRecord;
use yii\base\Exception;

/**
 * DefaultSetting model
 * @property string $id
 * @property string $country
 * @property string $state
 * @property string $city
 * @property string $themes
 * @property string $background
 * @property string $lazy_load
 * @property string $currency
 * @property string $language
 * @property string $active
 * @property string $codecanyon_username
 * @property string $purchase_code
 * @property string $script
 * @property string $updated_at
 *
 */
class DefaultSettingbkp extends ActiveRecord
{


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'default_setting';
    }

    /**
     * @inheritdoc
     */

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['themes', 'codecanyon_username', 'purchase_code', 'script'], 'safe'],
            ['theme', 'safe'],
            ['background', 'safe'],
            ['language', 'safe'],
            ['country', 'safe'],
            ['state', 'safe'],
            ['city', 'safe'],
            ['flag_icon', 'safe'],
            ['currency', 'safe'],
            ['active', 'safe'],
            ['lazy_load', 'safe'],
            ['updated_at', 'default', 'value' => time()]

        ];
    }

    public static function getDefaultSetting()
    {
        $default = DefaultSetting::find()->select('updated_at')->where(['active' => '1'])->one();
        $session = Yii::$app->session;
        $update = ($session->get('locationUpdate' !== NULL)) ? $session->get('locationUpdate') : false;

        $updatedTime = ($session->get('updatesettings') !== NULL) ? $session->get('updatesettings') : time();
        if ($updatedTime < $default->updated_at and $updatedTime !== $default->updated_at) {
            $default = DefaultSetting::find()->where(['active' => '1'])->one();

            $session->set('themes', $default['themes']);// database value
            $session->set('background', $default['background']);// database value
            $session->set('language', $default['language']);// database value

            $geoData = self::setgeodata();
            if (!$geoData) {
                $session->set('country', $default['country']);// database value
                $session->set('state', $default['state']);// database value
                $session->set('city', $default['city']);// database value
                $session->set('CurrCity', $default['city']);// database value
                $session->set('locationtype', 'city');
                $session->set('flag', $default['flag_icon']);// database value
                $session->set('currency', $default['currency']);// database value
            }


            $d_themes = $default['themes'];
            $ses_background = $default['background'];
            $ses_language = $default['language'];
            $session->set('updatesettings', $default->updated_at);
        };

        $ses_flag = $session->get('flag');//d
        $ses_country = $session->get('country');//d
        $ses_state = $session->get('state');
        $ses_city = $session->get('city');
        $ses_CurrCity = $session->get('CurrCity');
        $ses_language = $session->get('language');
        $ses_direction = $session->get('direction');
        $d_flag = ($ses_flag) ? strtolower($ses_flag) : strtolower($default['flag_icon']);
        $d_flag = ($ses_flag) ? strtolower($ses_flag) : strtolower($ses_flag);
        $background = $session->get('background');
        $ses_background = isset($background) ? $background : false;;
        $ses_lat = $session->get('lat');
        $ses_lng = $session->get('lng');
        $ses_L_type = $session->get('locationtype');
        $d_currency = $session->get('currency');
        $d_themes = $session->get('themes');;


        if (!isset($ses_CurrCity, $d_themes)) {

            $geoData = Track::getLocationInfoByIp();
            if ($geoData) {
                $ses_flag = $geoData['code'];
                $d_flag = ($ses_flag) ? strtolower($ses_flag) : strtolower($ses_flag);
                $ses_country = $geoData['country'];
                $ses_state = $geoData['state'];
                $ses_city = $geoData['city'];
                $ses_lat = $geoData['lat'];
                $ses_lng = $geoData['lng'];
                $ses_CurrCity = $geoData['city'];
                $d_currency = $geoData['currency_symbol'];
                //here some code for set default theme
                if (!$d_themes or !$d_currency or !$ses_background) {
                    $default = DefaultSetting::find()->where(['active' => '1'])->one();

                    $session->set('themes', $default['themes']);// database value
                    $session->set('background', $default['background']);// database value
                    $session->set('language', $default['language']);// database value

                    $session->set('country', $geoData['country']);// IP value
                    $session->set('state', $geoData['state']);// IP value
                    $session->set('city', $geoData['city']);// IP value
                    $session->set('CurrCity', $geoData['city']);// IP value
                    $session->set('lat', $geoData['lat']);// IP value
                    $session->set('lng', $geoData['lng']);// IP value
                    $session->set('locationtype', 'city');
                    $session->set('flag', $geoData['code']);// IP value
                    $session->set('currency', $geoData['currency_code']);// IP value

                    $d_themes = $default['themes'];
                    $ses_background = $default['background'];
                    $ses_language = $default['language'];
                }

                // theme code is end here
                $ses_L_type = 'city';
                $ses_Currency_symbol = $geoData['currency_symbol'];
            } elseif ($update === true or $update < time()) {
                $ses_flag = $session->get('flag');//d
                $ses_country = $session->get('country');//d
                $ses_state = $session->get('state');
                $ses_city = $session->get('city');
                $ses_CurrCity = $session->get('CurrCity');
                $ses_language = $session->get('language');
                $ses_direction = $session->get('direction');
                //$d_flag = ($ses_flag)?strtolower($ses_flag):strtolower($default['flag_icon']);
                $d_flag = ($ses_flag) ? strtolower($ses_flag) : strtolower($ses_flag);
                $background = $session->get('background');
                $ses_background = isset($background) ? $background : false;;
                $ses_lat = $session->get('lat');
                $ses_lng = $session->get('lng');
                $ses_L_type = $session->get('locationtype');
                $d_currency = $session->get('currency');
                $d_themes = $session->get('themes');;
            } else {
                $default = DefaultSetting::find()->where(['active' => '1'])->one();

                if (!$d_themes) {
                    $session->set('themes', $default['themes']);
                };

                $ses_flag = $session->get('flag');//d
                $d_flag = ($ses_flag) ? strtolower($ses_flag) : strtolower($default['flag_icon']);
                $ses_country = $default['country'];
                $ses_state = $default['state'];
                $ses_city = $default['city'];

                // ================   change this value according to your native place lat/lang values =====================//
                $ses_lat = "24.578941"; // change this value according to your native place lat values
                $ses_lng = "24.5879"; // change this value according to your native place lang values
                // ================   change this value according to your native place lat/lang values =====================//
                $ses_CurrCity = $session->get('CurrCity');
                $ses_L_type = $session->get('locationtype');

                $session->set('country', $default['country']);// database value
                $session->set('state', $default['city']);// database value
                $session->set('city', $default['state']);// database value
                $session->set('flag', $default['flag_icon']);// database value
                $session->set('lat', $ses_lat);// IP value
                $session->set('lng', $ses_lng);// IP value
                if (!$d_themes && !$d_currency && !$ses_background) {
                    $session->set('themes', $default['themes']);
                    $session->set('currency', $default['currency']);
                    $session->set('background', $default['background']);
                    $session->set('language', $default['language']);
                    $d_currency = $default['currency'];
                    $d_themes = $default['themes'];
                    $ses_background = $default['background'];
                    $ses_language = $default['language'];

                } else {
                    $d_currency = $default['currency'];
                    $d_themes = $default['themes'];
                    $ses_background = $default['background'];
                }
            }
        }


        $data = array(
            'flag' => $d_flag,
            'country' => $ses_country,
            'state' => $ses_state,
            'city' => $ses_city,
            'lat' => $ses_lat,
            'lng' => $ses_lng,
            'currency' => $d_currency,
            'themes' => $d_themes,
            'background' => $ses_background,
            'language' => $ses_language,
            'direction' => ($ses_direction) ? $ses_direction : 'ltr',
            'CurrCity' => $ses_CurrCity,
            'l_type' => $ses_L_type
        );
        return $data;

    }

    public function setgeodata()
    {
        $geoData = Track::getLocationInfoByIp();
        if ($geoData) {
            $session = Yii::$app->session;
            $ses_flag = $geoData['code'];
            $d_flag = ($ses_flag) ? strtolower($ses_flag) : strtolower($ses_flag);
            $ses_country = $geoData['country'];
            $ses_state = $geoData['state'];
            $ses_city = $geoData['city'];
            $ses_lat = $geoData['lat'];
            $ses_lng = $geoData['lng'];
            $ses_CurrCity = $geoData['city'];
            $d_currency = $geoData['currency_symbol'];


            // theme code is end here
            $session->set('country', $geoData['country']);// IP value
            $session->set('state', $geoData['state']);// IP value
            $session->set('city', $geoData['city']);// IP value
            $session->set('CurrCity', $geoData['city']);// IP value
            $session->set('lat', $geoData['lat']);// IP value
            $session->set('lng', $geoData['lng']);// IP value
            $session->set('locationtype', 'city');
            $session->set('flag', $geoData['code']);// IP value
            $session->set('currency', $geoData['currency_code']);// IP value
            // theme code is end here
            $ses_L_type = 'city';
            $ses_Currency_symbol = $geoData['currency_symbol'];

            return true;
        } else {
            return false;
        }
    }

    /**
     * @inheritdoc
     */

    public static function purchase($purchase, $user)
    {
        $code = trim($purchase); // have we got a valid purchase code?
        // $url = "https://api.envato.com/v3/market/author/sale?code=".$code;
        // $curl = curl_init($url);
        $sata = new States();
        $usercookie = $sata->cookie;
        $userItem = $sata->param;
// If you took $code from user input it's a good idea to trim it:

        $code = trim($purchase);

// Make sure the code is valid before sending it to Envato:

        if (!preg_match("/^(\w{8})-((\w{4})-){3}(\w{12})$/", $code))
            throw new \yii\web\HttpException(400, 'Invalid code', 405);

// Query using CURL:

        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => "https://api.envato.com/v3/market/author/sale?code={$code}",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_TIMEOUT => 20,

            CURLOPT_HTTPHEADER => array(
                "Authorization: Bearer " . $usercookie,
                "User-Agent: Enter a description of your app here for the API team"
            )
        ));

// Execute CURL with warnings suppressed:

        $response = @curl_exec($ch);


        if (curl_errno($ch) > 0) {
            throw new \yii\web\HttpException(400, "Failed to query Envato API: " . curl_error($ch), 405);
        };

// Validate response:

        $responseCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);


        if ($responseCode === 404) {
            throw new \yii\web\HttpException(400, "The purchase code was invalid ", 405);

        };


        if ($responseCode !== 200) {
            throw new \yii\web\HttpException(200, "Failed to validate code due to an error: HTTP {$responseCode}", 200);

        }

// Verify that the purchase code is for the correct item:
// (Replace the numbers 17022701 with your item's ID from its URL)

        $body = json_decode($response);

        if ($body->item->id !== $userItem) {
            throw new \yii\web\HttpException(400, "The purchase code provided is for a different item ", 405);

        };
        if ($body->buyer !== $user) {
            //  throw new Exception("The User detail provided is for a different item");
            throw new \yii\web\HttpException(400, 'The User detail provided is for a different item', 405);
        } else {
            return true;
        }
    }


    public $baseop = "MQ==";

    public function cookeyCreate()
    {
        return base64_decode($this->baseop);
    }
}
