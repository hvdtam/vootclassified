<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "component".
 *
 * @property int $id
 * @property int $priority
 * @property string $widgets_name
 * @property string $position
 * @property string $status
 */
class Component extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'component';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['priority', 'widgets_name', 'position', 'status'], 'required'],
            [['priority'], 'integer'],
            [['widgets_name', 'position', 'status'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'priority' => Yii::t('app', 'Priority'),
            'widgets_name' => Yii::t('app', 'Widgets Name'),
            'position' => Yii::t('app', 'Position'),
            'status' => Yii::t('app', 'Status'),
        ];
    }
}
