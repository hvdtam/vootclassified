<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "widgets".
 *
 * @property int $id
 * @property string $image
 * @property string $name
 * @property string $page
 * @property string $type
 * @property string $template
 * @property string $position
 * @property string $column
 * @property string $grid_size
 * @property int $priority
 * @property int $number_of_item
 * @property string $options
 * @property string $status
 */
class Widgets extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'widgets';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['image', 'name', 'page', 'type', 'template', 'position', 'column', 'grid_size', 'priority', 'number_of_item', 'options', 'status'], 'required'],
            [['page', 'type', 'position', 'column', 'grid_size', 'status'], 'string'],
            [['priority', 'number_of_item'], 'integer'],
            [['image', 'name', 'options', 'template'], 'string', 'max' => 225],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'image' => Yii::t('app', 'Image'),
            'name' => Yii::t('app', 'Name'),
            'page' => Yii::t('app', 'Page'),
            'type' => Yii::t('app', 'Type'),
            'template' => Yii::t('app', 'Template'),
            'position' => Yii::t('app', 'Position'),
            'column' => Yii::t('app', 'Column'),
            'grid_size' => Yii::t('app', 'Grid Size'),
            'priority' => Yii::t('app', 'Priority'),
            'number_of_item' => Yii::t('app', 'Number Of Item'),
            'options' => Yii::t('app', 'options'),
            'status' => Yii::t('app', 'Status'),
        ];
    }
}
