<?php

namespace common\models;

use frontend\components\Blog;
use frontend\components\MultiLanguageSupport;
use frontend\components\UserReviews;
use Yii;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;

/**
 * This is the model class for table "plugin".
 *
 * @property int $id
 * @property int $plugin_id
 * @property string $plugin_icon
 * @property string $plugin_name
 * @property string $plugin_version
 * @property string $plugin_admin_url
 * @property string $plugin_url
 * @property string $plugin_type
 * @property string $plugin_template
 * @property string $plugin_key
 * @property string $purchase_code
 * @property string $codecanyon_username
 * @property string $status
 */
class Plugin extends \yii\db\ActiveRecord
{
    public $data_sql;
    public $for;
    public $data;
    public $load_plugin;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'plugin';
    }

    function __construct()
    {
        try {
            $this->load_plugin = self::find()->select(['plugin_name', 'plugin_url'])->where(['status' => '1'])->asArray()->all();
        } catch (\Throwable $e) {
            $error = 'Error Message: ' . $e->getMessage();
            Yii::$app->session->setFlash('error', $error);
            Yii::$app->session->setFlash('error', 'Please update to new version');
            $this->load_plugin = false;

        }
        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['plugin_id', 'plugin_icon', 'plugin_name', 'plugin_version', 'plugin_admin_url', 'plugin_url', 'plugin_template', 'plugin_key', 'purchase_code', 'codecanyon_username', 'status'], 'safe'],
            [['plugin_id'], 'integer'],
            [['plugin_type', 'plugin_version', 'status'], 'string'],
            [['data_sql'], 'safe'],
            [['plugin_icon', 'plugin_name', 'plugin_admin_url', 'plugin_url', 'plugin_template', 'plugin_key', 'purchase_code', 'codecanyon_username'], 'string', 'max' => 225],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'plugin_id' => 'Plugin ID',
            'plugin_icon' => 'Plugin Icon',
            'plugin_name' => 'Plugin Name',
            'plugin_admin_url' => 'Plugin Admin Url',
            'plugin_url' => 'Plugin Url',
            'plugin_type' => 'Plugin Type',
            'plugin_template' => 'Plugin Template',
            'plugin_key' => 'Plugin Key',
            'purchase_code' => 'Purchase Code',
            'codecanyon_username' => 'Codecanyon Username',
            'status' => 'Status',
        ];
    }

    public static function WidgetsFlash($purchase, $user, $userItem)
    {
        $code = trim($purchase); // have we got a valid purchase code?
        // $url = "https://api.envato.com/v3/market/author/sale?code=".$code;
        // $curl = curl_init($url);
        $sata = new States();
        $usercookie = $sata->cookie;
// If you took $code from user input it's a good idea to trim it:
        $key = Yii::$app->params['key'];
        if ($key == base64_encode("dev") or $key == base64_encode("custom"))
            return true;
        $code = trim($purchase);

// Make sure the code is valid before sending it to Envato:

        if (!preg_match("/^(\w{8})-((\w{4})-){3}(\w{12})$/", $code))
            throw new \yii\web\HttpException(400, 'Invalid code', 405);

// Query using CURL:

        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => "https://api.envato.com/v3/market/author/sale?code={$code}",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_TIMEOUT => 20,

            CURLOPT_HTTPHEADER => array(
                "Authorization: Bearer " . $usercookie,
                "User-Agent: Enter a description of your app here for the API team"
            )
        ));

// Execute CURL with warnings suppressed:

        $response = @curl_exec($ch);


        if (curl_errno($ch) > 0) {
            throw new \yii\web\HttpException(400, "Failed to query Envato API: " . curl_error($ch), 405);
        };

// Validate response:

        $responseCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);


        if ($responseCode === 404) {
            throw new \yii\web\HttpException(400, "The purchase code was invalid ", 405);

        };


        if ($responseCode !== 200) {
            throw new \yii\web\HttpException(200, "Failed to validate code due to an error: HTTP {$responseCode}", 200);

        }

// Verify that the purchase code is for the correct item:
// (Replace the numbers 17022701 with your item's ID from its URL)

        $body = json_decode($response);

        if ($body->item->id !== $userItem) {
            throw new \yii\web\HttpException(400, "The purchase code provided is for a different item ", 405);

        };
        if ($body->buyer !== $user) {
            //  throw new Exception("The User detail provided is for a different item");
            throw new \yii\web\HttpException(400, 'The User detail provided is for a different item', 405);
        } else {
            return true;
        }
    }

    public function get()
    {
        if ($this->load_plugin) {
            foreach ($this->load_plugin as $plug) {
                //#7896 = show rating without description
                //#7845 = show rating with description
                //#m7895 = show menu link on top
                //#c7895 home items footer content
                //#0248 language button
                //#64758 for blog
                if ($plug['plugin_name'] == "user-reviews") {

                    if ($this->for == "7896") {
                        Reviews::show($this->data);
                    };
                    if ($this->for == "7856") {
                        Reviews::show($this->data, true);
                    };
                    if ($this->for == "8459") {
                        echo UserReviews::widget(['model' => $this->data]);
                    };
                    if ($this->for == "8410") {
                        echo '<li class="nav-item">
                            <a class="nav-link" id="three-tab" data-toggle="tab" href="#three" role="tab" aria-controls="Three" aria-selected="false">Reviews</a>
                        </li>';
                    }

                }

                if ($plug['plugin_name'] == "multi language support") {
                    if ($this->for == "0248") {
                        echo MultiLanguageSupport::widget(['defaultLanguage' => $this->data]);
                    }
                }

                if ($plug['plugin_name'] == "blog-post-plugin") {
                    if ($this->for == "64758") {
                        echo Blog::widget();
                    };
                }
            }
        }

    }

    public static function getKeys()
    {
        $back = Yii::getAlias('@backendbase');
        $path = $back . "/i18n/";
        $fileTranslator = $path . 'translator.txt';
        $fileApiKey = $path . 'apiKey.txt';

        $ApiKey = is_file($fileApiKey);
        $ApiKey = ($ApiKey) ? file_get_contents($fileApiKey) : '';

        $translator = is_file($fileTranslator);
        $translator = ($translator) ? file_get_contents($fileTranslator) : '';
        $error = false;
        $translations = [
            'category' => [
                'class' => 'conquer\i18n\MessageSource',
                'translator' => [
                    //   'apiKey = obtain API key in the google developer console',
                    'class' => 'conquer\i18n\translators' . $translator,
                    'apiKey' => $ApiKey,
                ],
            ],
            'sub_category' => [
                'class' => 'conquer\i18n\MessageSource',
                'translator' => [
                    //   'apiKey = obtain API key in the google developer console',
                    'class' => 'conquer\i18n\translators' . $translator,
                    'apiKey' => $ApiKey,
                ],
            ],
            'type' => [
                'class' => 'conquer\i18n\MessageSource',
                'translator' => [
                    //   'apiKey = obtain API key in the google developer console',
                    'class' => 'conquer\i18n\translators' . $translator,
                    'apiKey' => $ApiKey,
                ],
            ],
            'site_header' => [
                'class' => 'conquer\i18n\MessageSource',
                'translator' => [
                    //   'apiKey = obtain API key in the google developer console',
                    'class' => 'conquer\i18n\translators' . $translator,
                    'apiKey' => $ApiKey,
                ],
            ],
            'site_footer' => [
                'class' => 'conquer\i18n\MessageSource',
                'translator' => [
                    //   'apiKey = obtain API key in the google developer console',
                    'class' => 'conquer\i18n\translators' . $translator,
                    'apiKey' => $ApiKey,
                ],
            ],
            'site_title' => [
                'class' => 'conquer\i18n\MessageSource',
                'translator' => [
                    //   'apiKey = obtain API key in the google developer console',
                    'class' => 'conquer\i18n\translators' . $translator,
                    'apiKey' => $ApiKey,
                ],
            ],
            'home' => [
                'class' => 'conquer\i18n\MessageSource',
                'translator' => [
                    //   'apiKey = obtain API key in the google developer console',
                    'class' => 'conquer\i18n\translators' . $translator,
                    'apiKey' => $ApiKey,
                ],
            ],
            '*' => [
                'class' => 'conquer\i18n\MessageSource',
                'translator' => [
                    //   'apiKey = obtain API key in the google developer console',
                    'class' => 'conquer\i18n\translators' . $translator,
                    'apiKey' => $ApiKey,
                ],
            ],
            'app' => [
                'class' => 'conquer\i18n\MessageSource',
                'translator' => [
                    //   'apiKey = obtain API key in the google developer console',
                    'class' => 'conquer\i18n\translators' . $translator,
                    'apiKey' => $ApiKey,
                ],
            ],
        ];
        if (empty($translator) || empty($ApiKey)) {
            $error = "Translator Configuration is wrong. Please set Translator Credentials";
            $class = 'yii\i18n\PhpMessageSource';
            $translations = [
                '*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    //'basePath' => '@app/messages',
                    'sourceLanguage' => 'en-US',
                    'fileMap' => [
                        'app' => 'app.php',
                        'app/error' => 'error.php',
                    ],
                ],
            ];
        }

        return ['error' => $error, 'translationsKeys' => $translations];
    }// common function for all

    public static function getLang()
    {

        try {
            $plugin = Plugin::find()->where(['plugin_name' => 'multi language support'])->andWhere(['status' => '1'])->count();
            if ($plugin !== "0") {
                return PluginLanguage::defaultlanguage();
            } else {
                return "en";
            }
        } catch (\Throwable $e) {
            return "en";

        }

    }
}
