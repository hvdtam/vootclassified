<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "widget_templat".
 *
 * @property int $id
 * @property int $widget_id
 * @property string $preview
 * @property string $template
 * @property string $name
 */
class WidgetTemplat extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'widget_templat';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['name', 'safe'],
            [['widget_id', 'preview', 'template'], 'required'],
            [['widget_id'], 'integer'],
            [['preview', 'template'], 'string', 'max' => 225],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'widget_id' => Yii::t('app', 'Widget ID'),
            'preview' => Yii::t('app', 'Preview'),
            'template' => Yii::t('app', 'Template'),
        ];
    }

    static function getNameByTemp($temp)
    {
        $modal = static::find('name')->where(['template' => $temp])->one();
        return $modal['name'];
    }
}
