<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "footer_content".
 *
 * @property int $id
 * @property int $column_id
 * @property string $type
 * @property string $target
 * @property string $name
 * @property string $url
 */
class FooterContent extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'footer_content';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['column_id', 'type', 'target', 'name', 'url'], 'required'],
            [['column_id'], 'integer'],
            [['type', 'target', 'url'], 'string'],
            [['name', 'name_en', 'name_jp'], 'string', 'max' => 225],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'column_id' => Yii::t('app', 'Column ID'),
            'type' => Yii::t('app', 'Type'),
            'target' => Yii::t('app', 'Target'),
            'name' => Yii::t('app', 'Name'),
            'url' => Yii::t('app', 'Url'),
        ];
    }
}
