<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use yii\web\UploadedFile;

/**
 * SiteSettings model
 *
 * @property integer $id
 * @property integer $site_name
 * @property string $site_title
 * @property string $fav_icon
 * @property string $logo
 * @property string $logo_width
 * @property string $logo_height
 * @property string $logo_padding
 * @property string $logo_margin
 * @property string $min_withdrawal_balance
 * @property string $meta_keyword
 * @property string $meta_description
 * @property string $script_version
 *
 */
define('IMG_SITE_DIR', \yii::getAlias('@frontendbase') . '/images/site/logo/');
define('FAV_SITE_DIR', \yii::getAlias('@frontendbase') . '/images/site/fav/');


class SiteSettings extends ActiveRecord
{

    /**
     * @var UploadedFile
     */

    public static function tableName()
    {
        return 'site_settings';
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
//fav_icon
            [['logo'], 'file', 'extensions' => ['jpg', 'jpeg', 'png'], 'checkExtensionByMimeType' => false],
            [['fav_icon'], 'file', 'extensions' => ['jpg', 'jpeg', 'ico', 'png'], 'checkExtensionByMimeType' => false],

            ['meta_keyword', 'default', 'value' => 'classified'],
            ['meta_description', 'default', 'value' => 'classified'],

            [['logo', 'fav_icon', 'logo_height', 'logo_width', 'logo_padding', 'logo_margin', 'site_title', 'meta_keyword', 'meta_description', 'site_name', 'script_version'], 'safe'],


        ];
    }

    public static function logo()
    {
        $pic = static::find()->one();
        $pic->logo;
        return $pic->logo;
    }


    public function uploadLogo()
    {
        $name = rand(137, 999) . time();
        $this->logo->saveAs(IMG_SITE_DIR . $name . '.' . $this->logo->extension);
        return $name . '.' . $this->logo->extension;
    }

    public function uploadFav()
    {
        $name = rand(137, 999) . time();
        $this->fav_icon->saveAs(FAV_SITE_DIR . $name . '.' . $this->fav_icon->extension);
        return $name . '.' . $this->fav_icon->extension;
    }


}
