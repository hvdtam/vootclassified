<?php

namespace common\models;

use Symfony\Component\HttpFoundation\File\Exception\AccessDeniedException;
use Yii;

/**
 * This is the model class for table "languages".
 *
 * @property int $id
 * @property string $translator
 * @property string $name
 * @property string $iso_code
 * @property string $country_sort_name
 * @property string $direction
 * @property string $active
 */
class Languages extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'languages';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['translator', 'name', 'iso_code', 'country_sort_name', 'direction', 'active'], 'safe'],
            [['translator', 'direction', 'active'], 'string'],
            [['name'], 'string', 'max' => 225],
            [['iso_code'], 'string', 'max' => 100],
            [['country_sort_name'], 'string', 'max' => 3],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'translator' => Yii::t('app', 'Translator'),
            'name' => Yii::t('app', 'Name'),
            'iso_code' => Yii::t('app', 'Iso Code'),
            'country_sort_name' => Yii::t('app', 'Country Sort Name'),
            'direction' => Yii::t('app', 'Direction'),
            'active' => Yii::t('app', 'Active'),
        ];
    }

    public static function setTranslator($translator, $apiKey)
    {
        //echo $layout;die;
        $path = Yii::$app->basePath . "/web/text/";
        $fileTranslator = $path . 'translator.txt';
        $fileApiKey = $path . 'apiKey.txt';

        //$uniq = file_get_contents($file);
        file_put_contents($fileTranslator, $translator);
        file_put_contents($fileApiKey, $apiKey);
        return $translator;
    }

    public static function getTranslator()
    {
        $back = Yii::getAlias('@backend');
        $path = $back . "/web/text/";
        //$file = $path.'layout.txt';

        $fileTranslator = $path . 'translator.txt';
        $fileApiKey = $path . 'apiKey.txt';

        $translator = file_get_contents($fileTranslator);
        $ApiKey = file_get_contents($fileApiKey);
        if ($translator === NULL or $ApiKey === NULL) {
            $translator == "YandexTranslator";
            throw new \yii\web\HttpException(503, "Translator Configuration is wrong. Please set Translator Credentials", 503);
        }
        // file_put_contents($file, $layout);

        return ['translator' => $translator, 'api_key' => $ApiKey];
    }

    public static function getDefault()
    {
        //  $default = DefaultSetting::find()->where(['active'=>'1'])->one();
        $session = Yii::$app->session;
        $ses_language = $session->get('language');
        $language = ($ses_language) ? $ses_language : "en";
        return $language;

    }

}
