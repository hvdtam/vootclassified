<?php

namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * SubCategory model
 *
 * @property integer $id
 * @property string $name
 * @property string $parent
 * @property string $item
 */
class SubCategory extends ActiveRecord
{


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%sub_category}}';
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['name', 'required'],
            ['parent', 'required'],

        ];
    }

    public static function findName($id)
    {
        $name = static::find()->where(['id' => $id,])->one();
        return ($name) ? $name['name'] : 'Sub Category Not Set, Set Sub Category';
    }

    public static function findId($name)
    {
        $name = static::find()->where(['name' => $name,])->one();
        return ($name) ? $name['id'] : false;
    }

    public static function addCounter($name)
    {
        $model = static::find()->where(['name' => $name])->one();
        $model->item = $model->item + '1';
        $model->save(false);
    }

    public static function removeCounter($name)
    {
        $model = static::find()->where(['name' => $name])->one();
        $model->item = $model->item - '1';
        $model->save(false);
    }

    public static function findParent($name)
    {
        $model = static::find()->where(['name' => $name])->one();
        //var_dump($name);
        // die();
        return $parentName = Category::findNameById($model['parent']);
    }


}
