<?php

namespace common\models;

use trntv\filekit\behaviors\UploadBehavior;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * ads model
 *
 * @property integer $id
 * @property string $user_id
 * @property string $guest
 * @property string $category
 * @property string $sub_category
 * @property string $type
 * @property string $ad_type
 * @property string $ad_title
 * @property string $currency_symbol
 * @property string $thumbnail_base_url
 * @property string $thumbnail_path
 * @property array $attachments
 * @property string $currency_ini
 * @property string $price
 * @property string $ad_description
 * @property string $image
 * @property string $name
 * @property string $mobile
 * @property string $email
 * @property string $active
 * @property string $premium
 * @property string $lat
 * @property string $lng
 * @property string $country
 * @property string $states
 * @property string $address
 * @property string $city
 * @property string $tags
 * @property string $view
 * @property string $more
 * @property string $fake
 * @property string $rating
 * @property integer $reviews
 * @property string $created_at
 * @property string $updated_at
 *
 */
class Ads extends ActiveRecord
{

    const STATUS_PUBLISHED = 1;
    const STATUS_DRAFT = 0;

    /**
     * @var array
     */
    public $attachments;

    /**
     * @var array
     */
    public $thumbnail;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%ads}}';
    }

    /**
     * @return array statuses list
     */
    public static function statuses()
    {
        return [
            self::STATUS_DRAFT => Yii::t('common', 'Draft'),
            self::STATUS_PUBLISHED => Yii::t('common', 'Published'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
            //BlameableBehavior::class,
            /*[
                'class' => SluggableBehavior::class,
                'attribute' => 'title',
                'immutable' => true,
            ],*/
            /*[
                'class' => UploadBehavior::class,
                'attribute' => 'attachments',
                'multiple' => true,
                'uploadRelation' => 'articleAttachments',
                'pathAttribute' => 'path',
                'baseUrlAttribute' => 'base_url',
                'orderAttribute' => 'order',
                'typeAttribute' => 'type',
                'sizeAttribute' => 'size',
                'nameAttribute' => 'name',
            ],*/
            [
                'class' => UploadBehavior::class,
                'attribute' => 'thumbnail',
                'pathAttribute' => 'thumbnail_path',
                'baseUrlAttribute' => 'thumbnail_base_url',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [

            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],

            ['category', 'required'],
            ['category', 'string', 'max' => 255],

            [['sub_category', 'currency_symbol', 'rating', 'reviews', 'currency_ini', 'more', 'fake', 'guest'], 'safe'],
            [['price', 'currency_symbol'], 'safe'],
            ['type', 'safe'],
            ['ad_title', 'required'],
            ['ad_title', 'string', 'max' => 255],

            ['ad_description', 'required'],
            ['ad_description', 'string', 'max' => 500],

//            [['image'], 'image','extensions' => 'png, jpg, gif','minWidth' => 300, 'maxWidth' => 300,
//                'minHeight' => 800, 'maxHeight' =>800,'message'=>'min image size 300x300 pixel'],
//
            [['thumbnail_base_url', 'thumbnail_path'], 'string', 'max' => 1024],
            [['image'], 'image', 'skipOnEmpty' => true, 'extensions' => ['jpg', 'jpeg', 'png'], 'maxFiles' => 5],
            ['name', 'required'],
            ['mobile', 'number'],
            ['mobile', 'required'],
            ['lat', 'safe'],
            ['lng', 'safe'],

            ['active', 'safe'],
            ['country', 'safe'],
            ['states', 'required'],
            ['city', 'required'],
            ['created_at', 'safe'],
            [['attachments', 'thumbnail'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'email' => Yii::t('app', 'Email'),
            'category' => Yii::t('app', 'Category'),
            'sub_category' => Yii::t('app', 'Sub Category'),
            'price' => Yii::t('app', 'Price'),
            'type' => Yii::t('app', 'Type'),
            'ad_title' => Yii::t('app', 'Ad Title'),
            'ad_description' => Yii::t('app', 'Ad Description'),
            'image' => Yii::t('app', 'Image'),
            'name' => Yii::t('app', 'Name'),
            'mobile' => Yii::t('app', 'Mobile Number'),
            'thumbnail' => Yii::t('app', 'Thumbnail'),
        ];
    }

    public static function view($id)
    {
        $view = static::find()->where(['id' => $id,])->one();
        $viewCount = $view->view;
        $view->view = $viewCount + 1;
        $view->save(false);
    }


    public static function getTitle($id)
    {
        $view = static::find()->where(['id' => $id])->one();
        return ($view) ? $view['ad_title'] : "NA";
    }

    public function ScreenShot()
    {
        foreach ($this->image as $file) {
            $name = rand(137, 999) . time();
            $screen[] = $name . '.' . $file->extension;
            $file->saveAs(SCREENSHOT . $name . '.' . $file->extension);
        }

        return $ScreenChunk = implode(",", $screen);

    }

    public static function findDistance($lat2, $lon2)
    {
        $lat1 = 26.9124;
        $lon1 = 78.7873;

        $unit = "K";
        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $unit = strtoupper($unit);

        if ($unit == "K") {
            $result = ($miles * 1.609344);
            $return = substr($result, 0, 5);
            return $return;
        } else if ($unit == "N") {
            return ($miles * 0.8684);
        } else {
            return substr($miles, 0, 4);
        }
    }

    public static function changeStatus($id, $status)
    {
        $modal = Ads::find()->where(['id' => $id])->one();
        $modal->active = $status;
        $modal->save(false);
    }

    public static function Price($price)
    {
        //102 78
        $count = strlen($price);
        $digit = $count - 2;
        if ($digit == 2) {
            return substr_replace($price, '', 2, 2) . " k";
        } elseif ($digit == 3) {
            return substr($price, 0, 2) . "K";
        } elseif ($digit == 4) {
            return substr_replace($price, '', 2, 4) . " Lac";
        } elseif ($digit == 5) {
            return substr_replace($price, '', 2, 5) . " Millions";
        } elseif ($digit == 6) {
            return substr_replace($price, '', 2, 6) . " billions";
        } elseif ($digit == 7) {
            return substr_replace($price, '', 2, 7) . " Cr";
        } else {
            return $price;
        }

        //return $count;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdsAttachments()
    {
        return $this->hasMany(AdsAttachment::class, ['ads_id' => 'id']);
    }

}

