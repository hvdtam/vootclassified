<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "ads_settings".
 *
 * @property int $id
 * @property string $ads_approval
 * @property int $number_of_photo
 * @property string $watermark_status
 * @property string $watermark_image
 * @property string $login_required
 * @property string $create_guest_account
 * @property int $number_of_tags
 */
define('WATERMARK', Yii::getAlias('@frontend') . '/web/images/site/watermark/');

class AdsSettings extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $model;
    public $adId;

    public static function tableName()
    {
        return 'ads_settings';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ads_approval', 'number_of_photo', 'watermark_status', 'login_required', 'watermark_image', 'number_of_tags'], 'required'],
            [['create_guest_account'], 'safe'],
            [['create_guest_account'], 'default', 'value' => 0],

            [['ads_approval', 'watermark_status'], 'string'],
            [['number_of_photo', 'number_of_tags'], 'integer', 'max' => 99],
            [['watermark_image'], 'file', 'checkExtensionByMimeType' => false, 'skipOnEmpty' => true, 'extensions' => 'png', 'maxFiles' => 1],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'ads_approval' => Yii::t('app', 'Ads Approval'),
            'number_of_photo' => Yii::t('app', 'Number Of Photo'),
            'watermark_status' => Yii::t('app', 'Watermark Status'),
            'watermark_image' => Yii::t('app', 'Watermark Image'),
            'login_required' => Yii::t('app', 'Registration For Post an Ads '),
            'create_guest_account' => Yii::t('app', 'Create Guest Account After posting Ad'),

            'number_of_tags' => Yii::t('app', 'Number Of Tags'),
        ];
    }

    public function uploadWaterMark()
    {
        $name = 'watermark-' . time();
        $this->watermark_image->saveAs(WATERMARK . $name . '.png');
        return $name . '.png';
    }

    public function Check()
    {


    }


    public function afterPost()
    {
        $model = $this->model;
        $check = User::find()->where(['email' => $model['email']])->one();

        if ($check) {
            return [
                'user' => true,
                'process' => false,
                'userId' => $check['id'],
                'error' => false
            ];
        } else {
            $username = $model['name'] . time();
            $user = new User();
            $user->email = $model['email'];
            $user->username = $username;
            $user->setPassword("12356");
            $user->generateAuthKey();
            $user->first_name = $model['name'];
            $user->last_name = "guest";
            $user->phone_number = $model['mobile'];
            $user->type = "individual";
            $user->about_you = "i am a guest user";
            if ($user->save(false)) {
                $userID = $user->id;
                Yii::$app->mailer->compose('welcomGuest', ['model' => $model, 'adId' => $this->adId, 'username' => $username])
                    ->setTo($model['email'])
                    ->setFrom([Yii::$app->params['adminEmail'] => Yii::$app->name])
                    ->setSubject('Welcome to ' . Yii::$app->name)
                    ->send();
                return [
                    'user' => false,
                    'process' => true,
                    'userId' => $userID,
                    'error' => false
                ];
            } else {
                return [
                    'user' => false,
                    'process' => false,
                    'userId' => false,
                    'error' => $user->errors
                ];
            }

        }
    }

    public function createAccount()
    {
        $model = $this->model;
        $check = User::find()->where(['email' => $model['email']])->andWhere(['!=', 'email', null])->one();
        if ($check) {
            return [
                'user' => true,
                'process' => false,
                'userId' => $check['id'],
                'error' => false
            ];
        } else {
            $username = $model['name'] . time();
            $user = new User();
            $user->email = $model['email'];
            $user->username = $username;
            $user->setPassword("12356");
            $user->generateAuthKey();
            $user->first_name = $model['name'];
            $user->last_name = "guest";
            $user->phone_number = $model['mobile'];
            $user->type = "individual";
            $user->about_you = "i am a guest user";
            if ($user->save(false)) {
                $userID = $user->id;
                Yii::$app->mailer->compose('welcomGuest', ['model' => $model, 'username' => $username])
                    ->setTo($model['email'])
                    ->setFrom([Yii::$app->params['adminEmail'] => Yii::$app->name])
                    ->setSubject('Welcome to ' . Yii::$app->name)
                    ->send();
                return [
                    'user' => false,
                    'process' => true,
                    'userId' => $userID,
                    'error' => false
                ];
            } else {
                return [
                    'user' => false,
                    'process' => false,
                    'userId' => false,
                    'error' => $user->errors
                ];
            }

        }
    }
}
