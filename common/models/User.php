<?php

namespace common\models;

use borales\extensions\phoneInput\PhoneInputValidator;
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\Url;
use yii\web\IdentityInterface;

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $auth_key
 * @property string $image
 * @property string $type
 * @property string $first_name
 * @property string $last_name
 * @property string $phone_number
 * @property string $about_you
 * @property string $lat
 * @property string $lng
 * @property string $city
 * @property string $state
 * @property string $country
 * @property integer $status
 * @property integer $flag_v
 * @property integer $fake
 * @property string $purchase_code
 * @property string $url_key
 * @property string $otp
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password write-only password
 *
 *  * @property UserProfile $userProfile
 */
class User extends ActiveRecord implements IdentityInterface
{
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 10;
    public $password_input;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [

            ['username', 'filter', 'filter' => 'trim'],
            ['username', 'required'],
            ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This username has already been taken.'],
            ['username', 'string', 'min' => 2, 'max' => 255],

            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This email address has already been taken.'],
            ['first_name', 'required'],
            ['first_name', 'string', 'max' => 21],

            ['last_name', 'required'],
            ['last_name', 'string', 'max' => 21],

            ['password_input', 'required'],
            ['password_input', 'string', 'max' => 21],

            [['image'], 'image', 'skipOnEmpty' => true, 'extensions' => 'png, jpg', 'maxFiles' => 5],
            ['image', 'required'],
            ['url_key', 'safe'],
            ['phone_number', 'required'],
            [['phone_number'], 'string'],
            [['phone_number'], PhoneInputValidator::class],

            [['about_you'], 'default', 'value' => 'Hi!!!, i am guest user of ' . \Yii::$app->name],
            ['about_you', 'string', 'max' => 500, 'min' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'email' => Yii::t('app', 'Email'),
            'category' => Yii::t('app', 'Category'),
            'sub_category' => Yii::t('app', 'Sub Category'),
            'price' => Yii::t('app', 'Price'),
            'type' => Yii::t('app', 'Type'),
            'ad_title' => Yii::t('app', 'Ad Title'),
            'ad_description' => Yii::t('app', 'Ad Description'),
            'image' => Yii::t('app', 'Image'),
            'first_name' => Yii::t('app', 'First Name'),
            'last_name' => Yii::t('app', 'Last Name'),
            'phone_number' => Yii::t('app', 'Mobile Number'),


        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int)substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    public static function getNameById($id)
    {
        $model = static::findOne($id);
        return $model->first_name;
    }

    public static function getImg($id)
    {
        $model = static::findOne($id);
        return $model->image;
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    public function validateServer($response = false)
    {
        $current = base64_encode($_SERVER['SERVER_NAME']);
        if ($current == "bG9jYWxob3N0" || $current == "MTI3LjAuMC4x") {
            return true;
        } else {
            if ($response) {
                return $this->validateUrl($current, true);
            } else {
                return $this->validateUrl($current);
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    /**
     * Create iamge thumb
     */
    public static function createThumb($path1, $path2, $file_type, $new_w, $new_h, $squareSize = '')
    {
        /* read the source image */
        $source_image = FALSE;

        if (preg_match("/jpg|JPG|jpeg|JPEG/", $file_type)) {
            $source_image = imagecreatefromjpeg($path1);
        } elseif (preg_match("/png|PNG/", $file_type)) {

            if (!$source_image = @imagecreatefrompng($path1)) {
                $source_image = imagecreatefromjpeg($path1);
            }
        } elseif (preg_match("/gif|GIF/", $file_type)) {
            $source_image = imagecreatefromgif($path1);
        }
        if ($source_image == FALSE) {
            $source_image = imagecreatefromjpeg($path1);
        }

        $orig_w = imageSX($source_image);
        $orig_h = imageSY($source_image);

        if ($orig_w < $new_w && $orig_h < $new_h) {
            $desired_width = $orig_w;
            $desired_height = $orig_h;
        } else {
            $scale = min($new_w / $orig_w, $new_h / $orig_h);
            $desired_width = ceil($scale * $orig_w);
            $desired_height = ceil($scale * $orig_h);
        }

        if ($squareSize != '') {
            $desired_width = $desired_height = $squareSize;
        }

        /* create a new, "virtual" image */
        $virtual_image = imagecreatetruecolor($desired_width, $desired_height);
        // for PNG background white----------->
        $kek = imagecolorallocate($virtual_image, 255, 255, 255);
        imagefill($virtual_image, 0, 0, $kek);

        if ($squareSize == '') {
            /* copy source image at a resized size */
            imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $desired_width, $desired_height, $orig_w, $orig_h);
        } else {
            $wm = $orig_w / $squareSize;
            $hm = $orig_h / $squareSize;
            $h_height = $squareSize / 2;
            $w_height = $squareSize / 2;

            if ($orig_w > $orig_h) {
                $adjusted_width = $orig_w / $hm;
                $half_width = $adjusted_width / 2;
                $int_width = $half_width - $w_height;
                imagecopyresampled($virtual_image, $source_image, -$int_width, 0, 0, 0, $adjusted_width, $squareSize, $orig_w, $orig_h);
            } elseif (($orig_w <= $orig_h)) {
                $adjusted_height = $orig_h / $wm;
                $half_height = $adjusted_height / 2;
                imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $squareSize, $adjusted_height, $orig_w, $orig_h);
            } else {
                imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $squareSize, $squareSize, $orig_w, $orig_h);
            }
        }

        if (@imagejpeg($virtual_image, $path2, 90)) {
            imagedestroy($virtual_image);
            imagedestroy($source_image);
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public static function LiveStatus()
    {
        $urlD = Url::canonical();
        $Detail = Verify::find()->one();
        $cSession = curl_init();
        $url = "http://localhost/check/php/tracked.php?unique_id=" . $Detail['unique_id'] . "&url=" . $urlD . "&purchaseCode=" . $Detail['purchase_code'] . "&otp=" . $Detail['otp'] . "&item=quik";

//step2
        curl_setopt($cSession, CURLOPT_URL, $url);
        curl_setopt($cSession, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($cSession, CURLOPT_HEADER, false);
//step3
        $result = curl_exec($cSession);

        curl_close($cSession);
        if (!$result) {
            $count = Verify::find()->count();
            if ($count > 0) {
                $stop = Verify::find()->one();
                $stop->status = '0';
                $stop->save(false);
            }

        } else {
            $stop = Verify::find()->one();
            $stop->status = '1';
            $stop->save(false);
        }
        return $result;// ($result == null)?false:true;

//step4
    }

    public function validateUrl($url, $response = false)
    {
        $default = DefaultSetting::find()->one();
        $current = base64_decode($url);
        $urlKey = $default['url_key'];
        if ($urlKey != NULL) {
            if (Yii::$app->security->validatePassword($current, $urlKey)) {
                return true;
            } else {
                $default->active = "0";
                $default->script = "0";
                $default->codecanyon_username = "0";
                $default->purchase_code = "0";
                $default->url_key = NULL;
                $default->save(false);
                if ($response) {
                    return false;
                } else {
                    return Yii::$app->controller->redirect(Url::toRoute('site/active'));
                }
            }
        } else {
            $default->active = "0";
            $default->script = "0";
            $default->codecanyon_username = "0";
            $default->purchase_code = "0";
            $default->url_key = NULL;
            $default->save(false);
            if ($response) {
                return false;
            } else {
                return Yii::$app->controller->redirect(Url::toRoute('site/active'));
            }
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserProfile()
    {
        return $this->hasOne(UserProfile::class, ['user_id' => 'id']);
    }
}
