<?php

namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * version model
 *
 * @property integer $id
 * @property integer $email
 * @property string $purchase_code
 * @property string $otp
 * @property string $version
 */
class Version extends ActiveRecord
{


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%version}}';
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email', 'required'],
            ['purchase_code', 'required'],
            ['otp', 'safe']

        ];
    }

    public static function version()
    {
        $model = static::find()->one();
        return $model->version;
    }

    public static function isDemo()
    {
        return Yii::$app->params['demo'];
    }

}
