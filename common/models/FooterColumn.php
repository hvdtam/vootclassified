<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "footer_column".
 *
 * @property int $id
 * @property string $name
 * @property string $column_class
 */
class FooterColumn extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'footer_column';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'column_class'], 'required'],
            [['name', 'column_class'], 'string', 'max' => 225],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'column_class' => Yii::t('app', 'Column Class'),
        ];
    }

    public static function getName($id)
    {
        $modal = self::findOne($id);
        return $modal->name;
    }
}
