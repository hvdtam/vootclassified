<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "category".
 *
 * @property integer $id
 * @property string $ads_id
 * @property string $user_id
 */
class SavedAds extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'saved_ads';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'ads_id'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */

    public static function add($property)
    {
        $uid = Yii::$app->user->identity->getId();
        $find = SavedAds::find()->where(['user_id' => $uid])->andWhere(['ads_id' => base64_decode($property)])->one();

        if ($find) {
            return "liked";
        } else {
            $model = new SavedAds();
            $model->ads_id = base64_decode($property);
            $model->user_id = $uid;
            if ($model->save(false)) {
                //$like = Ads::findOne(base64_decode($property));
                // $like->likes = $like->likes + 1;
                // $like->save(false);
                return true;
            } else {
                return false;
            }
        }


    }

    public static function remove($property)
    {
        $uid = Yii::$app->user->identity->getId();
        $model = SavedAds::find()->where(['user_id' => $uid])->andWhere(['ads_id' => base64_decode($property)])->one();
        if ($model->delete()) {
//            $like = Ads::findOne(base64_decode($property));
//            $like->likes = $like->likes - 1;
//            $like->save(false);
            return true;
        } else {
            return false;
        }

    }

}
