<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */
//$detailUrlc =  \yii\helpers\Url::toRoute('item/'.$list['ad_title'].'/'.$list['id']); // URl of detail page
$filtrUrlFind = array('/\,/', '/\s+/', '/\?+/', '/\'/');
$filtrUrlReplace = array('_', '_', '_');

$detailUrl = \yii\helpers\Url::to([
    'ads/detail',
    'title' => trim(preg_replace($filtrUrlFind, $filtrUrlReplace, $model['ad_title'])),
    'IID' => $model['id']
]);
//$detailUrl = preg_replace('/\s+/','-',$detailUrlc);
?>
<img src="https://www.viacom18.com/images/voot1.png">
<br>
<h2>Hello <?= Html::encode($model->name) ?>,</h2>
<p>
    Congratulation!!! Your Ad is Approved. You can see your ad here.
    <a href="<?= $detailUrl; ?>"></a>
</p>
<p>
    Thanks for submission <?= Yii::$app->name ?>.
</p>

