<?php
return [
    'adminEmail' => 'demo@gmail.com',
    'supportEmail' => 'demo@gmail.com',
    'user.passwordResetTokenExpire' => 3600,
    'demo' => false,
    'key' => 'dfgd56f74d894s6df51df84g8gh16548t65',
    'availableLocales' => [
        'vi' => 'Tiếng Việt',
        'en-US' => 'English (US)'
    ],
];
