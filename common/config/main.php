<?php
/**
 * Require core files
 */
require_once(__DIR__ . '/../helpers.php');

return [
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm' => '@vendor/npm-asset',
    ],
    'bootstrap' => new \yii\helpers\ReplaceArrayValue([]),
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'dcache' => [
            'class' => 'yii\caching\FileCache',
            'cachePath' => '@common/runtime/dcache'
        ],

        'scache' => [
            'class' => 'yii\caching\FileCache',
            'cachePath' => '@common/runtime/scache'
        ],
        'i18n' => [
            'translations' => [
                'app' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@common/messages',
                ],
                '*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@common/messages',
                    'fileMap' => [
                        'ads' => 'ads.php',
                        'common' => 'common.php',
                        'backend' => 'backend.php',
                        'frontend' => 'frontend.php',
                    ],
                    'on missingTranslation' => ['\backend\modules\i18n\Module', 'missingTranslation']
                ],
            ],
        ],
        'fileStorage' => [
            'class'      => '\trntv\filekit\Storage',
            //'class' => \common\components\filekit\FileStorage::class,
            'baseUrl' => '@storageUrl/web/source',
            'filesystem' => [
                'class' => 'common\components\filesystem\LocalFlysystemBuilder',
                'path' => '@storage/web/source'
            ],
            /*'as log' => [
                'class' => 'common\behaviors\FileStorageLogBehavior',
                'component' => 'fileStorage'
            ]*/
        ],

        'keyStorage' => [
            'class' => 'common\components\keyStorage\KeyStorage',
            'cachingDuration' => \common\helpers\TimeHelper::SECONDS_IN_A_DAY
        ],
    ],
    'as locale' => [
        'class' => 'common\behaviors\LocaleBehavior',
        'enablePreferredLanguage' => true
    ]
];
