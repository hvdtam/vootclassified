<?php
Yii::setAlias('@base', dirname(__DIR__, 2) . '/');
Yii::setAlias('@api', dirname(__DIR__, 2) . '/api');
Yii::setAlias('@common', dirname(__DIR__));
Yii::setAlias('@frontend', dirname(dirname(__DIR__)) . '/frontend');
Yii::setAlias('@backend', dirname(dirname(__DIR__)) . '/backend');
Yii::setAlias('@console', dirname(dirname(__DIR__)) . '/console');
Yii::setAlias('@storage', dirname(__DIR__, 2) . '/storage');

/**
 * Setting url aliases
 */
//Yii::setAlias('@apiUrl', env_value('API_HOST_INFO') . env_value('API_BASE_URL'));
//Yii::setAlias('@frontendUrl', env_value('FRONTEND_HOST_INFO') . env_value('FRONTEND_BASE_URL'));
//Yii::setAlias('@backendUrl', env_value('BACKEND_HOST_INFO') . env_value('BACKEND_BASE_URL'));
//Yii::setAlias('@storageUrl', env_value('STORAGE_HOST_INFO') . env_value('STORAGE_BASE_URL'));

/*
 * Define Cache Constrant
 * */
define('CACHE_SYSTEM_COMMON', 'aa');
define('CACHE_ARTICLE_DETAIL', 'ad');
define('CACHE_CATEGORY_DETAIL', 'ac');