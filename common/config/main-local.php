<?php
return [
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=vootv2',
            'username' => 'root',
            'password' => '',
            'charset' => 'utf8',
            //'enableSchemaCache' => true,
            // Duration of schema cache.
            'schemaCacheDuration' => 3600,

            // Name of the cache component used to store schema information
            'schemaCache' => 'cache',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
//            'transport'=>
//                [
//                    'class' => 'Swift_SmtpTransport',
//                    'host'=>'smtp.gmail.com',
//                    'username'=>'demo@gmail.com',
//                    'password'=>'evgwwodlfppwafkj',
//                    'port'=>'587',
//                    'encryption'=>'tls'
//                ]
        ],
    ],
];
