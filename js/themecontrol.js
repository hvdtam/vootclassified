/**
 * Created by Mayank Singh on 12/13/2018.
 */

$(document).ready(function($) {
//banner js function
    $("#BannerInputTitle").keyup(function () {
        var data = $("#BannerInputTitle").val();
        $('#bannerFormTitle').text(data);
    });
    $("#BannerInputTagline").keyup(function () {
        var data = $("#BannerInputTagline").val();
        $('#bannerFormTagLine').text(data);
    });
    $("#BannerInputHeight").keyup(function () {
        var data = $("#BannerInputHeight").val();
        $('#bannerFormImage').css("height", data);
    });
    $("#BannerInputPositions").change(function () {
        var data = $("#BannerInputPositions").val();
        $('#bannerFormImage').css("background-position", "center "+data);
    });


    $("#BannerInputImage").change(function () {
        var urlImg = $('#bannerFormImage').attr('img-url');
        var data = urlImg + $("#BannerInputImage").val();
        $('#bannerFormImage').css("background-image", "url(" + data + ")");
    });

    $("#BannerTagColor").change(function () {
        var data = $("#BannerTagColor").val();
        $('#bannerFormTagLine').css('color', data);
        $("#BannerTagColor").attr('banner-tag-color') == data;
    });
    $("#BannerTitleColor").change(function () {
        var data = $("#BannerTitleColor").val();
        $('#bannerFormTitle').css('color', data);
    });
    $("#background").change(function () {
        var data = $("#background").val();
        var path = $('#dBgBtn').attr('data-url')+data;
        $('body').css('background-image', 'url('+path+')');
    });
//banner js function end
});

function changeTheme(theme)
{
    //dthemeUrlDemo
    var url = $('#dthemeUrlDemo').val()+theme;
    $('#themeCss').attr('href',url).delay(500);
}