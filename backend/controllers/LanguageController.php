<?php

namespace backend\controllers;

use common\models\Analytic;
use common\models\Category;
use common\models\I18nMessage;
use common\models\I18nTranslation;
use common\models\Languages;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\Controller;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\data\ActiveDataProvider;

/**
 * Language controller
 */
class LanguageController extends Controller
{

    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        if (\Yii::$app->user->isGuest) {
            return $this->goHome();
        };
        $control = Yii::$app->params['demo'];

        if (isset($_POST['hasEditable']) and $control == true) {
            Yii::$app->session->setFlash('error', 'Its Demo You Cannot Modify anything');
            $out = Json::encode(['output' => '', 'message' => 'Its Demo Version You Cannot Change Any Value']);
            return $out;
        } else {
            return parent::beforeAction($action);
        }
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
        if (\Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        if (isset($_POST['hasEditable'])) {
            $langId = Yii::$app->request->post('editableKey');
            $language = Languages::findOne($langId);

            // store a default json response as desired by editable
            $out = Json::encode(['output' => '', 'message' => '']);
            $posted = current($_POST['Languages']);
            $post = ['Languages' => $posted];
            if ($language->load($post)) {
                // can save model or do something before saving model
                $language->save();
            }
            return $out;
        }

        $searchModel = new Languages();
        $query_s = Languages::find()->indexBy('id');
        $dataProvider = new ActiveDataProvider([
            'query' => $query_s,
        ]);
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);

    }

    public function actionTranslate2()
    {
        if (\Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        if (isset($_POST['hasEditable'])) {
            $langId = Yii::$app->request->post('editableKey');
            $language = I18nTranslation::findOne($langId);

            // store a default json response as desired by editable
            $out = Json::encode(['output' => '', 'message' => '']);
            $posted = current($_POST['I18nTranslation']);

            $post = ['I18nTranslation' => $posted];
            if ($language->load($post)) {
                // can save model or do something before saving model
                $language->save();
            }
            return $out;
        }

        $searchModel = new I18nTranslation();
        $query_s = I18nTranslation::find()->innerJoin('i18n_message', 'i18n_translation.message_id = i18n_message.message_id')->indexBy('message_id');
        $dataProvider = new ActiveDataProvider([
            'query' => $query_s,
        ]);
        return $this->render('translate2', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);

    }

    public function actionTranslate()
    {
        if (\Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $model = I18nTranslation::find()->groupBy('language')->count('*');

        $leadsCount = I18nTranslation::find()
            ->select(['language', 'COUNT(*) AS cnt'])
            ->groupBy(['language'])
            ->all();
        return $this->render('translate', [
            'model' => $leadsCount,
        ]);

    }

    public function actionWordSettings($iso)
    {
        if (\Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $model = I18nTranslation::find()->where(['language' => $iso])->all();

        if (isset($_POST['hasEditable'])) {
            $langId = Yii::$app->request->post('editableKey');
            $id = json_decode($langId, true);
            $language = I18nTranslation::find()->where(['message_id' => $id['message_id']])->andWhere(['language' => $id['language']])->one();

            // store a default json response as desired by editable
            $out = Json::encode(['output' => '', 'message' => '']);

            $posted = current($_POST['I18nTranslation']);
            $post = ['I18nTranslation' => $posted];
            if ($language->load($post)) {
                // can save model or do something before saving model
                $language->save();
            }
            return $out;
        }
        if (isset($_GET['I18nTranslation'])) {
            $msgId = $_GET['I18nTranslation']['message_id'];

            $searchModel = new I18nTranslation();
            $query_s = I18nTranslation::find()->where(['message_id' => $msgId])->andWhere(['language' => $iso])->indexBy('message_id');
            $dataProvider = new ActiveDataProvider([
                'query' => $query_s,
            ]);
            return $this->render('translate2', [
                'dataProvider' => $dataProvider,
                'searchModel' => $searchModel,
                'iso' => $iso
            ]);
        }
        $searchModel = new I18nTranslation();
        $query_s = I18nTranslation::find()->where(['language' => $iso])->innerJoin('i18n_message', 'i18n_translation.message_id = i18n_message.message_id')->indexBy('message_id');
        $dataProvider = new ActiveDataProvider([
            'query' => $query_s,
        ]);
        return $this->render('translate2', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'iso' => $iso
        ]);

    }

    public function actionTranslator()
    {
        if (\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        return $this->render('translator');

    }

    // delete all translated message
    public function actionDelete($iso)
    {
        if (\Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $model = I18nTranslation::deleteAll(['language' => $iso]);
        $url = Url::to(['language/translate']);
        return $this->redirect($url);

    }


}
