<?php

namespace backend\controllers;

use backend\models\FakeAdsForm;
use backend\models\FakeUsersForm;
use common\models\Ads;
use common\models\Category;
use common\models\CustomFields;
use common\models\SubCategory;
use common\models\Transactions;
use common\models\Type;
use common\models\User;
use Faker\Factory;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\helpers\Json;
use yii\data\ActiveDataProvider;
use yii\web\UploadedFile;

/**
 * Faker Controller
 */
class FakerController extends Controller
{

    public function beforeAction($action)
    {
        $control = Yii::$app->params['demo'];
        if (\Yii::$app->user->isGuest) {
            return $this->goHome();
        };
        if ($control == true and (Yii::$app->request->post() or Yii::$app->request->get())) {
            throw new BadRequestHttpException('Its Demo You Cannot Modify anything');
        } else {
            return parent::beforeAction($action);
        }

    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {

        return $this->render('index');

    }

    public function actionUsers()
    {
        if (\Yii::$app->user->isGuest) {
            return $this->redirect(Url::toRoute('site/login'));
        }
        $searchModel = new User();

        if (isset($_POST['hasEditable'])) {
            $userId = Yii::$app->request->post('editableKey');
            $UserModel = User::findOne($userId);

            // store a default json response as desired by editable
            $out = Json::encode(['output' => '', 'message' => '']);
            $posted = current($_POST['User']);
            $post = ['User' => $posted];
            if ($UserModel->load($post)) {
                // can save model or do something before saving model
                $UserModel->save(false);
            }
            return $out;
        }

        $query_s = User::find()->where(['fake' => '1'])->indexBy('id');
        $dataProvider = new ActiveDataProvider([
            'query' => $query_s,
        ]);


        return $this->render('users', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);

    }

    public function actionUsersCreate()
    {
        $modal = new FakeUsersForm();
        if ($modal->load(Yii::$app->request->post())) {
            $totalAds = $modal['total'];
            $faker = Factory::create("en_US");
            $imgArray = explode(',', $modal->image_directory);

            for ($i = 0; $i < $totalAds; $i++) {
                $fake = new User();
                $fake->first_name = $faker->firstName($modal->gender);
                $fake->last_name = $faker->lastName;
                $fake->username = $faker->userName;
                $fake->email = $faker->email;
                $fake->type = "individual";
                $fake->phone_number = $faker->phoneNumber;
                if ($modal->with_image == true) {
                    $imgRand = $faker->randomElement($imgArray);
                    $fake->image = Self::getImage($imgRand, 'users');
                } else {
                    $fake->image = "default.jpg";
                }
                $fake->phone_number = $faker->phoneNumber;
                $fake->about_you = $faker->realText(['maxNbChars' => 20]);
                $fake->fake = '1';
                $fake->country = $modal['country'];
                $fake->state = $modal['state'];
                $fake->city = $modal['city'];

                $fake->created_at = time();
                $fake->save(false);


            }
            Yii::$app->getSession()->setFlash('success', 'successfully Create ' . $totalAds . ' fakes record');
        }
        return $this->render('users-create', ['modal' => $modal]);

    }

    public function actionAds()
    {

        $modal = new FakeAdsForm();
        $fakesAds = Ads::find()->where(['fake' => '1'])->all();
        if ($modal->load(Yii::$app->request->post())) {
            $totalAds = $modal['total'];
            $faker = Factory::create("en_US");
            $defaultSettings = \common\models\DefaultSetting::getDefaultSetting();

            for ($i = 0; $i < $totalAds; $i++) {

                $fake = new Ads();
                Category::addCounter($modal['category']);
                SubCategory::addCounter($modal['sub_category']);
                $subCatId = SubCategory::findId($modal['sub_category']);

                $fake->user_id = $faker->randomElement(ArrayHelper::map(User::find()->where(['fake' => '1'])->all(), 'id', 'id'));
                $fake->ad_title = $faker->realText(50, 2);
                $fake->ad_description = $faker->realText(1500);
                $fake->category = $modal['category'];
                $fake->sub_category = $modal['sub_category'];

//              =============================  Setting of "TYPE" parameter =====================================
                $randomType = ArrayHelper::map(Type::find()->where(['parent' => $subCatId])->all(), 'name', 'name');
                $type = $faker->randomElement($randomType);
                $fake->type = $type;
                Type::addCounter($type, $modal['sub_category']);
//              ============================  Setting of "TYPE" parameter =====================================

                $fake->ad_type = $faker->randomElement(['public', 'Private']);;
                $fake->currency_symbol = $defaultSettings['currency'];
                $fake->currency_ini = $defaultSettings['currency'];
                $fake->price = $faker->numberBetween($min = 11000, $max = 9000);
                $fake->image = $faker->file($sourceDir = $modal->image_directory, $targetDir = Yii::getAlias('@frontend/web/images/ads/'), false);
                $fake->name = $faker->firstName;
                $fake->mobile = $faker->phoneNumber;
                $fake->email = $faker->email;
                $fake->active = 'yes';
                $fake->premium = $faker->randomElement(['', 'featured', 'urgent']);
                $fake->lat = $faker->latitude;
                $fake->lng = $faker->longitude;
                $fake->country = $defaultSettings['country'];
                $fake->states = $defaultSettings['state'];
                $fake->city = $defaultSettings['city'];
                $fake->address = $faker->address;
                $fake->view = $modal['views'];
                $fake->more = CustomFields::fakerData($modal['sub_category']);
                $fake->created_at = time();
                $fake->updated_at = time();
                $fake->fake = '1';
                $fake->save(false);
            }
            Yii::$app->getSession()->setFlash('success', 'successfully Create ' . $totalAds . ' fakes record');
            return $this->redirect(Url::toRoute('faker/ads'));
        } else {

            $isFake = User::find()->where(['fake' => '1'])->all();;
            if (!$isFake) {
                Yii::$app->getSession()->setFlash('warning', '<b>Warning : </b> You Should create fake users first before create fake ads. Thank you');

                return $this->redirect(Url::toRoute('faker/users'));
            }

            if (isset($_POST['hasEditable'])) {
                $adsId = Yii::$app->request->post('editableKey');
                $ads = Ads::findOne($adsId);

                // store a default json response as desired by editable
                $out = Json::encode(['output' => '', 'message' => '']);
                $posted = current($_POST['Ads']);
                $post = ['Ads' => $posted];
                if ($ads->load($post)) {
                    // can save model or do something before saving model
                    $ads->save(false);
                }
                return $out;;
            }

            $searchModel = new Ads();
            $query_s = Ads::find()->where(['fake' => '1'])->indexBy('id');
            $dataProvider = new ActiveDataProvider([
                'query' => $query_s,
            ]);

            return $this->render('ads', [
                'dataProvider' => $dataProvider,
                'searchModel' => $searchModel,
                'modal' => $modal
            ]);
        }


    }

    public function actionAdsCreate()
    {

        $modal = new FakeAdsForm();
        if ($modal->load(Yii::$app->request->post())) {
            $totalAds = $modal['total'];
            $faker = Factory::create("en_US");
            $defaultSettings = \common\models\DefaultSetting::getDefaultSetting();
            $imgArray = explode(',', $modal->image_directory);

            for ($i = 0; $i < $totalAds; $i++) {

                $fake = new Ads();
                Category::addCounter($modal['category']);
                SubCategory::addCounter($modal['sub_category']);
                $subCatId = SubCategory::findId($modal['sub_category']);

                $fake->user_id = $faker->randomElement(ArrayHelper::map(User::find()->where(['fake' => '1'])->all(), 'id', 'id'));
                $fake->ad_title = $faker->realText(50, 2);
                $fake->ad_description = $faker->realText(1500);
                $fake->category = $modal['category'];
                $fake->sub_category = $modal['sub_category'];

//              =============================  Setting of "TYPE" parameter =====================================
                $randomType = ArrayHelper::map(Type::find()->where(['parent' => $subCatId])->all(), 'name', 'name');
                $type = $faker->randomElement($randomType);
                $fake->type = $type;
                Type::addCounter($type, $modal['sub_category']);
//              ============================  Setting of "TYPE" parameter =====================================

                $fake->ad_type = $faker->randomElement(['public', 'Private']);;
                $fake->currency_symbol = $defaultSettings['currency'];
                $fake->currency_ini = $defaultSettings['currency'];
                $fake->price = $faker->numberBetween($min = 11000, $max = 9000);
                if ($modal->with_image == true) {
                    $imgRand = $faker->randomElement($imgArray);
                    $fake->image = Self::getImage($imgRand, 'ads');
                } else {
                    $fake->image = "default.jpg";
                }
                $fake->name = $faker->firstName;
                $fake->mobile = $faker->phoneNumber;
                $fake->email = $faker->email;
                $fake->active = 'yes';
                $fake->premium = $faker->randomElement(['', 'featured', 'urgent']);
                $fake->lat = $faker->latitude;
                $fake->lng = $faker->longitude;
                $fake->country = $defaultSettings['country'];
                $fake->states = $defaultSettings['state'];
                $fake->city = $defaultSettings['city'];
                $fake->address = $faker->address;
                $fake->view = $modal['views'];
                $fake->more = CustomFields::fakerData($modal['sub_category']);
                $fake->created_at = time();
                $fake->updated_at = time();
                $fake->fake = '1';
                $fake->save(false);
            }
            Yii::$app->getSession()->setFlash('success', 'successfully Create ' . $totalAds . ' fakes record');
        } else {

            $isFake = User::find()->where(['fake' => '1'])->all();;
            if (!$isFake) {
                Yii::$app->getSession()->setFlash('warning', '<b>Warning : </b> You Should create fake users first before create fake ads. Thank you');

                return $this->redirect(Url::toRoute('faker/users'));
            }

        }

        return $this->render('ads-create', ['modal' => $modal]);

    }


    public function actionGetSub()
    {
        $cat = $_GET['value'];
        $parent = Category::findId($cat);
        $modal = SubCategory::find()->where(['parent' => $parent])->all();
        foreach ($modal as $list) {
            $val = $list['name'];
            echo "<option value='$val'>$val</option>";
        }
    }

    public function getImage($img, $type)
    {
        $NewImgName = 'FakeImg_' . time() . '.jpg';
        $data = file_get_contents($img);//Get content of file
        //New file
        $new = Yii::getAlias('@frontend/web/images/' . $type . '/' . $NewImgName);
        // Write the contents back to a new file
        file_put_contents($new, $data);
        return $NewImgName;
    }

}
//$json1 = '{"Features":"Air conditionar,Music System,GPS,Security System,Parking Sensor,parking camera,Stepney,Jack,Auto gear,Fog lamp,ABS,Airbags"}';
//$json = '{"Brand":["Audi A6"],"Model":["A6"],"Year of registration":["2017"],"Kms Driven":["14000"],"Features":["Air conditionar","Music System","GPS","Security System","Parking Sensor","parking camera","Jack","Auto gear","ABS","Airbags"],"Fuel Type":[""],"Product Condition":["Used"]}';