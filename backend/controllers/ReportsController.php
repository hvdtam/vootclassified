<?php

namespace backend\controllers;

use backend\models\CategoryForm;
use backend\models\ReportForm;
use common\models\AdminNotification;
use common\models\AdReport;
use common\models\Ads;
use common\models\Analytic;
use common\models\Category;
use common\models\MainMenu;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use common\models\LoginForm;
use yii\filters\VerbFilter;

/**
 * Analytic controller
 */
class ReportsController extends Controller
{

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
        if (\Yii::$app->user->isGuest) {
            return $this->goHome();
        };
        if (isset($_GET['sort'])) {
            $sort = $_GET['sort'];
            $modal = AdReport::find()->where(['status' => $sort])->all();
        } else {
            $sort = "unread";
            $modal = AdReport::find()->where(['seen' => 'unseen'])->all();

        }
        AdReport::updateAll(['seen' => 'seen']);
        return $this->render('index', ['modal' => $modal, 'active' => $sort]);

    }

    public function actionDetail($id)
    {
        if (\Yii::$app->user->isGuest) {
            return $this->goHome();
        };
        $modal = AdReport::findOne($id);
        $advertiserEmail = Ads::findOne($modal->ad_id);
        $report = new ReportForm();
        if ($report->load(Yii::$app->request->post())) {
            // echo $report->action;die;
            $status = $report->action;
            if ($status == "accept") {
                Ads::changeStatus($modal->ad_id, $status);
            }
            AdReport::changeStatus($modal->id, $status);

            Yii::$app->mailer->compose()
                ->setTo($report->respond_to)
                ->setFrom([Yii::$app->params['adminEmail'] => Yii::$app->name])
                ->setSubject('Report : ' . $modal->reason)
                ->setTextBody($report->message)
                ->send();
            Yii::$app->getSession()->setFlash('success', 'Update successfully');
        }
        return $this->render('detail', ['list' => $modal, 'report' => $report, 'advertiserEmail' => $advertiserEmail->email]);

    }


}
