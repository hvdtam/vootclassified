<?php

namespace backend\controllers;

use common\models\AdminLoginForm;
use common\models\Ads;
//use common\models\Track;
use common\models\SavedAds;
use common\models\User;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\Controller;
use common\models\LoginForm;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

/**
 * Site controller
 */
class UserController extends Controller
{
    //  public $layout = 'plain';
    /**
     * @inheritdoc
     */

    public function beforeAction($action)
    {
        if (\Yii::$app->user->isGuest) {
            return $this->goHome();
        };
        $control = Yii::$app->params['demo'];
        if (Yii::$app->request->isAjax and $control == true) {
            Yii::$app->session->setFlash('error', 'Its Demo You Cannot Modify anything');
            $out = Json::encode(['output' => '', 'message' => 'Its Demo Version You Cannot Change Any Value']);
            return $out;
        } else {
            return parent::beforeAction($action);
        }
    }

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error', 'user', 'detail'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
        if (\Yii::$app->user->isGuest) {
            return $this->redirect(Url::toRoute('site/login'));
        }
        $this->layout = "main";
        $user = \common\models\User::find()->orderBy(['created_at' => SORT_DESC])->limit('5')->all();
        $ads = Ads::find()->orderBy(['created_at' => SORT_DESC])->limit('5')->all();;

        //$statistics = Track::find()->limit('5')->all();
        $adsTotal = Ads::find()->count();
        $pending = Ads::find()->where(['active' => 'yes'])->count();
        $active = ($adsTotal - $pending);
        return $this->render('index', [
            'total' => $adsTotal,
            'pending' => $pending,
            'active' => $active,
            //  'statistics'=>$statistics,
            'member' => $user,
            'ads' => $ads
        ]);
    }

    public function actionUser($id = false)
    {
        //$this->layout = "main";
        if (\Yii::$app->user->isGuest) {
            return $this->redirect(Url::toRoute('site/login'));
        };

//        if($id)
//        {
//
//            $ads = \common\models\Ads::find()->where(['user_id'=>$id])->all();
//            $adsPremium = \common\models\Ads::find()->where(['user_id'=>$id])->andWhere('premium != ""')->count();
//
//            $user = \common\models\User::find()->where(['id'=>$id])->one();
//            if ($user->load(Yii::$app->request->post())) {
//                $user->save(false);
//                Yii::$app->session->setFlash('success', 'save settings');
//            }
//            return $this->render('profile',[
//                'member'=>$user,
//                'premium'=>$adsPremium,
//                'ads'=>$ads
//            ]);
//        }
//        else
//        {
//            $user = \common\models\User::find()->orderBy(['created_at'=>SORT_DESC])->limit('5')->all();
//            return $this->render('user',[
//                'member'=>$user
//            ]);
//        }


        // $user = new User();
        //$main = ArrayHelper::map(Category::find()->select(['id','name'])->all(),'id','name');

        if (isset($_POST['hasEditable'])) {
            $userId = Yii::$app->request->post('editableKey');
            $UserModel = User::findOne($userId);

            // store a default json response as desired by editable
            $out = Json::encode(['output' => '', 'message' => '']);
            $posted = current($_POST['User']);
            $post = ['User' => $posted];
            if ($UserModel->load($post)) {
                // can save model or do something before saving model
                $UserModel->save();
            }
            return $out;
        }

        $searchModel = new User();
        $query_s = User::find()->indexBy('id');
        $dataProvider = new ActiveDataProvider([
            'query' => $query_s,
        ]);


        return $this->render('user', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            //'data'=>$main,
        ]);

    }

    public function actionUserDetail()
    {
        if (isset($_POST['expandRowKey'])) {
            $model = User::findOne($_POST['expandRowKey']);
            //$variable = ['title'=>$model->name." Detail"]; //anydata want to send in expanded view
            return $this->renderPartial('_profile.php', ['model' => $model]);
        } else {
            return '<div class="alert alert-danger">No data found</div>';

        }
    }

    public function actionDetail($id)
    {
        $this->layout = "main";
        $model = User::findOne($id);
        $ads = \common\models\Ads::find()->where(['user_id' => $model['id']])->all();
        $adsPremium = \common\models\Ads::find()->where(['user_id' => $model['id']])->andWhere('premium != ""')->count();
        $Like = SavedAds::find()->where(['user_id' => $model['id']])->count();

        return $this->render('index.php', ['like' => $Like, 'model' => $model, 'member' => $model, 'premium' => $adsPremium, 'ads' => $ads]);

    }


}
