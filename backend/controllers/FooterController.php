<?php

namespace backend\controllers;

use common\models\FooterColumn;
use common\models\FooterContent;
use common\models\Pages;
use yii\web\BadRequestHttpException;
use yii\web\NotAcceptableHttpException;
use Yii;
use yii\helpers\Json;
use yii\web\Controller;
use yii\data\ActiveDataProvider;

/**
 * footer controller
 */
class FooterController extends Controller
{

    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        if (\Yii::$app->user->isGuest) {
            return $this->goHome();
        };
        $control = Yii::$app->params['demo'];
        if (isset($_POST['hasEditable']) and $control == true) {
            Yii::$app->session->setFlash('error', 'Its Demo You Cannot Modify anything');
            $out = Json::encode(['output' => '', 'message' => 'Its Demo Version You Cannot Change Any Value']);
            return $out;
        };
        if ($control == true and (Yii::$app->request->post() or Yii::$app->request->get())) {
            throw new BadRequestHttpException('Its Demo You Cannot Modify anything');
        } else {
            return parent::beforeAction($action);
        }
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
        if (\Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $column = FooterColumn::find()->all();
        if (isset($_POST['hasEditable'])) {
            $CatId = Yii::$app->request->post('editableKey');
            $Category = FooterContent::findOne($CatId);

            // store a default json response as desired by editable
            $out = Json::encode(['output' => '', 'message' => '']);
            $posted = current($_POST['FooterContent']);
            $post = ['FooterContent' => $posted];
            if ($Category->load($post)) {
                // can save model or do something before saving model
                $Category->save(false);
            }
            return $out;
        }

        $searchModel = new FooterContent();
        $query_s = FooterContent::find()->indexBy('id');
        $dataProvider = new ActiveDataProvider([
            'query' => $query_s,
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'column' => $column

        ]);

    }

    public function actionFooterColumn()
    {
        if (\Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $modal = new FooterColumn();
        // $saved = FooterColumn::findOne(1);

        if ($modal->load(Yii::$app->request->post())) {
            Yii::$app->getSession()->setFlash('success', 'Add successfully');

            $modal->save(false);

        }
        if (isset($_POST['hasEditable'])) {
            $CatId = Yii::$app->request->post('editableKey');
            $Category = FooterColumn::findOne($CatId);

            // store a default json response as desired by editable
            $out = Json::encode(['output' => '', 'message' => '']);
            $posted = current($_POST['FooterColumn']);
            $post = ['FooterColumn' => $posted];
            if ($Category->load($post)) {
                // can save model or do something before saving model
                $Category->save();
            }
            return $out;
        }

        $searchModel = new FooterColumn();
        $query_s = FooterColumn::find()->indexBy('id');
        $dataProvider = new ActiveDataProvider([
            'query' => $query_s,
        ]);

        return $this->render('footer-column', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'modal' => $modal

        ]);

    }

    public function actionFooterContent()
    {
        if (\Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $modal = new FooterContent();

        if ($modal->load(Yii::$app->request->post())) {
            Yii::$app->getSession()->setFlash('success', 'Add successfully');

            $modal->save(false);

        }

        return $this->render('footer-content', [
            'modal' => $modal
        ]);
    }

    public function actionGetType()
    {
        $type = $_GET['type'];
        return $this->render('get-type', [
            'type' => $type
        ]);

    }
}
