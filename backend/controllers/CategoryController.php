<?php

namespace backend\controllers;

use common\models\Category;
use common\models\SubCategory;
use common\models\Type;
use Yii;
use yii\data\Pagination;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\data\ActiveDataProvider;
use yii\web\UploadedFile;

/**
 * Category controller
 */
class CategoryController extends Controller
{

    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        if (\Yii::$app->user->isGuest) {
            return $this->goHome();
        };
        $control = Yii::$app->params['demo'];

        if (isset($_POST['hasEditable']) and $control == true) {
            Yii::$app->session->setFlash('error', 'Its Demo You Cannot Modify anything');
            $out = Json::encode(['output' => '', 'message' => 'Its Demo Version You Cannot Change Any Value']);
            echo $out;
            return false;
        } else {
            return parent::beforeAction($action);
        }
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
        if (\Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $cat = new Category();
        // $main = ArrayHelper::map(Category::find()->select(['id','name'])->all(),'id','name');

        if (isset($_POST['hasEditable'])) {
            $CatId = Yii::$app->request->post('editableKey');
            $Category = Category::findOne($CatId);

            // store a default json response as desired by editable
            $out = Json::encode(['output' => '', 'message' => '']);
            $posted = current($_POST['Category']);
            $post = ['Category' => $posted];
            //  return var_dump($posted);

            if ($Category->load($post)) {
                // can save model or do something before saving model
                $Category->save(false);
            }
            // echo $out;
            return $out;
        }

        $searchModel = new Category();
        $query_s = Category::find()->indexBy('id');
        $dataProvider = new ActiveDataProvider([
            'query' => $query_s,
        ]);

        //dd($dataProvider->getModels());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);

    }

    public function actionChangeIcon($id)
    {

        $model = Category::findOne($id);
        if ($model->load(Yii::$app->request->post())) {
            $model->fa_icon = UploadedFile::getInstance($model, 'fa_icon');
            $icon = $model->uploadIcon();
            $model->fa_icon = $icon;

            $model->save(false);

            Yii::$app->getSession()->setFlash('success', 'Update successfully');
        }
        return $this->render('change-icon', [
            'model' => $model,
        ]);
    }

    public function actionSubcategory()
    {
        if (\Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $cat = new SubCategory();
        $main = ArrayHelper::map(Category::find()->select(['id', 'name'])->all(), 'id', 'name');

        if (isset($_POST['hasEditable'])) {
            $SCatId = Yii::$app->request->post('editableKey');
            $SubCategory = SubCategory::findOne($SCatId);

            // store a default json response as desired by editable
            $out = Json::encode(['output' => '', 'message' => '']);
            $posted = current($_POST['SubCategory']);
            $post = ['SubCategory' => $posted];
            if ($SubCategory->load($post)) {
                // can save model or do something before saving model
                $SubCategory->save();
            }
            echo $out;
            return;
        }

        $searchModel = new SubCategory();
        $query_s = SubCategory::find()->indexBy('id');
        $dataProvider = new ActiveDataProvider([
            'query' => $query_s,
        ]);

        // validate if there is a editable input saved via AJAX
        $datas = ['hello' => 'hello', 'wow' => 'wow'];
        // non-ajax - render the grid by default
        return $this->render('subcategory', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'data' => $main,
        ]);
    }

    public function actionExpandView()
    {
        if (isset($_POST['expandRowKey'])) {
            $model = SubCategory::findOne($_POST['expandRowKey']);
            //$variable = ['title'=>$model->name." Detail"]; //anydata want to send in expanded view
            return $this->renderPartial('_detail-subcategory.php', ['model' => $model]);
        } else {
            return '<div class="alert alert-danger">No data found</div>';

        }
    }

    public function actionDetail()
    {
        $model = SubCategory::findOne(2);
        return $this->render('detail', ['model' => $model]);

    }

    public function actionType()
    {
        if (\Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $cat = new Type();
        $main = ArrayHelper::map(SubCategory::find()->select(['id', 'name'])->all(), 'id', 'name');


        // your default model and dataProvider generated by gii

        if (isset($_POST['hasEditable'])) {
            $SCatId = Yii::$app->request->post('editableKey');
            $SubCategory = Type::findOne($SCatId);

            // store a default json response as desired by editable
            $out = Json::encode(['output' => '', 'message' => '']);
            $posted = current($_POST['Type']);
            $post = ['Type' => $posted];
            if ($SubCategory->load($post)) {
                // can save model or do something before saving model
                $SubCategory->save();
            }
            echo $out;
            return;
        }

        $searchModel = new Type();
        $query_s = Type::find()->indexBy('id');
        $dataProvider = new ActiveDataProvider([
            'query' => $query_s,
        ]);

        // validate if there is a editable input saved via AJAX
        $datas = ['hello' => 'hello', 'wow' => 'wow'];
        // non-ajax - render the grid by default
        return $this->render('type', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'data' => $main,
        ]);
    }


}
