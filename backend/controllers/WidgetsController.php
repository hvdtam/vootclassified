<?php

namespace backend\controllers;

use common\models\Widgets;
use common\models\WidgetTemplat;
use yii\helpers\ArrayHelper;
use Yii;
use yii\helpers\Json;
use yii\web\NotAcceptableHttpException;

class WidgetsController extends \yii\web\Controller
{
    public function actionIndex()
    {
        if (\Yii::$app->user->isGuest) {
            return $this->goHome();
        };
        $modal = Widgets::find()->all();
        return $this->render('index', ['modal' => $modal]);
    }

    public function actionEdit($id)
    {
        if (\Yii::$app->user->isGuest) {
            return $this->goHome();
        };
        $modal = Widgets::findOne($id);
        $template = WidgetTemplat::find()->where(['widget_id' => $id])->all();
        $data = ArrayHelper::map($template, 'template', 'name');

        return $this->render('edit', ['widget' => $modal, 'data' => $data, 'template' => $template]);
    }
}
