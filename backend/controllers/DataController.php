<?php

namespace backend\controllers;

use backend\models\FakeAdsForm;
use backend\models\FakeUsersForm;
use common\models\Ads;
use common\models\Category;
use common\models\CustomFields;
use common\models\FooterColumn;
use common\models\FooterContent;
use common\models\Pages;
use common\models\SubCategory;
use common\models\Transactions;
use common\models\Type;
use common\models\User;
use Faker\Factory;
use Google\Cloud\Translate\V2\TranslateClient;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Inflector;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\helpers\Json;
use yii\data\ActiveDataProvider;
use yii\web\UploadedFile;

/**
 * Faker Controller
 */
class DataController extends Controller
{

    public function beforeAction($action)
    {
        $control = Yii::$app->params['demo'];
        if (\Yii::$app->user->isGuest) {
            return $this->goHome();
        };
        if ($control == true and (Yii::$app->request->post() or Yii::$app->request->get())) {
            throw new BadRequestHttpException('Its Demo You Cannot Modify anything');
        } else {
            return parent::beforeAction($action);
        }

    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionSubCategory()
    {
        $translate = new TranslateClient([
            'key' => 'AIzaSyCw9qKRtwPaGM6S8zW4GbZM7W4TDMq7n-4'
        ]);

        $allModel = SubCategory::find()->all();
        foreach ($allModel as $model) {
            echo "<pre>";
            echo $model->name;
            echo "</pre>";
            $result = $translate->translate($model->name, [
                'target' => 'vi'
            ]);

            echo $result['text'] . "\n";
            $model->name = $result['text'];
            $model->save();
        }

        dd('done');
    }

    public function actionLocate()
    {
        $translate = new TranslateClient([
            'key' => 'AIzaSyCw9qKRtwPaGM6S8zW4GbZM7W4TDMq7n-4'
        ]);

        $allModel = Pages::find()->all();
        foreach ($allModel as $model) {
            echo "<pre>";
            echo $model->title;
            echo "</pre>";
            $result = $translate->translate($model->title, [
                'target' => 'vi'
            ]);
            $msg = Inflector::slug($result['text']);
            $msg = str_replace('-', '_', $msg);

            echo $result['text'] . "\n";
            $model->title_en = $model->title;
            $model->title_slug = $msg;
            $model->title = $result['text'];
            $model->save();
        }

        dd('done');
    }

    public function actionCat()
    {
        $allModel = SubCategory::find()->all();
        foreach ($allModel as $model) {
            echo "<pre>";
            echo $model->name;
            echo "</pre>";
            $msg = Inflector::slug($model->name);
            $msg = str_replace('-', '_', $msg);
            $model->name = $msg;
            $model->save();
        }

        dd('done');
    }

}