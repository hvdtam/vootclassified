<?php

namespace backend\controllers;

class ComponentController extends \yii\web\Controller
{
    public function actionIndex()
    {
        if (\Yii::$app->user->isGuest) {
            return $this->goHome();
        };
        return $this->render('index');
    }

}
