<?php

namespace backend\controllers;

use common\models\Category;
use common\models\CustomFields;
use common\models\SubCategory;
use Yii;
use yii\helpers\Json;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CustomController implements the CRUD actions for QnewCustomFields model.
 */
class CustomController extends Controller
{
    /**
     * @inheritdoc
     */

    public function beforeAction($action)
    {
        if (\Yii::$app->user->isGuest) {
            return $this->goHome();
        };
        $control = Yii::$app->params['demo'];
        if (isset($_POST['hasEditable']) and $control == true) {
            Yii::$app->session->setFlash('error', 'Its Demo You Cannot Modify anything');
            $out = Json::encode(['output' => '', 'message' => 'Its Demo Version You Cannot Change Any Value']);
            return $out;
        } else {
            return parent::beforeAction($action);
        }
    }

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all QnewCustomFields models.
     * @return mixed
     */
    public function actionIndex()
    {
        if (\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $dataCat = \yii\helpers\ArrayHelper::map(\common\models\Category::find()->orderBy('name')->asArray()->all(), 'id', 'name');
        $dataSubCat = \yii\helpers\ArrayHelper::map(\common\models\SubCategory::find()->orderBy('name')->asArray()->all(), 'id', 'name');

        if (isset($_POST['hasEditable'])) {
            $CustomId = Yii::$app->request->post('editableKey');
            $Custom = CustomFields::findOne($CustomId);

            // store a default json response as desired by editable
            // $out = Json::encode(['output'=>'', 'message'=>'']);
            $posted = current($_POST['CustomFields']);
            $post = ['CustomFields' => $posted];
            $column = array_keys($posted);
            $value = array_values($posted);
            if ($column['0'] == "custom_catid") {
                $mod = Category::findNameById($value['0']);
                $out = Json::encode(['output' => $mod, 'message' => '']);
            } elseif ($column['0'] == "custom_subcatid") {
                $mod = SubCategory::findName($value['0']);
                $out = Json::encode(['output' => $mod, 'message' => '']);
            } else {
                $out = Json::encode(['output' => '', 'message' => '']);
            };

            if ($Custom->load($post)) {

                $Custom->save(false);
            };
            return $out;
        }

        $searchModel = new CustomFields();
        $query_s = CustomFields::find()->indexBy('custom_id');
        $dataProvider = new ActiveDataProvider([
            'query' => $query_s,
        ]);
        if (isset($_GET['CustomFields'])) {
            $searchModel = new CustomFields();
            $query_s = CustomFields::find()
                ->where(['custom_catid' => $_GET['CustomFields']['custom_catid']])
                ->orwhere(['custom_subcatid' => $_GET['CustomFields']['custom_subcatid']])
                ->orwhere(['custom_type' => $_GET['CustomFields']['custom_type']])
                ->orwhere(['custom_title' => $_GET['CustomFields']['custom_title']])
                ->indexBy('custom_id');
            $dataProvider = new ActiveDataProvider([
                'query' => $query_s,
            ]);
            return $this->render('index', [
                'dataProvider' => $dataProvider,
                'searchModel' => $searchModel,
                'dataCat' => $dataCat,
                'dataSubCat' => $dataSubCat
            ]);
        }
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'dataCat' => $dataCat,
            'dataSubCat' => $dataSubCat
        ]);


    }

    /**
     * Displays a single QnewCustomFields model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new QnewCustomFields model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CustomFields();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->custom_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing CustomFields model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->custom_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing CustomFields model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the CustomFields model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return CustomFields the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CustomFields::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


    public function actionCat($id)
    {
        $p_id = Category::find()->where(['id' => $id])->one();
        $count = SubCategory::find()
            ->where(['parent' => $p_id['id']])
            ->All();
        $subCat = SubCategory::find()
            ->where(['parent' => $p_id['id']])
            ->All();
        if ($count >= 1) {
            echo "<option value='other'> Other</option>";
            foreach ($subCat as $name) {
                echo "<option value='" . $name['id'] . "'>" . $name['name'] . "</option>";
            }

        } else {
            echo "<option value='other'> Other</option>";
        }
    }
}
