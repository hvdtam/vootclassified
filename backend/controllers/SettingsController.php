<?php

namespace backend\controllers;

use common\models\Admin;
use common\models\Ads;
use common\models\Adsense;
use common\models\AdsSettings;
use common\models\ApiKeys;
use common\models\Cities;
use common\models\Contact;
use common\models\DefaultSetting;
use common\models\Faq;
use common\models\FooterColumn;
use common\models\FooterContent;
use common\models\SiteBanner;
use common\models\SiteSettings;
use common\models\MainMenu;
use common\models\Product;
use common\models\States;
use common\models\Track;
use Yii;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\UploadedFile;
use yii\web\User;
use yii\data\ActiveDataProvider;
use yii\web\NotAcceptableHttpException;

/**
 * Settings controller
 */


define('LOGO_W', 223);
define('LOGO_H', 50);
define('FAV_ICON_SIZE', 10);
define('IMG_BANNER_DIR2', \yii::getAlias('@frontend') . '/web/images/site/');

class SettingsController extends Controller
{

    public function beforeAction($action)
    {
        if (\Yii::$app->user->isGuest) {
            return $this->goHome();
        };
        $control = Yii::$app->params['demo'];
        if (isset($_POST['hasEditable']) and $control == true) {
            Yii::$app->session->setFlash('error', 'Its Demo You Cannot Modify anything');
            $out = Json::encode(['output' => '', 'message' => 'Its Demo Version You Cannot Change Any Value']);
            return $out;
        };
        if (Yii::$app->controller->action->id !== "default" and $control == true and (Yii::$app->request->post() or Yii::$app->request->get())) {
            throw new BadRequestHttpException('Its Demo You Cannot Modify anything');
        } else {
            return parent::beforeAction($action);
        }
    }

    /**
     * @inheritdoc
     */

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionSite()
    {
        // echo IMG_SITE_DIR;die;
        $count = SiteSettings::find()->count();//die;

        if ($count == "0") {
            $model = new SiteSettings();

            if ($model->load(Yii::$app->request->post())) {
                $model->logo = UploadedFile::getInstance($model, 'logo');
                $logo = $model->uploadLogo();
                $model->logo = $logo;
                $fav = $model->uploadFav();
                $model->fav_icon = $fav;
                $model->save(false);

                Yii::$app->getSession()->setFlash('success', 'Update successfully');
            }
            return $this->render('site', ['model' => $model]);
        } else {
            $model = SiteSettings::find()->one();
            $save = SiteSettings::find()->one();
            if ($model->load(Yii::$app->request->post())) {
                //$model->image = UploadedFile::getInstance($model, 'image');

                if (UploadedFile::getInstance($model, 'logo') != null) {
                    $model->logo = UploadedFile::getInstance($model, 'logo');
                    $logo = $model->uploadLogo();
                    $model->logo = $logo;

                } else {
                    $model->logo = $save->logo;
                };
                if (UploadedFile::getInstance($model, 'fav_icon') != null) {
                    $model->fav_icon = UploadedFile::getInstance($model, 'fav_icon');
                    $fav = $model->uploadFav();
                    $model->fav_icon = $fav;
                } else {
                    $model->fav_icon = $save->fav_icon;;
                }

                $model->save(false);
                Yii::$app->getSession()->setFlash('success', 'Update successfully');
            }
            return $this->render('site', ['model' => $model]);
        }


    }

    //dashboard Action

    public function actionDashboard()
    {
        $user = \common\models\User::find()->orderBy(['created_at' => SORT_DESC])->limit('5')->all();
        $ads = Ads::find()->orderBy(['created_at' => SORT_DESC])->limit('5')->all();;

        $statistics = Track::find()->limit('5')->all();
        $adsTotal = Ads::find()->count();
        $pending = Ads::find()->where(['active' => 'yes'])->count();
        $active = ($adsTotal - $pending);
        return $this->render('index', [
            'total' => $adsTotal,
            'pending' => $pending,
            'active' => $active,
            'statistics' => $statistics,
            'member' => $user,
            'ads' => $ads
        ]);
    }

    //adsense Action

    public function actionAdsense()
    {
        if (\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        if (isset($_POST['hasEditable'])) {
            $AdSenseId = Yii::$app->request->post('editableKey');
            $AdSense = Adsense::findOne($AdSenseId);

            // store a default json response as desired by editable
            $out = Json::encode(['output' => '', 'message' => '']);
            $posted = current($_POST['Adsense']);
            $post = ['Adsense' => $posted];
            if ($AdSense->load($post)) {
                // can save model or do something before saving model
                $AdSense->save(false);
            }
            return $out;
        }

        $searchModel = new Adsense();
        $query_s = Adsense::find()->indexBy('id');
        $dataProvider = new ActiveDataProvider([
            'query' => $query_s,
        ]);
        return $this->render('adsense', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    //adsense expand Action
    public function actionAdsenseDetail()
    {
        if (isset($_POST['expandRowKey'])) {
            $model = Adsense::findOne($_POST['expandRowKey']);
            //$variable = ['title'=>$model->name." Detail"]; //anydata want to send in expanded view
            return $this->renderPartial('_adsense-detail.php', ['model' => $model]);
        } else {
            return '<div class="alert alert-danger">No data found</div>';

        }
    }

    public function actionAdsenseDelete($id)
    {
        $model = Adsense::findOne($id);
        $model->delete();
        $this->redirect(Url::toRoute('settings/adsense'));
    }

    //dashboard Action

    public function actionDefault()
    {
        if (\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $searchModel = new DefaultSetting();

        $modelData = DefaultSetting::find()->where(['id' => '1'])->one();
        if ($modelData->load(Yii::$app->request->post())) {
            // can save model or do something before saving model
            if (DefaultSetting::purchase($modelData['purchase_code'], $modelData['codecanyon_username']))
                $modelData->script = $searchModel->cookeyCreate();
            $cookies = Yii::$app->response->cookies;
            $cookies->add(new \yii\web\Cookie([
                'name' => 'settingChange',
                'value' => "1",
            ]));
            $modelData->updated_at = time();
            $modelData->save(false);
        }
        return $this->render('default', [
            'modelData' => $modelData
        ]);
    }

    public function actionCountry()
    {
        if ($_GET['id']) {
            $country = \common\models\Countries::find()->where(['name' => $_GET['id']])->orderBy(['id' => SORT_DESC])->one();
            $state = States::find()->where(['country_id' => $country['id']])->all();
            foreach ($state as $name) {
                echo "<option value='" . $name['name'] . "'>" . $name['name'] . "</option>";
            }
            return;

        }
        $country = \common\models\Countries::find()->orderBy(['id' => SORT_DESC])->all();

        return $this->render('country', [
            'model' => $country
        ]);
    }

    public function actionState()
    {
        if ($_GET['id']) {
            $state = \common\models\States::find()->where(['name' => $_GET['id']])->orderBy(['id' => SORT_DESC])->one();
            $city = Cities::find()->where(['state_id' => $state['id']])->all();
            foreach ($city as $name) {
                echo "<option value='" . $name['name'] . "'>" . $name['name'] . "</option>";
            }
            return;

        }
        echo "PAGE NOT FOUND: ERROR 404.";
    }

    public function actionBanner()
    {
        $model = new \common\models\SiteBanner();
        if ($model->load(Yii::$app->request->post())) {
            $file = UploadedFile::getInstance($model, 'name');

            $Imgname = rand(137, 999999) . time();


            $file->name = $Imgname . '.' . $file->extension; // override the file name

            $model->name = $file;

            if ($model->validate() && $model->save()) {
                $file->saveAs(IMG_BANNER_DIR2 . $model->name);
                Yii::$app->getSession()->setFlash('success', 'Upload successfully');

            }


        }
        $banner = SiteBanner::find()->all();

        return $this->render('banner', ['model' => $model, 'banner' => $banner]);

    }

    public function actionFaqDelete($id)
    {
        $delete = Faq::find()->where(['id' => $id])->one();
        $delete->delete();
        return $this->redirect(Url::toRoute('settings/faq'));

        //return $this->render('faq', ['model'=>$model,'list'=>$list]);
    }

    public function actionFaqedit($id)
    {
        $model = Faq::find()->where(['id' => $id])->one();
        if ($model->load(Yii::$app->request->post())) {
            //$model->image = UploadedFile::getInstance($model, 'image');
            $model->save();
            Yii::$app->getSession()->setFlash('success', 'Update successfully');
            return $this->redirect(Url::toRoute('settings/faq'));
        }
        $list = Faq::find()->all();
        return $this->render('faq-edit', ['model' => $model, 'list' => $list]);

        //return $this->render('faq', ['model'=>$model,'list'=>$list]);
    }

    public function actionFaq()
    {
        // echo IMG_SITE_DIR;die;
        $count = Faq::find()->count();//die;

        if ($count == "0") {
            $model = new Faq();

            if ($model->load(Yii::$app->request->post())) {
                $model->save(false);

                Yii::$app->getSession()->setFlash('success', 'Update successfully');
            }
            return $this->render('faq', ['model' => $model, 'list' => false]);
        } else {

            $model = new Faq();
            if ($model->load(Yii::$app->request->post())) {
                //$model->image = UploadedFile::getInstance($model, 'image');
                $model->save();
                Yii::$app->getSession()->setFlash('success', 'Update successfully');
            }
            $list = Faq::find()->all();
            return $this->render('faq', ['model' => $model, 'list' => $list]);
        }


    }

    public function actionAdmin()
    {
        $this->layout = "main";
        $model = Admin::find()->one();
        if ($model->load(Yii::$app->request->post())) {
            $model->change();
            Yii::$app->getSession()->setFlash('success', 'Update successfully');
        }
        return $this->render('admin', ['model' => $model]);


    }

    //ajax function

    //Api key Action

    public function actionApi()
    {
        $api = ApiKeys::find()->all();

        return $this->render('api', [
            'modal' => $api
        ]);
    }

    public function actionApiStatus()
    {
        if (isset($_GET["status"])) {
            $status = $_GET["status"];
            $id = $_GET["id"];
            $modal = ApiKeys::find()->where(['id' => $id])->one();
            $modal->status = $status;
            if ($modal->save(false)) {
                return true;
            } else {
                return false;
            }
        }
    }


    public function actionUpdate()
    {
        $sql = "ALTER TABLE `default_setting` ADD `url_key` TEXT NOT NULL AFTER `active`;";
        $sql = "ALTER TABLE `default_setting` CHANGE `themes` `themes` ENUM('style','style_old','style_one','style_two','style_three','style_four','style_five','style_six') CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL;";
        $sql = "INSERT INTO `plugin` (`id`, `plugin_id`, `plugin_icon`, `plugin_name`, `plugin_version`, `plugin_admin_url`, `plugin_url`, `plugin_type`, `plugin_template`, `plugin_key`, `purchase_code`, `codecanyon_username`, `status`) VALUES ('4', '254', 'fa fa-user', 'user-reviews', '1.0', 'user-reviews', 'reviews', 'module', 'reviews', '', '', '', '0');";
        $sql = "INSERT INTO `plugin` (`id`, `plugin_id`, `plugin_icon`, `plugin_name`, `plugin_version`, `plugin_admin_url`, `plugin_url`, `plugin_type`, `plugin_template`, `plugin_key`, `purchase_code`, `codecanyon_username`, `status`) VALUES ('5', '781', '', 'multi language', '1.0', '', '', 'system', '', '', '', '', '0');";

    }

    //Ads settings Action

    public function actionAds()
    {
        $modal = AdsSettings::findOne(1);
        $saved = AdsSettings::findOne(1);

        if ($modal->load(Yii::$app->request->post())) {
            if (UploadedFile::getInstance($modal, 'watermark_image') != null) {
                $modal->watermark_image = UploadedFile::getInstance($modal, 'watermark_image');
                $logo = $modal->uploadWaterMark();
                $modal->watermark_image = $logo;
            } else {
                //echo UploadedFile::getInstance($modal,'watermark_image');;

                $modal->watermark_image = $saved->watermark_image;
            }
            $modal->save(false);

        }

        return $this->render('ads', [
            'modal' => $modal
        ]);
    }


}
