<?php

namespace backend\controllers;

use common\models\Ads;
use common\models\Category;
use common\models\SubCategory;
use common\models\Type;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\data\ActiveDataProvider;

/**
 * Ads controller
 */
class ItemsController extends Controller
{


    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        if (\Yii::$app->user->isGuest) {
            return $this->goHome();
        };
        $control = Yii::$app->params['demo'];

        if ($control) {
            if (isset($_POST['hasEditable'])) {
                Yii::$app->session->setFlash('error', 'Its Demo You Cannot Modify anything');
                $out = Json::encode(['output' => '', 'message' => 'Its Demo Version You Cannot Change Any Value']);
                return $out;
            };
            return parent::beforeAction($action);
        } else {
            return parent::beforeAction($action);

        }

    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {


        if (\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        if (isset($_POST['hasEditable'])) {

            $adsId = Yii::$app->request->post('editableKey');
            $ads = Ads::findOne($adsId);

            // store a default json response as desired by editable
            $out = Json::encode(['output' => $ads->active, 'message' => '']);
            $posted = current($_POST['Ads']);
            $post = ['Ads' => $posted];
            if ($ads->load($post)) {
                // can save model or do something before saving model
                if ($ads->save(false)) {
                    $out = Json::encode(['output' => $ads->errors, 'message' => '']);
                } else {
                    $out = Json::encode(['output' => 'error', 'message' => 'Failed']);

                }
            }

            return $out;
        }

        $searchModel = new Ads();
        $query_s = Ads::find()->indexBy('id');
        $dataProvider = new ActiveDataProvider([
            'query' => $query_s,
        ]);


        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);

    }

    public function actionView()
    {

        if (isset($_POST['expandRowKey'])) {
            $model = Ads::findOne($_POST['expandRowKey']);
            return $this->renderPartial('_view.php', ['model' => $model]);
        } else {
            return '<div class="alert alert-danger">No data found</div>';

        }

    }

    public function actionEdit($id)
    {
        $saved = Ads::find()->where(['id' => $id])->one();
        $model = Ads::find()->where(['id' => $id])->one();
        if ($model->load(Yii::$app->request->post())) {

            if (UploadedFile::getInstances($model, 'image')) {
                $model->image = UploadedFile::getInstances($model, 'image');
                $screen = $model->ScreenShot();
                $model->image = $screen;
            } else {
                $model->image = $saved->image;
            }
            $model->save(false);
            Yii::$app->session->setFlash('success', 'update successfully');

            return $this->render('edit', [
                'model' => $model,
            ]);
        } else {
            return $this->render('edit', [
                'model' => $model,
            ]);
        }

    }

    public function actionApproved($id)
    {

        $ads = Ads::find()->where(['id' => $id])->one();
        $ads->active = 'yes';
        $ads->save(false);
        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionReject($id)
    {

        $ads = Ads::find()->where(['id' => $id])->one();
        $ads->active = 'no';
        $ads->save(false);
        return $this->redirect(Yii::$app->request->referrer);
    }


    public function actionFormtype($name)
    {
        $p_id = SubCategory::find()->where(['name' => $name])->one();
        $count = Type::find()
            ->where(['parent' => $p_id['id']])
            ->All();
        $city = Type::find()
            ->where(['parent' => $p_id['id']])
            ->All();
        if ($count > 0) {
            echo "<option value='other'> Other</option>";
            foreach ($city as $name) {
                echo "<option value='" . $name->name . "'>" . $name->name . "</option>";
            }

        } else {
            echo "<option value='other'> other </option>";
        }
    }

    public function actionCat($name)
    {
        $p_id = Category::find()->where(['name' => $name])->one();
        $count = Type::find()
            ->where(['parent' => $p_id['id']])
            ->All();
        $city = SubCategory::find()
            ->where(['parent' => $p_id['id']])
            ->All();
        if ($count > 0) {
            echo "<option value='other'> Other</option>";
            foreach ($city as $name) {
                echo "<option value='" . $name->name . "'>" . $name->name . "</option>";
            }

        } else {
            echo "<option value='other'> other </option>";
        }
    }

    public function actionDetail()
    {
        $adId = $_GET['id'];
        if (isset($adId)) {
            $model = Ads::findOne($adId);
            return $this->render('detail.php', ['model' => $model]);
        } else {
            return '<div class="alert alert-danger">No data found</div>';

        }
    }


}
