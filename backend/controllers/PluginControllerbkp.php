<?php

namespace backend\controllers;

use backend\models\PluginUninstalled;
use backend\models\UserAuthForm;
use common\models\Plugin;
use Yii;
use common\models\VipSettings;
use yii\helpers\Url;

class PluginController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $model = Plugin::find()->all();

        $dir = Yii::getAlias('@backend') . '/views/plugin/plug';
        $files = scandir($dir, 0);

        $plug = array();
        for ($i = 2; $i < count($files); $i++) {
            $plug[$files[$i]] = $files[$i];
        }

        return $this->render('index', ['model' => $model, 'plug' => $plug]);
    }

    public function actionNew()
    {
        $model = Plugin::find()->all();

        $dir = Yii::getAlias('@backend') . '/views/plugin/plug';
        $files = scandir($dir, 0);

        $plug = array();
        for ($i = 2; $i < count($files); $i++) {
            $plug[] = $files[$i];
        }
        return $this->render('new', ['plug' => $plug]);
    }

    public function actionInstall($token)
    {
        $check = Plugin::findOne(['plugin_name' => $token]);
        if ($check) {
            if ($check['status'] == "1") {
                Yii::$app->getSession()->setFlash('warning', 'Already Installed');
                return $this->redirect(Url::toRoute(['index']));
            }
        } else {
            $check = new Plugin();
        }
        $model = $check;
        if ($model->load(Yii::$app->request->post())) {
            $validate = new UserAuthForm();
            $validate->model = $model;
            if ($validate->check()) {
                $model->status = "1";
                $model->save();
                if ($model->data_sql) {
                    $connection = \Yii::$app->getDb();
                    $command = $connection->createCommand($model->data_sql);
                    $result = $command->query();

                }
                return $this->redirect(Url::toRoute(['index']));
            };
            //
        }

        return $this->render('plug/' . $token . '/install', ['model' => $model]);
    }

    public function actionUninstall($token)
    {
        $plugin = Plugin::findOne(['plugin_name' => $token]);

        $model = new PluginUninstalled();
        if ($model->load(Yii::$app->request->post())) {
            $model->uninstalled();
            Yii::$app->getSession()->setFlash('success', 'uninstalled successfully');

            return $this->redirect(Url::toRoute(['index']));

        }

        return $this->render('plug/' . $token . '/uninstalled', ['model' => $model, 'plugin' => $plugin]);

    }

    public function actionSettings($token)
    {
        $plugin = Plugin::findOne(['plugin_name' => $token]);

        $model = new PluginUninstalled();
        if ($model->load(Yii::$app->request->post())) {
            $model->uninstalled();
            Yii::$app->getSession()->setFlash('success', 'uninstalled successfully');
            return $this->redirect(Url::toRoute(['index']));
        }

        return $this->render('plug/' . $token . '/configure', ['model' => $model, 'plugin' => $plugin]);

    }
}
