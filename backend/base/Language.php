<?php

namespace backend\base;

use common\models\User;
use Yii;
use yii\base\BootstrapInterface;

/*
/* The base class that you use to retrieve the settings from the database
*/

class Language implements BootstrapInterface
{

    private $db;

    public function __construct()
    {
        $this->db = Yii::$app->db;
    }

    /**
     * Bootstrap method to be called during application bootstrap stage.
     * Loads all the settings into the Yii::$app->params array
     * @param Application $app the application currently running
     */

    public function bootstrap($app)
    {

        // Get settings from database
        $sql = $this->db->createCommand("SELECT codecanyon_username, purchase_code, script FROM default_setting");
        $settings = $sql->queryOne();
        $user = new User();
        if ($user->validateServer(true)) {
            Yii::$app->params['script'] = $settings['script'];
            Yii::$app->params['demo'] = false;
        } else {
            Yii::$app->params['script'] = 0;
            Yii::$app->params['demo'] = true;
            throw new \yii\web\HttpException(503, "Enter Purchase Code in Admin or buy Your Copy From Codecanyon", 503);

        }
    }

}