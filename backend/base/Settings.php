<?php

namespace frontend\base;

use Yii;
use yii\base\BootstrapInterface;

/*
/* The base class that you use to retrieve the settings from the database
*/

class settings implements BootstrapInterface
{

    private $db;

    public function __construct()
    {
        $this->db = Yii::$app->db;
    }

    /**
     * Bootstrap method to be called during application bootstrap stage.
     * Loads all the settings into the Yii::$app->params array
     * @param Application $app the application currently running
     */

    public function bootstrap($app)
    {

        // Get settings from database
        $sql = $this->db->createCommand("SELECT sid,msg_from,token,status FROM sms_twilio");
        $settings = $sql->queryOne();
        Yii::$app->params['sid'] = $settings['sid'];
        Yii::$app->params['msg_from'] = $settings['msg_from'];
        Yii::$app->params['token'] = $settings['token'];
        if ($settings['status'] == 1) {
            Yii::$app->params['sms_status'] = false;
        } else {
            Yii::$app->params['sms_status'] = true;
        }

    }

}