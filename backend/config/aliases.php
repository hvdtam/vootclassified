<?php
Yii::setAlias('@frontendbase', realpath(dirname(__FILE__) . '/../../'));
Yii::setAlias('@logo', realpath(dirname(__FILE__) . '/../../images/site/logo'));
Yii::setAlias('@fav', realpath(dirname(__FILE__) . '/../../images/site/fav'));
Yii::setAlias('@userimage', realpath(dirname(__FILE__) . '/../../images/user'));
Yii::setAlias('@backendBase', str_replace('\backend', '', dirname(__DIR__)));

