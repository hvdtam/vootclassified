<?php

use \yii\web\Request;

$baseUrl = str_replace('/admin', '', (new Request)->getBaseUrl());

$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => [
        'log',
        'backend\base\Language',
    ],
    'modules' => [
        'gridview' => [
            'class' => '\kartik\grid\Module'
            // enter optional module parameters below - only if you need to
            // use your own export download action or custom translation
            // message source
            // 'downloadAction' => 'gridview/export/download',
            // 'i18n' => []
        ],
        'gridviewKrajee' => [
            'class' => '\kartik\grid\Module',
            // your other grid module settings
        ],
        'i18n' => [
            'class' => backend\modules\i18n\Module::class,
            'defaultRoute' => 'i18n-message/index'
        ],
        'tool' => [
            'class' => backend\modules\tool\Module::class,
        ],
        'file' => [
            'class' => backend\modules\file\Module::class,
        ],
    ],
    'components' => [
        'user' => [
            'identityClass' => 'common\models\Admin',
            'enableAutoLogin' => true,
        ],
        'urlManagerFrontend' => [
            'class' => 'yii\web\urlManager',
            'baseUrl' => $baseUrl . '/',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
        ],

        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'assetManager' => [
            'bundles' => [
//                'yii\web\JqueryAsset' => [
//                    'sourcePath' => null,
//                    'basePath' => '@webroot',
//                    'baseUrl' => '@web',
//                    'js' => [
//                       'themes/assets/js/core/jquery.3.3.1.min.js',
//                    ]
//                ],
//                'yii\web\JqueryAsset' => [
//                    'js'=>[]
//                ],
//                'yii\bootstrap\BootstrapPluginAsset' => [
//                    'js'=>[]
//                ],
                'yii\bootstrap\BootstrapAsset' => [
                    'css' => [],
                ]

            ],
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => array(
                'index' => 'index',
                'location/form-state/<id>' => 'location/form-state',
                'ads/formtype/<name>' => 'ads/formtype',
                'ads/cat/<name>' => 'ads/cat',
                'payment/edit/<id>' => 'payment/edit',
                'pages/edit/<id>' => 'pages/edit',
                'payment/delete/<id>' => 'payment/delete',
                'payment/ads-edit/<ads>' => 'payment/ads-edit',
                'settings/faqedit/<id>' => 'settings/faqedit',
                'settings/faq-delete/<id>' => 'settings/faq-delete',

                'delete/category/<id>' => 'delete/category',

                'delete/sub-category/<id>' => 'delete/sub-category',
                'delete/type/<id>' => 'delete/type',
                'delete/user/<id>' => 'delete/user',
                'delete/page/<id>' => 'delete/page',
                'delete/city/<id>' => 'delete/city',
                'delete/state/<id>' => 'delete/state',
                'delete/country/<id>' => 'delete/country',
                'delete/item/<id>' => 'delete/item',
                'delete/adsense/<id>' => 'delete/adsense',
                'delete/paypal/<id>' => 'delete/paypal',
                'delete/currency/<id>' => 'delete/currency',
                'delete/custom/<id>' => 'delete/custom',
                'delete/languages/<id>' => 'delete/languages',
                'delete/footer-content/<id>' => 'delete/footer-content',
                'delete/footer-column/<id>' => 'delete/footer-column',
                'widgets/edit/<id>' => 'widgets/edit',
                'reports/detail/<id>' => 'reports/detail',
                'user/detail/<id>' => 'user/detail',
                'site/user-detail' => 'site/user-detail',
                'settings/adsense-detail' => 'settings/adsense-detail',
            ),
        ],
    ],
    'params' => $params,
];
