<?php

use yii\bootstrap\ActiveForm;

$this->title = "Upload";
?>
<div class="col-sm">
    <div class="tool-default-index">
        <h1><?= $this->context->action->uniqueId ?></h1>
        <p>
            This is the view content for action "<?= $this->context->action->id ?>".
            The action belongs to the controller "<?= get_class($this->context) ?>"
            in the "<?= $this->context->module->id ?>" module.
        </p>
        <p>
            You may customize this page by editing the following file:<br>
            <code><?= __FILE__ ?></code>
        </p>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col">
            <?php $form = ActiveForm::begin([
                'enableClientValidation' => false,
                'enableAjaxValidation' => true,
            ]) ?>
            <div class="col-sm">
                <?php echo $form->field($model, 'thumbnail')->widget(
                    \trntv\filekit\widget\Upload::class,
                    [
                        'url' => ['/file/storage/upload'],
                        'maxFileSize' => 5000000, // 5 MiB,
                        'acceptFileTypes' => new \yii\web\JsExpression('/(\.|\/)(gif|jpe?g|png)$/i'),
                    ]);
                ?>
            </div>
            <?php ActiveForm::end() ?>
        </div>
    </div>
</div>
