<?php

namespace backend\modules\tool\controllers;

use common\models\Ads;
use common\models\Category;
use yii\web\Controller;
use Yii;

/**
 * Default controller for the `tool` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionUpload()
    {
        $article = new Ads();


        if ($article->load(Yii::$app->request->post()) && $article->save()) {
            return $this->redirect(['index']);
        }


        return $this->render('upload',[
            'model' => $article,
        ]);
    }
}
