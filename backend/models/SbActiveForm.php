<?php
namespace backend\models;

use common\models\Admin;
use common\models\DefaultSetting;
use common\models\States;
use Yii;
use yii\base\Model;

/**
 * Login form
 */
class SbActiveForm extends Model
{
    public $domain_name;
    public $contact_email;
    public $purchase_code;
    public $codecanyon_username;
    public $admin_username;
    public $admin_password;
    private $_user;
    private $item = "23023335";
    private $cookie = "iDrIpduCFDh37HuF3oC9YPYXJNHUT0U8";
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['domain_name','purchase_code','codecanyon_username','admin_username','admin_password'], 'required'],
            // rememberMe must be a boolean value
            ['contact_email', 'trim'],
            ['contact_email', 'required'],
            ['contact_email', 'email'],
            ['contact_email', 'string', 'max' => 255],
            ['purchase_code', 'validateCode'],
            ['admin_password', 'string', 'min' => 5],
            ['domain_name','match','pattern'=>'/^[w]{3}\.[a-zA-Z0-9][a-zA-Z0-9-]{1,61}[a-zA-Z0-9]\.[a-zA-Z]{2,}$/','message'=>'invalid Url'],
            // password is validated by validatePassword()

        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validateCode($attribute, $params)
    {
        if (!$this->hasErrors()) {
            if (!preg_match("/^(\w{8})-((\w{4})-){3}(\w{12})$/", $this->purchase_code)) {
                $this->addError($attribute, 'Incorrect username or purchase code.');
            }
        }
    }

    /**
     * Logs in a user using the provided username and password.
     *
     * @return boolean whether the user is logged in successfully
     */
    public function Activate()
    {
        if ($this->validate())
        {
           $res = $this->check();
            if($res['success'])
            {
                return  $this->control();

            }
            else
            {
                return $res;
            }
        } else {
              return $error = [
                'message'=>'Failed',
                'success'=>false
            ];
        }
    }

    protected function control()
    {
        $model = new Admin();
        $model->username = $this->admin_username;
        $model->email = $this->contact_email;
        $model->password = $this->admin_password;
        $model->set();
        //-set default
        $searchModel = new DefaultSetting();
        $modelData = DefaultSetting::find()->where(['id'=>'1'])->one();
        $modelData->codecanyon_username = $this->codecanyon_username;
        $modelData->purchase_code = $this->purchase_code;
        $modelData->url_key = Yii::$app->security->generatePasswordHash($this->domain_name);
        $modelData->script = $searchModel->cookeyCreate();
        $modelData->active = $searchModel->cookeyCreate();
        $modelData->save(false);
//        $cookies = Yii::$app->response->cookies;
//        $cookies->add(new \yii\web\Cookie([
//            'name' => 'settingChange',
//            'value' => "1",
//        ]));
       return $error = [
            'message'=>'Success',
            'success'=>true
        ];
    }

    protected function check($userItem=false)
    {
        $purchase = $this->purchase_code;
        $error = [
            'message'=>'Success',
            'success'=>true
        ];
        $user = $this->codecanyon_username;
        $userItem = ($userItem)?$userItem:$this->item;
        // $url = "https://api.envato.com/v3/market/author/sale?code=".$code;
        // $curl = curl_init($url);
        $usercookie = $this->cookie;
// If you took $code from user input it's a good idea to trim it:
        $key =  Yii::$app->params['key'];
        if($key == base64_encode("dev") or $key == base64_encode("custom"))
            return true;
        $code = trim($purchase);

// Make sure the code is valid before sending it to Envato:

        if (!preg_match("/^(\w{8})-((\w{4})-){3}(\w{12})$/", $code))
            return  $error['error'] = "Incorrect username or purchase code.";
            //$this->addError('purchase_code', 'Incorrect username or purchase code.');

// Query using CURL:

        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => "https://api.envato.com/v3/market/author/sale?code={$code}",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_TIMEOUT => 20,

            CURLOPT_HTTPHEADER => array(
                "Authorization: Bearer ".$usercookie,
                "User-Agent: Enter a description of your app here for the API team"
            )
        ));

// Execute CURL with warnings suppressed:

        $response = @curl_exec($ch);


        if (curl_errno($ch) > 0)
        {
           return $error = [
                'message'=>"Failed to query Envato API: " . curl_error($ch),
                'success'=>false
            ];
            //return  $error['error'] = "Failed to query Envato API: " . curl_error($ch);
            //$this->addError('purchase_code', 'Incorrect username or purchase code.');
            //throw new \yii\web\HttpException(400, "Failed to query Envato API: " . curl_error($ch), 405);
        };

// Validate response:

        $responseCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);


        if ($responseCode === 404)
        {
            //throw new \yii\web\HttpException(400, "The purchase code was invalid ", 405);
            return $error = [
                'message'=>"The purchase code was invalid",
                'success'=>false
            ];
        };


        if ($responseCode !== 200)
        {
            //throw new \yii\web\HttpException(200, "Failed to validate code due to an error: HTTP {$responseCode}", 200);
            return $error = [
                'message'=>"Failed to validate code due to an error: HTTP {$responseCode}",
                'success'=>false
            ];
        }

// Verify that the purchase code is for the correct item:
// (Replace the numbers 17022701 with your item's ID from its URL)

        $body = json_decode($response);

        if ($body->item->id !== $userItem)
        {
           // throw new \yii\web\HttpException(400, "The purchase code provided is for a different item ", 405);
            return $error = [
                'message'=>"The purchase code provided is for a different item",
                'success'=>false
            ];
        };
        if($body->buyer !== $user)
        {
            //  throw new Exception("The User detail provided is for a different item");
            //throw new \yii\web\HttpException(400, 'The User detail provided is for a different item', 405);
            return $error = [
                'message'=>"The purchase code provided is for a different item",
                'success'=>false
            ];
        }
        else
        {
            return $error = [
                'message'=>"Success",
                'success'=>true
            ];
        }
    }
}
