<?php
namespace backend\models;

use common\models\AdminNotification;
use common\models\Plugin;
use yii\base\Model;
use common\models\User;
use borales\extensions\phoneInput\PhoneInputValidator;

/**
 * Signup form
 */
class PluginUninstalled extends Model
{
    public $id;
    public $sql;
    public function rules()
    {
        return [
            [['id', 'sql'], 'safe'],
            [['id'], 'integer'],
        ];
    }
    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function uninstalled()
    {
        $id = $this->id;
        $sqlData = $this->sql;
        $model = Plugin::findOne($id);
        $model->status = "0";
        $model->save(false);
        if($sqlData)
        {
            if(is_array($sqlData))
            {
                foreach($sqlData as $sql)
                {
                    $connection = \Yii::$app->getDb();
                    $command = $connection->createCommand($sql);
                    $result = $command->query();
                }
            }
            else
            {
                $connection = \Yii::$app->getDb();
                $command = $connection->createCommand($sqlData);
                $result = $command->query();
            };
        }

        return true;

    }
}
