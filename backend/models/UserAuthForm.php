<?php
namespace backend\models;

use common\models\AdminNotification;
use common\models\DefaultSetting;
use common\models\Plugin;
use yii\base\Model;
use common\models\User;
use borales\extensions\phoneInput\PhoneInputValidator;

/**
 * Signup form
 */
class UserAuthForm extends Model
{
    public $username;
    public $code;
    public $id;
    public $model;
    public $sql;
    public function rules()
    {
        return [
            [['username', 'code', 'model'], 'safe'],
            [['id'], 'integer'],
        ];
    }
    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function check()
    {
        $model = $this->model;
        $id = $model->plugin_key;
         $code = $model->purchase_code;
        $username = $model->codecanyon_username;

        if(Plugin::WidgetsFlash($code,$username,$id))
        {
            return true;
        }
        else
        {
            return false;
        }

    }
}
