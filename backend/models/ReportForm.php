<?php
namespace backend\models;

use common\models\Ads;
use common\models\Category;
use common\models\User;
use yii\base\Model;
use Yii;
use yii\web\UploadedFile;


class ReportForm extends Model
{
    public $respond_to;
    public $message;
    public $action;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['respond_to', 'filter', 'filter' => 'trim'],
            ['respond_to', 'required'],
            ['respond_to', 'email'],
            ['respond_to', 'string', 'max' => 255],

            ['message', 'required'],
            ['message', 'string', 'max' => 500],

            ['action', 'required'],
            ['action', 'string'],


        ];
    }
    public function attributeLabels()
    {
        return [
            'respond_to' => Yii::t('app', 'Respond to'),
            'message' => Yii::t('app', 'Message'),
            'action' => Yii::t('app', 'Action On ad'),
        ];
    }

}
