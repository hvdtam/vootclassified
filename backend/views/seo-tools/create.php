<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\SeoTools */

$this->title = 'Create Seo Tools';
$this->params['breadcrumbs'][] = ['label' => 'Seo Tools', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="col-12">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
