<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\SeoTools */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin(); ?>

<div class="card mt-3">
    <div class="card-header">
        <h6 class="title">
            Verification Tools
        </h6>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                <?php echo $form->field($model, 'google')->textInput(['class' => 'form-control h6'])->label("Google site verification content") ?>

            </div>
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                <?php echo $form->field($model, 'alexa')->textInput(['class' => 'form-control h6'])->label("Alexa  site verification content") ?>

            </div>

            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                <?php echo $form->field($model, 'yandex')->textInput(['class' => 'form-control h6'])->label("Yandex site verification content") ?>

            </div>
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                <?php echo $form->field($model, 'bing')->textInput(['class' => 'form-control h6'])->label("Bing site verification content"); ?>

            </div>
        </div>
    </div>

</div>
<div class="card mt-3">
    <div class="card-header bg-gray">
        <h6 class="title">Posts Permalink Settings</h6>
    </div>
    <div class="card-body">
        <div class="row">

            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <?= $form->field($model, 'pherma_link')->dropDownList(
                    ['{slug}-{id}' => '{slug}-{id}', '{slug}/{id}' => '{slug}/{id}', '{slug}_{id}' => '{slug},{id}'],
                    ['prompt' => 'Select...']) ?>

            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <?= $form->field($model, 'pherma_extension')->dropDownList(
                    ['php' => '.php', 'html' => '.html', 'htm' => '.htm', 'aspx' => '.aspx'],
                    ['prompt' => 'Select...']) ?>

            </div>


        </div>
    </div>

</div>
<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <?= Html::submitButton('Save', ['class' => 'btn btn-lg btn-success']) ?>

</div>
<?php ActiveForm::end(); ?>

