<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\SeoTools */

$this->title = "View Entry";
$this->params['breadcrumbs'][] = ['label' => 'Seo Tools', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="col-12">
    <div class="card mt-3">
        <div class="card-header bg-gray">
            <h6 class="title">View Entry</h6>
        </div>
        <div class="card-body">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'google:ntext',
                    'alexa:ntext',
                    'yandex:ntext',
                    'bing:ntext',
                    'pherma_link:ntext',
                    'pherma_extension:ntext',
                ],
            ]) ?>
        </div>
        <div class="card-footer">
            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ?>
        </div>
    </div>

</div>
