<?php
/* @var $this yii\web\View */

use \yii\helpers\Url;

$this->title = "Display Widgets Settings";
$this->params['breadcrumbs'][] = ['label' => 'Site Settings', 'url' => ['settings/site']];
$this->params['breadcrumbs'][] = ['label' => 'Default Settings', 'url' => ['settings/default']];

$this->params['breadcrumbs'][] = $this->title;

?>
<div class="col-md-12">
    <hr>
    <div class="card card-transparent">

        <div class="card-body">
            <div class="row">
                <div class="col-4 col-md-3">
                    <div class="nav flex-column nav-pills nav-secondary nav-pills-no-bd">
                        <a href="<?= Url::toRoute('settings/site') ?>" class="nav-link">Site Settings</a>

                        <a href="<?= Url::toRoute('settings/default') ?>" class="nav-link">Default Settings</a>
                        <a href="<?= Url::toRoute('display/home-page') ?>" class="nav-link">Home Page Settings</a>
                        <a href="<?= Url::toRoute('widgets/index') ?>" class="nav-link active">Widget Settings</a>
                        <a href="<?= Url::toRoute('settings/admin') ?>" class="nav-link">Admin Settings</a>
                        <a class="nav-link" href="<?= Url::toRoute('settings/api') ?>">Api Settings</a>
                        <a class="nav-link" href="<?= Url::toRoute('settings/ads') ?>">Ads Settings</a>

                    </div>
                </div>
                <div class="col-7 col-md-9">
                    <?php
                    foreach ($modal as $widget) {
                        if ($widget['id'] == 6) {
                            $option = json_decode($widget['options'], true);

                            ?>
                            <div class="col-md-12">
                                <div class="card card-round">
                                    <div class="card-header">
                                        <div class="card-title">
                                            Banner Widget
                                            <div class="pull-right">
                                                <input type="checkbox" class="display-home-page"
                                                       data-display-url="<?= \yii\helpers\Url::toRoute("display/status") ?>"
                                                       data-display-id="<?= $widget['id']; ?>"
                                                       data-status="<?= $widget['status']; ?>"
                                                       value="<?= $widget['status']; ?>"
                                                       name="status" <?= ($widget['status'] == "enable") ? "checked" : ""; ?>
                                                       data-toggle="toggle" data-onstyle="success"
                                                       data-style="btn-round">
                                                <i id="display_<?= $widget['id'] ?>"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-body ">

                                        <div class="row">
                                            <div class="col-12">
                                                <div align="center"
                                                     img-url="<?= Yii::$app->urlManagerFrontend->baseUrl . '/images/banner/' ?>"
                                                     id="bannerFormImage"
                                                     style="padding: 80px 10px;color: #fff;background-image: url(<?= Yii::$app->urlManagerFrontend->baseUrl . '/images/banner/' . $widget['template'] ?>);background-position: center center">
                                                    <h1 id="bannerFormTitle"
                                                        style="color: <?= $option['title_color']; ?>">
                                                        <?= $option['title']; ?>
                                                    </h1>
                                                    <h3 id="bannerFormTagLine"
                                                        style="color: <?= $option['tag_color']; ?>">
                                                        <?= $option['tag line']; ?>
                                                    </h3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-footer">
                                        <input type="hidden" id="BannerUpdateUrl"
                                               value="<?= \yii\helpers\Url::toRoute('edit/banner') ?>">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <input type="text" style="text-indent: 28px;"
                                                       value="<?= $option['title']; ?>" id="BannerInputTitle"
                                                       class="form-control">
                                                <input type="color" banner-title-color=""
                                                       value="<?= $option['title_color']; ?>" name="titleColor"
                                                       id="BannerTitleColor" class="colorInput">

                                            </div>

                                            <div class="col-md-6">
                                                <input type="text" style="text-indent: 28px;"
                                                       value="<?= $option['tag line']; ?>" id="BannerInputTagline"
                                                       class="form-control">
                                                <input type="color" banner-tag-color=""
                                                       value="<?= $option['tag_color']; ?>" name="tagColor"
                                                       id="BannerTagColor" class="colorInput">

                                            </div>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <div class="col-md-2">
                                                <select class="form-control" name="bg" id="BannerInputImage">
                                                    <option value="bg0.jpg">Image Zero</option>
                                                    <option value="bg1.jpg">Image One</option>
                                                    <option value="bg2.jpg">Image Two</option>
                                                    <option value="bg3.jpg">Image Three</option>
                                                    <option value="bg4.jpg">Image Four</option>
                                                    <option value="bg5.jpg">Image Five</option>
                                                    <option value="bg6.jpg">Image Six</option>
                                                    <option value="bg7.jpg">Image seven</option>
                                                    <option value="bg8.jpg">Image eight</option>
                                                    <option value="bg9.jpg">Image nine</option>
                                                    <option value="bg10.jpg">Image 10</option>
                                                    <option value="bg11.jpg">Image 11</option>
                                                    <option value="bg12.jpg">Image 12</option>
                                                    <option value="bg13.jpg">Image 13</option>
                                                    <option value="bg21.jpg">Image 21</option>

                                                </select>

                                            </div>
                                            <div class="col-md-2">
                                                <div style="padding: 1px;">

                                                </div>

                                            </div>
                                            <div class="col-md-2 ">
                                                <button type="button" id="BannerSave" class="btn-success btn btn-md">
                                                    <i class="fa fa-upload"></i> <b>UPDATE</b>
                                                </button>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php
                            continue;
                        }
                        ?>
                        <div class="col-md-12">
                            <div class="card card-round">
                                <div class="card-body ">
                                    <div class="row">
                                        <div class="col-md-4" align="center">
                                            <img class="img-thumbnail img-responsive"
                                                 src="<?= Yii::getAlias('@web') ?>/images/widgets/<?= $widget['image'] ?>"
                                                 alt="Card image cap">

                                            <hr>
                                            <h6 class="text-capitalize" style="color:#666;">
                                                <?= $widget['name'] ?>
                                            </h6>
                                            <a href="<?= Url::toRoute('widgets/edit/' . $widget['id']) ?>"
                                               class="btn btn-block btn-success">
                                                <i class="fa fa-pencil"></i> Edit
                                            </a>

                                        </div>
                                        <div class="col-md-8">
                                            <table class="table">


                                                <tr>
                                                    <td>Page :</td>
                                                    <td><?= $widget['page'] ?></td>
                                                </tr>
                                                <tr>
                                                    <td>Type :</td>
                                                    <td><?= $widget['type'] ?></td>
                                                </tr>
                                                <tr>
                                                    <td>Template :</td>
                                                    <td><?= $widget['template'] ?></td>
                                                </tr>
                                                <tr>
                                                    <td>Priority :</td>
                                                    <td><?= $widget['priority'] ?></td>
                                                </tr>


                                            </table>

                                        </div>

                                    </div>


                                </div>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
















