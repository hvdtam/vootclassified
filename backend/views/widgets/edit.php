<?php
/* @var $this yii\web\View */

use \yii\bootstrap\ActiveForm;

$this->title = "Edit Widgets Settings";
$this->params['breadcrumbs'][] = ['label' => 'Site Settings', 'url' => ['settings/site']];
$this->params['breadcrumbs'][] = ['label' => 'Default Settings', 'url' => ['settings/default']];

$this->params['breadcrumbs'][] = ['label' => 'Widgets Settings', 'url' => ['widgets/index']];

$this->params['breadcrumbs'][] = $this->title;
?>
<div class="modal fade" id="addRowModal" tabindex="-1" role="dialog" aria-hidden="true">
</div>
<div class="col-md-6">
    <div class="card card-post card-round">
        <img class="card-img-top" src="<?= Yii::getAlias('@web') ?>/images/widgets/<?= $widget['image'] ?>"
             id="widgetPreviewImgS" alt="<?= $widget['name'] ?>">
        <div class="card-body">

            <div class="seperator-solid"></div>
            <p class="card-category text-info mb-1"><a href="#"><?= $widget['page'] ?></a></p>
            <h3 class="card-title">
                <a href="#">
                    <?= $widget['name']; ?>
                </a>
            </h3>
            <table class="table">
                <tr>
                    <th>Page :</th>
                    <th><?= $widget['page'] ?></th>
                </tr>
                <tr>
                    <th>Type :</th>
                    <th><?= $widget['type'] ?></th>
                </tr>
                <tr>
                    <th>Template :</th>
                    <th><?= \common\models\WidgetTemplat::getNameByTemp($widget['template']); //$widget['name']  ?></th>
                </tr>
                <tr>
                    <th>Priority :</th>
                    <th><?= $widget['priority'] ?></th>
                </tr>
                <tr>
                    <th>Number of Item :</th>
                    <th><?= $widget['number_of_item'] ?></th>
                </tr>
                <tr>
                    <th>Status :</th>
                    <th><?= $widget['status'] ?></th>
                </tr>

            </table>

        </div>
    </div>
</div>
<div class="col-lg-6 ml-auto mr-auto">
    <div class="card">
        <div class="card-header">
            <div class="card-title">Edit <span class="text-info text-capitalize"><?= $widget['name']; ?></span> widget
                Settings
            </div>
        </div>
        <?php $form = ActiveForm::begin([
            'action' => ['edit/edit-widget?id=' . $widget['id']],
            //'method' => 'get',
            'options' => ['id' => 'edit_widget_form', 'class' => 'master-form']
        ]); ?>

        <div class="card-body">
            <?= $form->field($widget, 'template')->dropDownList($data); ?>

            <?php /// $form->field($widget, 'priority') ?>
            <div class="row">
                <div class="col-md-6">
                    <?= $form->field($widget, 'number_of_item')->textInput(['disable']) ?>
                </div>
                <div class="col-md-6">
                    <br>
                    <br>

                    <label>Widgets Status</label>
                    <input type="checkbox" class="display-home-page"
                           data-display-url="<?= \yii\helpers\Url::toRoute("display/status") ?>"
                           data-display-id="<?= $widget['id']; ?>" data-status="<?= $widget['status']; ?>"
                           value="<?= $widget['status']; ?>"
                           name="status" <?= ($widget['status'] == "enable") ? "checked" : ""; ?> data-toggle="toggle"
                           data-onstyle="success" data-style="btn-round">
                    <i id="display_<?= $widget['id'] ?>"></i>
                </div>
            </div>
            <input type="hidden" value="<?= Yii::getAlias('@web') ?>/images/widgets/" id="widgetsImgUrl">

            <?= $form->field($widget, 'image')->hiddenInput()->label(false);; ?>


        </div>
        <div class="card-action">
            <button type="submit" class="btn btn-success">Submit</button>
            <button class="btn btn-danger">Cancel</button>
            <a href="<?= \yii\helpers\Url::toRoute('widgets/index') ?>" class="btn btn-info">Back</a>
        </div>
        <?php ActiveForm::end(); ?>

    </div>
</div>
