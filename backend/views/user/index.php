<?php
$this->title = "User profile";

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>
<div class="col-md-12">
    <div class="row">
        <div class="col-md-4">
            <div class="card card-profile card-secondary">
                <div class="card-header"
                     style="background-image: url('<?= Yii::$app->urlManagerFrontend->baseUrl ?>/assets/img/blogpost.jpg')">
                    <div class="profile-picture">
                        <img src="<?= Yii::$app->urlManagerFrontend->baseUrl . '/images/users/' . $model['image'] ?>"
                             alt="Profile Picture">
                    </div>
                </div>
                <div class="card-body">
                    <div class="user-profile text-center">
                        <div class="name"><?= $model['first_name']; ?> <?= $model['last_name']; ?></div>
                        <div class="job"><?= $model['type']; ?></div>
                        <div class="desc"><?= $model['about_you']; ?></div>
                        <div class="view-profile">
                            <a href="#" class="btn btn-secondary btn-block">
                                Member From <i class="fa fa-angle-right"></i>
                                <?= date('d-m-Y', $model['created_at']); ?>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <div class="row user-stats text-center">
                        <div class="col">
                            <div class="number"><?= count($ads); ?></div>
                            <div class="title">Ads</div>
                        </div>
                        <div class="col">
                            <div class="number"><?= $premium; ?></div>
                            <div class="title">Premium</div>
                        </div>
                        <div class="col">
                            <div class="number"><?= $like; ?></div>
                            <div class="title">Like Ads</div>
                        </div>
                    </div>
                </div>
            </div>


        </div>
        <div class="col-md-8">

            <div class="card">
                <div class="card-header">
                    <strong class="card-title">More About <?= $model['first_name'] ?> (User Detail)</strong>
                    <ul class="nav nav-pills nav-secondary nav-pills-no-bd pull-right" id="pills-tab-without-border"
                        role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="pills-home-tab-icon" data-toggle="pill"
                               href="#detail_panel_<?= $model['id'] ?>" role="tab" aria-controls="pills-home-icon"
                               aria-selected="true">
                                <i class="flaticon-user-1"></i>
                                Detail
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="pills-profile-tab-icon" data-toggle="pill"
                               href="#ads_panel_<?= $model['id'] ?>" role="tab" aria-controls="pills-profile-icon"
                               aria-selected="false">
                                <i class="flaticon-cart-1"></i>
                                Ads ( <?= count($ads) ?> )
                            </a>
                        </li>

                    </ul>

                </div>
                <div class="card-body">
                    <div class="tab-content mb-3" id="pills-with-icon-tabContent_detail_panel_<?= $model['id'] ?>">
                        <div class="tab-pane fade in show active" id="detail_panel_<?= $model['id'] ?>" role="tabpanel"
                             aria-labelledby="pills-home-tab-icon">
                            <table class="table">
                                <?php
                                foreach ($model as $key => $user) {
                                    $restrict = ['password_hash' => '1', 'id' => '1', 'updated_at' => '1', 'image' => '1', 'status' => '1', 'password_reset_token' => '1', 'lat' => '1', 'lng' => '1', 'flag_v' => '1', 'purchase_code' => '1', 'otp' => '1', 'auth_key' => '1'];
                                    if (array_key_exists($key, $restrict)) {
                                        continue;
                                    }
                                    ?>
                                    <tr>
                                        <td>
                                            <?= $key; ?>
                                        </td>
                                        <td class="text-capitalize">
                                            <?php
                                            if ($key == "created_at") {
                                                echo date('d-m-Y', $user);
                                            } else {
                                                echo str_replace("_", " ", $user);
                                            }
                                            ?>
                                        </td>

                                    </tr>
                                    <?php
                                }
                                ?>
                            </table>
                        </div>
                        <div class="tab-pane fade in" id="ads_panel_<?= $model['id'] ?>" role="tabpanel"
                             aria-labelledby="pills-profile-tab-icon">
                            <div class="card card-round">
                                <div class="card-body">
                                    <div class="card-title">Posted Ads</div>
                                    <div class="card-list">
                                        <?php
                                        if (count($ads) == "0") {
                                            ?>
                                            <div class="item-list">
                                                <div class="info-user ml-3">
                                                    NO AD POSTED YET
                                                </div>

                                            </div>
                                            <?php

                                        };
                                        foreach ($ads as $list) {
                                            ?>
                                            <div class="item-list">
                                                <img src="<?= Yii::$app->urlManagerFrontend->baseUrl . '/images/users/' . $model['image'] ?>"
                                                     alt="Ads" class="small-pic">
                                                <div class="info-user ml-3">
                                                    <div class="username"><?= $list['ad_title'] ?></div>
                                                    <div class="status"><?= $list['category'] ?></div>
                                                </div>
                                                <a href="<?= \yii\helpers\Url::toRoute('ads/detail?id=' . $list['id']) ?>"
                                                   class="btn btn-add">
                                                    <i class="la la-eye"></i>
                                                </a>
                                            </div>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>