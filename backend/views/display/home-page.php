<?php
$this->title = "Home Page Display Settings";

$this->params['breadcrumbs'][] = ['label' => 'Site Settings', 'url' => ['settings/site']];
$this->params['breadcrumbs'][] = ['label' => 'Default Settings', 'url' => ['settings/default']];

$this->params['breadcrumbs'][] = $this->title;

?>


<?php
foreach ($modal as $widget) {
    if ($widget['id'] == 6) {
        $option = json_decode($widget['options'], true);

        ?>
        <div class="col-md-12">
            <div class="card card-round">
                <div class="card-header">
                    <div class="card-title">
                        Banner Widget
                        <div class="pull-right">
                            <input type="checkbox" class="display-home-page"
                                   data-display-url="<?= \yii\helpers\Url::toRoute("display/status") ?>"
                                   data-display-id="<?= $widget['id']; ?>" data-status="<?= $widget['status']; ?>"
                                   value="<?= $widget['status']; ?>"
                                   name="status" <?= ($widget['status'] == "enable") ? "checked" : ""; ?>
                                   data-toggle="toggle" data-onstyle="success" data-style="btn-round">
                            <i id="display_<?= $widget['id'] ?>"></i>
                        </div>
                    </div>
                </div>
                <div class="card-body ">

                    <div class="row">
                        <div class="col-12">
                            <div align="center"
                                 img-url="<?= Yii::$app->urlManagerFrontend->baseUrl . '/images/banner/' ?>"
                                 id="bannerFormImage"
                                 style="padding: 80px 10px;color: #fff;background-image: url(<?= Yii::$app->urlManagerFrontend->baseUrl . '/images/banner/' . $widget['template'] ?>);background-position: center center">
                                <h1 id="bannerFormTitle" style="color: <?= $option['title_color']; ?>">
                                    <?= $option['title']; ?>
                                </h1>
                                <h3 id="bannerFormTagLine" style="color: <?= $option['tag_color']; ?>">
                                    <?= $option['tag line']; ?>
                                </h3>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <input type="hidden" id="BannerUpdateUrl" value="<?= \yii\helpers\Url::toRoute('edit/banner') ?>">
                    <div class="row">
                        <div class="col-md-6">
                            <label><strong>Banner Title & Color</strong></label>

                            <input type="text" style="text-indent: 28px;" value="<?= $option['title']; ?>"
                                   id="BannerInputTitle" class="form-control">
                            <input type="color" style="top: 37px; !important;" banner-title-color=""
                                   value="<?= $option['title_color']; ?>" name="titleColor" id="BannerTitleColor"
                                   class="colorInput">

                        </div>

                        <div class="col-md-6">
                            <label><strong>Banner Sub Title & Color</strong></label>

                            <input type="text" style="text-indent: 28px;" value="<?= $option['tag line']; ?>"
                                   id="BannerInputTagline" class="form-control">
                            <input type="color" style="top: 37px; !important;" banner-tag-color=""
                                   value="<?= $option['tag_color']; ?>" name="tagColor" id="BannerTagColor"
                                   class="colorInput">

                        </div>

                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-4">
                            <label><strong>Banner Height</strong></label>
                            <input type="text" class="form-control" id="BannerInputHeight"
                                   value="<?= $option['height']; ?>" name="bannerheight">
                            <small> use px after height, example: 350px</small>
                        </div>
                        <div class="col-md-4">
                            <label><strong>Banner Position</strong></label>

                            <select class="form-control" name="BannerPositions" id="BannerInputPositions">
                                <option value="top">Top</option>
                                <option value="center">Center</option>
                                <option value="bottom">bottom</option>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <label><strong>Banner Background</strong></label>

                            <select class="form-control" name="bg" id="BannerInputImage">
                                <option value="bg0.jpg" <?= ($widget['image'] == 'bg0.jpg') ? 'selected' : ''; ?>>Image
                                    Zero
                                </option>
                                <option value="bg1.jpg" <?= ($widget['image'] == 'bg1.jpg') ? 'selected' : ''; ?>>Image
                                    One
                                </option>
                                <option value="bg2.jpg" <?= ($widget['image'] == 'bg2.jpg') ? 'selected' : ''; ?>>Image
                                    Two
                                </option>
                                <option value="bg3.jpg" <?= ($widget['image'] == 'bg3.jpg') ? 'selected' : ''; ?>>Image
                                    Three
                                </option>
                                <option value="bg4.jpg" <?= ($widget['image'] == 'bg4.jpg') ? 'selected' : ''; ?>>Image
                                    Four
                                </option>
                                <option value="bg5.jpg" <?= ($widget['image'] == 'bg5.jpg') ? 'selected' : ''; ?>>Image
                                    Five
                                </option>
                                <option value="bg6.jpg" <?= ($widget['image'] == 'bg6.jpg') ? 'selected' : ''; ?>>Image
                                    Six
                                </option>
                                <option value="bg7.jpg" <?= ($widget['image'] == 'bg7.jpg') ? 'selected' : ''; ?>>Image
                                    seven
                                </option>
                                <option value="bg8.jpg" <?= ($widget['image'] == 'bg8.jpg') ? 'selected' : ''; ?>>Image
                                    eight
                                </option>
                                <option value="bg9.jpg" <?= ($widget['image'] == 'bg9.jpg') ? 'selected' : ''; ?>>Image
                                    nine
                                </option>
                                <option value="bg10.jpg" <?= ($widget['image'] == 'bg10.jpg') ? 'selected' : ''; ?>>
                                    Image 10
                                </option>
                                <option value="bg11.jpg" <?= ($widget['image'] == 'bg11.jpg') ? 'selected' : ''; ?>>
                                    Image 11
                                </option>
                                <option value="bg12.jpg" <?= ($widget['image'] == 'bg12.jpg') ? 'selected' : ''; ?>>
                                    Image 12
                                </option>
                                <option value="bg13.jpg" <?= ($widget['image'] == 'bg13.jpg') ? 'selected' : ''; ?>>
                                    Image 13
                                </option>
                                <option value="bg14.jpg" <?= ($widget['image'] == 'bg14.jpg') ? 'selected' : ''; ?>>
                                    Image 14
                                </option>
                                <option value="bg15.jpg" <?= ($widget['image'] == 'bg15.jpg') ? 'selected' : ''; ?>>
                                    Image 15
                                </option>
                                <option value="bg16.jpg" <?= ($widget['image'] == 'bg16.jpg') ? 'selected' : ''; ?>>
                                    Image 16
                                </option>
                                <option value="bg17.jpg" <?= ($widget['image'] == 'bg17.jpg') ? 'selected' : ''; ?>>
                                    Image 17
                                </option>
                                <option value="bg18.jpg" <?= ($widget['image'] == 'bg18.jpg') ? 'selected' : ''; ?>>
                                    Image 18
                                </option>
                                <option value="bg19.jpg" <?= ($widget['image'] == 'bg19.jpg') ? 'selected' : ''; ?>>
                                    Image 19
                                </option>
                                <option value="bg20.jpg" <?= ($widget['image'] == 'bg20.jpg') ? 'selected' : ''; ?>>
                                    Image 20
                                </option>


                            </select>

                        </div>
                    </div>
                    <br>
                    <div class="row">

                        <div class="col-md-6">
                            <div style="padding: 1px;">

                            </div>

                        </div>
                        <div class="col-md-6 ">
                            <button type="button" id="BannerSave" class="btn-success btn btn-md">
                                <i class="fa fa-upload"></i> <b>UPDATE</b>
                            </button>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
        continue;
    }
    ?>
    <div class="col-md-12">
        <div class="card card-round">
            <div class="card-body ">
                <div class="row">
                    <div class="col-md-4">
                        <img class="img-thumbnail img-responsive"
                             src="<?= Yii::getAlias('@web') ?>/images/widgets/<?= $widget['image'] ?>"
                             alt="Card image cap">
                    </div>
                    <div class="col-md-8">
                        <table class="table">

                            <tr>
                                <th>
                                    <?= $widget['name'] ?>
                                </th>
                                <th>
                                    <input type="checkbox" class="display-home-page"
                                           data-display-url="<?= \yii\helpers\Url::toRoute("display/status") ?>"
                                           data-display-id="<?= $widget['id']; ?>"
                                           data-status="<?= $widget['status']; ?>" value="<?= $widget['status']; ?>"
                                           name="status" <?= ($widget['status'] == "enable") ? "checked" : ""; ?>
                                           data-toggle="toggle" data-onstyle="success" data-style="btn-round">
                                    <i id="display_<?= $widget['id'] ?>"></i>
                                </th>
                            </tr>
                            <tr>
                                <td>Page :</td>
                                <td><?= $widget['page'] ?></td>
                            </tr>
                            <tr>
                                <td>Type :</td>
                                <td><?= $widget['type'] ?></td>
                            </tr>
                            <tr>
                                <td>Template :</td>
                                <td><?= $widget['template'] ?></td>
                            </tr>
                            <tr>
                                <td>Priority :</td>
                                <td><?= $widget['priority'] ?></td>
                            </tr>


                        </table>

                    </div>

                </div>


            </div>
        </div>
    </div>
    <?php

}
?>