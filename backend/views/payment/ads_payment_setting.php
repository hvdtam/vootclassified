<?php
/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;

$this->title = 'Premium Plan';

$uid = Yii::$app->user->id;
$usrInfo = \common\models\User::findOne($uid);
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="col-md-12">
    <div class="card">
        <div class="card-header">
            <div class="d-flex align-items-center">
                <h4 class="card-title">
                    <i class="flaticon-coins"></i> <?= Html::encode($this->title) ?>
                </h4>
            </div>
        </div>
        <div class="card-body">
            <?php
            if (!$all) {
                echo "<p> No premium feature found </p>";
            }
            ?>
            <table class="table table-bordered">
                <tr>
                    <th>Plan Name</th>
                    <th>Plan Price</th>
                    <th>Visible on Home Page</th>
                    <th>Duration</th>
                    <th>Display in</th>
                    <th>Action</th>

                </tr>
                <tbody>

                <?php

                foreach ($all as $list) {
                    ?>

                    <tr>
                        <td>
                            <b class="text-info">  <?= $list->name; ?> </b>
                        </td>
                        <td>

                            <span class="label label-success">$<?= $list->price; ?></span>
                        </td>
                        <td>
                            <span class="label label-info"><?= $list->home_page; ?></span>

                        </td>
                        <td>
                            <span class="label label-info"><?= $list->duration; ?> Month</span>

                        </td>
                        <td>
                            <span class="label label-info"><?= $list->display_in; ?></span>

                        </td>
                        <td class="td-actions text-right">
                            <a href="<?= \yii\helpers\Url::toRoute('payment/ads-edit/' . $list->id) ?>" rel="tooltip"
                               title="Edit Task" class="btn btn-info btn-simple btn-xs">
                                <i class="fa fa-edit"></i>
                            </a>
                        </td>
                    </tr>

                    <?php
                }
                ?>


                </tbody>
            </table>
        </div>
    </div>
</div>


