<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\bootstrap\ActiveForm;
use kartik\editable\Editable;
use \yii\helpers\Url;

$this->title = "Custom Field";
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="col-md-12">
    <div class="card">
        <div class="card-header">
            <div class="d-flex align-items-center">
                <h4 class="card-title">
                    Custom Field
                </h4>
                <button class="btn btn-primary btn-round ml-auto" data-toggle="modal" data-target="#addRowModal">
                    <i class="la la-plus"></i>
                    Add Custom Field
                </button>
            </div>
        </div>
        <div class="card-body">


            <?php
            $gridColumns = [
                // the buy_amount column configuration
                ['class' => 'kartik\grid\SerialColumn'],


                [
                    'class' => 'kartik\grid\EditableColumn',
                    'attribute' => 'custom_catid',
                    'value' => function ($model) {
                        return \common\models\Category::findNameById($model->custom_catid);
                    },
                    'filter' => $dataCat,
                    'filterType' => GridView::FILTER_SELECT2,
                    'filterWidgetOptions' =>
                        [
                            'pluginOptions' => ['allowClear' => true],
                        ],
                    'filterInputOptions' => ['placeholder' => 'CATEGORY'],
                    'editableOptions' => [
                        'header' => 'Edit Main Category',
                        'inputType' => Editable::INPUT_DROPDOWN_LIST,
                        'data' => $dataCat,
                    ],
                    'pageSummary' => true,

                ],
                [
                    'class' => 'kartik\grid\EditableColumn',
                    'attribute' => 'custom_subcatid',
                    'value' => function ($model) {
                        return \common\models\SubCategory::findName($model->custom_subcatid);
                    },
                    'filter' => $dataSubCat,
                    'filterType' => GridView::FILTER_SELECT2,
                    'filterWidgetOptions' =>
                        [
                            'pluginOptions' => ['allowClear' => true],
                        ],
                    'filterInputOptions' => ['placeholder' => 'SUB CATEGORY'],
                    'editableOptions' => [
                        'header' => 'Edit SubCategory',
                        'inputType' => Editable::INPUT_DROPDOWN_LIST,
                        'data' => $dataSubCat,
                    ],
                    'pageSummary' => true
                ],
                [
                    'class' => 'kartik\grid\EditableColumn',
                    'attribute' => 'custom_type',

                    'filter' => ['textinput' => 'text', 'textarea' => 'textarea', 'checkbox' => 'checkbox', 'radio' => 'radio'],
                    'filterType' => GridView::FILTER_SELECT2,
                    'filterWidgetOptions' =>
                        [
                            'pluginOptions' => ['allowClear' => true],
                        ],
                    'filterInputOptions' => ['placeholder' => 'INPUT TYPE'],
                    'editableOptions' => [
                        'header' => 'Change Type of input',
                        'inputType' => Editable::INPUT_DROPDOWN_LIST,
                        'data' => ['textinput' => 'text', 'textarea' => 'textarea', 'checkbox' => 'checkbox', 'radio' => 'radio'],
                    ],
                    'pageSummary' => true
                ],
                [
                    'class' => 'kartik\grid\EditableColumn',
                    'attribute' => 'custom_title',
                    'editableOptions' => [
                        'header' => 'Edit Title For Field',
                        'inputType' => \kartik\editable\Editable::INPUT_TEXT,
                    ],
                    'pageSummary' => true
                ],
                [
                    'class' => 'kartik\grid\EditableColumn',
                    'attribute' => 'custom_options',
                    'editableOptions' => [
                        'header' => 'Edit custom options For Field',
                        'placement' => \kartik\popover\PopoverX::ALIGN_BOTTOM,
                        'inputType' => \kartik\editable\Editable::INPUT_TEXT,
                    ],
                    'pageSummary' => true
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{leadDelete}',
                    'header' => 'Actions',
                    'headerOptions' => ['style' => 'color:#337ab7'],
                    'buttons' => [

                        'leadDelete' => function ($url, $model, $key) {
                            $url = 'javascript:void()';//Url::to(['controller/lead-delete', 'id' => $model->id]);
                            return Html::a('<span class="flaticon-interface-5"></span>', $url, [
                                'title' => 'delete',
                                'class' => 'record-delete',
                                //'data-confirm' => 'Are you sure you want to delete this item?',//this for pre action confirmation popup box.
                                //'data-method'  => 'post',//this for data method [get/post]
                                'record-delete-msg' => 'Record Deleted',
                                'record-delete-status' => 'active',
                                'record-delete-child' => false,
                                'record-delete-url' => Url::toRoute('delete/custom/' . $model->custom_id),
                                'record-data-key' => $key,


                            ]);
                        },

                    ]
                ]


            ];
            ?>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'pjax' => true,
                'pjaxSettings' => [
                    'neverTimeout' => true,
                    'beforeGrid' => 'My fancy content before.',
                    'afterGrid' => 'My fancy content after.',
                ],

                'responsive' => true,
                'floatHeader' => false,
                'containerOptions' => ['style' => 'overflow: auto'], // only set when $responsive = false
                'headerRowOptions' => ['class' => 'kartik-sheet-style'],
                'filterRowOptions' => ['class' => 'kartik-sheet-style'],
//                'toolbar' =>  [
//                    ['content' =>
//                        Html::button('<i class="glyphicon glyphicon-plus"></i>', ['type' => 'button', 'title' => 'Add Book', 'class' => 'btn btn-success', 'onclick' => 'alert("This will launch the book creation form.\n\nDisabled for this demo!");']) . ' '.
//                        Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['grid-demo'], ['data-pjax' => 0, 'class' => 'btn btn-default', 'title' => 'Reset Grid'])
//                    ],
//                    '{export}',
//                    '{toggleData}',
//                ],
                // set export properties
                'export' => [
                    'fontAwesome' => true
                ],

                'columns' => $gridColumns,


            ]) ?>
        </div>
    </div>
</div>
<!--            Model start here-->
<div class="modal fade" id="addRowModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header no-bd">
                <h5 class="modal-title">
					<span class="fw-mediumbold">
						Add New
                    </span>
                    <span class="fw-light">
                        City
                    </span>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?php $form = ActiveForm::begin([
                'action' => ['edit/add-custom'],
                //'method' => 'get',
                'options' => ['id' => 'custom_form', 'class' => 'master-form']
            ]); ?>
            <div class="modal-body">
                <p class="small">Create a new row using this form, make sure you fill them all</p>
                <div class="row">


                    <div class="col-sm-12">
                        <?php
                        $model = $searchModel;
                        ?>
                        <?= $form->field($model, 'custom_catid')->dropDownList(\yii\helpers\ArrayHelper::map(common\models\Category::find()->all(), 'id', 'name'),
                            [
                                'prompt' => 'Choos Category',
                                'onchange' => '
                        $.get("cat?id=' . '"+$(this).val(),function(data)
                        {
                        var cat = $("#adsform-category").val();
                        if(cat == "Jobs")
                        {
                        $("#spFor").hide();
                        }
                        else
                        {
                        $("#spFor").show();
                        }
                            $("select#customfields-custom_subcatid").html(data);


                        });',

                            ]) ?>

                        <?= $form->field($model, 'custom_subcatid')->dropDownList(
                            [
                                'prompt' => 'Select sub category',

                            ]) ?>
                        <?php // $form->field($model, 'custom_subcatid')->textInput() ?>

                        <?= $form->field($model, 'custom_name')->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model, 'custom_title')->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model, 'custom_type')
                            ->dropDownList(
                                array(
                                    'textinput' => 'text input',
                                    'select' => 'select',
                                    'textarea' => 'text area',
                                    'checkbox' => 'checkbox',
                                    'radio' => 'radio'
                                )
                            )
                        ?>

                        <?= $form->field($model, 'custom_options')->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model, 'custom_min')->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model, 'custom_max')->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model, 'custom_required')->dropDownList(
                            array(
                                '0' => 'No',
                                '1' => 'Yes',

                            )
                        ) ?>


                        <?= $form->field($model, 'custom_default')->textInput(['maxlength' => true]) ?>

                    </div>


                </div>
            </div>
            <div class="modal-footer no-bd">
                <?php echo Html::submitButton('Add', ['class' => 'btn btn-primary', 'id' => 'submit_id']) ?>
                <?php echo Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
                <?php echo Html::button('Close', ['class' => 'btn btn-danger', 'data-dismiss' => 'modal']) ?>

            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
<!--            model add row closed-->







