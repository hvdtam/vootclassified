<?php
/* @var $this yii\web\View */

/* @var $form yii\bootstrap\ActiveForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\file\FileInput;
use \yii\helpers\Url;

$this->title = 'Site Settings';
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="col-md-12">
    <div class="card card-transparent">
        <div class="card-header">
            <h4 class="card-title">Website General Settings (Logo and Others)</h4>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-4 col-md-3">
                    <div class="nav flex-column nav-pills nav-secondary nav-pills-no-bd">
                        <a href="" class="nav-link active">Site Settings</a>
                        <a href="<?= Url::toRoute('settings/default') ?>" class="nav-link ">Default Settings</a>
                        <a href="<?= Url::toRoute('display/home-page') ?>" class="nav-link">Home Page Settings</a>
                        <a href="<?= Url::toRoute('widgets/index') ?>" class="nav-link">Widget Settings</a>
                        <a href="<?= Url::toRoute('settings/admin') ?>" class="nav-link">Admin Settings</a>
                        <a class="nav-link" href="<?= Url::toRoute('settings/api') ?>">Api Settings</a>
                        <a class="nav-link" href="<?= Url::toRoute('settings/ads') ?>">Ads Settings</a>

                    </div>
                </div>
                <div class="col-7 col-md-9">
                    <div class="card">
                        <div class="card-header">
                            <div class="d-flex align-items-center">
                                <h4 class="card-title">
                                    <i class="flaticon-settings"></i> <?= Html::encode($this->title) ?>
                                </h4>
                            </div>
                        </div>
                        <div class="card-body">
                            <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>
                            <?= $form->field($model, 'site_name') ?>
                            <?= $form->field($model, 'site_title') ?>
                            <?php
                            echo $form->field($model, 'logo', [
                                'template' => '<div class="form-group row">{label}<div class="col-sm-8">{input}{error}{hint}</div></div>'])->widget(FileInput::classname(), [
                                'options' => ['accept' => 'image/*'],
                                'pluginOptions' => [
                                    'maxFileCount' => 1,
                                    'overwriteInitial' => false,
                                    'theme' => 'fa',
                                    'initialPreviewAsData' => 'true',
                                    'cancelLabel' => '',
                                    'cancelClass' => 'hidden',
                                    'browseLabel' => Yii::t('app', 'Logo'),
                                    'browseClass' => 'btn btn-primary',
                                    'browseIcon' => '<i class="fa fa-image"></i>',
                                    'removeLabel' => '',
                                    'removeIcon' => '<i class="fa fa-trash"></i>',
                                    'uploadClass' => 'd-none',
                                    'uploadLabel' => '',
                                    'uploadIcon' => '<i class="fa fa-upload"></i>',
                                    'previewFileType' => 'image',
                                    'allowedFileExtensions' => ['jpg', 'jpeg', 'png'],
                                    'previewClass' => 'bg-light'
                                ]

                            ])->label(Yii::t('app', 'Logo'));
                            ?>


                            <div class="row" style="padding: 20px;">
                                <div class="col-6">
                                    <?= $form->field($model, 'logo_width')->textInput(['placeholder' => 'eg, 50px'])->hint('format : 50px,50%,50vh etc. define (px) at the end') ?>
                                </div>

                                <div class="col-6">
                                    <?= $form->field($model, 'logo_height')->textInput(['placeholder' => 'eg, 50px'])->hint('format : 50px,50%,50vh etc. define (px) at the end') ?>
                                </div>

                                <div class="col-6">
                                    <?= $form->field($model, 'logo_padding')->textInput(['placeholder' => 'eg, 5px or 5px 5px'])->hint('example : 5px, or 5px 5px, or 5px 3px 2px 5px ') ?>
                                </div>

                                <div class="col-6">
                                    <?= $form->field($model, 'logo_margin')->textInput(['placeholder' => 'eg, 5px or 5px 5px'])->hint('example : 5px, or 5px 5px, or 5px 3px 2px 5px ') ?>
                                </div>
                            </div>

                            <div>
                                <h5>Current:</h5>
                                <img src="<?= Yii::$app->urlManagerFrontend->baseUrl . '/images/site/logo/' . $model->logo ?>"
                                     class="img-thumbnail">
                                <hr>
                            </div>
                            <?php
                            echo $form->field($model, 'fav_icon', [
                                'template' => '<div class="form-group row">{label}<div class="col-sm-8">{input}{error}{hint}</div></div>'])->widget(FileInput::classname(), [
                                'options' => ['accept' => 'image/*'],
                                'pluginOptions' => [
                                    'maxFileCount' => 1,
                                    'overwriteInitial' => false,
                                    'theme' => 'fa',
                                    'initialPreviewAsData' => 'true',
                                    'cancelLabel' => '',
                                    'cancelClass' => 'hidden',
                                    'browseLabel' => Yii::t('app', 'Fav Icon'),
                                    'browseClass' => 'btn btn-primary',
                                    'browseIcon' => '<i class="fa fa-image"></i>',
                                    'removeLabel' => '',
                                    'removeIcon' => '<i class="fa fa-trash"></i>',
                                    'uploadClass' => 'd-none',
                                    'uploadLabel' => '',
                                    'uploadIcon' => '<i class="fa fa-upload"></i>',
                                    'previewFileType' => 'image',
                                    'allowedFileExtensions' => ['jpg', 'jpeg', 'png'],
                                    'previewClass' => 'bg-light'
                                ]

                            ])->label(Yii::t('app', 'Fav Icon'));
                            ?>
                            <div class="form-group">
                                <?= Html::submitButton('Submit', ['class' => 'btn btn-primary', 'name' => 'site-settings-button']) ?>
                            </div>
                            <?php ActiveForm::end(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
















