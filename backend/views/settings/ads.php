<?php

/* @var $this yii\web\View */

/* @var $form yii\bootstrap\ActiveForm */

use kartik\grid\GridView;

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\editable\Editable;
use \yii\helpers\Url;

$this->title = 'Ads Settings';
$this->params['breadcrumbs'][] = ['label' => 'Site Settings', 'url' => ['settings/site']];

$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    .form-check, .form-group {

        margin-bottom: 0;
        padding: 0px 5px !important;

    }
</style>
<div class="col-md-12">
    <div class="card card-transparent">
        <div class="card-header">
            <h4 class="card-title">Ads Settings</h4>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-4 col-md-3">
                    <div class="nav flex-column nav-pills nav-secondary nav-pills-no-bd">
                        <a href="<?= Url::toRoute('settings/site') ?>" class="nav-link">Site Settings</a>
                        <a class="nav-link" href="<?= Url::toRoute('settings/default') ?>">Default Settings</a>
                        <a href="<?= Url::toRoute('display/home-page') ?>" class="nav-link">Home Page Settings</a>
                        <a href="<?= Url::toRoute('widgets/index') ?>" class="nav-link">Widget Settings</a>
                        <a href="<?= Url::toRoute('settings/admin') ?>" class="nav-link">Admin Settings</a>
                        <a class="nav-link" href="<?= Url::toRoute('settings/api') ?>">Api Settings</a>
                        <a class="nav-link active" href="<?= Url::toRoute('settings/ads') ?>">Ads Settings</a>

                    </div>
                </div>
                <div class="col-7 col-md-9">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Ads Settings</h4>
                        </div>

                        <?php $form = ActiveForm::begin(
                            [
                                'layout' => 'horizontal',
                                // 'action' => ['edit/edit-ads?id='.$modal['id']],

                                'fieldConfig' =>
                                    [
                                        'horizontalCssClasses' =>
                                            [
                                                'label' => 'col-md-3',
                                                'offset' => '',
                                                'wrapper' => 'col',
                                                'error' => 'col-md-5',
                                                'hint' => 'col-md-8  col-md-push-3 imgHintS',
                                            ],
                                    ],
                                'options' => ['id' => 'edit_ads_form', 'enctype' => 'multipart/form-data', 'class' => 'master-form22']
                            ]);
                        ?>
                        <div class="card-body">
                            <br>
                            <?php
                            echo $form->field($modal, 'ads_approval')
                                ->dropDownList(array('yes' => 'Auto', 'pending' => 'Manual'));
                            echo $form->field($modal, 'number_of_photo');
                            echo $form->field($modal, 'watermark_status')->dropDownList(array("enable" => "Apply Every Time", "disable" => "Not Apply"))->label('Watermark Apply');
                            ?>
                            <br>
                            <div class="row">

                                <div class="col-md-3">
                                    <div class="card">
                                        <div class="card-image">
                                            <img class="card-image img-responsive"
                                                 src="<?= Yii::$app->urlManagerFrontend->baseUrl . '/images/site/watermark/' . $modal['watermark_image']; ?>"
                                                 alt="WaterMark">

                                        </div>

                                        <div class="card-footer">
                                            <strong>Current Watermark</strong>

                                        </div>
                                    </div>

                                </div>
                                <div class="col-md-9">

                                    <?php
                                    echo \kartik\file\FileInput::widget([
                                        'model' => $modal,
                                        'attribute' => 'watermark_image',
                                        'id' => 'watermark_image',
                                        'options' => [
                                            'multiple' => true
                                        ],
                                        'pluginOptions' => [
                                            'uploadUrl' => WATERMARK,
                                            'uploadExtraData' => [
                                                'album_id' => 21,
                                                'cat_id' => 'logo'
                                            ],
                                            'maxFileCount' => 1,
                                            'overwriteInitial' => false,
                                            'theme' => 'fa',
                                            'initialPreviewAsData' => 'true',
                                            'cancelLabel' => '',
                                            'cancelClass' => 'hidden',
                                            'browseLabel' => 'Upload Watermark Image',
                                            'browseClass' => 'btn btn-success',
                                            'browseIcon' => '<i class="fa fa-image"></i>',
                                            'removeLabel' => '',
                                            'removeIcon' => '<i class="fa fa-trash"></i>',
                                            'uploadLabel' => '',
                                            'uploadIcon' => '<i class="fa fa-upload"></i>',
                                            'previewFileType' => 'image',
                                            'allowedFileExtensions' => ['jpg', 'jpeg', 'png'],
                                            'previewClass' => 'bg-light'
                                        ]
                                    ]);
                                    ?>
                                </div>
                            </div>
                            <br>
                            <?php
                            echo $form->field($modal, 'login_required')
                                ->dropDownList(array('1' => 'yes', '0' => 'Never'));
                            echo $form->field($modal, 'create_guest_account')
                                ->dropDownList(array('1' => 'yes, and send the Login Detail by mail', '0' => 'No, Let them post ads freely.'));

                            echo $form->field($modal, 'number_of_tags');

                            ?>
                        </div>
                        <div class="card-footer no-bd">
                            <?php echo Html::submitButton('UPDATE', ['class' => 'btn btn-primary', 'id' => 'submit_id']) ?>
                            <?php echo Html::resetButton('RESET', ['class' => 'btn btn-default']) ?>

                        </div>
                        <?php ActiveForm::end(); ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



