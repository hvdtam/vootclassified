<?php

/* @var $this yii\web\View */

/* @var $form yii\bootstrap\ActiveForm */

use kartik\grid\GridView;

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\editable\Editable;
use \yii\helpers\Url;

$this->title = 'Site Settings';
$this->params['breadcrumbs'][] = ['label' => 'Site Settings', 'url' => ['settings/site']];

$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    .form-check, .form-group {

        margin-bottom: 0;
        padding: 0px 5px !important;

    }
</style>
<div class="col-md-12">
    <div class="card card-transparent">
        <div class="card-header">
            <h4 class="card-title">Default Settings</h4>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-4 col-md-3">
                    <div class="nav flex-column nav-pills nav-secondary nav-pills-no-bd">
                        <a href="<?= Url::toRoute('settings/site') ?>" class="nav-link">Site Settings</a>
                        <a class="nav-link active">Default Settings</a>
                        <a href="<?= Url::toRoute('display/home-page') ?>" class="nav-link">Home Page Settings</a>
                        <a href="<?= Url::toRoute('widgets/index') ?>" class="nav-link">Widget Settings</a>
                        <a href="<?= Url::toRoute('settings/admin') ?>" class="nav-link">Admin Settings</a>
                        <a class="nav-link" href="<?= Url::toRoute('settings/api') ?>">Api Settings</a>
                        <a class="nav-link" href="<?= Url::toRoute('settings/ads') ?>">Ads Settings</a>

                    </div>
                </div>
                <div class="col-7 col-md-9">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Default Settings</h4>
                        </div>
                        <?php $form = ActiveForm::begin(
                            [
                                'layout' => 'horizontal',
                                'fieldConfig' =>
                                    [
                                        'horizontalCssClasses' =>
                                            [
                                                'label' => 'col-md-3',
                                                'offset' => '',
                                                'wrapper' => 'col',
                                                'error' => 'col-md-5',
                                                'hint' => 'col-md-8  col-md-push-3 imgHintS',
                                            ],
                                    ],
                                'options' => ['enctype' => 'multipart/form-data']
                            ]);
                        $country = common\models\Countries::find()->all();
                        ?>
                        <div class="card-body">
                            <br>
                            <?php
                            echo $form->field($modelData, 'codecanyon_username');
                            echo $form->field($modelData, 'purchase_code');

                            echo $form->field($modelData, 'themes')->dropDownList(
                                array(
                                    "style" => "Redis",
                                    "style_old" => "Classic",
                                    "style_one" => "Regular blue",
                                    "style_two" => "Awesome Blue",
                                    "style_three" => "Regular yellow",
                                    "style_four" => "Black red",
                                    "style_five" => "Theme Five",
                                    "style_six" => "Theme Six",
                                )
                            );
                            echo $form->
                            field($modelData, 'background')->
                            dropDownList(
                                array(
                                    "default" => "Default Gray Color",
                                    "bg1.jpg" => "bg1.jpg",
                                    "bg2.jpg" => "bg2.jpg",
                                    "bg3.jpg" => "bg3.jpg",
                                    "bg4.jpg" => "bg4.jpg",
                                    "bg5.jpg" => "bg5.jpg",
                                    "bg6.jpg" => "bg6.jpg",
                                    "bg7.jpg" => "bg7.jpg",
                                    "bg8.jpg" => "bg8.jpg",
                                    "bg9.jpg" => "bg9.jpg")
                            )->label('Background Image');

                            echo $form->field($modelData, 'lazy_load')->dropDownList(
                                array(
                                    "lazy" => "On",
                                    "off" => "Off",
                                )
                            )->hint('Lazy Load improve website performance, turn it on.');
                            echo $form->field($modelData, 'country')
                                ->dropDownList(\yii\helpers\ArrayHelper::map($country, 'name', 'name'),
                                    [
                                        'prompt' => 'Select Country',
                                        'onchange' => '
                                $.get("country?id=' . '"+$(this).val(),function(data)
                                {
                                 $("select#defaultsetting-state").html(data);
                                 });',
                                    ]);
                            echo $form->field($modelData, 'state')
                                ->dropDownList([$modelData['state'] => $modelData['state']],
                                    [
                                        'prompt' => 'Select State',
                                        'onchange' => '$.get("state?id=' . '"+$(this).val(),function(data)
                                    {
                                     $("select#defaultsetting-city").html(data);
                                     });',
                                    ]);
                            echo $form->field($modelData, 'city')->dropDownList([$modelData['city'] => $modelData['city']],
                                [
                                    'prompt' => 'Select City',

                                ]);
                            echo $form->field($modelData, 'flag_icon')->dropDownList(\yii\helpers\ArrayHelper::map($country, 'sortname', 'sortname'),
                                [
                                    'prompt' => 'Select Flag Icon',
                                ]);
                            echo $form->field($modelData, 'currency')->dropDownList(\yii\helpers\ArrayHelper::map(\common\models\Currencies::find()->all(), 'symbol', 'currency', 'country'),
                                [
                                    'prompt' => 'Select currency',
                                ]);

                            ?>
                        </div>
                        <div class="card-footer no-bd">
                            <?php echo Html::submitButton('UPDATE', ['class' => 'btn btn-primary', 'id' => 'submit_id']) ?>
                            <?php echo Html::resetButton('RESET', ['class' => 'btn btn-default']) ?>

                        </div>
                        <?php ActiveForm::end(); ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



