<?php

/* @var $this \yii\web\View */

/* @var $content string */

use backend\assets\ThemeAssets;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;
use common\models\AdminNotification;

ThemeAssets::register($this);
backend\assets\TopAsset::register($this);
$siteSettings = \common\models\SiteSettings::find()->one();
// $this->registerJsFile('@web/themes/assets/js/core/jquery.3.2.1.min.js');
// $this->registerJsFile('@web/themes/assets/js/core/bootstrap.min.js');
$admin = \common\models\Admin::find()->one();
$notimodal = AdminNotification::latest();
$NotificationCount = ($notimodal['count']) ? $notimodal['count'] : '';;
$NotificationModal = $notimodal['modal'];
$NotificationTrue = ($notimodal['modal']) ? true : false;

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport'/>
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?= $this->head() ?>

    <script>
        WebFont.load({
            google: {"families": ["Montserrat:100,200,300,400,500,600,700,800,900"]},
            custom: {
                "families": ["Flaticon", "LineAwesome"],
                urls: ['<?= Yii::getAlias('@web') ?>/themes/assets/css/fonts.css']
            },
            active: function () {
                sessionStorage.fonts = true;
            }
        });
    </script>
    <!-- Fonts and icons -->

</head>
<body>

<body class="login">
<?php $this->beginBody() ?>

<div class="wrapper wrapper-login">
    <?= Alert::widget() ?>
    <?= $content ?>

</div>


<?php $this->endBody() ?>

<!-- jQuery 2.1.4 -->
<script src="<?= Yii::getAlias('@web') ?>/themes/assets/js/core/jquery.3.2.1.min.js"></script>
<!-- Bootstrap 3.3.5 -->
<script src="<?= Yii::getAlias('@web') ?>/themes/assets/js/core/bootstrap.min.js"></script>
</body>
</html>
<?php $this->endPage() ?>
