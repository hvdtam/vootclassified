<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\SmsTwilio */
/* @var $form yii\widgets\ActiveForm */
?>


<div class="card mt-3">
    <div class="card-header">
        <h6 class="title">
            Sms twilio Settings
        </h6>
    </div>
    <div class="card-body">

        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'sid')->textInput(['maxlength' => true]); ?>

        <?= $form->field($model, 'msg_from')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'token')->textarea(['rows' => 6]) ?>

        <div class="row">
            <div class="col-lg-6">
                <?= $form->field($model, 'verification')->dropDownList(['off' => 'Off', 'on' => 'On'], ['prompt' => 'Select']) ?>
            </div>
            <div class="col-lg-6">
                <?= $form->field($model, 'otp_length')->textInput(['maxlength' => '10']) ?>
            </div>

        </div>

        <?= $form->field($model, 'status')->dropDownList(['0' => 'disable', '1' => 'active'], ['prompt' => 'status']) ?>

        <p>
            Get a Twilio Account <a href="https://www.twilio.com/">here</a>.
        </p>
        <div class="form-group">
            <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>

</div>