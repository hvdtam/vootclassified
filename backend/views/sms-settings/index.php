<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\SmsTwilio */

$this->title = 'Sms Settings Twilio';
$this->params['breadcrumbs'][] = ['label' => 'Sms Twilios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="col-8">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
