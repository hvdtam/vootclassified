<?php

/* @var $this yii\web\View */

/* @var $form yii\bootstrap\ActiveForm */

use kartik\grid\GridView;

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\editable\Editable;
use \yii\helpers\Url;

$this->title = 'Payment Gateway Settings';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="col-md-12">
    <div class="card">
        <div class="card-header">
            <div class="d-flex align-items-center">
                <h4 class="card-title">
                    Listed Payment Method
                </h4>

            </div>
        </div>
        <div class="card-body">


            <?php
            $gridColumns = [
                // the buy_amount column configuration
                ['class' => 'kartik\grid\SerialColumn'],

                [
                    'class' => 'kartik\grid\DataColumn',
                    'attribute' => 'name',

                    'pageSummary' => true
                ],


                [
                    'class' => 'kartik\grid\EditableColumn',
                    'attribute' => 'config',
                    'label' => "Config Json",
                    'value' => function ($model) {
                        return substr($model->config, 0, 20);
                    },
                    'editableOptions' => [
                        'header' => 'Edit Config Settings',
                        'inputType' => \kartik\editable\Editable::INPUT_TEXTAREA,
                    ],
                    'pageSummary' => true
                ],
                [
                    //'class'=>'kartik\grid\DataColumn',
                    'attribute' => 'hint',
                    'format' => 'raw',
                    //'pageSummary'=>true,

                    //'contentOptions' => ['style' => 'color:#337ab7'],
                ],
                [
                    'class' => 'kartik\grid\EditableColumn',
                    'attribute' => 'status',
                    'pageSummary' => true,
                    'editableOptions' => [
                        'header' => 'Change Paypal Account Satus',
                        'inputType' => Editable::INPUT_DROPDOWN_LIST,
                        'placement' => \kartik\popover\PopoverX::ALIGN_LEFT,
                        'data' => ['enable' => 'enable', 'disable' => 'disable'], // any list of values
                    ],
                ],


            ];
            ?>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                // 'filterModel' => $searchModel,
                'pjax' => true,
                'pjaxSettings' => [
                    'neverTimeout' => true,
                    'beforeGrid' => 'My fancy content before.',
                    'afterGrid' => 'My fancy content after.',
                ],

                'responsive' => true,
                'floatHeader' => false,
                'containerOptions' => ['style' => 'overflow: auto'], // only set when $responsive = false
                'headerRowOptions' => ['class' => 'kartik-sheet-style'],
                'filterRowOptions' => ['class' => 'kartik-sheet-style'],
//                'toolbar' =>  [
//                    ['content' =>
//                        Html::button('<i class="glyphicon glyphicon-plus"></i>', ['type' => 'button', 'title' => 'Add Book', 'class' => 'btn btn-success', 'onclick' => 'alert("This will launch the book creation form.\n\nDisabled for this demo!");']) . ' '.
//                        Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['grid-demo'], ['data-pjax' => 0, 'class' => 'btn btn-default', 'title' => 'Reset Grid'])
//                    ],
//                    '{export}',
//                    '{toggleData}',
//                ],
                // set export properties
                'export' => [
                    'fontAwesome' => true
                ],

                'columns' => $gridColumns,


            ]) ?>
        </div>
    </div>
</div>





