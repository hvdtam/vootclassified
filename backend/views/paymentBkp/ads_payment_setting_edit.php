<?php
/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;

$this->title = 'Edit Premium Ads price';

$uid = Yii::$app->user->id;
$usrInfo = \common\models\User::findOne($uid);
$this->params['breadcrumbs'][] = ['label' => 'Premium Plan', 'url' => ['payment/ads']];

$this->params['breadcrumbs'][] = $this->title;

?>
<div class="col-md-6 ml-auto mr-auto">
    <div class="card">
        <div class="card-header">
            <div class="d-flex align-items-center">
                <h4 class="card-title">
                    <i class="flaticon-coins"></i> <?= Html::encode($this->title) ?>
                </h4>
            </div>
        </div>

        <div class="card-body">
            <?php $form = ActiveForm::begin([

                'options' => ['enctype' => 'multipart/form-data']
            ]) ?>

            <?= $form->field($new, 'name')->label("type of ads") ?>
            <?= $form->field($new, 'duration')->dropDownList(array(
                '1' => '1 month',
                '2' => '2 month',
                '3' => '3 month',
                '6' => '6 month',
                '8' => '8 month',
                '10' => '10 month',
                '12' => '12 month',
            ))->label("ad active duration in month") ?>
            <?= $form->field($new, 'display_in')->dropDownList(array(
                'city' => 'inside the city',
                'state' => 'inside the state',
                'country' => 'in the country',
            ))->label("ad active duration in month") ?>
            <?= $form->field($new, 'price')->label("Price of your premium ads") ?>

            <?= $form->field($new, 'home_page')->dropDownList([
                'no' => 'No',
                'yes' => 'Yes '
                ,])->label('ads display at home page?') ?>
            <br>
            <div class="clearfix"></div>

            <div align="center">
                <?= Html::submitButton('Submit', ['class' => 'btn btn-success', 'name' => 'ads-button']) ?>

            </div>

            <div class="clearfix"></div>


            <?php ActiveForm::end(); ?>
        </div>
    </div>

</div>




