<?php
if ($type == "pages_link") {
    ?>
    <div class="form-group field-footercontent-url required has-error">
        <label class="control-label col-md-3" for="footercontent-url">Url</label>
        <div class="col">
            <select id="footercontent-url" class="form-control" name="FooterContent[url]">
                <?= Pages::pagesOpValue() ?>
            </select>
            <p class="help-block help-block-error col-md-5">Url cannot be blank.</p>
        </div>

    </div>
    <?php
} elseif ($type == "customUrl") {
    ?>
    <div class="form-group field-footercontent-url required">
        <label class="control-label col-md-3" for="footercontent-url">Url</label>
        <div class="col">
            <input type="text" id="footercontent-url" class="form-control" name="FooterContent[url]"
                   aria-required="true" aria-invalid="true">
            <p class="help-block help-block-error col-md-5">Url cannot be blank.</p>
        </div>

    </div>
    <?php
} else {
    ?>
    <div class="form-group field-footercontent-url required">
        <label class="control-label col-md-3" for="footercontent-url">Url (controllers/action)</label>
        <div class="col">
            <input type="text" id="footercontent-url" placeholder="format controllers/action example site/login"
                   class="form-control" name="FooterContent[url]" aria-required="true" aria-invalid="true">
            <p class="help-block help-block-error col-md-5">Url cannot be blank.</p>
        </div>

    </div>
    <?php
}