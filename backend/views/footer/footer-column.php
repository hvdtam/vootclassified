<?php

/* @var $this yii\web\View */

/* @var $form yii\bootstrap\ActiveForm */

use kartik\grid\GridView;

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\editable\Editable;
use \yii\helpers\Url;

$this->title = 'Footer Column Settings';
$this->params['breadcrumbs'][] = ['label' => 'Footer ', 'url' => ['footer/index']];

$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    .form-check, .form-group {

        margin-bottom: 0;
        padding: 0px 5px !important;

    }
</style>
<div class="col-12">
    <div class="row">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <div class="d-flex align-items-center">
                        <h4 class="card-title">
                            Footer Column
                        </h4>

                    </div>
                </div>
                <div class="card-body">


                    <?php
                    $gridColumns = [
                        // the buy_amount column configuration
                        ['class' => 'kartik\grid\SerialColumn'],
                        [
                            'class' => 'kartik\grid\EditableColumn',
                            'attribute' => 'name',
                            'editableOptions' => [
                                'header' => 'Change Column name',
                                'inputType' => \kartik\editable\Editable::INPUT_TEXT,
                            ],
                            'pageSummary' => true
                        ],
                        [
                            'class' => 'kartik\grid\EditableColumn',
                            'attribute' => 'column_class',

                            'editableOptions' => [
                                'header' => 'Change Footer Column',
                                'inputType' => Editable::INPUT_DROPDOWN_LIST,
                                'data' => array(
                                    "col-lg-12 col-md-12 col-xs-6" => "col-12",
                                    "col-lg-6 col-md-6 col-xs-6" => "col-6",
                                    "col-lg-4 col-md-4 col-xs-12" => "col-4",
                                    "col-lg-3 col-md-3 col-xs-12" => "col-3",
                                    "col-lg-2 col-md-2 col-xs-12" => "col-2",

                                )
                            ],
                            'pageSummary' => true
                        ],

                        [
                            'class' => 'yii\grid\ActionColumn',
                            'template' => '{leadDelete}',
                            'header' => 'Actions',
                            'headerOptions' => ['style' => 'color:#337ab7'],
                            'buttons' => [

                                'leadDelete' => function ($url, $model, $key) {
                                    $url = 'javascript:void()';//Url::to(['controller/lead-delete', 'id' => $model->id]);
                                    return Html::a('<span class="flaticon-interface-5"></span>', $url, [
                                        'title' => 'delete',
                                        'class' => 'record-delete',
                                        //'data-confirm' => 'Are you sure you want to delete this item?',//this for pre action confirmation popup box.
                                        //'data-method'  => 'post',//this for data method [get/post]
                                        'record-delete-msg' => 'Record Deleted',
                                        'record-delete-status' => 'active',
                                        'record-delete-child' => false,
                                        'record-delete-url' => Url::toRoute('delete/footer-column/' . $model->id),
                                        'record-data-key' => $key,


                                    ]);
                                },
                            ]
                        ]

                    ];
                    ?>
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        // 'filterModel' => $searchModel,
                        'pjax' => true,
                        'pjaxSettings' => [
                            'neverTimeout' => true,
                            'beforeGrid' => 'My fancy content before.',
                            'afterGrid' => 'My fancy content after.',
                        ],

                        'responsive' => true,
                        'floatHeader' => false,
                        'containerOptions' => ['style' => 'overflow: auto'], // only set when $responsive = false
                        'headerRowOptions' => ['class' => 'kartik-sheet-style'],
                        'filterRowOptions' => ['class' => 'kartik-sheet-style'],
//                'toolbar' =>  [
//                    ['content' =>
//                        Html::button('<i class="glyphicon glyphicon-plus"></i>', ['type' => 'button', 'title' => 'Add Book', 'class' => 'btn btn-success', 'onclick' => 'alert("This will launch the book creation form.\n\nDisabled for this demo!");']) . ' '.
//                        Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['grid-demo'], ['data-pjax' => 0, 'class' => 'btn btn-default', 'title' => 'Reset Grid'])
//                    ],
//                    '{export}',
//                    '{toggleData}',
//                ],
                        // set export properties
                        'export' => [
                            'fontAwesome' => true
                        ],

                        'columns' => $gridColumns,


                    ]) ?>
                </div>
            </div>
        </div>
        <div class="col-4 col-md-4">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Add New Column</h4>
                </div>

                <?php $form = ActiveForm::begin(
                    [
                        // 'layout' => 'horizontal',
                        // 'action' => ['edit/edit-ads?id='.$modal['id']],


                        'options' => ['id' => 'edit_ads_form', 'enctype' => 'multipart/form-data', 'class' => 'master-form22']
                    ]);
                ?>
                <div class="card-body">
                    <br>
                    <?php
                    echo $form->field($modal, 'name')->label('New Column Name');
                    echo $form->field($modal, 'column_class')
                        ->dropDownList(
                            array(
                                "col-lg-12 col-md-12 col-xs-6" => "col-12",
                                "col-lg-6 col-md-6 col-xs-6" => "col-6",
                                "col-lg-4 col-md-4 col-xs-12" => "col-4",
                                "col-lg-3 col-md-3 col-xs-12" => "col-3",
                                "col-lg-2 col-md-2 col-xs-12" => "col-2",

                            )
                        );
                    ?>
                    <br>

                </div>
                <div class="card-footer no-bd">
                    <?php echo Html::submitButton('SAVE', ['class' => 'btn btn-primary', 'id' => 'submit_id']) ?>
                    <?php echo Html::resetButton('RESET', ['class' => 'btn btn-default']) ?>

                </div>
                <?php ActiveForm::end(); ?>

            </div>
        </div>
    </div>
</div>



