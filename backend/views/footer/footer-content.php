<?php

/* @var $this yii\web\View */

/* @var $form yii\bootstrap\ActiveForm */

use kartik\grid\GridView;

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\editable\Editable;
use \yii\helpers\Url;

$this->title = 'Footer Column Link Settings';
$this->params['breadcrumbs'][] = ['label' => 'Footer Setting', 'url' => ['footer/index']];

$this->params['breadcrumbs'][] = $this->title;
?>

<div class="col-7 col-md-9 ml-auto mr-auto">
    <div class="card">
        <div class="card-header">
            <h4 class="card-title"><?= $this->title; ?></h4>
        </div>
        <div class="card-body">
            <?php $form = ActiveForm::begin(
                [
                    'layout' => 'horizontal',
                    // 'action' => ['edit/edit-ads?id='.$modal['id']],

                    'fieldConfig' =>
                        [
                            'horizontalCssClasses' =>
                                [
                                    'label' => 'col-md-3',
                                    'offset' => '',
                                    'wrapper' => 'col',
                                    'error' => 'col-md-5',
                                    'hint' => 'col-md-8  col-md-push-3 imgHintS',
                                ],
                        ],
                    'options' => ['id' => 'edit_ads_form', 'enctype' => 'multipart/form-data', 'class' => 'master-form22']
                ]);
            ?>
            <div class="card-body">
                <input id="footerContentU" type="hidden" value="<?= Url::toRoute('footer/get-type') ?>">
                <br>
                <?php
                echo $form->field($modal, 'name');

                echo $form->field($modal, 'column_id')->dropDownList(
                    \yii\helpers\ArrayHelper::map(\common\models\FooterColumn::find()->all(), 'id', 'name')
                )->label('column');
                echo $form->field($modal, 'type')
                    ->dropDownList(
                        array(
                            "customUrl" => "Add custom Url",
                            "pages_link" => "Created Pages link",
                            "siteAction" => "Site Page Link (controllers/action)",

                        )
                    );
                echo $form->field($modal, 'target')
                    ->dropDownList(
                        array(
                            "_blank" => "Open in new window",
                            "_parent" => "open in same window",

                        )
                    );

                ?>
                <div id="displayFooterC">
                    <div class="form-group field-footercontent-url required">
                        <label class="control-label col-md-3" for="footercontent-url">Url</label>
                        <div class="col">
                            <input type="text" id="footercontent-url" class="form-control" name="FooterContent[url]"
                                   aria-required="true" aria-invalid="true">
                            <p class="help-block help-block-error col-md-5">Url cannot be blank.</p>
                        </div>

                    </div>
                </div>
                <br>

            </div>
            <div class="card-footer no-bd">
                <?php echo Html::submitButton('UPDATE', ['class' => 'btn btn-primary', 'id' => 'submit_id']) ?>
                <?php echo Html::resetButton('RESET', ['class' => 'btn btn-default']) ?>

            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>



