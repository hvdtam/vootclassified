<?php

/* @var $this yii\web\View */

/* @var $form yii\bootstrap\ActiveForm */

use kartik\grid\GridView;

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\editable\Editable;
use \yii\helpers\Url;

$this->title = 'Footer Column Link Settings';
$this->params['breadcrumbs'][] = ['label' => 'Footer Setting', 'url' => ['footer/index']];

$this->params['breadcrumbs'][] = $this->title;
?>
<div class="col-12">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <?php
                foreach ($column as $col) {
                    $content = \common\models\FooterContent::find()->where(['column_id' => $col['id']])->all();

                    ?>
                    <div class="<?= $col['column_class'] ?>">
                        <strong>
                            <?= $col['name'] ?>
                        </strong>

                        <ul class="list-unstyled">
                            <?php
                            foreach ($content as $link) {
                                ?>
                                <li>
                                    <a href="<?= $link['url'] ?>">
                                        <?= $link['name'] ?>
                                    </a>
                                </li>
                                <?php
                            }
                            ?>
                        </ul>
                    </div>
                    <?php
                }
                ?>

            </div>

        </div>
    </div>


</div>

<div class="col-md-12">
    <div class="card">
        <div class="card-header">
            <div class="d-flex align-items-center">
                <h4 class="card-title">
                    Footer Links
                </h4>
                <a class="btn btn-primary btn-round ml-auto" href="<?= Url::toRoute('footer/footer-content') ?>">
                    <i class="la la-plus"></i>
                    Add New Link
                </a>
            </div>
        </div>
        <div class="card-body">


            <?php
            $gridColumns = [
                // the buy_amount column configuration
                ['class' => 'kartik\grid\SerialColumn'],
                [
                    'class' => 'kartik\grid\EditableColumn',
                    'attribute' => 'name',
                    'editableOptions' => [
                        'header' => 'Change Column name',
                        'inputType' => \kartik\editable\Editable::INPUT_TEXT,
                    ],
                    'pageSummary' => true
                ],
                [
                    'class' => 'kartik\grid\EditableColumn',
                    'attribute' => 'column_id',
                    'label' => 'Column',
                    'value' => function ($model) {
                        return \common\models\FooterColumn::getName($model->column_id);
                    },
                    'editableOptions' => [
                        'header' => 'Change Footer Column',
                        'inputType' => Editable::INPUT_DROPDOWN_LIST,
                        'data' => \yii\helpers\ArrayHelper::map(\common\models\FooterColumn::find()->all(), 'id', 'name')
                    ],
                    'pageSummary' => true
                ],
                [
                    'class' => 'kartik\grid\EditableColumn',
                    'attribute' => 'type',
                    'pageSummary' => true,
                    'editableOptions' => [
                        'header' => 'Change Type',
                        'inputType' => Editable::INPUT_DROPDOWN_LIST,
                        'data' => array(
                            "customUrl" => "Add custom Url",
                            "pages_link" => "Created Pages link",
                            "siteAction" => "Site Page Link (controllers/action)",

                        ), // any list of values
                    ],
                ],
                [
                    'class' => 'kartik\grid\EditableColumn',
                    'attribute' => 'url',
                    'editableOptions' => [
                        'header' => 'Change Url',
                        'inputType' => \kartik\editable\Editable::INPUT_TEXT,
                    ],
                    'pageSummary' => true
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{leadDelete}',
                    'header' => 'Actions',
                    'headerOptions' => ['style' => 'color:#337ab7'],
                    'buttons' => [

                        'leadDelete' => function ($url, $model, $key) {
                            $url = 'javascript:void()';//Url::to(['controller/lead-delete', 'id' => $model->id]);
                            return Html::a('<span class="flaticon-interface-5"></span>', $url, [
                                'title' => 'delete',
                                'class' => 'record-delete',
                                //'data-confirm' => 'Are you sure you want to delete this item?',//this for pre action confirmation popup box.
                                //'data-method'  => 'post',//this for data method [get/post]
                                'record-delete-msg' => 'Record Deleted',
                                'record-delete-status' => 'active',
                                'record-delete-child' => false,
                                'record-delete-url' => Url::toRoute('delete/footer-content/' . $model->id),
                                'record-data-key' => $key,


                            ]);
                        },
                    ]
                ]

            ];
            ?>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                // 'filterModel' => $searchModel,
                'pjax' => true,
                'pjaxSettings' => [
                    'neverTimeout' => true,
                    'beforeGrid' => 'My fancy content before.',
                    'afterGrid' => 'My fancy content after.',
                ],

                'responsive' => true,
                'floatHeader' => false,
                'containerOptions' => ['style' => 'overflow: auto'], // only set when $responsive = false
                'headerRowOptions' => ['class' => 'kartik-sheet-style'],
                'filterRowOptions' => ['class' => 'kartik-sheet-style'],
//                'toolbar' =>  [
//                    ['content' =>
//                        Html::button('<i class="glyphicon glyphicon-plus"></i>', ['type' => 'button', 'title' => 'Add Book', 'class' => 'btn btn-success', 'onclick' => 'alert("This will launch the book creation form.\n\nDisabled for this demo!");']) . ' '.
//                        Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['grid-demo'], ['data-pjax' => 0, 'class' => 'btn btn-default', 'title' => 'Reset Grid'])
//                    ],
//                    '{export}',
//                    '{toggleData}',
//                ],
                // set export properties
                'export' => [
                    'fontAwesome' => true
                ],

                'columns' => $gridColumns,


            ]) ?>
        </div>
    </div>
</div>
