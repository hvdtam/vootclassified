<?php
$this->title = "Admin Dashboard";
/**
 * Created by PhpStorm.
 * User: Mayank Singh
 * Date: 10/31/2018
 * Time: 6:38 PM
 */
?>
<div class="row col-12">
    <div class="col-sm-6 col-md-3">
        <div class="card card-stats card-primary card-round">
            <div class="card-body ">
                <div class="row">
                    <div class="col-5">
                        <div class="icon-big text-center">
                            <i class="flaticon-interface"></i>
                        </div>
                    </div>
                    <div class="col-7 col-stats">
                        <div class="numbers">
                            <p class="card-category">Total Page View</p>
                            <h4 class="card-title"><?= $page_view; ?></h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3">
        <div class="card card-stats card-info card-round">
            <div class="card-body">
                <div class="row">
                    <div class="col-5">
                        <div class="icon-big text-center">
                            <i class="flaticon-users"></i>
                        </div>
                    </div>
                    <div class="col-7 col-stats">
                        <div class="numbers">
                            <p class="card-category">Total Members</p>
                            <h4 class="card-title"><?= $totalUser; ?></h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3">
        <div class="card card-stats card-success card-round">
            <div class="card-body ">
                <div class="row">
                    <div class="col-5">
                        <div class="icon-big text-center">
                            <i class="flaticon-graph"></i>
                        </div>
                    </div>
                    <div class="col-7 col-stats">
                        <div class="numbers">
                            <p class="card-category">Total Ads</p>
                            <h4 class="card-title"><?= $total; ?></h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3">
        <div class="card card-stats card-secondary card-round">
            <div class="card-body ">
                <div class="row">
                    <div class="col-5">
                        <div class="icon-big text-center">
                            <i class="flaticon-success"></i>
                        </div>
                    </div>
                    <div class="col-7 col-stats">
                        <div class="numbers">
                            <p class="card-category">New Report</p>
                            <h4 class="card-title"><?= $report; ?></h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-md-9 mt-9 mb-3">
    <br>
    <div class="row">
        <div class="col-md-6">
            <div class="card card-round">
                <div class="card-body">
                    <div class="card-title">Top Five New People</div>
                    <div class="card-list">
                        <?php
                        foreach ($member as $list) {
                            ?>
                            <div class="item-list">
                                <img src="<?= Yii::$app->urlManagerFrontend->baseUrl . '/images/users/' . $list['image'] ?>"
                                     alt="users" class="small-pic">
                                <div class="info-user ml-3">
                                    <div class="username"><?= $list['first_name'] ?></div>
                                    <div class="status"><?= \common\models\Analytic::time_elapsed_string($list['created_at']) ?></div>
                                </div>
                                <a href="<?= \yii\helpers\Url::toRoute('user/detail/' . $list['id']) ?>"
                                   class="btn btn-add">
                                    <i class="flaticon-medical"></i>
                                </a>
                            </div>
                            <?php
                        }
                        ?>


                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card card-round">
                <div class="card-body">
                    <div class="card-title">Latest Five Ads</div>
                    <div class="card-list">
                        <?php
                        foreach ($ads as $adsList) {
                            $imagebase = $adsList['image'];
                            $imageArray = explode(",", $imagebase);
                            $img = reset($imageArray);
                            ?>
                            <div class="item-list">
                                <img src="<?= Yii::$app->urlManagerFrontend->baseUrl . '/images/item/' . $img ?>"
                                     alt="users" class="small-pic">
                                <div class="info-user ml-3">
                                    <div class="username"><?= $adsList['ad_title'] ?></div>
                                    <div class="status"><?= \common\models\Analytic::time_elapsed_string($adsList['created_at']) ?></div>
                                </div>
                                <a href="<?= \yii\helpers\Url::toRoute('ads/detail?id=' . $adsList['id']) ?>"
                                   class="btn btn-add">
                                    <i class="flaticon-medical"></i>
                                </a>
                            </div>

                            <?php
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="card card-round">
                <div class="card-body">
                    <div class="card-title">Latest Website Statistics</div>
                    <table class="table">
                        <tr>
                            <th>Visitor Agent</th>
                            <th>Visitor Browser</th>
                            <th>Visitor System</th>
                            <th>Visitor iP</th>
                            <th>Visitor City / Country</th>
                            <th>Refer Url</th>

                        </tr>
                        <?php
                        foreach ($statistics as $list) {
                            ?>
                            <tr>

                                <td><?= \common\models\Track::ExactOs($list['agent']); ?></td>
                                <td><?= \common\models\Track::ExactBrowserName($list['agent']); ?></td>
                                <td> <?= $list['system']; ?></td>
                                <td> <?= $list['ip']; ?></td>
                                <td><?= $list['city']; ?> - <?= $list['country']; ?></td>
                                <td>
                                    <a target="_blank" href="<?= $list['ref']; ?>">
                                        <?= $list['ref']; ?>
                                    </a>
                                </td>
                            </tr>
                            <?php

                        }
                        ?>
                    </table>
                </div>
            </div>

        </div>
    </div>
</div>
<div class="col-md-3 mt-3 mb-3">
    <div class="card card-round">
        <div class="card-header">
            <h4 class="card-title">
                Category wise Ads Distribution
            </h4>
        </div>
        <div class="card-body">
            <?php
            $count = \common\models\Ads::find()->count();
            $cat = \common\models\Category::find()->all();
            foreach ($cat as $data) {
                $ads_count = \common\models\Ads::find()->where(['category' => $data['name']])->count();;

                if ($ads_count > '0') {
                    $a = $ads_count / $count;
                    $b = $a * 100;
                    $c = $b * 3.6;
                } else {
                    $c = $ads_count * 3.6;
                }


                if ($c > 0) {
                    ?>
                    <div class="progress-card">
                        <div class="d-flex justify-content-between mb-1">
                            <span class="text-muted"><?= $data['name']; ?> </span>
                            <span class="text-muted fw-bold"> <?= $ads_count; ?></span>
                        </div>
                        <div class="progress mb-2" style="height: 7px;">
                            <div class="progress-bar bg-success" role="progressbar" style="width: <?= $b ?>%"
                                 aria-valuenow="78" aria-valuemin="0" aria-valuemax="100" data-toggle="tooltip"
                                 data-placement="top" title="78%"></div>

                        </div>
                    </div>

                    <?php
                } else {
                    ?>
                    <div class="progress-card">
                        <div class="d-flex justify-content-between mb-1">
                            <span class="text-muted"><?= $data['name']; ?> </span>
                            <span class="text-muted fw-bold"> <?= $ads_count; ?></span>
                        </div>
                        <div class="progress mb-2" style="height: 7px;">
                            <div class="progress-bar bg-success" role="progressbar" style="width: <?= $c ?>%"
                                 aria-valuenow="<?= $c ?>" aria-valuemin="0" aria-valuemax="100" data-toggle="tooltip"
                                 data-placement="top" title="" data-original-title="<?= $c ?>%"></div>
                        </div>
                    </div>

                    <?php
                    //'<div style="height: '.$c.'px;background-color:#eee"><div class="rotet">'.$data['name'].' ('.$ads_count.')</div></div>';
                }
            }
            ?>


        </div>
    </div>
</div>
