<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */

/* @var $exception Exception */

use yii\helpers\Html;

//$this->title = $name;
?>
<br><br>
<div class="col-lg-8 m-auto">
    <div class="card">
        <div class="card-header">
            <b> <?= Html::encode($name) ?></b>
        </div>
        <div class="card-body">
            <div class="alert alert-danger">
                <?= nl2br(Html::encode($message)) ?>
            </div>

            <p>
                The above error occurred while the Web server was processing your request.
            </p>
            <p>
                Please contact us if you think this is a server error. Thank you.
            </p>
        </div>
    </div>
</div>
