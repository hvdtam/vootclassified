<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$logo = \common\models\SiteSettings::find()->one();

$this->title = $logo['site_name'] . ' Activation';
$this->params['breadcrumbs'][] = $this->title;
$background = \common\models\DefaultSetting::find()->select(['background'])->one();
//themes/assets/img/
?>

<div class="container container-login animated fadeIn d-block">
    <h3 class="text-center"><?= $logo['site_name']; ?> Activation</h3>
    <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

    <div class="row">
        <div class="col-lg-12">
            <?= \common\widgets\Alert::widget() ?>

            <?php echo $form->field($model, 'domain_name', [
                'template' => '<div class="form-floating-label">
                                {label} {input}{error}{hint}
                                </div>'
            ])->textInput(['class' => 'form-control input-border-bottom', 'placeholder' => 'Enter Domain Name'])->hint('Enter full domain name without eg : www.google.com')->label(false)
            ?>
            <?php echo $form->field($model, 'contact_email', [
                'template' => '<div class=" form-floating-label">
                                {label} {input}{error}{hint}
                                </div>'
            ])->textInput(['class' => 'form-control input-border-bottom', 'placeholder' => 'Your Contact Email'])->label(false)
            ?>
            <?php echo $form->field($model, 'codecanyon_username', [
                'template' => '<div class=" form-floating-label">
                                {label} {input}{error}{hint}
                                </div>'
            ])->textInput(['class' => 'form-control input-border-bottom', 'placeholder' => 'Enter Codecanyon Username'])->label(false)
            ?>
            <?php echo $form->field($model, 'purchase_code', [
                'template' => '<div class="form-floating-label">
                                {label} {input}{error}{hint}
                                </div>'
            ])->passwordInput(['placeholder' => 'Enter Your Purchase Code here', 'class' => 'form-control input-border-bottom'])->label(false)
            ?>


            <?php echo $form->field($model, 'admin_username', [
                'template' => '<div class="form-floating-label">
                                {label} {input}{error}{hint}
                                </div>'
            ])->textInput(['class' => 'form-control input-border-bottom', 'placeholder' => 'New Admin Username'])->label(false)
            ?>

            <?php echo $form->field($model, 'admin_password', [
                'template' => '<div class="form-floating-label">
                                {label} {input}{error}{hint}
                                </div>'
            ])->passwordInput(['placeholder' => 'New Admin Password', 'class' => 'form-control input-border-bottom'])->label(false)
            ?>


            <div class="login-account">
            <span class="msg">
                Warning! Please do not share your Admin Password from anyone.
            </span>
            </div>
            <div class="col-12 m-0 d-flex justify-content-center mt-5">

                <div class="col col-md-6">
                    <?= Html::submitButton('Active Now', ['class' => 'btn btn-block btn-primary', 'name' => 'login-button']) ?>
                </div>
            </div>
        </div>

    </div>

    <?php ActiveForm::end(); ?>
</div>

