<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Buy Services');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">


    <div class="col-md-12">
        <div class="card ">
            <div class="header">
                <?= Html::a(Yii::t('app', 'Create Buy Services'), ['create'], ['class' => 'btn btn-success pull-right']) ?>

                <h4 class="title"><?= $this->title; ?></h4>
                <p class="category">Backend development</p>

            </div>
            <div class="content">
                <div class="buy-services-index">

                    <?php Pjax::begin(); ?>


                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],

                            'module_id',
                            'service_for',
                            'title',
                            'price',
                            //'label',
                            //'feature_home_page',
                            //'feature_featured_item',
                            //'featured_search_results',
                            'status',

                            ['class' => 'yii\grid\ActionColumn'],
                        ],
                    ]); ?>
                    <?php Pjax::end(); ?>
                </div>

            </div>
        </div>
    </div>

</div>

