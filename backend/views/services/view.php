<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\BuyServices */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Buy Services'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">


    <div class="col-md-8 col-md-offset-2">
        <div class="card ">
            <div class="header">
                <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary pull-right']) ?>
                <span class="pull-right">&nbsp;</span>
                <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger pull-right',
                    'data' => [
                        'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                        'method' => 'post',
                    ],
                ]) ?>
                <h4 class="title"><?= Html::encode($this->title) ?></h4>
                <p class="category">Backend development</p>

            </div>
            <div class="content">
                <div class="buy-services-index">

                    <div class="buy-services-create">
                        <?= DetailView::widget([
                            'model' => $model,
                            'attributes' => [
                                'id',
                                'module_id',
                                'service_for',
                                'title',
                                'price',
                                'label',
                                'feature_home_page',
                                'feature_featured_item',
                                'featured_search_results',
                                'status',
                            ],
                        ]) ?>


                    </div>
                </div>

            </div>
        </div>
    </div>

</div>

