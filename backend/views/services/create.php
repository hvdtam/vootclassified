<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\BuyServices */

$this->title = Yii::t('app', 'Create Buy Services');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Buy Services'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">


    <div class="col-md-8 col-md-offset-2">
        <div class="card ">
            <div class="header">

                <h4 class="title"><?= Html::encode($this->title) ?></h4>
                <p class="category">Backend development</p>

            </div>
            <div class="content">
                <div class="buy-services-index">

                    <div class="buy-services-create">
                        <?= $this->render('_form', [
                            'model' => $model,
                        ]) ?>

                    </div>
                </div>

            </div>
        </div>
    </div>

</div>


