<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\BuyServices */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="buy-services-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'module_id')->textInput() ?>

    <?= $form->field($model, 'service_for')->dropDownList(['owners' => 'Owners', 'brokers' => 'Brokers', 'builders' => 'Builders',], ['prompt' => '']) ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'price')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'label')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'feature_home_page')->dropDownList(['0', '1',], ['prompt' => '']) ?>

    <?= $form->field($model, 'feature_featured_item')->dropDownList(['0', '1',], ['prompt' => '']) ?>

    <?= $form->field($model, 'featured_search_results')->dropDownList(['0', '1',], ['prompt' => '']) ?>

    <?= $form->field($model, 'status')->dropDownList(['0', '1',], ['prompt' => '']) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
