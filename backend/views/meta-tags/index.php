<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Meta Tags';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="col-md-12">
    <div class="card">
        <div class="card-header">
            <div class="d-flex align-items-center">
                <h4 class="card-title">
                    <?= $this->title; ?>
                </h4>

                <?= Html::a('Create Meta Tags', ['create'], ['class' => 'btn btn-primary btn-round ml-auto']) ?>
            </div>
        </div>
        <div class="card-body">
            <?php Pjax::begin(); ?>



            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    //'id',
                    'page',
                    'title',
                    'description:ntext',
                    'keyword',
                    'status',


                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => ' {view} {update} {delete}',
                        'header' => 'Actions',
                        'headerOptions' => ['style' => 'color:#337ab7'],

                        'buttons' => [
                            'update' => function ($url) {
                                return Html::a(
                                    '<span class="la la-edit"></span>',
                                    $url,
                                    [
                                        'title' => 'update',
                                        'data-pjax' => '0',
                                    ]
                                );
                            },
                            'view' => function ($url) {
                                return Html::a(
                                    '<span class="la la-eye"></span>',
                                    $url,
                                    [
                                        'title' => 'view',
                                        'data-pjax' => '0',
                                    ]
                                );
                            },
                            'delete' => function ($url) {
                                return Html::a(
                                    '<span class="la la-remove"></span>',
                                    $url,
                                    [
                                        'title' => 'delete',
                                        'data-pjax' => '0',
                                    ]
                                );
                            },

                        ],
                    ],
                ],
            ]); ?>
            <?php Pjax::end(); ?>
        </div>
    </div>
</div>


