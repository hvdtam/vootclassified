<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\MetaTags */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="card mt-3">
    <div class="card-header">
        <h6 class="title">
            Create Meta Tags
        </h6>
    </div>
    <div class="card-body">
        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'page')->dropDownList(['home page' => 'Home page', 'login' => 'Login', 'register' => 'Register', 'post ads' => 'Post ads', 'contact' => 'Contact',], ['prompt' => '']) ?>

        <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

        <?= $form->field($model, 'keyword')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'status')->dropDownList(['active' => 'Active', 'disable' => 'Disable',], ['prompt' => '']) ?>

        <div class="row">
            <div class="col-4 ml-auto">
                <div class="form-group">
                    <?= Html::submitButton('Save', ['class' => 'btn btn-block btn-success']) ?>
                </div>
            </div>
        </div>

        <?php ActiveForm::end(); ?>
    </div>

</div>
