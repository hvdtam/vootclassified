<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MetaTags */

$this->title = 'Create Meta Tags';
$this->params['breadcrumbs'][] = ['label' => 'Meta Tags', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="col-8">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
