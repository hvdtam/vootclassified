<?php

/* @var $this yii\web\View */

/* @var $form yii\bootstrap\ActiveForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Reports Review';
$this->params['breadcrumbs'][] = ['label' => 'Reports', 'url' => ['reports/index']];

$this->params['breadcrumbs'][] = $this->title;
?>
<div class="col-md-8 ml-auto mr-auto">
    <div class="card">
        <div class="card-header">
            <span class="card-title" style="font-size:14px;color: #777;font-weight: 600;">
                <i class="flaticon-round"></i>
                An report found for <i><?= $list['reason'] ?></i>
            </span>
        </div>
        <div class="card-body">

            <p class="panel" style="font-size: 12px;color: #777;">
                        <span>
                           <b>
                               Main Reason

                               <i class="fa fa-angle-right"></i>

                               <small class="badge badge-success"><?= $list['reason'] ?></small>
                           </b>
                        </span>
            </p>
            <p class="panel" style="font-size: 12px;color: #777;">
                        <span>
                           <b>
                               Description <i class="fa fa-angle-right"></i>
                           </b>
                            <?= $list['message'] ?>
                        </span>
            </p>
            <a class="text-danger" href="<?= \yii\helpers\Url::toRoute('ads/detail?id=' . $list['ad_id']) ?>">
                Check Detail
            </a>
        </div>
        <div class="card-footer">
                        <span style="font-size: 11px;color: #999;">
                             Report From:
                            <b>
                                <i><?= $list['email'] ?></i>
                            </b>
                        </span>

            <div class="btn-group pull-right">
                <button class="btn btn-xs"><?= $list['status'] ?></button>
                <a target="_blank" href="<?= $list['url'] ?>" class="btn btn-success btn-xs ">
                    <i class="fa fa-eye"></i> Review
                </a>

            </div>

        </div>
    </div>
    <div class="col-3 ml-auto mr-auto">
        <a href="#respondReport" class="btn btn-lg btn-success btn-block" data-toggle="modal">
            Respond
        </a>

    </div>

</div>
<!--            Model start here-->
<div class="modal fade in" id="respondReport" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header no-bd">
                <h5 class="modal-title">
					<span class="fw-mediumbold">
						Respond
                    </span>
                </h5>

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?php $form = ActiveForm::begin([
                //'action' => ['report/index'],
                //'method' => 'get',
                'options' => ['id' => 'report_form']
            ]); ?>
            <div class="modal-body">
                <div class="row">


                    <div class="col-sm-12">
                        <small style="padding: 10px;">
                            Respond from email. notify them about report.
                        </small>
                        <?php echo $form->field($report, 'action', [
                            'template' => '
                                <div class="form-group form-group-default">
                                    {label} {input}{error}{hint}
                                </div>'
                        ])->dropDownList(
                            array(
                                'decline' => 'Report Declined',
                                'solve' => 'Issue Solve',
                                'decline' => 'Report Declined',
                                'accept' => 'Block Ad',
                                'observation' => 'Report Under Observation',
                            ),

                            ['prompt' => 'Choose an Action']) ?>

                        <?php echo $form->field($report, 'respond_to', [
                            'template' => '
                                <div class="form-group form-group-default">
                                    {label} {input}{error}{hint}
                                </div>'
                        ])->dropDownList(
                            array($list['email'] => 'Respond to Reporter:' . $list['email'], $advertiserEmail => 'Respond to Advertiser :' . $advertiserEmail),
                            ['prompt' => 'Select user to Respond']) ?>

                        <?php echo $form->field($report, 'message', [
                            'template' => '<div class="form-group form-group-default">
                                {label} {input}{error}{hint}
                                </div>'
                        ])->textInput(['placeholder' => 'write note to respond...']) ?>
                    </div>

                </div>
            </div>
            <div class="modal-footer no-bd">
                <?php echo Html::button('Close', ['class' => 'btn btn', 'data-dismiss' => 'modal']) ?>
                <?php echo Html::submitButton('Send Mail', ['class' => 'btn btn-success', 'id' => 'submit_id']) ?>

            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
<!--            model Respond closed-->

