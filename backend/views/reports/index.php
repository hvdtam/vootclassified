<?php

/* @var $this yii\web\View */

/* @var $form yii\bootstrap\ActiveForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Reports';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="col-md-8 ml-auto mr-auto">

    <div class="card">
        <div class="card-header">
            <div class="card-head-row">
                <h4 class="card-title">
                    Reports
                </h4>

                <div class="card-tools">
                    <ul class="nav nav-pills nav-success nav-pills-no-bd nav-sm" id="pills-tab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link <?= ("unread" != $active) ? '' : 'active'; ?>" id="pills-home"
                               href="?sort=unread" aria-selected="true">
                                New
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link <?= ("pending" != $active) ? '' : 'active'; ?>" id="pills-profile"
                               href="?sort=pending" aria-selected="false">
                                Pending
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link <?= ("solve" != $active) ? '' : 'active'; ?>" id="pills-contact"
                               href="?sort=solve" aria-selected="false">
                                solve
                            </a>
                        </li>
                    </ul>
                </div>
            </div>

        </div>

        <div class="card-body">
            <?php
            foreach ($modal as $list) {
                ?>
                <div class="card">
                    <div class="card-header">
                        <span class="card-title" style="font-size:14px;color: #777;font-weight: 600;">
                            <i class="flaticon-round"></i>
                            An report found for <i><?= $list['reason'] ?></i>
                        </span>

                    </div>
                    <div class="card-body">

                        <p class="panel" style="font-size: 12px;color: #777;">
                        <span>
                           <b>
                               Main Reason

                               <i class="fa fa-angle-right"></i>

                               <small class="badge badge-success"><?= $list['reason'] ?></small>
                           </b>
                        </span>
                        </p>
                        <p class="panel" style="font-size: 12px;color: #777;">
                        <span>
                           <b>
                               Description <i class="fa fa-angle-right"></i>
                           </b>
                            <?= substr($list['message'], 0, 32) ?>...
                        </span>
                        </p>
                        <a class="text-danger" href="<?= \yii\helpers\Url::toRoute('reports/detail/' . $list['id']) ?>">
                            <b>Read More</b>
                        </a>
                    </div>
                    <div class="card-footer">
                        <span style="font-size: 11px;color: #999;">
                             Report From:
                            <b>
                                <i><?= $list['email'] ?></i>
                            </b>
                        </span>

                        <div class="btn-group pull-right">
                            <button class="btn btn-xs"><?= $list['status'] ?></button>


                        </div>

                    </div>
                </div>
                <?php
            };
            if (!$modal) {
                ?>
                <blockquote>
                    No New Report Found.
                </blockquote>
                <?php
            }
            ?>
        </div>

    </div>


</div>


