<?php

/* @var $this yii\web\View */

/* @var $form yii\bootstrap\ActiveForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Contact Settings';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="col-md-8 ml-auto mr-auto">
    <div class="card">
        <div class="card-header">
            <div class="d-flex align-items-center">
                <h4 class="card-title">
                    <i class="flaticon-customer-support"></i> <?= Html::encode($this->title) ?>
                </h4>


            </div>
        </div>
        <div class="card-body">
            <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>


            <?= $form->field($model, 'address')->label('Head office address') ?>

            <?= $form->field($model, 'phone')->label('Official Contact Number') ?>

            <?= $form->field($model, 'email')->label('Official Email Id') ?>

            <?= $form->field($model, 'about')->textarea()->label('who are we') ?>
            <?= $form->field($model, 'facebook_profile')->textarea(); ?>
            <?= $form->field($model, 'twitter_profile')->textarea(); ?>
            <?= $form->field($model, 'linkedin_profile')->textarea(); ?>
            <?= $form->field($model, 'instagram_profile')->textarea(); ?>
            <?= $form->field($model, 'pinterest_profile')->textarea(); ?>
            <?= $form->field($model, 'youtube_profile')->textarea(); ?>


            <div class="form-group">
                <?= Html::submitButton('Submit', ['class' => 'btn btn-primary', 'name' => 'site-settings-button']) ?>

            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>