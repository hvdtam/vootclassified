<?php
$this->title = 'Add New Plugin';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="col-md-12">
    <div class="card">
        <div class="card-header">
            <div class="d-flex align-items-center">
                <h4 class="card-title">
                    <i class="la la-plug"></i> <?= $this->title; ?>
                </h4>
                <a class="btn btn-xs btn-success ml-auto" href="<?php echo \yii\helpers\Url::toRoute('install'); ?>">
                    <i class="la la-plus"></i>
                    Add New
                </a>
            </div>
        </div>
        <ul class="list-group list-group-flush card-list">
            <?php
            if (!$plug) {
                echo "<div class='p-3 bg-light h6'>No Plugin installed</div>";
            }
            ?>


            <?php foreach ($plug as $key => $list): ?>
                <li class="list-group-item d-flex">
                    Q <?= $key + 1; ?> : <strong><?= $list ?></strong>

                    <div class="ml-auto">
                        <a href="<?= \yii\helpers\Url::toRoute(['install', 'token' => $list]) ?>"
                           class="btn btn-xs btn-primary">
                            <i class="la la-gear"></i> Install
                        </a>

                    </div>

                </li>
            <?php endforeach; ?>
        </ul>

    </div>
</div>