<?php
$this->title = 'Installed Plugin';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="col-md-12">
    <div class="card">
        <div class="card-header">
            <div class="d-flex align-items-center">
                <h4 class="card-title">
                    <i class="la la-plug"></i> <?= $this->title; ?>
                </h4>

            </div>
        </div>
        <ul class="list-group list-group-flush card-list">
            <?php
            if (!$model) {
                echo "<div class='p-3 bg-light h6'>No Plugin installed</div>";
            }
            ?>
            <?php
            foreach ($model as $key => $list): ?>
                <li class="list-group-item d-flex">
                    <strong><i class="<?= $list['plugin_icon'] ?>"></i> <?= $list['plugin_name'] ?></strong>

                    <div class="ml-auto">
                        <?php
                        if (isset($plug[$list['plugin_name']]) != $list['plugin_name']) { ?>
                            <a target="_blank" href="https://codecanyon.net/user/krishnatechnosoftindia/portfolio"
                               class="btn btn-xs btn-success">
                                <i class="la la-shopping-cart"></i> Buy Now
                            </a>
                            <?php
                        } elseif (isset($plug[$list['plugin_name']]) == $list['plugin_name'] and $list['status'] == "0") { ?>
                            <a href="<?= \yii\helpers\Url::toRoute(['install', 'token' => $list['plugin_name']]) ?>"
                               class="btn btn-xs btn-default">
                                <i class="la la-gear"></i> Install
                            </a>
                            <?php
                        } else {
                            ?>
                            <a href="<?= \yii\helpers\Url::home() . '/' . $list['plugin_admin_url'] ?>"
                               class="btn btn-xs btn-primary">
                                <i class="la la-gear"></i> Configure
                            </a>
                            <a href="<?= \yii\helpers\Url::toRoute(['uninstall', 'token' => $list['plugin_name']]) ?>"
                               class="btn btn-xs btn-danger">
                                <i class="la la-gear"></i> UnInstall
                            </a>
                        <?php } ?>
                    </div>

                </li>
            <?php endforeach; ?>
            <?php
            foreach ($plug as $key => $list) {
                $installed = \yii\helpers\ArrayHelper::map($model, 'plugin_name', 'plugin_url');
                if (isset($installed[$list])) {
                    continue;
                };

                ?>
                <li class="list-group-item d-flex">
                    <strong> <?= $list ?></strong>

                    <div class="ml-auto">
                        <a href="<?= \yii\helpers\Url::toRoute(['install', 'token' => $list]) ?>"
                           class="btn btn-xs btn-default">
                            <i class="la la-gear"></i> Install
                        </a>
                    </div>

                </li>
            <?php }; ?>
        </ul>

    </div>
</div>