<?php
$this->title = 'Install Plugin';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="col-md-12">
    <div class="card">
        <div class="card-header">
            <div class="d-flex align-items-center">
                <h4 class="card-title">
                    <i class="la la-plug"></i> <?= $this->title; ?>
                </h4>
                <a class="btn btn-xs btn-success ml-auto" href="<?php echo \yii\helpers\Url::toRoute('new'); ?>">
                    <i class="la la-plus"></i>
                    Add New
                </a>
            </div>
        </div>
        <ul class="list-group list-group-flush card-list">
            <?php
            if (!$model) {
                echo "<div class='p-3 bg-light h6'>No Plugin installed</div>";
            }
            ?>
            <?php foreach ($model as $key => $list): ?>
                <li class="list-group-item d-flex">
                    Q <?= $key + 1; ?> : <strong><?= $list['pugin_name'] ?></strong>

                    <div class="ml-auto">
                        <a href="<?= \yii\helpers\Url::toRoute(['edit', 'token' => $list['id']]) ?>"
                           class="btn btn-xs btn-primary">
                            <i class="la la-pencil"></i> Edit
                        </a>
                        <a title="delete" data-toggle="tooltip"
                           href="<?= \yii\helpers\Url::toRoute(['delete', 'token' => $list['id']]) ?>"
                           class="btn btn-xs  btn-danger">
                            <i class="la la-trash"></i>
                        </a>
                    </div>

                </li>
            <?php endforeach; ?>
        </ul>

    </div>
</div>