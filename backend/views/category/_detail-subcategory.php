<?php
//$delete = '0';
$child = ($model['item'] == 0) ? false : true;
?>
<div class="col-md-12" id="de_clmn_<?= $model['id']; ?>">
    <div class="card">
        <div class="card-header">
            <div class="d-flex align-items-center">
                <h4 class="card-title">
                    Sub Category <code> <?= $model['name']; ?> </code> detail
                </h4>

            </div>
        </div>
        <div class="card-body">
            <div class="col-md-4"></div>
            <div class="col-md-12">
                <div class="row">
                    <div class="col-sm-6 col-md-4">
                        <div class="card card-stats card-round">
                            <div class="card-body ">
                                <div class="row">
                                    <div class="col-5">
                                        <div class="icon-big text-center icon-success">
                                            <i class="flaticon-interface-6 text-warning"></i>
                                        </div>
                                    </div>
                                    <div class="col-7 col-stats">
                                        <div class="numbers">
                                            <p class="card-category">Subcategory Name</p>
                                            <h4 class="card-title"><?= $model['name']; ?></h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4">
                        <div class="card card-stats card-round">
                            <div class="card-body ">
                                <div class="row">
                                    <div class="col-5">
                                        <div class="icon-big text-center icon-warning">
                                            <i class="flaticon-pie-chart text-warning"></i>
                                        </div>
                                    </div>
                                    <div class="col-7 col-stats">
                                        <div class="numbers">
                                            <p class="card-category">Main Categorye</p>
                                            <h4 class="card-title"><?= \common\models\Category::findNameById($model['parent']) ?></h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4">
                        <div class="card card-stats card-round">
                            <div class="card-body ">
                                <div class="row">
                                    <div class="col-5">
                                        <div class="icon-big text-center icon-primary">
                                            <i class="flaticon-cart text-warning"></i>
                                        </div>
                                    </div>
                                    <div class="col-7 col-stats">
                                        <div class="numbers">
                                            <p class="card-category">Total Item</p>
                                            <h4 class="card-title"><?= $model['item']; ?></h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>
</div>
<script>

    //    //== Class definition
    //    var SweetAlert2Demo = function() {
    //
    //        //== Demos
    //        var initDemos = function() {
    //            //== Sweetalert Demo 1
    //
    //            $('#delete_<?//= $model['id'] ?>//_0').click(function(e) {
    //                swal("Warning !!!", "Some item associate with '<?//= $model['name']; ?>//'  sub category.  You Cannot Delete this Sub Category. Please Remove/Change the Sub Category of relevent item.", {
    //                    icon : "error",
    //                    buttons: {
    //                        confirm: {
    //                            className : 'btn btn-danger'
    //                        }
    //                    }
    //                });
    //            });
    //            $('#delete_<?//= $model['id'] ?>//_1').click(function(e) {
    //                swal({
    //                    title: 'Are you sure?',
    //                    text: "You won't be able to revert this!",
    //                    type: 'warning',
    //                    buttons:{
    //                        cancel: {
    //                            visible: true,
    //                            text : 'No, cancel!',
    //                            className: 'btn btn-danger'
    //                        },
    //                        confirm: {
    //                            text : 'Yes, delete it!',
    //                            className : 'btn btn-success'
    //                        }
    //                    }
    //                }).then((willDelete) => {
    //                    if(willDelete){
    //                        swal("Poof! This Sub Category named <?//= $model['name']; ?>// has been deleted!",
    //                            {
    //                            icon: "success",
    //                            buttons : {
    //                                confirm : {
    //                                    className: 'btn btn-success'
    //                                }
    //                            }
    //                        });
    //                    } else {
    //                        swal("Sub Category named <?//= $model['name']; ?>//  is safe!", {
    //                    buttons : {
    //                        confirm : {
    //                            className: 'btn btn-success'
    //                        }
    //                    }
    //                });
    //            }
    //            });
    //            })
    //
    //        };
    //
    //        return {
    //            //== Init
    //            init: function()
    //            {
    //                initDemos();
    //            }
    //        }
    //    }();
    //
    //    //== Class Initialization
    //    jQuery(document).ready(function() {
    //        SweetAlert2Demo.init();
    //    });
    //
    //
    //    //
    //    // new
    //    //


</script>