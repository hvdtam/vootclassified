<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use \yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\Category */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="icon-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>

    <div class="row">
        <div class="col-12">
            <?=
            $form->field($model, 'fa_icon', [
                'template' => '<div class="form-group row">{label}<div class="col-sm-8">{input}{error}{hint}</div></div>'])->widget(FileInput::classname(), [
                'options' => ['accept' => 'image/*'],
                'pluginOptions' => [
                    'maxFileCount' => 1,
                    'overwriteInitial' => false,
                    'theme' => 'fa',
                    'initialPreviewAsData' => 'true',
                    'cancelLabel' => '',
                    'cancelClass' => 'hidden',
                    'browseLabel' => Yii::t('app', 'Change Icon'),
                    'browseClass' => 'btn btn-primary',
                    'browseIcon' => '<i class="fa fa-image"></i>',
                    'removeLabel' => '',
                    'removeIcon' => '<i class="fa fa-trash"></i>',
                    'uploadClass' => 'd-none',
                    'uploadLabel' => '',
                    'uploadIcon' => '<i class="fa fa-upload"></i>',
                    'previewFileType' => 'image',
                    'allowedFileExtensions' => ['jpg', 'jpeg', 'png'],
                    'previewClass' => 'bg-light'
                ]

            ])->label(Yii::t('app', 'Change Icon')); ?>
        </div>

    </div>

    <div class="form-group" align="center">
        <?= Html::submitButton('Save & Upload Icon', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
