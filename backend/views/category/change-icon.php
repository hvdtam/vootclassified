<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Incentive */

$this->title = 'Change ' . $model['name'] . ' Category Icon';
$this->params['breadcrumbs'][] = ['label' => 'Category', 'url' => ['category/index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="col-lg-8">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title"><?= Html::encode($this->title) ?></h3>
        </div>
        <div class="card-body">
            <?= $this->render('_icon-form', [
                'model' => $model,
            ]) ?>
        </div>
    </div>
</div>