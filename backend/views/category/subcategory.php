<?php
/**
 * Created by PhpStorm.
 * User: Asus
 * Date: 11-05-2016
 * Time: 11:36
 */

//var_dump($model);

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use Yii\bootstrap\Modal;
use yii\bootstrap\ActiveForm;
use kartik\editable\Editable;
use \yii\helpers\Url;

$this->title = 'Sub Category Model Settings';
$this->params['breadcrumbs'][] = $this->title;

?>
<input type="hidden" name="SubCatUrl" id="subaddurl" value="<?= \yii\helpers\Url::toRoute('category/sub-category') ?>">

<div class="col-md-12">
    <div class="card">
        <div class="card-header">
            <div class="d-flex align-items-center">
                <h4 class="card-title">
                    Sub Category
                </h4>
                <button class="btn btn-primary btn-round ml-auto" data-toggle="modal" data-target="#addRowModal">
                    <i class="la la-plus"></i>
                    Add Row
                </button>
            </div>
        </div>
        <div class="card-body">


            <?php
            $gridColumns = [
                // the buy_amount column configuration
                ['class' => 'kartik\grid\SerialColumn'],
                [
                    'class' => '\kartik\grid\ExpandRowColumn',
                    'header' => 'Detail',
                    'headerOptions' => ['style' => 'color:#337ab7'],
                    'expandIcon' => '<span class="flaticon-download-1 text-primary"></span>',

                    'value' => function ($model, $key, $index, $column) {
                        return GridView::ROW_COLLAPSED;
                    },
                    'detailUrl' => Yii::$app->request->getBaseUrl() . '/category/expand-view',
                ],
                [
                    'class' => 'kartik\grid\EditableColumn',
                    'attribute' => 'name',
                    'editableOptions' => [
                        'header' => 'Name',
                        'inputType' => \kartik\editable\Editable::INPUT_TEXT,
                    ],
                    'pageSummary' => true
                ],
// the name column configuration
                [
                    'class' => 'kartik\grid\EditableColumn',
                    'attribute' => 'parent',
                    'pageSummary' => true,
                    'value' => function ($model) {
                        return \common\models\Category::findNameById($model->parent);
                    },
                    'filterType' => GridView::FILTER_SELECT2,
                    'filter' => \yii\helpers\ArrayHelper::map(\common\models\Category::find()->orderBy('name')->asArray()->all(), 'id', 'name'),
                    'filterWidgetOptions' =>
                        [
                            'pluginOptions' => ['allowClear' => true],
                        ],
                    'filterInputOptions' => ['placeholder' => 'Any category'],
                    'editableOptions' => [
                        'header' => 'Main Category',
                        'inputType' => Editable::INPUT_DROPDOWN_LIST,
                        'data' => $data, // any list of values
                    ],
                ],

                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{leadDelete}',
                    'header' => 'Actions',
                    'headerOptions' => ['style' => 'color:#337ab7'],
                    'buttons' => [

                        'leadDelete' => function ($url, $model, $key) {
                            $url = 'javascript:void()';//Url::to(['controller/lead-delete', 'id' => $model->id]);
                            return Html::a('<span class="flaticon-interface-5"></span>', $url, [
                                'title' => 'delete',
                                'class' => 'record-delete',
                                //'data-confirm' => 'Are you sure you want to delete this item?',//this for pre action confirmation popup box.
                                //'data-method'  => 'post',//this for data method [get/post]
                                'record-delete-msg' => 'Record Deleted',
                                'record-delete-status' => 'active',
                                'record-delete-child' => ($model['item'] == 0) ? 0 : 1,
                                'record-delete-url' => Url::toRoute('delete/sub-category/' . $model->id),
                                'record-data-key' => $key,


                            ]);
                        },
                    ]
                ]

            ];
            ?>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                // 'filterModel' => $searchModel,
                'pjax' => true,
                'pjaxSettings' => [
                    'neverTimeout' => true,
                    'beforeGrid' => 'My fancy content before.',
                    'afterGrid' => 'My fancy content after.',
                ],

                'responsive' => true,
                'floatHeader' => false,
                'containerOptions' => ['style' => 'overflow: auto'], // only set when $responsive = false
                'headerRowOptions' => ['class' => 'kartik-sheet-style'],
                'filterRowOptions' => ['class' => 'kartik-sheet-style'],
//                'toolbar' =>  [
//                    ['content' =>
//                        Html::button('<i class="glyphicon glyphicon-plus"></i>', ['type' => 'button', 'title' => 'Add Book', 'class' => 'btn btn-success', 'onclick' => 'alert("This will launch the book creation form.\n\nDisabled for this demo!");']) . ' '.
//                        Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['grid-demo'], ['data-pjax' => 0, 'class' => 'btn btn-default', 'title' => 'Reset Grid'])
//                    ],
//                    '{export}',
//                    '{toggleData}',
//                ],
                // set export properties
                'export' => [
                    'fontAwesome' => true
                ],

                'columns' => $gridColumns,


            ]) ?>
        </div>
    </div>
</div>
<!--            Model start here-->
<div class="modal fade" id="addRowModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header no-bd">
                <h5 class="modal-title">
					<span class="fw-mediumbold">
						New
                    </span>
                    <span class="fw-light">
                        Sub Category
                    </span>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?php $form = ActiveForm::begin([
                'action' => ['edit/add-sub-category'],
                //'method' => 'get',
                'options' => ['id' => 'subcategory_form', 'class' => 'master-form']
            ]); ?>
            <div class="modal-body">
                <p class="small">Create a new row using this form, make sure you fill them all</p>
                <div class="row">


                    <div class="col-sm-12">
                        <?php echo $form->field($searchModel, 'name', [
                            'template' => '<div class="form-group form-group-default">
                                {label} {input}{error}{hint}
                                </div>'
                        ])->textInput(['placeholder' => 'Enter sub category name?']) ?>

                        <?php echo $form->field($searchModel, 'parent', [
                            'template' => '
                                <div class="form-group form-group-default">
                                    {label} {input}{error}{hint}
                                </div>'
                        ])->dropDownList(
                            $data,
                            ['prompt' => 'Select...']) ?>

                    </div>


                </div>
            </div>
            <div class="modal-footer no-bd">
                <?php echo Html::submitButton('Add', ['class' => 'btn btn-primary', 'id' => 'submit_id']) ?>
                <?php echo Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
                <?php echo Html::button('Close', ['class' => 'btn btn-danger', 'data-dismiss' => 'modal']) ?>

            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
<!--            model add row closed-->
