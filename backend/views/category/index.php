<?php
/**
 * Created by PhpStorm.
 * User: Asus
 * Date: 11-05-2016
 * Time: 11:36
 */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use Yii\bootstrap\Modal;
use kartik\editable\Editable;
use \yii\helpers\Url;
use kartik\grid\DataColumn;

$this->title = 'Category Model Settings';

?>
<div class="col-md-12">
    <div class="card">
        <div class="card-header">
            <div class="d-flex align-items-center">
                <h4 class="card-title">
                    Main Category

                </h4>
                <button class="btn btn-primary btn-round ml-auto" data-toggle="modal" data-target="#addRowModal">
                    <i class="la la-plus"></i>
                    Add Row
                </button>
            </div>
        </div>
        <div class="card-body">


            <?php
            $gridColumns = [
                // the buy_amount column configuration
                ['class' => 'kartik\grid\SerialColumn'],

                [
                    'class' => 'kartik\grid\EditableColumn',
                    'attribute' => 'name',
                    'editableOptions' => [
                        'header' => 'Name',
                        'inputType' => \kartik\editable\Editable::INPUT_TEXT,
                    ],
                    'pageSummary' => true
                ],

                [
                    'class' => 'kartik\grid\EditableColumn',
                    'attribute' => 'name',
                    'editableOptions' => [
                        'header' => 'Name VN',
                        'inputType' => \kartik\editable\Editable::INPUT_TEXT,
                    ],
                    'pageSummary' => true
                ],
                [
                    'class' => 'kartik\grid\DataColumn',
                    'attribute' => 'fa_icon',
                    'header' => Yii::t('app', 'Category Icon'),
                    'value' => function ($model) {
                        $url = Yii::$app->urlManagerFrontend->baseUrl . '/images/icon/' . $model['fa_icon'];
                        $editUrl = Url::to(['category/change-icon', 'id' => $model->id]);
                        return "<img width='50' src='$url' /> <a class='ml-2' href='$editUrl'>Change?</a> ";
                    },

                    'format' => 'raw',
                    'pageSummary' => true
                ],
                [
                    'class' => 'yii\grid\DataColumn',
                    'attribute' => 'item',
                    'header' => 'Item Listed',
                    'headerOptions' => ['style' => 'color:#337ab7'],
                    'contentOptions' => ['style' => 'color:#337ab7'],

                    // 'pageSummary'=>true
                ],
// the name column configuration
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{leadDelete}{change-icon}',
                    'header' => Yii::t('app', 'Actions'),
                    'headerOptions' => ['style' => 'color:#337ab7'],
                    'buttons' => [

                        'leadDelete' => function ($url, $model, $key) {
                            $url = 'javascript:void()';//Url::to(['controller/lead-delete', 'id' => $model->id]);
                            return Html::a('<span class="flaticon-interface-5"></span> '. Yii::t('app', 'Delete'), $url, [
                                'title' => Yii::t('app', 'delete'),
                                'class' => 'record-delete btn btn-danger btn-xs',
                                //'data-confirm' => 'Are you sure you want to delete this item?',//this for pre action confirmation popup box.
                                //'data-method'  => 'post',//this for data method [get/post]
                                'record-delete-msg' => 'Record Deleted',
                                'record-delete-status' => 'active',
                                'record-delete-child' => ($model['item'] == 0) ? 0 : 1,
                                'record-delete-url' => Url::toRoute('delete/category/' . $model->id),
                                'record-data-key' => $key,


                            ]);
                        },
                        'change-icon' => function ($url, $model, $key) {
                            Url::to(['category/change-icon', 'id' => $model->id]);
                            return Html::a('<span class="fa fa-pencil"></span> ' .Yii::t('app', 'Change Icon'), $url, [
                                'title' => Yii::t('app', 'Change Icon'),
                                'class' => 'btn btn-primary btn-xs ml-2',


                            ]);
                        },
                    ]
                ]

            ];
            ?>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                // 'filterModel' => $searchModel,
                'pjax' => true,
                'pjaxSettings' => [
                    'neverTimeout' => true,
                    'beforeGrid' => 'My fancy content before.',
                    'afterGrid' => 'My fancy content after.',
                ],

                'responsive' => true,
                'floatHeader' => false,
                'containerOptions' => ['style' => 'overflow: auto'], // only set when $responsive = false
                'headerRowOptions' => ['class' => 'kartik-sheet-style'],
                'filterRowOptions' => ['class' => 'kartik-sheet-style'],
//                'toolbar' =>  [
//                    ['content' =>
//                        Html::button('<i class="glyphicon glyphicon-plus"></i>', ['type' => 'button', 'title' => 'Add Book', 'class' => 'btn btn-success', 'onclick' => 'alert("This will launch the book creation form.\n\nDisabled for this demo!");']) . ' '.
//                        Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['grid-demo'], ['data-pjax' => 0, 'class' => 'btn btn-default', 'title' => 'Reset Grid'])
//                    ],
//                    '{export}',
//                    '{toggleData}',
//                ],
                // set export properties
                'export' => [
                    'fontAwesome' => true
                ],

                'columns' => $gridColumns,


            ]) ?>
        </div>
    </div>
</div>
<!--            Model start here-->
<div class="modal fade" id="addRowModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header no-bd">
                <h5 class="modal-title">
					<span class="fw-mediumbold">
						New
                    </span>
                    <span class="fw-light">
                        Sub Category
                    </span>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?php $form = ActiveForm::begin([
                'action' => ['edit/add-category'],
                //'method' => 'get',
                'options' => ['id' => 'subcategory_form', 'class' => 'master-form']
            ]); ?>
            <div class="modal-body">
                <p class="small">Create a new row using this form, make sure you fill them all</p>
                <div class="row">


                    <div class="col-sm-12">
                        <?php echo $form->field($searchModel, 'name', [
                            'template' => '<div class="form-group form-group-default">
                                {label} {input}{error}{hint}
                                </div>'
                        ])->textInput(['placeholder' => 'Enter sub category name?']) ?>


                        <?php echo $form->field($searchModel, 'fa_icon', [
                            'template' => '<div class="form-group form-group-default">
                                {label} {input}{error}{hint}
                                </div>'
                        ])->textInput(['placeholder' => 'Enter sub category name?']) ?>


                    </div>
                </div>
                <div class="modal-footer no-bd">
                    <?php echo Html::submitButton('Add', ['class' => 'btn btn-primary', 'id' => 'submit_id']) ?>
                    <?php echo Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
                    <?php echo Html::button('Close', ['class' => 'btn btn-danger', 'data-dismiss' => 'modal']) ?>

                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
    <!--            model add row closed-->

