<?php
/**
 * Created by PhpStorm.
 * User: Asus
 * Date: 11-05-2016
 * Time: 11:36
 */

//var_dump($model);

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use Yii\bootstrap\Modal;
use yii\bootstrap\ActiveForm;
use kartik\editable\Editable;
use \yii\helpers\Url;
use borales\extensions\phoneInput\PhoneInput;

$this->title = 'Items List';
$this->params['breadcrumbs'][] = $this->title;

?>
<input type="hidden" name="ItemsUrl" id="ItemsUrl" value="<?= \yii\helpers\Url::toRoute('Items/index') ?>">

<div class="col-md-12">
    <div class="card">
        <div class="card-header">
            <div class="d-flex align-items-center">
                <h4 class="card-title">
                    Items List
                </h4>

            </div>
        </div>
        <div class="card-body">


            <?php
            $gridColumns = [
                // the buy_amount column configuration
                ['class' => 'kartik\grid\SerialColumn'],
                ///======== Detail button section =========////

                [
                    'class' => '\kartik\grid\ExpandRowColumn',
                    'header' => 'Detail',
                    // 'headerOptions' => ['style' => 'color:#337ab7'],
                    'expandIcon' => '<span class="flaticon-download-1 text-primary"></span>',

                    'value' => function ($model, $key, $index, $column) {
                        return GridView::ROW_COLLAPSED;
                    },
                    'detailUrl' => Yii::$app->request->getBaseUrl() . '/items/view',
                ],

                ///======== Items Title section =========////
                [
                    'class' => 'kartik\grid\EditableColumn',
                    'attribute' => 'ad_title',
                    'editableOptions' => [
                        'header' => 'Edit Title',
                        'inputType' => \kartik\editable\Editable::INPUT_TEXT,
                    ],
                    'pageSummary' => true
                ],
                ///======== category section =========////
                [
                    'class' => 'kartik\grid\EditableColumn',
                    'attribute' => 'category',
                    'pageSummary' => true,
                    'editableOptions' => [
                        'header' => 'Edit Category',
                        'inputType' => Editable::INPUT_TEXT,
                    ],
                ],
                ///======== subcategory section =========////
                [
                    'class' => 'kartik\grid\EditableColumn',
                    'attribute' => 'sub_category',
                    'pageSummary' => true,
                    'editableOptions' => [
                        'header' => 'Edit Sub Category',
                        'inputType' => Editable::INPUT_TEXT,
                    ],
                ],
                ///======== Items Type section =========////
                [
                    'class' => 'kartik\grid\EditableColumn',
                    'attribute' => 'type',
                    'pageSummary' => true,

                    'editableOptions' => [
                        'header' => 'Edit Items Type',
                        'inputType' => Editable::INPUT_DROPDOWN_LIST,
                        'data' => ['professional', 'individual'], // any list of values
                    ],
                ],
                ///======== Items Type section =========////
                [
                    'class' => 'kartik\grid\EditableColumn',
                    'attribute' => 'ad_type',
                    'pageSummary' => true,

                    'editableOptions' => [
                        'header' => 'Edit Items type',
                        'inputType' => Editable::INPUT_DROPDOWN_LIST,
                        'data' => ['professional', 'individual'], // any list of values
                    ],
                ],
                ///======== Items City section =========////
                [
                    'class' => 'kartik\grid\EditableColumn',
                    'attribute' => 'city',
                    'pageSummary' => true,

                    'editableOptions' => [
                        'header' => 'Edit Items City',
                        'inputType' => Editable::INPUT_TEXT,
                    ],
                ],
                ///======== Items View section =========////
                [
                    'class' => 'kartik\grid\DataColumn',

                    'attribute' => 'view',
                    'pageSummary' => true,

                    'contentOptions' => ['style' => 'color:#337ab7'],

                ],
                ///======== Phone number section =========////
                [
                    'class' => 'kartik\grid\DataColumn',
                    'attribute' => 'user_id',
                    'pageSummary' => true,
                    'header' => 'User',
                    'headerOptions' => ['style' => 'color:#337ab7'],
                    'contentOptions' => ['style' => 'color:#337ab7'],

                    'value' => function ($model) {
                        $userInfo = \common\models\User::findIdentity($model->user_id);//($model->about_you,0,10);
                        return isset($userInfo) ? $userInfo['first_name'] . " " . $userInfo['last_name'] : "";
                    },

                ],
                ///======== Items Type section =========////
                [
                    'class' => 'kartik\grid\EditableColumn',
                    'attribute' => 'active',
                    'header' => 'Item Status',
                    'headerOptions' => ['style' => 'color:#337ab7'],
                    'pageSummary' => true,
                    'value' => function ($model) {
                        if ($model->active == 'yes') {
                            return "Active";
                        } elseif ($model->active == 'no') {
                            return "Rejected";
                        } else {
                            return "Pending";
                        }
                    },

                    'editableOptions' => [
                        'header' => 'Edit Items Status',
                        'inputType' => Editable::INPUT_DROPDOWN_LIST,
                        'placement' => \kartik\popover\PopoverX::ALIGN_LEFT,
                        'data' => ['yes' => 'yes', 'pending' => 'pending', 'no' => 'no'], // any list of values
                    ],
                ],

                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{leadDelete}',
                    'header' => 'Actions',
                    'headerOptions' => ['style' => 'color:#337ab7'],
                    'buttons' => [

                        'leadDelete' => function ($url, $model, $key) {
                            $url = 'javascript:void()';//Url::to(['controller/lead-delete', 'id' => $model->id]);
                            return Html::a('<span class="flaticon-interface-5"></span>', $url, [
                                'title' => 'delete',
                                'class' => 'record-delete',
                                //'data-confirm' => 'Are you sure you want to delete this item?',//this for pre action confirmation popup box.
                                //'data-method'  => 'post',//this for data method [get/post]
                                'record-delete-msg' => 'Record Deleted',
                                'record-delete-status' => 'active',
                                'record-delete-child' => false,
                                'record-delete-url' => Url::toRoute('delete/item/' . $model->id),
                                'record-data-key' => $key,


                            ]);
                        },
                    ]
                ]

            ];
            ?>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                // 'filterModel' => $searchModel,
                'pjax' => true,
                'pjaxSettings' => [
                    'neverTimeout' => true,
                    'beforeGrid' => 'My fancy content before.',
                    'afterGrid' => 'My fancy content after.',
                ],

                'responsive' => true,
                'floatHeader' => false,
                'containerOptions' => ['style' => 'overflow: auto'], // only set when $responsive = false
                'headerRowOptions' => ['class' => 'kartik-sheet-style'],
                'filterRowOptions' => ['class' => 'kartik-sheet-style'],
//                'toolbar' =>  [
//                    ['content' =>
//                        Html::button('<i class="glyphicon glyphicon-plus"></i>', ['type' => 'button', 'title' => 'Add Book', 'class' => 'btn btn-success', 'onclick' => 'alert("This will launch the book creation form.\n\nDisabled for this demo!");']) . ' '.
//                        Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['grid-demo'], ['data-pjax' => 0, 'class' => 'btn btn-default', 'title' => 'Reset Grid'])
//                    ],
//                    '{export}',
//                    '{toggleData}',
//                ],
                // set export properties
                'export' => [
                    'fontAwesome' => true
                ],

                'columns' => $gridColumns,


            ]) ?>
        </div>
    </div>
</div>

