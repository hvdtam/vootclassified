<?php
$this->title = $model->ad_title;

use  \common\models\Message;

\common\models\Ads::view($model->id);
$currency_default = common\models\Currency::default_currency();

$current_url = \yii\helpers\Url::current();
\yii\helpers\Url::remember($current_url, 'currency_p');

$session2 = Yii::$app->cache;
$default_selected = $session2->get('default_currency');
$default = $currency_default;

?>
<?php
if ($model->category == 'Jobs' or $model->sub_category == 'Services') {
    $view = "none";
} else {
    $view = "block";
}

?>
<!--single-page-->
<div class="row">
    <div class="col-sm-6 col-md-3">
        <div class="card card-stats card-primary card-round">
            <div class="card-body">
                <div class="row">
                    <div class="col-5">
                        <div class="icon-big text-center">
                            <i class="flaticon-users"></i>
                        </div>
                    </div>
                    <div class="col-7 col-stats">
                        <div class="numbers">
                            <p class="card-category">Visitors</p>
                            <h4 class="card-title"><?= $model->view; ?></h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3">
        <div class="card card-stats card-info card-round">
            <div class="card-body">
                <div class="row">
                    <div class="col-5">
                        <div class="icon-big text-center">
                            <i class="flaticon-interface-6"></i>
                        </div>
                    </div>
                    <div class="col-7 col-stats">
                        <div class="numbers">
                            <p class="card-category">Category</p>
                            <h4 class="card-title"><?= $model->category; ?></h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3">
        <div class="card card-stats card-success card-round">
            <div class="card-body ">
                <div class="row">
                    <div class="col-5">
                        <div class="icon-big text-center">
                            <i class="flaticon-location"></i>
                        </div>
                    </div>
                    <div class="col-7 col-stats">
                        <div class="numbers">
                            <p class="card-category">Location</p>
                            <h4 class="card-title"> <?= $model->city; ?></h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3">
        <div class="card card-stats card-secondary card-round">
            <div class="card-body ">
                <div class="row">
                    <div class="col-5">
                        <div class="icon-big text-center">
                            <i class="flaticon-success"></i>
                        </div>
                    </div>
                    <div class="col-7 col-stats">
                        <div class="numbers">
                            <p class="card-category">Active ?</p>
                            <h4 class="card-title"><?= $model->active; ?></h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="card">
    <div class="card-header">
        <h5 class="card-title">
            Ad Title <i class="flaticon-right-arrow"></i> <code><?= $model->ad_title ?></code>


        </h5>

    </div>

    <div class="card-body">

        <div class="row">
            <div class="col-12 col-md-12">
                <ul class="nav nav-pills pull-left nav-secondary nav-pills-no-bd" id="pills-tab-without-border"
                    role="tablist">
                    <li class="nav-item submenu">
                        <a class="nav-link active show" id="ads_image_tab_<?= $model->id; ?>" data-toggle="pill"
                           href="#ads_image_<?= $model->id; ?>" role="tab" aria-controls="v-pills-home-icons"
                           aria-selected="true">
                            <i class="flaticon-picture"></i>
                            Image
                        </a>
                    </li>
                    <li class="nav-item submenu">
                        <a class="nav-link" id="ads_detail_tab_<?= $model->id; ?>" data-toggle="pill"
                           href="#ads_detail_<?= $model->id; ?>" role="tab" aria-controls="v-pills-profile-icons"
                           aria-selected="false">
                            <i class="flaticon-user-4"></i>
                            Detail
                        </a>
                    </li>

                </ul>
            </div>
        </div>

        <div class="row">
            <div class="col-12 col-md-12">
                <div class="tab-content" id="v-pills-with-icon-tabContent">

                    <div class="tab-pane fade in active show" id="ads_image_<?= $model->id; ?>" role="tabpanel"
                         aria-labelledby="ads_image_tab_<?= $model->id; ?>">
                        <?php
                        $image = $model->image;
                        $imgChunck = explode(",", $image);

                        foreach ($imgChunck as $arr => $img) {
                            ?>
                            <img src="<?= Yii::$app->urlManagerFrontend->baseUrl ?>/images/item/<?= $img ?>"
                                 class="col-lg-6 img-thumbnail" alt="img"/>

                            <?php
                        }
                        ?>

                    </div>

                    <div class="tab-pane fade" id="ads_detail_<?= $model->id; ?>" role="tabpanel"
                         aria-labelledby="ads_detail_tab_<?= $model->id; ?>">
                        <table class="table">
                            <?php
                            foreach ($model as $key => $user) {
                                $restrict = ['cron_1' => '1', 'id' => '1', 'updated_at' => '1', 'image' => '1', 'status' => '1', 'lat' => '1', 'lng' => '1'];
                                if (array_key_exists($key, $restrict)) {
                                    continue;
                                }
                                ?>
                                <tr>
                                    <td>
                                        <?= $key; ?>
                                    </td>
                                    <td class="text-capitalize">
                                        <?php
                                        if ($key == "created_at") {
                                            echo date('d-m-Y', $user);
                                        } else {
                                            echo str_replace("_", " ", $user);
                                        };
                                        if ($key == "user_id ") {
                                            $us = \common\models\User::findIdentity($user);
                                            echo $us['name'];
                                        }

                                        ?>
                                    </td>

                                </tr>
                                <?php
                            }
                            ?>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


