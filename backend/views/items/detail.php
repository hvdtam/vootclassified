<?php
$this->title = $model->ad_title;

use  \common\models\Message;

\common\models\Ads::view($model->id);
$currency_default = common\models\Currency::default_currency();

$current_url = \yii\helpers\Url::current();
\yii\helpers\Url::remember($current_url, 'currency_p');

$session2 = Yii::$app->cache;
$default_selected = $session2->get('default_currency');
$default = $currency_default;

?>
<?php
if ($model->category == 'Jobs' or $model->sub_category == 'Services') {
    $view = "none";
} else {
    $view = "block";
}

?>
<!--single-page-->

<div class="col-md-12">
    <div class="row">
        <div class="col-sm-6 col-md-3">
            <div class="card card-stats card-primary card-round">
                <div class="card-body">
                    <div class="row">
                        <div class="col-5">
                            <div class="icon-big text-center">
                                <i class="flaticon-users"></i>
                            </div>
                        </div>
                        <div class="col-7 col-stats">
                            <div class="numbers">
                                <p class="card-category">Visitors</p>
                                <h4 class="card-title"><?= $model->view; ?></h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-3">
            <div class="card card-stats card-info card-round">
                <div class="card-body">
                    <div class="row">
                        <div class="col-5">
                            <div class="icon-big text-center">
                                <i class="flaticon-interface-6"></i>
                            </div>
                        </div>
                        <div class="col-7 col-stats">
                            <div class="numbers">
                                <p class="card-category">Category</p>
                                <h4 class="card-title"><?= $model->category; ?></h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-3">
            <div class="card card-stats card-success card-round">
                <div class="card-body ">
                    <div class="row">
                        <div class="col-5">
                            <div class="icon-big text-center">
                                <i class="flaticon-location"></i>
                            </div>
                        </div>
                        <div class="col-7 col-stats">
                            <div class="numbers">
                                <p class="card-category">Location</p>
                                <h4 class="card-title"> <?= $model->city; ?></h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-3">
            <div class="card card-stats card-secondary card-round">
                <div class="card-body ">
                    <div class="row">
                        <div class="col-5">
                            <div class="icon-big text-center">
                                <i class="flaticon-success"></i>
                            </div>
                        </div>
                        <div class="col-7 col-stats">
                            <div class="numbers">
                                <p class="card-category">Active ?</p>
                                <h4 class="card-title"><?= $model->active; ?></h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-header">
            <h5 class="card-title">
                Ad Title <i class="flaticon-right-arrow"></i> <code><?= $model->ad_title ?></code>


            </h5>

        </div>

        <div class="card-body">

            <div class="row">
                <div class="col-5 col-md-5">
                    <?php
                    $image = $model->image;
                    $imgChunck = explode(",", $image);

                    foreach ($imgChunck as $arr => $img) {
                        ?>
                        <img src="<?= Yii::$app->urlManagerFrontend->baseUrl ?>/images/item/<?= $img ?>"
                             class="img-thumbnail" alt="img"/>

                        <?php
                    }
                    ?>
                </div>

                <div class="col-7 col-md-7">
                    <table class="table table-bodered">
                        <?php
                        foreach ($model as $key => $user) {
                            $restrict = ['cron_1' => '1', 'id' => '1', 'updated_at' => '1', 'image' => '1', 'status' => '1', 'lat' => '1', 'lng' => '1'];
                            if (array_key_exists($key, $restrict)) {
                                continue;
                            }
                            ?>
                            <tr>
                                <td class="col-md-3" style="font-weight: 700">
                                    <?php

                                    if ($key == "user_id") {
                                        echo "Name of user";
                                    } else {
                                        echo str_replace("_", " ", $key);;
                                    }
                                    ?>
                                </td>
                                <td class="text-capitalize col-md-9" style="font-weight: 600;color: #777">
                                    <?php
                                    if ($key == "user_id") {
                                        echo \common\models\User::getNameById($user);
                                        continue;
                                    };
                                    if ($key == "created_at") {
                                        echo date('d-m-Y', $user);
                                    } else {
                                        echo str_replace("_", " ", $user);
                                    };


                                    ?>
                                </td>

                            </tr>
                            <?php
                        }
                        ?>
                    </table>
                </div>
            </div>

        </div>
    </div>

</div>


