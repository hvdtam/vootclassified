<?php

/* @var $this yii\web\View */

/* @var $form yii\bootstrap\ActiveForm */

use kartik\grid\GridView;

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\editable\Editable;
use \yii\helpers\Url;

$this->title = 'Create Fake Ads';
$this->params['breadcrumbs'][] = ['label' => 'Faker', 'url' => ['faker/index']];

$this->params['breadcrumbs'][] = $this->title;
?>
<div class="col-md-12">
    <div class="card">
        <div class="card-header">
            <div class="d-flex align-items-center">
                <h4 class="card-title">
                    Fake Ads List
                </h4>
                <a class="btn btn-primary btn-round ml-auto" href="<?= Url::toRoute('faker/ads-create') ?>">
                    <i class="la la-plus"></i>
                    Create New Fake Record
                </a>
            </div>
        </div>
        <div class="card-body">


            <?php
            $gridColumns = [
                // the buy_amount column configuration
                ['class' => 'kartik\grid\SerialColumn'],
                ///======== Detail button section =========////

                [
                    'class' => '\kartik\grid\ExpandRowColumn',
                    'header' => 'Detail',
                    // 'headerOptions' => ['style' => 'color:#337ab7'],
                    'expandIcon' => '<span class="flaticon-download-1 text-primary"></span>',

                    'value' => function ($model, $key, $index, $column) {
                        return GridView::ROW_COLLAPSED;
                    },
                    'detailUrl' => Yii::$app->request->getBaseUrl() . '/items/view',
                ],

                ///======== Ads Title section =========////
                [
                    'class' => 'kartik\grid\EditableColumn',
                    'attribute' => 'ad_title',
                    'editableOptions' => [
                        'header' => 'Edit Title',
                        'inputType' => \kartik\editable\Editable::INPUT_TEXT,
                    ],
                    'pageSummary' => true
                ],
                ///======== category section =========////
                [
                    'class' => 'kartik\grid\EditableColumn',
                    'attribute' => 'category',
                    'pageSummary' => true,
                    'editableOptions' => [
                        'header' => 'Edit Category',
                        'inputType' => Editable::INPUT_TEXT,
                    ],
                ],
                ///======== subcategory section =========////
                [
                    'class' => 'kartik\grid\EditableColumn',
                    'attribute' => 'sub_category',
                    'pageSummary' => true,
                    'editableOptions' => [
                        'header' => 'Edit Sub Category',
                        'inputType' => Editable::INPUT_TEXT,
                    ],
                ],
                ///======== ads Type section =========////
                [
                    'class' => 'kartik\grid\EditableColumn',
                    'attribute' => 'type',
                    'pageSummary' => true,

                    'editableOptions' => [
                        'header' => 'Edit Ads Type',
                        'inputType' => Editable::INPUT_DROPDOWN_LIST,
                        'data' => ['professional', 'individual'], // any list of values
                    ],
                ],
                ///======== ads Type section =========////
                [
                    'class' => 'kartik\grid\EditableColumn',
                    'attribute' => 'ad_type',
                    'pageSummary' => true,

                    'editableOptions' => [
                        'header' => 'Edit Ads type',
                        'inputType' => Editable::INPUT_DROPDOWN_LIST,
                        'data' => ['professional', 'individual'], // any list of values
                    ],
                ],
                ///======== Ads City section =========////
                [
                    'class' => 'kartik\grid\EditableColumn',
                    'attribute' => 'city',
                    'pageSummary' => true,

                    'editableOptions' => [
                        'header' => 'Edit Ads City',
                        'inputType' => Editable::INPUT_TEXT,
                    ],
                ],
                ///======== Ads View section =========////
                [
                    'class' => 'kartik\grid\DataColumn',

                    'attribute' => 'view',
                    'pageSummary' => true,

                    'contentOptions' => ['style' => 'color:#337ab7'],

                ],
                ///======== Phone number section =========////
                [
                    'class' => 'kartik\grid\DataColumn',
                    'attribute' => 'user_id',
                    'pageSummary' => true,
                    'header' => 'User',
                    'headerOptions' => ['style' => 'color:#337ab7'],
                    'contentOptions' => ['style' => 'color:#337ab7'],

                    'value' => function ($model) {
                        $userInfo = \common\models\User::findIdentity($model->user_id);//($model->about_you,0,10);
                        return $userInfo['first_name'] . " " . $userInfo['last_name'];
                    },

                ],
                ///======== ads Type section =========////
                [
                    'class' => 'kartik\grid\EditableColumn',
                    'attribute' => 'active',
                    'header' => 'Ads Status',
                    'headerOptions' => ['style' => 'color:#337ab7'],
                    'pageSummary' => true,
                    'value' => function ($model) {
                        if ($model->active = 'yes') {
                            return "Active";
                        } elseif ($model->active = 'no') {
                            return "Rejected";
                        } else {
                            return "Pending";
                        }
                    },

                    'editableOptions' => [
                        'header' => 'Edit Ads Status',
                        'inputType' => Editable::INPUT_DROPDOWN_LIST,
                        'data' => ['yes', 'pending', 'no'], // any list of values
                    ],
                ],

                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{leadDelete}',
                    'header' => 'Actions',
                    'headerOptions' => ['style' => 'color:#337ab7'],
                    'buttons' => [

                        'leadDelete' => function ($url, $model, $key) {
                            $url = 'javascript:void()';//Url::to(['controller/lead-delete', 'id' => $model->id]);
                            return Html::a('<span class="flaticon-interface-5"></span>', $url, [
                                'title' => 'delete',
                                'class' => 'record-delete',
                                //'data-confirm' => 'Are you sure you want to delete this item?',//this for pre action confirmation popup box.
                                //'data-method'  => 'post',//this for data method [get/post]
                                'record-delete-msg' => 'Record Deleted',
                                'record-delete-status' => 'active',
                                'record-delete-child' => false,
                                'record-delete-url' => Url::toRoute('delete/user/' . $model->id),
                                'record-data-key' => $key,


                            ]);
                        },
                    ]
                ]

            ];
            ?>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                // 'filterModel' => $searchModel,
                'pjax' => true,
                'pjaxSettings' => [
                    'neverTimeout' => true,
                    'beforeGrid' => 'My fancy content before.',
                    'afterGrid' => 'My fancy content after.',
                ],

                'responsive' => true,
                'floatHeader' => false,
                'containerOptions' => ['style' => 'overflow: auto'], // only set when $responsive = false
                'headerRowOptions' => ['class' => 'kartik-sheet-style'],
                'filterRowOptions' => ['class' => 'kartik-sheet-style'],
//                'toolbar' =>  [
//                    ['content' =>
//                        Html::button('<i class="glyphicon glyphicon-plus"></i>', ['type' => 'button', 'title' => 'Add Book', 'class' => 'btn btn-success', 'onclick' => 'alert("This will launch the book creation form.\n\nDisabled for this demo!");']) . ' '.
//                        Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['grid-demo'], ['data-pjax' => 0, 'class' => 'btn btn-default', 'title' => 'Reset Grid'])
//                    ],
//                    '{export}',
//                    '{toggleData}',
//                ],
                // set export properties
                'export' => [
                    'fontAwesome' => true
                ],

                'columns' => $gridColumns,


            ]) ?>
        </div>
    </div>
</div>


