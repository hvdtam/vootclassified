<?php
/**
 * Created by PhpStorm.
 * User: Mayank Singh
 * Date: 3/28/2019
 * Time: 2:15 AM
 */
foreach ($modal as $list) {
    ?>
    <li class="list-group-item p-3 border-bottom-0">
        <a href="javascript:void(0)" onclick="popSubCat_a77('<?= $list['id']; ?>','<?= $list['name']; ?>');"
           data-postForm-sub="<?= $list['name']; ?>">
            <strong> <?= $list['name']; ?></strong>
        </a>
    </li>
    <?php
}
