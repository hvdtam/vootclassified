<?php

use common\models\States;

use common\models\Cities;
use common\models\Countries;

if ($countryCount and $statesCount and $citiesCount) {
    $countryData = Countries::find()->where(['like', 'name', $city . '%', false])->all();
    $statesData = States::find()->where(['like', 'name', $city . '%', false])->all();
    $citiesData = Cities::find()->where(['like', 'name', $city . '%', false])->all();
    foreach ($countryData as $country) {
        ?>
        <li onclick="setCity('<?= $country['name']; ?>',null,null,'country','<?= $country['sortname']; ?>')"
            class="list-group-item citySet">
            <b id="cityWaitingResult" data-city="<?= $country['name']; ?>">
                <i id="<?= $country['name']; ?>" class="icon-location-2"></i>
                <?= $country['name']; ?>, All in <?= $country['name']; ?>
            </b>
        </li>
        <?php
    };
    foreach ($statesData as $states) {
        $lac = $states;
        ?>
        <li onclick="setCity('<?= Countries::GetNameById($lac['country_id']) ?>','<?= $lac['name']; ?>',null,'states','<?= Countries::GetFlagById($lac['country_id']) ?>')"
            class="list-group-item citySet">
            <b id="cityWaitingResult" data-city="<?= $lac['name']; ?>">
                <i id="<?= $lac['name']; ?>" class="icon-location-2"></i>
                <?= $lac['name']; ?>, <?= Countries::GetNameById($lac['country_id']) ?>
            </b>
        </li>
        <?php
    };
    foreach ($citiesData as $cities) {
        $lac = $cities;
        $state = States::namebyid($lac['state_id']);
        ?>
        <li onclick="setCity('<?= Countries::GetNameByStateId($lac['state_id']);; ?>','<?= $state; ?>','<?= $lac['name']; ?>','city','<?= Countries::GetFlagByStateId($lac['state_id']) ?>')"
            class="list-group-item citySet">
            <b id="cityWaitingResult" data-city="<?= $lac['name']; ?>">
                <i id="<?= $lac['name']; ?>" class="icon-location-2"></i>
                <?= $lac['name']; ?>,
                <?= $state ?>,
                <?= Countries::GetNameByStateId($lac['state_id']);; ?>
            </b>
        </li>
        <?php
    };
} elseif ($statesCount and $citiesCount) {
    $statesData = States::find()->where(['like', 'name', $city . '%', false])->all();
    $citiesData = Cities::find()->where(['like', 'name', $city . '%', false])->all();
    foreach ($statesData as $states) {
        $lac = $states;
        $country = Countries::GetNameById($lac['country_id']);
        ?>
        <li onclick="setCity('<?= $country; ?>','<?= $lac['name']; ?>',null,'states','<?= Countries::GetFlagById($lac['country_id']) ?>')"
            class="list-group-item citySet">
            <b id="cityWaitingResult" data-city="<?= $lac['name']; ?>">
                <i id="<?= $lac['name']; ?>" class="icon-location-2"></i>
                <?= $lac['name']; ?>, <?= $country ?>
            </b>
        </li>
        <?php
    };
    foreach ($citiesData as $cities) {
        $lac = $cities;
        $state = States::namebyid($lac['state_id']);
        ?>
        <li onclick="setCity('<?= Countries::GetNameByStateId($lac['state_id']);; ?>','<?= $state; ?>','<?= $lac['name']; ?>','city','<?= Countries::GetFlagByStateId($lac['state_id']) ?>')"
            class="list-group-item citySet">
            <b id="cityWaitingResult" data-city="<?= $lac['name']; ?>">
                <i id="<?= $lac['name']; ?>" class="icon-location-2"></i>
                <?= $lac['name']; ?>,
                <?= $state ?>,
                <?= Countries::GetNameByStateId($lac['state_id']);; ?>
            </b>
        </li>
        <?php
    };
} elseif ($citiesCount) {
    $citiesData = Cities::find()->where(['like', 'name', $city . '%', false])->all();

    foreach ($citiesData as $cities) {
        $lac = $cities;
        $state = States::namebyid($lac['state_id']);
        ?>
        <li onclick="setCity('<?= Countries::GetNameByStateId($lac['state_id']);; ?>','<?= $state; ?>','<?= $lac['name']; ?>','city','<?= Countries::GetFlagByStateId($lac['state_id']) ?>')"
            class="list-group-item citySet">
            <b id="cityWaitingResult" data-city="<?= $lac['name']; ?>">
                <i id="<?= $lac['name']; ?>" class="icon-location-2"></i>
                <?= $lac['name']; ?>,
                <?= $state ?>,
                <?= Countries::GetNameByStateId($lac['state_id']);; ?>
            </b>
        </li>
        <?php
    };
} else {
    return "0";
}