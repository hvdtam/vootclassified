<?php
/**
 * Created by PhpStorm.
 * User: Mayank Singh
 * Date: 3/9/2019
 * Time: 9:21 PM
 */
?>

<ul class="list-group list-group-flush">
    <?php
    foreach ($category as $list) {
        $name = $list['name'];
        $url = \yii\helpers\Url::to(['ads/listing', 'cat' => $list['name'], 'location' => $default['city']]);
        $icon_padding = "22px";
        $font_size = "20px";
        $font_weight = "900";
        $marginRight = "10px";
        $icon_font_color = "#555";
        $icon = $list['fa_icon'];
        echo $list = '<li class="list-group-item p-3">
                    <a href="' . $url . '" class="light-text-1 text-uppercase">
                        <i class="' . $icon . '" style="font-size:' . $font_size . '; font-weight : ' . $font_weight . ';margin-right :' . $marginRight . ' "></i> ' . $name . ' - <small>( in  Category)</small>
                    </a>
                </li>';
        //  echo "<li class='list-group-item'>";
        // echo "in category ".$list['name'];
        //   echo "</li>";
    }
    foreach ($subcategory as $list) {
        $name = $list['name'];
        $parent = \common\models\SubCategory::findParent($name);
        $url = \yii\helpers\Url::to(['ads/listing', 'cat' => $parent, 'sub_cat' => $name, 'location' => $default['city']]);
        $filtrUrlFind = array('/\and/', '/\s+/', '/\?+/', '/\'/');
        $filtrUrlReplace = array('&', '-', '-');
        $name = preg_replace($filtrUrlFind, $filtrUrlReplace, $name);
        echo $list = '<li class="list-group-item p-3">
                    <a href="' . $url . '" class="light-text-1 text-uppercase">
                         ' . $name . ' - <small>( in  Sub Category)</small>
                    </a>
                </li>';
    }
    foreach ($type as $list) {
        $name = $list['name'];
        $parent = \common\models\SubCategory::findName($list['parent']);
        $url = \yii\helpers\Url::to(['ads/listing', 'type' => $name, 'sub_cat' => $parent, 'location' => $default['city']]);

        $filtrUrlFind = array('/\and/', '/\s+/', '/\?+/', '/\'/');
        $filtrUrlReplace = array('&', '-', '-');
        $name = preg_replace($filtrUrlFind, $filtrUrlReplace, $name);
        echo $list = '<li class="list-group-item p-3">
                    <a href="' . $url . '" class="light-text-1 text-uppercase">
                         ' . $name . ' - <small>( in  Brand / Type)</small>
                    </a>
                </li>';

    }
    ?>
    <?php if ($model == NULL && $type == NULL && $subcategory == NULL && $category == NULL): ?>
        <li class="list-group-item p-3">
            <?= Yii::t('home', 'Sorry...No Result Found..'); ?>
        </li>
    <?php endif ?>
</ul>

<ul class="media-list p-2 bg-white">
    <?php if ($model): ?>
        <?php foreach ($model as $list): ?>
            <?php

            $filtrUrlFind = array('/\,/', '/\s+/', '/\?+/', '/\'/');
            $filtrUrlReplace = array('-', '-', '-');
            $item = new \frontend\models\TemplatesDesign();
            $item->temp = "item";
            $item->detailUrl = \yii\helpers\Url::to([
                'ads/detail',
                'title' => preg_replace($filtrUrlFind, $filtrUrlReplace, $list['ad_title']),
                'IID' => $list['id']
            ]);
            $item->item($list);
            ?>
        <?php endforeach; ?>
    <?php endif ?>

</ul>