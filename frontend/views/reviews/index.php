<?php
$this->title = "Write a Review";
?>
<div class="card">
    <div class="card-header">
        Reviews
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-lg-8">

                <h4>Average user rating</h4>
                <h2 class="bold pb-0"><?= $model['rating']; ?> <small>/ 5</small></h2>
                <p>
                    <?php
                    echo \yii2mod\rating\StarRating::widget([
                        'name' => 'input_name',
                        'value' => $model['rating'],
//                                    'clientOptions' => [
//                                        // Your client options
//                                    ],
                    ]);
                    ?>
                    <?= $model['rating']; ?> out of 5 stars
                </p>
                <p>
                    Based on <?= $model['reviews']; ?> reviews
                </p>

            </div>
            <div class="col-lg-4">

                <?php
                $filtrUrlFind = array('/\,/', '/\s+/', '/\?+/', '/\'/');
                $filtrUrlReplace = array('_', '_', '_');

                $reviewUrl = \yii\helpers\Url::to([
                    'reviews/write',
                    'title' => trim(preg_replace($filtrUrlFind, $filtrUrlReplace, $model['ad_title'])),
                    'token' => $model['id']
                ]);
                ?>
                <a href="<?= $reviewUrl; ?>" class="btn btn-outline-primary">
                    Write a Review
                </a>
            </div>

            <div class="col-12 border-top">

                <ul class="media-list">
                    <?php
                    foreach ($reviews as $review) {
                        ?>
                        <li class="media border-bottom mb-2">
                            <div class="pull-left p-2">
                                <img class="media-object " src="https://www.tutorialspoint.com/latest/Angular-4.png"
                                     alt="Generic placeholder image">
                            </div>
                            <div class="media-body">
                                <h4 class="media-heading pb-0"><?= $review['name'] ?></h4>
                                <h6 class=" pb-1">
                                    <?php
                                    echo \yii2mod\rating\StarRating::widget([
                                        'name' => 'input_name',
                                        'value' => $review['rating'],
                                        'clientOptions' => [
                                            // Your client options
                                        ],
                                    ]);
                                    ?>
                                </h6>
                                <p class="mt-0">
                                    <?= $review['comment'] ?>
                                </p>
                                <small>
                                    <?= date("d/m/Y", $review['created_at']) ?>
                                </small>
                            </div>
                        </li>
                        <hr>
                        <?php
                    }
                    ?>
                </ul>
            </div>
        </div>


    </div>
</div>
