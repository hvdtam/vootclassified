<?php

use \yii\bootstrap\ActiveForm;

$reviewCount = $model['reviews'];
$reviewForm = new \common\models\CommentReply();
?>
<h2 class="page-header">
    <?= Yii::t('app', 'Reviews') ?>
</h2>
<div class="row">
    <div class="col-lg-8">

        <?php
        if ($reviewCount > 0) {
            ?>
            <h4><?= Yii::t('app', 'Average user rating') ?></h4>
            <h2 class="bold pb-0"><?= $model['rating']; ?> <small>/ 5</small></h2>
            <p>
                <?php
                echo \yii2mod\rating\StarRating::widget([
                    'name' => 'input_name',
                    'value' => $model['rating'],
//                                    'clientOptions' => [
//                                        // Your client options
//                                    ],
                ]);
                ?>
                <?= $model['rating']; ?> <?= Yii::t('app', 'out of 5 stars') ?>
            </p>
            <p>
                <?= Yii::t('app', 'Based on ') ?><?= $model['reviews']; ?> <?= Yii::t('app', 'Reviews') ?>
            </p>
            <?php
        } else {
            ?>
            <h4><?= Yii::t('app', 'Write a review') ?></h4>
            <p>
                <?= Yii::t('app', 'be the first person for write a review') ?>

            </p>
            <?php
        }
        ?>


    </div>
    <div class="col-lg-4">

        <?php
        $filtrUrlFind = array('/\,/', '/\s+/', '/\?+/', '/\'/');
        $filtrUrlReplace = array('_', '_', '_');

        $reviewUrl = \yii\helpers\Url::to([
            'reviews/write',
            'title' => trim(preg_replace($filtrUrlFind, $filtrUrlReplace, $model['ad_title'])),
            'token' => $model['id']
        ]);
        ?>
        <a href="<?= $reviewUrl; ?>" class="btn btn-outline-primary">
            <?= Yii::t('app', ' Write a Review') ?>
        </a>
    </div>

    <div class="col-12 border-top">

        <ul class="media-list">
            <?php
            foreach ($reviews as $review) {
                $ReviewComment = \common\models\CommentReply::find()->where(['comment_id' => $review['id']])->all();
                $id = $review['id'];


                ?>
                <li class="media border-bottom mb-2">
                    <div class="pull-left p-2">
                        <img style="width: 50px" class="media-object w100"
                             src="<?= Yii::getAlias('@web') ?>/images/users/<?= $review['image'] ?>"
                             alt="Generic placeholder image">
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading pb-0"><?= $review['name'] ?></h4>
                        <h6 class=" pb-1">
                            <?php
                            echo \yii2mod\rating\StarRating::widget([
                                'name' => 'input_name',
                                'value' => $review['rating'],
                                'clientOptions' => [
                                    // Your client options
                                ],
                            ]);
                            ?>
                        </h6>
                        <p class="pb-1 m-0">
                            <?= $review['comment'] ?>
                        </p>
                        <small>
                            <?= date("d/m/Y", $review['created_at']) ?>
                        </small>

                        <ul class="media-list p-2 bg-light">
                            <?php
                            if ($ReviewComment) {
                                foreach ($ReviewComment as $commentr) {
                                    $comment = $commentr['comment'];
                                    $time = date("d/m/Y", $commentr['created_at']);
                                    ?>
                                    <li class="media">
                                        <div class="media-body">
                                            <h4 class="media-heading pb-0"><i class="fa fa-user"></i> Seller</h4>
                                            <small>
                                                <strong>
                                                    <i class="fa fa-clock-o"></i> <?= $time; ?>
                                                </strong>
                                            </small>
                                            <p>
                                                <?= $comment; ?>
                                            </p>
                                        </div>

                                    </li>
                                    <?php
                                }
                            }
                            ?>
                        </ul>
                        <div class="d-block mt-2">
                            <a data-toggle="collapse" href="#collapseExample_<?= $id; ?>" role="button"
                               aria-expanded="false" aria-controls="collapseExample" class="btn btn-md btn-secondary">
                                Reply
                            </a>
                            <div class="form-control-feedback collapse" id="collapseExample_<?= $id; ?>">
                                <?php $form = ActiveForm::begin([
                                    'action' => ['reviews/reply'],
                                    //'method' => 'get',
                                    'options' => ['id' => 'comment_reply_form', 'class' => 'frontend-master-form']
                                ]); ?>

                                <?= $form->field($reviewForm, 'comment_id')->hiddenInput(['value' => $id])->label(false) ?>
                                <?= $form->field($reviewForm, 'comment')->textInput(['placeholder' => 'Write your comment'])->label("Your Reply") ?>


                                <?php echo \yii\helpers\Html::submitButton('Submit', ['class' => 'btn btn-success ']) ?>

                                <?php ActiveForm::end(); ?>
                            </div>
                        </div>
                    </div>

                </li>
                <hr>
                <?php
            }
            ?>
        </ul>

        <?php
        if ($reviewCount == 0) {
            echo "<div class='p-3 text-center text-secondary bg-grey'><h3>" . Yii::t('app', 'This Listing has no review yet') . "</h3></div>";
        }
        ?>
        <div class="post-promo text-center">
            <h2> <?= Yii::t('app', 'Any opinion about this listing') ?> </h2>
            <h5><?= Yii::t('app', 'Write a review about this listing, and help other people to choose right one'); ?></h5>
            <a href="<?= $reviewUrl; ?>" class="btn btn-lg btn-border btn-post btn-danger">
                <?= Yii::t('app', ' Write a Review') ?>
            </a>
        </div>
    </div>
</div>
