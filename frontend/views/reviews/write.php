<?php

use \yii\bootstrap\ActiveForm;
use yii\captcha\Captcha; ?>
<div class="container mt-5">
    <div class="row">
        <div class="col-md-9 page-content col-thin-right">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                            <img onerror="this.src='<?= Yii::getAlias('@web') ?>/images/users/profile-placeholder.jpg'"
                                 src="<?= Yii::getAlias('@web') ?>/images/logo/<?= $model['image'] ?>" class=""
                                 alt="<?= $model['ad_title'] ?>">
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <h2> <?= $model->ad_title; ?>
                                <small class="label label-default adlistingtype"><?= $model->ad_type; ?></small>
                            </h2>
                            <span class="info-row">
                    <span class="date">
                        <i class=" icon-clock"></i>
                        <?= date('d/m/Y', $model->created_at) ?>
                        <small>
                            (
                            <?= \common\models\Analytic::time_elapsed_string($model->created_at) ?>
                            <?= Yii::t('app', 'Ago') ?>
                            )
                        </small>
                    </span> -
                    <span class="category"><?= $model->category; ?> </span> -
                    <span class="item-location">
                        <i class="fa fa-map-marker"></i>
                        <?= $model->city; ?>
                    </span>
                </span>

                            <ul class="list-unstyled">
                                <li class="pt-2 text-secondary">
                                    <i class="fa fa-phone"></i>
                                    <?= Yii::t('app', $model['mobile']); ?>
                                </li>
                                <li class="pt-2 text-secondary">
                                    <i class="fa fa-envelope"></i>
                                    <?= Yii::t('app', $model['email']); ?>
                                </li>
                            </ul>
                        </div>
                    </div>


                </div>
            </div>
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">
                        Write a review for : <?= $model['ad_title']; ?>
                    </h4>
                </div>


                <?php $form = ActiveForm::begin() ?>

                <div class="card-body">

                    <?= $form->field($WriteReview, 'ad_id')->hiddenInput(['value' => $model['id']])->label(false) ?>

                    <?= $form->field($WriteReview, 'comment')->textarea(['rows' => 6, 'style' => 'height:150px']) ?>
                    <?php
                    echo $form->field($WriteReview, 'rating')->widget(\yii2mod\rating\StarRating::class, [
                        'options' => [
                            'cancel' => false,
                            'hints' => ['bad', 'poor', 'regular', 'good', 'gorgeous'],
                        ],
                        'clientOptions' => [
                            'cancel' => false,
                            'hints' => ['bad', 'poor', 'regular', 'good', 'gorgeous'],
                        ],
                    ]);
                    ?>

                </div>
                <div class="card-footer">
                    <?php echo \yii\helpers\Html::submitButton('Submit', ['class' => 'btn btn-success ']) ?>

                </div>
                <?php ActiveForm::end(); ?>


            </div>

        </div>


    </div>

</div>
