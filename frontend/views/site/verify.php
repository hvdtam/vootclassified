<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Verify panel';
$siteSetting = \common\models\SiteSettings::find()->one();

//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="main-container">
    <div class="container">
        <div class="row">
            <div class="col-sm-5 login-box">

                <div id="loginBoxForm" class="card card-default ">
                    <div class="panel-intro text-center">
                        <img src="<?= Yii::getAlias('@web/images/site/logo/' . $siteSetting['logo']) ?>" width="220px">
                        <span style="display: block;padding: 15px;font-size: 16px;color: #777" ">
                        Verify Buyer panel
                        </span>

                        <p class="title">
                        </p>
                        <?php
                        if ($msg) {
                            echo "<span style='color: red'>" . $msg . "</span>";
                        }
                        ?>

                    </div>

                    <div class="card-body">


                        <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

                        <?= $form->field($model, 'itemId')->label("Codecanyon item id"); ?>

                        <?= $form->field($model, 'purchaseCode') ?>
                        <?= $form->field($model, 'username')->label("Codecanyon Username") ?>
                        <?= $form->field($model, 'email')->label("Your Email") ?>

                        <div class="form-group">
                            <?= Html::submitButton('Verify', ['class' => 'btn btn-warning', 'name' => 'login-button']) ?>
                        </div>

                        <?php ActiveForm::end(); ?>
                    </div>
                    <div class="card-footer">

                        <div class="new_people">
                            <span style="color: #555">
                                I have OTP,
                            </span>
                            <a class="btn btn-link" href="<?= \yii\helpers\Url::toRoute('site/verify-me') ?>"> verify
                                now!</a>
                        </div>
                    </div>
                </div>
                <div class="login-box-btm text-center">
                    <p> Don't have an account? <br>

                        <a href="<?= \yii\helpers\Url::toRoute('site/signup') ?>"><strong>Sign Up !</strong> </a></p>
                </div>

            </div>
        </div>
    </div>
</div>


