<?php

/* @var $this yii\web\View */
/* @var \common\models\Category $category */


\yii\helpers\Url::remember(\yii\helpers\Url::current(), 'backToResult');

?>

<!-- Its Theme Control Settings only For Demo Please remove this code after purchase-->
<?php
if (Yii::$app->params['demo']) {
    ?>
    <div class="themeControll ">

        <h3 style=" color:#fff; font-size: 10px; line-height: 12px;" class="uppercase color-white text-center">
            <a target="_blank" href="#">
                Quick Settings
            </a>
        </h3>

        <div class="themeConntrolContent linkinner linkScroll scrollbar" style="overflow-y: scroll">
            <div class="card-body2">
                <h3 class="themecontrolTitle">
                    Change Background
                </h3>
                <select class="form-control" name="background" id="background">
                    <option value="bg1.jpg">background One</option>
                    <option value="bg2.jpg">background Two</option>
                    <option value="bg3.jpg">background Three</option>
                    <option value="bg4.jpg">background Four</option>
                    <option value="bg5.jpg">background Five</option>
                    <option value="bg6.jpg">background Six</option>
                    <option value="bg7.jpg">background Seven</option>
                    <option value="bg8.jpg">background Eight</option>
                    <option value="bg9.jpg">background Nine</option>

                </select>
                <!-- Theme Color Selection-->
                <h3 class="themecontrolTitle">
                    Color themes
                </h3>
                <div>
                    <span class="colorBlock" onclick="changeTheme('style.css')" title="hello"
                          style="background-color: #e55231;color: #fff">
                        <i class="fa fa-check"></i> &nbsp;
                    </span>
                    <span class="colorBlock" onclick="changeTheme('style_old.css')" data-original-title="Classic"
                          data-toggle="tooltip" data-placement="top" style="background-color: #16a085;color: #fff">
                        <i class="fa fa-check"></i> &nbsp;
                    </span>
                    <span class="colorBlock" onclick="changeTheme('style_two.css')" data-original-title="Awesome Blue"
                          data-toggle="tooltip" data-placement="top" style="background-color: #5984c7;color: #f6ea06">
                        <i class="fa fa-check"></i> &nbsp;
                    </span>
                    <span class="colorBlock" onclick="changeTheme('style_one.css')" data-original-title="Regular Blue"
                          data-toggle="tooltip" data-placement="top" style="background-color: #5984c7;color: #b3b3b3">
                        <i class="fa fa-check"></i> &nbsp;
                    </span>
                    <span class="colorBlock" onclick="changeTheme('style_three.css')"
                          data-original-title="Regular Yellow" data-toggle="tooltip" data-placement="top"
                          style="background-color: #f3e806;color: #363636">
                        <i class="fa fa-check"></i> &nbsp;
                    </span>
                    <span class="colorBlock" onclick="changeTheme('style_four.css')" data-original-title="Black Red"
                          data-toggle="tooltip" data-placement="top" style="background-color: #f6333b;color: #555555">
                        <i class="fa fa-check"></i> &nbsp;
                    </span>
                </div>
                <!-- Theme Color Selection end-->

                <!-- Display Widgets Settings-->

                <h3 class="themecontrolTitle">
                    Display Widget Demo
                </h3>
                <div style="display: block">
                    <div class="row">
                        <div class="col-md-12">
                            <label class="thLabel">Banner Title & Color</label>

                            <input type="text" style="text-indent: 28px;font-size: 12px"
                                   value="<?= Yii::t('app', $bannerOption['title']); ?>" id="BannerInputTitle"
                                   class="form-control">
                            <input type="color" banner-title-color="" value="<?= $bannerOption['title_color']; ?>"
                                   name="titleColor" id="BannerTitleColor" class="colorInput">

                        </div>


                        <div class="col-md-12" style="margin-top: 20px;">

                            <label class="thLabel">Banner Sub Title & Color</label>

                            <input type="text" style="text-indent: 28px;font-size: 12px"
                                   value="<?= Yii::t('app', $bannerOption['tag line']); ?>" id="BannerInputTagline"
                                   class="form-control">
                            <input type="color" banner-tag-color="" value="<?= $bannerOption['tag_color']; ?>"
                                   name="tagColor" id="BannerTagColor" class="colorInput">

                        </div>

                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-12">
                            <label class="thLabel">Banner Height</label>
                            <input type="text" class="form-control" id="BannerInputHeight"
                                   value="<?= $bannerOption['height']; ?>" name="bannerheight">
                            <small> use px after height, example: 350px</small>
                        </div>
                        <div class="col-md-6">
                            <label class="thLabel">Banner Position</label>

                            <select class="form-control" name="BannerPositions" id="BannerInputPositions">
                                <option value="top">Top</option>
                                <option value="center">Center</option>
                                <option value="bottom">bottom</option>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label class="thLabel">Background</label>

                            <select class="form-control" name="bg" id="BannerInputImage">
                                <option value="bg0.jpg">Image Zero</option>
                                <option value="bg1.jpg">Image One</option>
                                <option value="bg2.jpg">Image Two</option>
                                <option value="bg3.jpg">Image Three</option>
                                <option value="bg4.jpg">Image Four</option>
                                <option value="bg5.jpg">Image Five</option>
                                <option value="bg6.jpg">Image Six</option>
                                <option value="bg7.jpg">Image seven</option>
                                <option value="bg8.jpg">Image eight</option>
                                <option value="bg9.jpg">Image nine</option>
                                <option value="bg10.jpg">Image 10</option>
                                <option value="bg11.jpg">Image 11</option>
                                <option value="bg12.jpg">Image 12</option>
                                <option value="bg13.jpg">Image 13</option>
                                <option value="bg14.jpg">Image 14</option>
                                <option value="bg15.jpg">Image 15</option>
                                <option value="bg16.jpg">Image 16</option>
                                <option value="bg17.jpg">Image 17</option>
                                <option value="bg18.jpg">Image 18</option>
                                <option value="bg19.jpg">Image 19</option>
                                <option value="bg20.jpg">Image 20</option>


                            </select>

                        </div>
                    </div>
                    <br>
                </div>

                <!-- Display Widgets Settings end-->

            </div>
        </div>

        <p class="tbtn"><i class="fa fa-angle-double-left"></i></p>
    </div>
    <?php
}
?>

<!-- Its Theme Control Settings ( above ) only For Demo Please remove this code after purchase-->
<?= $this->render('_search', ['default' => $default]) ?>
<!-- /.intro -->
<div class="main-container mt-0 p-0">
    <div class="container-fluid p-0">
        <div class="row no-gutters">
            <div class="col">
                <div class="position-sticky  pr-2" style="top: 60px">
                    <?= \common\models\Adsense::show('left') ?>
                </div>
            </div>
            <div class="col-9 pt-3" style="padding: 0px 7px;;">
                <?= \frontend\models\Widgets::full_top_panel("home_page", $category) ?>

                <div class="row">
                    <div class="col-md-9 page-content col-thin-right">
                        <?= \frontend\models\Widgets::mid_right_panel("home_page", $category) ?>
                    </div>
                    <div class="col-md-3 page-sidebar col-thin-left">
                        <aside>
                            <div class="inner-box hidden">
                                <h2 class="title-2"><?= Yii::t('home', 'Popular Categories') ?> </h2>

                                <div class="inner-box-content">
                                    <ul class="cat-list arrow">
                                        <?php
                                        $modal = \common\models\Category::find()->where(['!=', 'item', '0'])->orderBy(['item' => SORT_DESC])->all();
                                        foreach ($modal as $list) {
                                            ?>
                                            <li>
                                                <a href="<?= \yii\helpers\Url::toRoute('ads/listing?category=' . $list['name']) ?>">
                                                    <?= Yii::t('category', $list['name']) ?>
                                                    (<?= $list['item'] ?> )
                                                </a>
                                            </li>
                                            <?php
                                        }
                                        ?>
                                    </ul>
                                </div>
                            </div>
                            <?= \frontend\models\Widgets::mid_left_panel("home_page", $category) ?>
                        </aside>
                    </div>
                </div>
                <?php
                $widgets->for = "64758";
                $widgets->get();
                ?>
                <div align="center">
                    <?= \common\models\Adsense::show('bottom') ?>
                </div>

            </div>
            <div class="col">
                <div class="position-sticky  pl-2" style="top: 60px">
                    <?= \common\models\Adsense::show('right') ?>
                </div>
            </div>

        </div>
    </div>
</div>
<!-- /.main-container -->