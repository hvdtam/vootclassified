<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use borales\extensions\phoneInput\PhoneInput;

$this->title = 'Signup';
//$this->params['breadcrumbs'][] = $this->title;
?>
<!-- /.main-container -->

<div class="main-container">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-lg-8 col-sm-12 col-xs-12 page-content">
                <div class="inner-box category-content">
                    <h2 class="title-2"><i
                                class="icon-user-add"></i><?= Yii::t('app', 'Create your account, Its free'); ?> </h2>

                    <div class="row">
                        <div class="col-sm-12" align="left">
                            <?php
                            $form = ActiveForm::begin([
                                'id' => 'signup-form',
                                'layout' => 'horizontal',
                                'fieldConfig' => [
                                    'options' => ['class' => 'form-group row', 'style' => 'margin-bottom: 0rem'],
                                    // 'template' => "{label}\n <div class='col-lg-6 col-sm-12 col-xs-12' style='display: inline-block;'>\n{input}\n{hint}\n{error}\n.</div>",
                                    'horizontalCssClasses' => [
                                        'label' => 'col-sm-4',
                                        'offset' => 'col-sm-offset-4',
                                        'wrapper' => 'col-lg-6 col-sm-12',
                                        'error' => '',
                                        'hint' => '',
                                    ],
                                ],
                            ]);
                            ?>


                            <?= $form->field($model, 'type')->radioList(
                                array('professional' => 'Professional', 'individual' => 'Individual')
                            )->label('You are a');
                            ?>
                            <?= $form->field($model, 'first_name')->textInput(['autofocus' => false]) ?>
                            <?= $form->field($model, 'last_name')->textInput(['autofocus' => false]) ?>
                            <?= $form->field($model, 'phone_number')->widget(PhoneInput::className(), [
                                'jsOptions' => [
                                    'preferredCountries' => ['in', 'us', 'au', 'uk'],
                                    'nationalMode' => false
                                ]
                            ]); ?>
                            <?= $form->field($model, 'about_you')->textarea(['row' => 5, 'placeholder' => 'About You..']) ?>

                            <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>

                            <?= $form->field($model, 'email') ?>

                            <?= $form->field($model, 'password')->passwordInput() ?>

                            <div class="form-group row">


                                <div class="col-md-12">
                                    <?php
                                    $rechapcha = \common\models\ApiKeys::find()->where(['id' => 6])->andWhere(['status' => 'enable'])->one();
                                    if ($rechapcha) {
                                        if ($rechapcha['clientSecret'] != '') {
                                            echo $form->field($model, 'reCaptcha')->widget(
                                                \himiklab\yii2\recaptcha\ReCaptcha::className(),
                                                ['siteKey' => $rechapcha['clientSecret']]
                                            );
                                        } else {
                                            echo "<div class='alert alert-danger'>Please Enter client Secret Key for enable Recapcha</div>";
                                        }
                                    }


                                    ?>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-4 control-label"></label>


                                <div class="col-md-8">
                                    <div class="termbox mb10">
                                        <label class="custom-control custom-checkbox mb-2 mr-sm-2 mb-sm-0">
                                            <input type="checkbox" class="custom-control-input">
                                            <span class="custom-control-indicator"></span>
                                            <span class="custom-control-description">
                                                <?= Yii::t('app', 'I have read and agree to the'); ?>
                                                <a href="<?= \yii\helpers\Url::toRoute('pages/Mw==-Term & Conditions') ?>">
                                                    <?= Yii::t('app', 'Terms & Conditions'); ?>
                                                </a>
                                            </span>
                                        </label>
                                    </div>
                                    <div style="clear:both"></div>
                                    <?= Html::submitButton('Register', ['class' => 'btn btn-primary', 'name' => 'Register']) ?>
                                </div>
                            </div>


                            <?php ActiveForm::end(); ?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.page-content -->

            <div class="col-md-4 col-lg-4 hidden-xs hidden-sm reg-sidebar">
                <div class="reg-sidebar-inner text-center">
                    <div class="promo-text-box"><i class=" icon-picture fa fa-4x icon-color-1"></i>

                        <h3><strong><?= Yii::t('app', 'Post a Free Classified'); ?></strong></h3>

                        <p><?= Yii::t('app', 'Post your free online classified ads with us. Lorem ipsum dolor sit amet, consectetur
                            adipiscing elit.'); ?>  </p>
                    </div>
                    <div class="promo-text-box"><i class=" icon-pencil-circled fa fa-4x icon-color-2"></i>

                        <h3><strong><?= Yii::t('app', 'Create and Manage Items'); ?></strong></h3>

                        <p>

                            <?= Yii::t('app', 'Nam sit amet dui vel orci venenatis ullamcorper eget in lacus.
                            Praesent tristique elit pharetra magna efficitur laoreet.'); ?>
                        </p>
                    </div>
                    <div class="promo-text-box"><i class="  icon-heart-2 fa fa-4x icon-color-3"></i>

                        <h3><strong><?= Yii::t('app', 'Create your Favorite ads list.'); ?></strong></h3>

                        <p><?= Yii::t('app', 'PostNullam quis orci ut ipsum mollis malesuada varius eget metus.
                            Nulla aliquet dui sed quam iaculis, ut finibus massa tincidunt.'); ?> </p>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->
</div>
<!-- /.main-container -->
