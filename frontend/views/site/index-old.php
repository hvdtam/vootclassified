<?php

use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
$banner = \common\models\Widgets::find()->where(['id' => '6'])->one();
$bannerImage = $banner['template'];
$bannerOption = json_decode($banner['options'], true);
$site = \common\models\SiteSettings::find()->select(['site_title'])->one();
//$this->title = Yii::t('site_title',  $site['site_title']);
\yii\helpers\Url::remember(\yii\helpers\Url::current(), 'backToResult');
$default = \common\models\DefaultSetting::getDefaultSetting();


?>

<!-- Its Theme Control Settings only For Demo Please remove this code after purchase-->
<?php
if (Yii::$app->params['demo']) {
    ?>
    <div class="themeControll ">

        <h3 style=" color:#fff; font-size: 10px; line-height: 12px;" class="uppercase color-white text-center">
            <a target="_blank" href="#">
                Quick Settings
            </a>
        </h3>

        <div class="themeConntrolContent linkinner linkScroll scrollbar" style="overflow-y: scroll">
            <div class="card-body2">
                <h3 class="themecontrolTitle">
                    Change Background
                </h3>
                <select class="form-control" name="background" id="background">
                    <option value="bg1.jpg">background One</option>
                    <option value="bg2.jpg">background Two</option>
                    <option value="bg3.jpg">background Three</option>
                    <option value="bg4.jpg">background Four</option>
                    <option value="bg5.jpg">background Five</option>
                    <option value="bg6.jpg">background Six</option>
                    <option value="bg7.jpg">background Seven</option>
                    <option value="bg8.jpg">background Eight</option>
                    <option value="bg9.jpg">background Nine</option>

                </select>
                <!-- Theme Color Selection-->
                <h3 class="themecontrolTitle">
                    Color themes
                </h3>
                <div>
                    <span class="colorBlock" onclick="changeTheme('style.css')" title="hello"
                          style="background-color: #e55231;color: #fff">
                        <i class="fa fa-check"></i> &nbsp;
                    </span>
                    <span class="colorBlock" onclick="changeTheme('style_old.css')" data-original-title="Classic"
                          data-toggle="tooltip" data-placement="top" style="background-color: #16a085;color: #fff">
                        <i class="fa fa-check"></i> &nbsp;
                    </span>
                    <span class="colorBlock" onclick="changeTheme('style_two.css')" data-original-title="Awesome Blue"
                          data-toggle="tooltip" data-placement="top" style="background-color: #5984c7;color: #f6ea06">
                        <i class="fa fa-check"></i> &nbsp;
                    </span>
                    <span class="colorBlock" onclick="changeTheme('style_one.css')" data-original-title="Regular Blue"
                          data-toggle="tooltip" data-placement="top" style="background-color: #5984c7;color: #b3b3b3">
                        <i class="fa fa-check"></i> &nbsp;
                    </span>
                    <span class="colorBlock" onclick="changeTheme('style_three.css')"
                          data-original-title="Regular Yellow" data-toggle="tooltip" data-placement="top"
                          style="background-color: #f3e806;color: #363636">
                        <i class="fa fa-check"></i> &nbsp;
                    </span>
                    <span class="colorBlock" onclick="changeTheme('style_four.css')" data-original-title="Black Red"
                          data-toggle="tooltip" data-placement="top" style="background-color: #f6333b;color: #555555">
                        <i class="fa fa-check"></i> &nbsp;
                    </span>
                </div>
                <!-- Theme Color Selection end-->

                <!-- Display Widgets Settings-->

                <h3 class="themecontrolTitle">
                    Display Widget Demo
                </h3>
                <div style="display: block">
                    <div class="row">
                        <div class="col-md-12">
                            <label class="thLabel">Banner Title & Color</label>

                            <input type="text" style="text-indent: 28px;font-size: 12px"
                                   value="<?= Yii::t('app', $bannerOption['title']); ?>" id="BannerInputTitle"
                                   class="form-control">
                            <input type="color" banner-title-color="" value="<?= $bannerOption['title_color']; ?>"
                                   name="titleColor" id="BannerTitleColor" class="colorInput">

                        </div>


                        <div class="col-md-12" style="margin-top: 20px;">

                            <label class="thLabel">Banner Sub Title & Color</label>

                            <input type="text" style="text-indent: 28px;font-size: 12px"
                                   value="<?= Yii::t('app', $bannerOption['tag line']); ?>" id="BannerInputTagline"
                                   class="form-control">
                            <input type="color" banner-tag-color="" value="<?= $bannerOption['tag_color']; ?>"
                                   name="tagColor" id="BannerTagColor" class="colorInput">

                        </div>

                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-12">
                            <label class="thLabel">Banner Height</label>
                            <input type="text" class="form-control" id="BannerInputHeight"
                                   value="<?= $bannerOption['height']; ?>" name="bannerheight">
                            <small> use px after height, example: 350px</small>
                        </div>
                        <div class="col-md-6">
                            <label class="thLabel">Banner Position</label>

                            <select class="form-control" name="BannerPositions" id="BannerInputPositions">
                                <option value="top">Top</option>
                                <option value="center">Center</option>
                                <option value="bottom">bottom</option>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label class="thLabel">Background</label>

                            <select class="form-control" name="bg" id="BannerInputImage">
                                <option value="bg0.jpg">Image Zero</option>
                                <option value="bg1.jpg">Image One</option>
                                <option value="bg2.jpg">Image Two</option>
                                <option value="bg3.jpg">Image Three</option>
                                <option value="bg4.jpg">Image Four</option>
                                <option value="bg5.jpg">Image Five</option>
                                <option value="bg6.jpg">Image Six</option>
                                <option value="bg7.jpg">Image seven</option>
                                <option value="bg8.jpg">Image eight</option>
                                <option value="bg9.jpg">Image nine</option>
                                <option value="bg10.jpg">Image 10</option>
                                <option value="bg11.jpg">Image 11</option>
                                <option value="bg12.jpg">Image 12</option>
                                <option value="bg13.jpg">Image 13</option>
                                <option value="bg14.jpg">Image 14</option>
                                <option value="bg15.jpg">Image 15</option>
                                <option value="bg16.jpg">Image 16</option>
                                <option value="bg17.jpg">Image 17</option>
                                <option value="bg18.jpg">Image 18</option>
                                <option value="bg19.jpg">Image 19</option>
                                <option value="bg20.jpg">Image 20</option>


                            </select>

                        </div>
                    </div>
                    <br>
                </div>

                <!-- Display Widgets Settings end-->

            </div>
        </div>

        <p class="tbtn"><i class="fa fa-angle-double-left"></i></p>
    </div>
    <?php
}
?>


<!-- Its Theme Control Settings ( above ) only For Demo Please remove this code after purchase-->


<div class="intro" id="bannerFormImage" img-url="<?= Yii::getAlias('@web') . '/images/banner/' ?>"
     style="background-image:url(<?= Yii::getAlias('@web') . '/images/banner/' . $bannerImage ?>) ;background-position: center <?= $bannerOption['position'] ?>;background-repeat: no-repeat;background-size: cover;filter: blur(0px);height: <?= $bannerOption['height'] ?> ">
    <div class="dtable hw100">
        <div class="dtable-cell hw100" style="padding-top: 32px;">
            <div class="container text-center">
                <h1 class="intro-title animated fadeInDown" id="bannerFormTitle"
                    style="color: <?= $bannerOption['title_color']; ?>">
                    <?= \yii\helpers\Html::decode(Yii::t('home', $bannerOption['title'])); ?> </h1>
                <p id="bannerFormTagLine" class="sub hidden-sm hidden-xs animateme fittext3 animated fadeIn typewriter"
                   style="color: <?= $bannerOption['tag_color']; ?>">
                    <?= \yii\helpers\Html::decode(Yii::t('home', $bannerOption['tag line'])); ?>
                </p>

                <div class="row search-row animated fadeInUp hidden">
                    <input type="hidden" name="SearchForm[category]"
                           class="form-control has-icon autocomplete-category-ajax" id="autocomplete-category-ajax"
                           value="">
                    <input type="hidden" name="SearchForm[type]" class="form-control has-icon autocomplete-type-ajax"
                           id="autocomplete-type-ajax" value="cat">
                    <?php $form = ActiveForm::begin(['action' => 'ads/all', 'class' => 'banner-form', 'method' => 'GET']) ?>

                    <div class="col-xl-4 col-sm-4 search-col relative locationicon">
                        <i class="icon-location-2 icon-append "></i>
                        <select class="form-control locinput input-rel searchtag-input has-icon"
                                name="SearchForm[category]">
                            <?php
                            foreach ($category as $list) {
                                ?>
                                <option value="<?= $list['name']; ?>">Select Category</option>
                                <?php
                            }
                            ?>
                        </select>

                    </div>
                    <div class="col-xl-4 col-sm-4 search-col relative">
                        <i class="icon-docs icon-append"></i>
                        <input type="text" name="SearchForm[item]" class="form-control has-icon"
                               placeholder="<?= Yii::t('home', 'I am looking for a') ?> ..." value="">
                    </div>
                    <div class="col-xl-4 col-sm-4 search-col">
                        <button class="btn btn-primary btn-search btn-block">
                            <i class="icon-search"></i>
                            <strong><?= Yii::t('home', 'Find') ?></strong>
                        </button>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
                <?php $form = ActiveForm::begin(['action' => 'ads/all', 'class' => 'banner-form', 'method' => 'GET']) ?>
                <div class="row search-row animated fadeInUp">
                    <div class="col-xl-5 col-sm-5 search-col relative">
                        <i class="icon-map icon-append"></i>
                        <input type="hidden" name="SearchForm[category]"
                               class="form-control has-icon autocomplete-category-ajax" id="autocomplete-category-ajax"
                               value="">
                        <input type="hidden" name="SearchForm[type]"
                               class="form-control has-icon autocomplete-type-ajax" id="autocomplete-type-ajax"
                               value="">


                        <!-- /* ================================================================== */-->
                        <!--                    /* CAT LIST FOR SEARCH MENU START-->
                        <!--  /* ================================================================== */-->
                        <?= \frontend\models\TemplatesDesign::HomeCategoryMenu($category) ?>

                        <!-- /* ================================================================== */-->
                        <!--                    /* CAT LIST FOR SEARCH MENU START-->
                        <!--  /* ================================================================== */-->
                    </div>
                    <div class="col-xl-5 col-sm-5 search-col relative">
                        <i class="icon-menu icon-append"></i>
                        <input type="text" name="SearchForm[item]" class="form-control has-icon"
                               placeholder="<?= Yii::t('home', 'I am looking for a') ?> ..." value="">
                    </div>

                    <div class="col-xl-2 col-sm-3 search-col">
                        <button type="submit" class="btn btn-primary btn-search btn-block">
                            <i class="icon-search"></i>
                            <strong><?= Yii::t('home', 'Find') ?></strong>
                        </button>
                    </div>
                </div>
                <?php ActiveForm::end(); ?>
                <br><br>
                <h5>
                    <p>
                        <?= Yii::t('home', 'Where') ?> <i id="dCity2"> <?= $default['city'] ?> </i>, <a
                                href="#selectRegion" data-toggle="modal"><?= Yii::t('home', 'Change') ?>?</a>
                    </p>
                </h5>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<!-- /.intro -->


<div class="main-container">
    <div class="container">
        <?= \frontend\models\Widgets::full_top_panel("home_page", $category) ?>

        <div class="row">


            <div class="col-md-9 page-content col-thin-right">
                <?= \frontend\models\Widgets::mid_right_panel("home_page", $category) ?>
            </div>
            <div class="col-md-3 page-sidebar col-thin-left">

                <aside>
                    <div class="inner-box no-padding">
                        <div class="inner-box-content">
                            <a href="#">

                                <?= \common\models\Adsense::show('left') ?>
                            </a>
                        </div>
                    </div>
                    <div class="inner-box hidden">
                        <h2 class="title-2"><?= Yii::t('home', 'Popular Categories') ?> </h2>

                        <div class="inner-box-content">
                            <ul class="cat-list arrow">
                                <?php
                                $modal = \common\models\Category::find()->where(['!=', 'item', '0'])->orderBy(['item' => SORT_DESC])->all();
                                foreach ($modal as $list) {
                                    ?>
                                    <li>
                                        <a href="<?= \yii\helpers\Url::toRoute('ads/listing?category=' . $list['name']) ?>">
                                            <?= Yii::t('category', $list['name']) ?>
                                            (<?= $list['item'] ?> )
                                        </a>
                                    </li>
                                    <?php
                                }
                                ?>
                            </ul>
                        </div>
                    </div>
                    <?= \frontend\models\Widgets::mid_left_panel("home_page", $category) ?>
                </aside>
            </div>
        </div>


        <?php //   \frontend\models\Widgets::full_bottom_panel("home_page",$category) ?>

        <div class="row" align="center">
            <div class="col-md-12">
                <?= \common\models\Adsense::show('bottom') ?>
            </div>
        </div>

    </div>
</div>
<!-- /.main-container -->