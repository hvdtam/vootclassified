<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $model \frontend\models\PasswordResetRequestForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Request password reset';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="main-container">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 login-box">
                <div id="loginBox" class="card card-default bg-dark bg-dark bg-faded hidden" style="padding: 30px 10px">
                    <div class="card-body" align="center">
                        <p style="font-size: 60px;color: #fff">
                            <i id="login-icon" class="fa fa-circle-o-notch fa-spin"></i>
                        </p>
                        <p style="font-size: 16px;color: #fff" id="login-text">
                            Checking Login Detail. Please wait!
                        </p>
                    </div>
                </div>
                <div id="loginBoxForm" class="card card-default ">
                    <div class="panel-intro text-center">
                        <h2 class="logo-title">
                            <!-- Original Logo will be placed here  -->
                            <?= Html::encode($this->title) ?>
                        </h2>
                        <p style="padding: 10px 25px;font-size: 12px;font-weight: 600;">
                            Please fill out your email. A link to reset password will be sent there.
                        </p>
                        <hr>
                    </div>
                    <?php $form = ActiveForm::begin(['id' => 'request-password-reset-form']); ?>

                    <div class="card-body">

                        <?= $form->field($model, 'email')->textInput(['autofocus' => true]) ?>

                        <div class="form-group">
                            <?= Html::submitButton('Send', ['class' => 'btn btn-primary']) ?>
                        </div>

                    </div>
                    <?php ActiveForm::end(); ?>

                </div>


            </div>
        </div>
    </div>
</div>
