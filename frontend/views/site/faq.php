<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $model \frontend\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'FAQ';
//$this->params['breadcrumbs'][] = $this->title;
?>

<div class="main-container">
    <div class="container">
        <div class="row">

            <div class="col-md-9 page-content col-thin-right">
                <div class="card">
                    <div class="inner card-body ads-details-wrapper">
                        <h2>
                            <?= Html::encode($this->title) ?>
                        </h2>
                    </div>
                    <div class="card-body text-left">
                        <?php

                        foreach ($modal as $list) {
                            ?>
                            <div class="panel">
                                <div class="panel-heading">
                                    <h4><b>Q.</b> <?= Yii::t('app', $list['question']) ?> </h4>
                                </div>
                                <div class="panel-body">
                                    <b>A.</b><?= Yii::t('app', $list['answer']) ?>
                                </div>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                    <div class="card-footer text-left">

                    </div>
                </div>
            </div>
            <div class="col-md-3 page-content col-thin-left">
                <?= \common\models\Adsense::show('left') ?>

            </div>
        </div>
    </div>
</div>


