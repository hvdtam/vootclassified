<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */

/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;
?>
<style>
    .error-page {
        background-color: #fff;
        padding: 70px;
        margin-top: 30px;
        text-align: center;
        border: 1px solid #ddd;
        -webkit-box-shadow: 0 3px 12px rgba(0, 0, 0, .175);
        box-shadow: 0 3px 12px rgba(0, 0, 0, .175);
    }

    .error-page-content {
        max-width: 675px;
        display: inline-block;
    }

    .error-page h2 {
        font-size: 55px;
        text-transform: capitalize;
        margin-bottom: 25px;
        margin-top 25px;
        color: #555;
        line-height: 1;
    }

    .error-page-content .btn-primary {
        margin: 30px 0;
    }
</style>
<div class="main-container inner-page">
    <div class="container">
        <div class="error-page">
            <div class="error-page-content">
                <br>
                <h2><?= nl2br(Html::encode($message)) ?></h2>
                <p style="padding: 25px;color: #f6333b;font-size: 20px;">
                    <?= Html::encode($this->title) ?>
                </p>
                <p>
                    <?= Yii::t('app', 'The above error occurred while the Web server was processing your request.') ?>
                </p>
                <p>
                    <?= Yii::t('app', 'Please contact us if you think this is a server error. Thank you.') ?>

                </p>
                <a href="<?= \yii\helpers\Url::toRoute('site/index') ?>"
                   class="btn btn-success"> <?= Yii::t('app', 'Back To Home'); ?>   </a>
                <span style="padding: 0px 20px;color: #777;display: inline-block;"> -- OR -- </span>
                <a href="<?= \yii\helpers\Url::toRoute('ads/post') ?>"
                   class="btn btn-warning"> <?= Yii::t('app', 'Post Free Ad'); ?>   </a>
            </div>
        </div>
        <center>
            <div class="col-md-12 col-md-offset-2">
                <br><br><br><br>
                <?= \common\models\Adsense::show('bottom') ?>

            </div>
        </center>
    </div>

</div>

