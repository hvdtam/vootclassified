<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

//$this->params['breadcrumbs'][] = $this->title;
$siteSettings = \common\models\SiteSettings::find()->one();

?>
<div class="main-container">
    <div class="container">
        <div class="row">
            <div class="col-sm-5 login-box">
                <div id="loginBox" class="card card-default bg-light hidden bg-faded "
                     style="padding: 30px 10px;border-radius: 10px;">
                    <div class="card-body" align="center">
                        <p style="font-size: 60px;color: #888">
                            <i id="login-icon" style="color: #888" class="fa fa-circle-o-notch fa-spin"></i>
                        </p>
                        <p style="font-size: 16px;color: #555555;font-family: proxima-regular" id="login-text">
                            <?= Yii::t('app', 'Checking Login Detail. Please wait!') ?>
                        </p>
                    </div>
                </div>
                <div id="loginBoxForm" class="card card-default ">
                    <div class="panel-intro text-center">
                        <h2 class="logo-title">
                            <?= $siteSettings['site_name']; ?> - Login
                        </h2>

                    </div>
                    <div id="loginFormError" class="card-header hidden">
                        <div class="btn btn-block btn-danger">

                            <b><?= Yii::t('app', 'Error') ?>: </b> <?= Yii::t('app', 'Invalid Username / Password') ?>.
                        </div>
                    </div>

                    <div class="card-body">


                        <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
                        <input type="hidden" id="loginPath" value="<?= \yii\helpers\Url::toRoute('site/login') ?>">
                        <div class="form-group">
                            <label for="sender-email" class="control-label"><?= Yii::t('app', 'Username') ?>:</label>
                            <div class="input-icon"><i class="icon-user fa"></i>
                                <input id="login_username" required type="text" placeholder="Username"
                                       class="form-control email">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="user-pass" class="control-label"><?= Yii::t('app', 'Password') ?>:</label>

                            <div class="input-icon"><i class="icon-lock fa"></i>
                                <input type="password" required class="form-control" placeholder="Password"
                                       id="login_password">
                            </div>
                        </div>
                        <div class="form-group">
                            <button type="button"
                                    class="btn btn-success  btn-block loginSubmit"><?= Yii::t('app', 'Login') ?></button>
                        </div>
                        <p class="or" align="center"><?= Yii::t('app', 'Login With Social') ?></p>

                        <div class="col-lg-12" align="center">
                            <?= yii\authclient\widgets\AuthChoice::widget(['baseAuthUrl' => ['site/auth']]) ?>
                        </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                    <div class="card-footer">

                        <div class="checkbox pull-left">
                            <label class="custom-control custom-checkbox mb-2 mr-sm-2 mb-sm-0">
                                <input type="checkbox" class="custom-control-input">
                                <span class="custom-control-indicator"></span>
                                <span class="custom-control-description"> <?= Yii::t('app', 'Keep me logged in') ?></span>
                            </label>
                        </div>


                        <p class="text-center pull-right">
                            <?= Html::a('Lost your password?', ['site/request-password-reset']) ?>
                        </p>

                        <div style=" clear:both"></div>
                    </div>
                </div>
                <div class="login-box-btm text-center">
                    <p> <?= Yii::t('app', 'Dont have an account?') ?> <br>

                        <a href="<?= \yii\helpers\Url::toRoute('site/signup') ?>"><strong><?= Yii::t('app', 'Sign Up') ?>
                                !</strong> </a></p>
                </div>

            </div>
        </div>
    </div>
</div>
<!-- /.main-container -->
