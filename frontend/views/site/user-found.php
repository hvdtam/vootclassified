<?php
/**
 * Created by PhpStorm.
 * User: Mayank Singh
 * Date: 5/5/2019
 * Time: 1:52 PM
 */
$this->title = "user verification";
?>
<div class="row  p-5 bg-white">

    <div class="col-4  m-auto">
        <h2 class="text-center text-secondary">
            A user Found with same email address <i class="text-primary"><?= $model->email ?></i>. this is your account?
        </h2>
        <div class="card card-profile card-secondary">
            <div class="card-header bg-secondary">
                <div class="profile-picture">
                    <img onerror="this.src='<?= Yii::getAlias('@web') ?>/images/users/profile-placeholder.jpg'"
                         src="<?= Yii::getAlias('@web') ?>/images/users/<?= $model->image ?>" class=""
                         alt="<?= $model->username ?>">

                </div>
            </div>
            <div class="card-body">
                <div class="user-profile text-center">
                    <div class="name"><?= $model->first_name . ' ' . $model->last_name ?></div>
                    <div class="job">@<?= $model->username; ?></div>

                </div>
            </div>

            <div class="card-footer">
                <a href="<?= \yii\helpers\Url::toRoute('site/login') ?>" class="btn btn-success">
                    Yes, Login Now
                </a>
                <a href="<?= \yii\helpers\Url::toRoute('site/index') ?>" class="btn btn-primary">
                    No,Continue without Login
                </a>
            </div>
        </div>
    </div>
</div>