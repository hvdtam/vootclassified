<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $model \frontend\models\ResetPasswordForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Reset password';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="main-container">
    <div class="container">
        <div class="row">
            <div class="col-sm-5 login-box">
                <div id="loginBox" class="card card-default bg-dark bg-dark bg-faded hidden" style="padding: 30px 10px">
                    <div class="card-body" align="center">
                        <p style="font-size: 60px;color: #fff">
                            <i id="login-icon" class="fa fa-circle-o-notch fa-spin"></i>
                        </p>
                        <p style="font-size: 16px;color: #fff" id="login-text">
                            Checking Login Detail. Please wait!
                        </p>
                    </div>
                </div>
                <div id="loginBoxForm" class="card card-default ">
                    <div class="panel-intro text-center">
                        <h2 class="logo-title">
                            <?= Html::encode($this->title) ?>
                        </h2>
                        <p style="padding: 10px 25px;font-size: 12px;font-weight: 600;">
                            Please choose your new password:
                        </p>
                    </div>
                    <div class="card-body">

                        <blockquote id="loginFormError" class="hidden"
                                    style="font-size: 12px;border-color:#e7232d;background-color:rgba(231,35,45,0.17);color: #e7232d;border-radius: 5px;">
                            <b>Error: </b> Invalid Username / Password.
                        </blockquote>
                        <?php $form = ActiveForm::begin(['id' => 'reset-password-form']); ?>

                        <?= $form->field($model, 'password')->passwordInput(['autofocus' => true]) ?>

                        <div class="form-group">
                            <?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>
                        </div>

                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
                <div class="login-box-btm text-center">
                    <p> Don't have an account? <br>

                        <a href="<?= \yii\helpers\Url::toRoute('site/signup') ?>"><strong>Sign Up !</strong> </a></p>
                </div>

            </div>
        </div>
    </div>
</div>
