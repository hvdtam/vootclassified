<?php
/**
 * Created by PhpStorm.
 * User: Mayank Singh
 * Date: 3/3/2020
 * Time: 2:56 PM
 */

use yii\bootstrap\ActiveForm;

?>

<section class="pt-3 position-sticky homeSearch" style="top: 0;z-index: 9">
    <div class="container-fluid">
        <div class="row no-gutters">
            <div class="col-lg-2">
                <span class="dropup ">
                    <button type="button" class="btn bg-white dropdown-toggle" data-toggle="dropdown">
                        <span class="text-secondary text-uppercase">
                            <?php echo Yii::t('site_header', 'Find in Category') ?>
                        </span>
                    </button>
                    <ul class="dropdown-menu dropdown-menu-left mt-3" role="menu">
                        <?php
                        \frontend\models\TemplatesDesign::DropDownCategory()
                        ?>
                    </ul>
                </span>
            </div>
            <div class="col-lg-2">
                <h1><?= Yii::t('home', 'Find Anything Here'); ?></h1>

            </div>
            <div class="col-lg-8">

                <div class="row search-row mt-0 mb-0 animated fadeInUp">

                    <div class="col-xl-8 col-lg-8 col-sm-10 search-col relative">
                        <i class="icon-menu icon-append"></i>
                        <input type="text" name="SearchForm[item]" id="dosearch7458" class="form-control has-icon"
                               placeholder="<?= Yii::t('home', 'I am looking for a') ?> ..." value="">
                        <div id="searchresult784259" class="suggetion AjaxHomeSeachsuggetion bg-green"></div>

                    </div>
                    <div class="col search-col">
                        <button type="button" data-target="#selectRegion" data-toggle="modal"
                                class="btn btn-primary btn-search btn-block">
                            <i class="fa fa-map-marker"></i>
                            <strong><?= Yii::t('home', 'SEARCH IN '); ?> <span
                                        class="CityText text-capitalize"><?= Yii::t('app', strtoupper($default['CurrCity'])); ?></span></strong>
                        </button>
                    </div>
                </div>

            </div>
            <div class="col-12 hidden">
                <span><i class="fa fa-search"></i> Recent Search : </span>
                <span>Car in Jodhpur | </span><span>Car in Jodhpur | </span><span>Car in Jodhpur | </span>
            </div>
        </div>
    </div>
</section>
