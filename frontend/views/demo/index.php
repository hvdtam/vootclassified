<?php
/**
 * Created by PhpStorm.
 * User: Mayank Singh
 * Date: 5/26/2019
 * Time: 11:46 AM
 */

use smladeoye\paystack\widget\PaystackWidget;
use yii\di\ServiceLocator;

$services = new ServiceLocator();
$services->setComponents([
    'Paystack' => [
        'class' => 'smladeoye\paystack\Paystack',
        'environment' => 'test',
        'testPublicKey' => 'pk_test_0e39a5223cf5bb9af433f25585abb6b8cef8cb32',
        'testSecretKey' => 'sk_test_9a3c3eae7769032115a4eb7e391767252ea4115e',
        'livePublicKey' => '',
        'liveSecretKey' => '',
    ],
    // ...
]);
// Retrieving the defined components:
$paystack = $services->get('Paystack');
$paystack = $services->Paystack;
//$paystack = $imap;
$transaction = $paystack->transaction();
$transaction->initialize(['email' => 'smladeoye@gmail.com', 'amount' => '100000', 'currency' => 'NGN']);

// check if an error occured during the operation
if (!$transaction->hasError) {
    //response property for response gotten for any operation
    $response = $transaction->getResponse();
    // redirect the user to the payment page gotten from the initialization
    $transaction->redirect();
} else {
    // display message
    echo $transaction->message;
    // get all the errors information regarding the operation from paystack
    $error = $transaction->getError();
}
