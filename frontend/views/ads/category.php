<?php

use yii\helpers\Url;
use frontend\helpers\AdsHelper;
use frontend\helpers\TextHelper;

/** @var String $cityDefault */
/** @var String $category */
/** @var String $sub_cat */
/** @var String $type */
/** @var String $sort */
/** @var String $near */
/** @var \common\models\Category $catModel */
/** @var \common\models\SubCategory $subCatModel */

$this->title = $catModel['name'] . ' tại ' . $cityDefault . ' - Voot Classified';

use \frontend\models\TemplatesDesign;

Url::remember(\yii\helpers\Url::current(), 'backToResult');
?>

<?php
//==================================================================//
//------------------ SEARCH MODULE RENDER CODE ---------------------//
//==================================================================//

echo $this->render('_search.php', ['category' => $category, 'cityDefault' => $cityDefault]);

//==================================================================//
//--------------- SEARCH MODULE RENDER CODE END---------------------//
//==================================================================//

$allAds = \frontend\models\TemplatesDesign::categoryUrl($cat, $sub_cat, $type, $sort, $near);
list($list, $grid, $bar) = AdsHelper::getTemplateShow($template);


if (isset($_GET['near'])) {
    $tag = "All ads within " . $_GET['near'] . " Km range.";
} elseif (isset($_GET['sort'])) {
    if ($_GET['sort'] == "lth") {
        $tag = "Display ads From Price Low to High";
    } elseif ($_GET['sort'] == "htl") {
        $tag = "Display ads From Price High to Low";
    } else {
        $tag = "Display Newly Posted Ads";
    }
} else {
    $tag = Yii::t("app", "All Ads");

}
$template = isset($_GET['listTPL']) ? base64_decode($_GET['listTPL']) : $template;
?>
<!-- Its Theme Control Settings only For Demo Please remove this code after purchase-->
<?php
if (Yii::$app->params['demo']) {
    ?>
    <div class="themeControll ">

        <h3 style=" color:#fff; font-size: 10px; line-height: 12px;" class="uppercase color-white text-center">
            <a target="_blank" href="#">
                All Pages
            </a>
        </h3>

        <div class="themeConntrolContent">
            <div class="card-body2">
                <!-- Theme Color Selection-->
                <h3 class="themecontrolTitle">
                    Color themes
                </h3>
                <div>
                    <span class="colorBlock" onclick="changeTheme('style.css')" title="hello"
                          style="background-color: #e55231;color: #fff">
                        <i class="fa fa-check"></i> &nbsp;
                    </span>
                    <span class="colorBlock" onclick="changeTheme('style_old.css')" data-original-title="Classic"
                          data-toggle="tooltip" data-placement="top" style="background-color: #16a085;color: #fff">
                        <i class="fa fa-check"></i> &nbsp;
                    </span>
                    <span class="colorBlock" onclick="changeTheme('style_two.css')" data-original-title="Awesome Blue"
                          data-toggle="tooltip" data-placement="top" style="background-color: #5984c7;color: #f6ea06">
                        <i class="fa fa-check"></i> &nbsp;
                    </span>
                    <span class="colorBlock" onclick="changeTheme('style_one.css')" data-original-title="Regular Blue"
                          data-toggle="tooltip" data-placement="top" style="background-color: #5984c7;color: #b3b3b3">
                        <i class="fa fa-check"></i> &nbsp;
                    </span>
                    <span class="colorBlock" onclick="changeTheme('style_three.css')"
                          data-original-title="Regular Yellow" data-toggle="tooltip" data-placement="top"
                          style="background-color: #f3e806;color: #363636">
                        <i class="fa fa-check"></i> &nbsp;
                    </span>
                    <span class="colorBlock" onclick="changeTheme('style_four.css')" data-original-title="Black Red"
                          data-toggle="tooltip" data-placement="top" style="background-color: #f6333b;color: #555555">
                        <i class="fa fa-check"></i> &nbsp;
                    </span>
                </div>
                <!-- Theme Color Selection end-->

                <!-- Display Widgets Settings-->

                <h3 class="themecontrolTitle">
                    List Style Settings
                </h3>
                <div style="display: block">
                    <ul class="list-group">
                        <li class="list-group-item">
                            <a href="<?= \yii\helpers\Url::toRoute('ads/listing') ?>?listTPL=<?= base64_encode('1') ?>">
                                List Style One
                                <?php
                                if ($template == '1') {
                                    echo '<span class="badge badge-info">ACTIVE</span>';
                                }
                                ?>

                            </a>
                        </li>
                        <li class="list-group-item">
                            <a href="<?= \yii\helpers\Url::toRoute('ads/listing') ?>?listTPL=<?= base64_encode('2') ?>">
                                List Style Two
                                <?php
                                if ($template == '2') {
                                    echo '<span class="badge badge-info">ACTIVE</span>';
                                }
                                ?>
                            </a>
                        </li>
                        <li class="list-group-item">
                            <a href="<?= \yii\helpers\Url::toRoute('ads/listing') ?>?listTPL=<?= base64_encode('3') ?>">
                                List Style Three
                                <?php
                                if ($template == '3') {
                                    echo '<span class="badge badge-info">ACTIVE</span>';
                                }
                                ?>
                            </a>
                        </li>
                        <li class="list-group-item">
                            <a href="<?= \yii\helpers\Url::toRoute('ads/listing') ?>?listTPL=<?= base64_encode('4') ?>">
                                List Style Four
                                <?php
                                if ($template == '4') {
                                    echo '<span class="badge badge-info">ACTIVE</span>';
                                }
                                ?>
                            </a>
                        </li>
                        <li class="list-group-item">
                            <a href="<?= \yii\helpers\Url::toRoute('ads/listing') ?>?listTPL=<?= base64_encode('0') ?>">
                                List Style Five
                                <?php
                                if ($template == '0') {
                                    echo '<span class="badge badge-info">ACTIVE</span>';
                                }
                                ?>
                            </a>
                        </li>

                    </ul>
                </div>


                <!-- Display Widgets Settings end-->

            </div>
        </div>

        <p class="tbtn"><i class="fa fa-angle-double-left"></i></p>
    </div>
    <?php
}
?>
<!-- Its Theme Control Settings ( above ) only For Demo Please remove this code after purchase-->
<div class="main-container">
    <div class="container-fluid">

        <div class="row">
            <!-- this (.mobile-filter-sidebar) part will be position fixed in mobile version -->
            <div class="col-md-3  page-sidebar mobile-filter-sidebar">
                <aside>
                    <div class="card">
                        <div class="card-body">
                            <div class="categories-list <?= $catsection ?>  list-filter">
                                <h5 class="list-title">
                                    <strong> <?= Yii::t('app', 'All Categories') ?> </strong>

                                </h5>
                                <ul class=" list-unstyled">
                                    <?php
                                    foreach ($category as $listSubCat) {

                                        $categoryUrl = \yii\helpers\Url::to(['ads/listing', 'cat' => TextHelper::getCategoryText($listSubCat['name']), 'location' => TextHelper::getCategoryText($cityDefault)]);

                                        ?>
                                        <li>
                                            <a href="<?= $categoryUrl ?>">
                                            <span class="title">
                                                <?= Yii::t('app', $listSubCat['name']) ?>
                                                (<?= ($listSubCat['item']) ? $listSubCat['item'] : '0' ?>)
                                            </span>
                                            </a>
                                        </li>
                                        <?php

                                    }
                                    ?>
                                </ul>
                            </div>
                            <div class="categories-list <?= $subcatsection ?>  list-filter">
                                <h5 class="list-title">
                                    <strong>
                                        <?= Yii::t('app', 'categories'); ?>
                                        <i class="fa fa-angle-right"></i>
                                        <?= Yii::t('category', $catModel['name']) ?>
                                    </strong>
                                    <a href="<?= \yii\helpers\Url::toRoute('ads/listing') ?>"
                                       class="pull-right tooltipHere" data-toggle="tooltip" data-placement="top"
                                       title="Jump top Category">
                                        <i class="fa fa-angle-up"></i>
                                    </a>
                                </h5>
                                <ul class=" list-unstyled">
                                    <?php
                                    foreach ($subList as $listSubCat) {
                                        $subCatListUrl = \yii\helpers\Url::to(['ads/listing', 'cat' => TextHelper::getCategoryText($cat), 'sub_cat' => TextHelper::getCategoryText($listSubCat['name']), 'location' => TextHelper::getCategoryText($cityDefault)]);
                                        ?>
                                        <li>
                                            <a href="<?= $subCatListUrl ?>">
                                            <span class="title">
                                                <?= Yii::t('sub_category', $listSubCat['name']); ?>
                                                (<?= ($listSubCat['item']) ? $listSubCat['item'] : '0' ?>)
                                            </span>
                                            </a>
                                        </li>
                                        <?php

                                    }
                                    ?>
                                </ul>
                            </div>
                            <!--/.categories-list-->
                            <div class="locations-list <?= $typetsection ?>   list-filter">
                                <h5 class="list-title">
                                    <strong><?= Yii::t('type', \frontend\models\TemplatesDesign::TypeDefiner($cat)) ?> </strong>
                                    <a href="<?= \yii\helpers\Url::to(['ads/listing', 'cat' => $cat, 'sub_cat' => $sub_cat, 'location' => $cityDefault]) ?>"
                                       class="pull-right tooltipHere" data-toggle="tooltip" data-placement="top"
                                       title="Jump top Category">
                                        <i class="fa fa-angle-up"></i>
                                    </a>
                                </h5>
                                <ul class="browse-list list-unstyled long-list">
                                    <?php
                                    foreach ($typeList as $listType) {
                                        $url = \yii\helpers\Url::to(['ads/listing', 'type' => $listType['name'], 'sub_cat' => $sub_cat, 'location' => $cityDefault]);

                                        ?>
                                        <li>
                                            <a href="<?= $url ?>">
                                            <span class="title">
                                                <?= Yii::t('app', $listType['name']); ?>
                                            </span>
                                            </a>
                                        </li>
                                        <?php

                                    }
                                    ?>
                                </ul>
                            </div>
                            <!--/. sub categories-list-->

                            <div class="locations-list  list-filter">
                                <h5 class="list-title"><strong><a
                                                href="#"><?= Yii::t('app', 'Location'); ?></a></strong></h5>
                                <ul class="browse-list list-unstyled long-list">
                                    <li>
                                        <a href="#selectRegion" id="dropdownMenu1" data-toggle="modal">
                                            <?= Yii::t('app', $cityDefault); ?>
                                            <span class="caret"></span>

                                        </a>
                                    </li>

                                </ul>
                            </div>
                            <!--/.locations-list-->


                            <!--/.list-filter-->
                            <div style="clear:both"></div>

                        </div>
                    </div>


                    <!--/.categories-list-->
                </aside>
                <div class="position-sticky  pr-2" style="top: 0px">
                    <?= \common\models\Adsense::show('left') ?>
                </div>
            </div>
            <!--/.page-side-bar-->
            <!--/.page-side-bar-->
            <div class="col-md-7 shadow-lg page-content col-thin-left">
                <div class="category-list">
                    <div class="tab-box ">

                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs add-tabs" role="tablist">
                            <li class=" nav-item">
                                <a class="nav-link " href="#all" data-url="<?= $allAds; ?>" role="tab"
                                   data-toggle="tab">
                                    <?= Yii::t('app', 'All Ads') ?>
                                    <span class="badge badge-secondary"><?= count($model);; ?></span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#business" data-url="<?= $allAds; ?>" role="tab"
                                   data-toggle="tab">
                                    <?= Yii::t('app', 'Business') ?>
                                    <span class="badge badge-primary"><?= count($business);; ?></span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#personal" data-url="<?= $allAds; ?>" role="tab"
                                   data-toggle="tab">
                                    <?= Yii::t('app', 'Personal') ?>
                                    <span class="badge badge-secondary"><?= count($personal);; ?></span>
                                </a>
                            </li>
                        </ul>


                        <div class="tab-filter">
                            <?php
                            if (isset($cat) && $sub_cat != '' && $type != '') {
                                $url = \yii\helpers\Url::to(['ads/listing', 'sub_cat' => $sub_cat, 'type' => $type, 'location' => $cityDefault], true);
                            } elseif (isset($cat) && $sub_cat != '') {
                                $url = \yii\helpers\Url::to(['ads/listing', 'cat' => $cat, 'sub_cat' => $sub_cat, 'location' => $cityDefault], true);
                            } else {
                                $url = \yii\helpers\Url::to(['ads/listing', 'cat' => $cat, 'location' => $cityDefault], true);

                            }
                            ?>
                            <select class="selectpicker select-sort-by" onchange="window.location.href = $(this).val()"
                                    data-style="btn-select" data-width="auto">
                                <option><?= Yii::t('app', 'Sort by') ?> </option>


                                <option value="<?= $url; ?>?sort=new<?= ($near) ? '&near=' . $near : null ?>"><?= Yii::t('app', 'New Ads') ?> </option>
                                <option value="<?= $url; ?>?sort=lth<?= ($near) ? '&near=' . $near : null ?>"><?= Yii::t('app', 'Price: Low to High') ?> </option>
                                <option value="<?= $url; ?>?sort=htl<?= ($near) ? '&near=' . $near : null ?>"><?= Yii::t('app', 'Price: High to Low') ?></option>
                            </select>

                        </div>
                    </div>
                    <!--/.tab-box-->

                    <div class="listing-filter">
                        <div class="pull-left col-xs-6">
                            <div class="breadcrumb-list">
                                <a href="#" class="current">
                                    <span><?= $tag; ?></span>
                                </a>
                                <?= Yii::t('app', 'in'); ?>

                                <!-- cityName will replace with selected location/area from location modal -->
                                <span class="cityName">
                                    <?= $cityDefault; ?>
                                </span>
                                <a href="#selectRegion" id="dropdownMenu1" data-toggle="modal">
                                    <span class="caret"></span>
                                </a>
                            </div>
                        </div>
                        <div class="pull-right col-xs-6 text-right listing-view-action">
                            <span class="list-view active <?= $list ?>">
                                <i class="  icon-th"></i>
                            </span>
                            <span class="compact-view <?= $bar ?>">
                                <i class=" icon-th-list  "></i>
                            </span>
                            <span class="grid-view <?= $grid ?>">
                                <i class=" icon-th-large "></i>
                            </span>
                        </div>
                        <div style="clear:both"></div>
                    </div>
                    <!--/.listing-filter-->


                    <!-- Mobile Filter bar-->
                    <div class="mobile-filter-bar col-xl-12  ">
                        <ul class="list-unstyled list-inline no-margin no-padding">
                            <li class="filter-toggle">
                                <a class="">
                                    <i class="  icon-th-list"></i>
                                    <?= Yii::t('app', 'Filters') ?>
                                </a>
                            </li>
                            <li>


                                <div class="dropdown"><a data-toggle="dropdown" class="dropdown-toggle">
                                        <?= Yii::t('app', 'Sort By') ?></a>
                                    <ul class="dropdown-menu">
                                        <li class="dropdown-item"><a href="#"
                                                                     rel="nofollow">Relevance<?= Yii::t('app', 'Relevance') ?></a>
                                        </li>
                                        <li class="dropdown-item"><a href="#"
                                                                     rel="nofollow"><?= Yii::t('app', 'Date') ?></a>
                                        </li>
                                        <li class="dropdown-item"><a href="#"
                                                                     rel="nofollow"><?= Yii::t('app', 'Company') ?></a>
                                        </li>
                                    </ul>
                                </div>

                            </li>
                        </ul>
                    </div>
                    <div class="menu-overly-mask"></div>
                    <!-- Mobile Filter bar End-->

                    <div class="adds-wrapper ">
                        <div class="tab-content">
                            <div class="tab-pane active fade-in " id="all">
                                <?= TemplatesDesign::ItemList($model, $template) ?>
                            </div>
                            <div class="tab-pane fade-in" id="business">
                                <?= TemplatesDesign::ItemList($business, $template) ?>
                            </div>
                            <div class="tab-pane fade-in" id="personal">
                                <?= TemplatesDesign::ItemList($personal, $template) ?>
                            </div>
                        </div>
                    </div>
                    <!--/.adds-wrapper-->
                    <div align="center">
                        <?= \common\models\Adsense::show('bottom') ?>
                    </div>
                    <div class="tab-box  save-search-bar text-center hidden"><a href="#"> <i
                                    class=" icon-star-empty"></i>
                            <?= Yii::t('app', 'Save Search') ?> </a></div>
                </div>

                <div class="pagination-bar text-center">
                    <nav aria-label="Page navigation " class="d-inline-b">
                        <?php
                        // display pagination

                        echo \yii\widgets\LinkPager::widget([
                            'pagination' => $pages,
                            // 'options'=>['class'=>'page-item'],
                            'linkOptions' => ['class' => 'page-link'],
                            'disabledPageCssClass' => 'page-link disabled',
                            'prevPageLabel' => Yii::t('app', 'Prev'),
                            'nextPageLabel' => Yii::t('app', 'Next')
                        ]);

                        ?>

                    </nav>
                </div>
                <!--/.pagination-bar -->

                <div class="post-promo text-center">
                    <h2> <?= Yii::t('app', 'Do you get anything for sell?') ?> </h2>
                    <h5><?= Yii::t('app', 'Sell your products online FOR FREE. Its easier than you think !'); ?></h5>
                    <a href="<?= \yii\helpers\Url::toRoute('ads/post') ?>"
                       class="btn btn-lg btn-border btn-post btn-danger">
                        <?= Yii::t('app', 'Post a Free Ad') ?>
                    </a>
                </div>
                <!--/.post-promo-->

            </div>
            <div class="col-md-2  page-sidebar mobile-filter-sidebar">
                <aside class="position-sticky  pr-2" style="top: 0px">
                    <div align="center">
                        <?= \common\models\Adsense::show('right') ?>
                    </div>

                    <!--/.categories-list-->
                </aside>
            </div>
        </div>
    </div>
</div>
<!-- /.main-container -->
<script>
    $(document).ready(function () {

        $('#viewCarousel').click(function () {
            if ($(this).hasClass('active')) return;
            $(this).addClass("active");
            $("#viewGrid").removeClass("active");

            setCarousel();
            $('.thumbs p').eq(0).clone().addClass("floating-thumb").appendTo("#window")
            $('.thumbs').fadeOut(300);
            setTimeout(function () {
                $('.floating-thumb').addClass('animate');
                $("#carousel").delay(200).fadeIn(200, function () {
                    $('.floating-thumb').remove();
                });
            }, 150);
        });

        $('#viewGrid').click(function () {
            if ($(this).hasClass('active')) return;
            $(this).addClass("active");
            $("#viewCarousel").removeClass("active");

            $("#carousel").fadeOut(200, function () {
                resetCarousel();
            });
            $("<p class='floating-thumb animate'><span></span><span class='short'></span></p>").appendTo("#window");
            $('.thumbs').show();
            var parentpos = $('#window').offset();
            var childpos = $('.thumbs p').eq(currentSlide - 1).offset();
            $('.thumbs').hide();

            $('.floating-thumb').removeClass('animate').css({
                'top': (childpos.top - parentpos.top) - 16 + "px",
                'left': (childpos.left - parentpos.left) + "px",
                "transition": "300ms cubic-bezier(0,.93,.33,.99)",
                'width': '155px',
                'height': "60px",
                "padding-top": "121px"
            });
            $('.thumbs').delay(300).fadeIn(200, function () {
                $('.floating-thumb').remove()
            });

        });

        /* ----  Image Gallery Carousel   ---- */

        var carousel = $('#carousel .innerCarousel');
        var carouselSlideWidth = 337;
        var currentSlide = 1;
        var isAnimating = false;
        var carouselSlides = $('.innerCarousel div');


        setCarousel();

        function resetCarousel() {
            $(carouselSlides).find('p').removeClass('current').eq(0).addClass('current');
            $("#carousel .innerCarousel").css('left', '-168px');
            currentSlide = 1;
        }

        function setCarousel() {

            // building the width of the casousel
            var carouselWidth = 0;
            $('#carousel div').each(function () {
                carouselWidth += carouselSlideWidth;
            });

            //$(carousel)[0].style = "";
            $(carousel).css('width', carouselWidth);
            // Load Next Image
            $(carouselSlides).eq(currentSlide).prev().find('p').unbind("click").click(function () {

                if ($(this).hasClass('current')) {
                    return;
                }
                var currentLeft = Math.abs(parseInt($(carousel).css("left")));
                var newLeft = currentLeft - carouselSlideWidth;


                if (isAnimating === true) {
                    return;
                }
                $(carousel).css({
                    'left': "-" + newLeft + "px",
                    "transition": "500ms cubic-bezier(0,.93,.33,.99)"
                });
                isAnimating = true;
                $(this).addClass("current");
                $(carouselSlides).eq(currentSlide).find('p')[0].className = "";
                setTimeout(function () {
                    isAnimating = false;
                    currentSlide--;
                    setCarousel();
                }, 500);

            });

            $(carouselSlides).eq(currentSlide).next().find('p').unbind("click").click(function () {

                if ($(this).hasClass('current')) {
                    return;
                }
                var currentLeft = Math.abs(parseInt($(carousel).css("left")));
                var newLeft = currentLeft + carouselSlideWidth;

                if (isAnimating === true) {
                    return;
                }
                $(this).addClass("current");
                $(carouselSlides).eq(currentSlide).find('p')[0].className = "";
                $(carousel).css({
                    'left': "-" + newLeft + "px",
                    "transition": "500ms cubic-bezier(0,.93,.33,.99)"
                });
                isAnimating = true;
                setTimeout(function () {
                    isAnimating = false;
                    currentSlide++;
                    setCarousel();
                }, 500);
            });

        }


    });
</script>
