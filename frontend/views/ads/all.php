<?php

$this->title = "All category";

use \frontend\models\TemplatesDesign;
use \yii\bootstrap\ActiveForm;

$citySet = '<script>document.write(localStorage.getItem("setcity"))</script>';
$siteSetting = \common\models\SiteSettings::find()->one();

$current_url = \yii\helpers\Url::current();
\yii\helpers\Url::remember($current_url, 'currency_p');

$session2 = Yii::$app->cache;
$default_selected = $session2->get('default_currency');
$default = (isset($default_selected)) ? $default_selected : $currency_default;
\yii\helpers\Url::remember(\yii\helpers\Url::current(), 'backToResult');
$defaultSetting = \common\models\DefaultSetting::getDefaultSetting();
?>

<?php
?>

<?php
//==================================================================//
//------------------ SEARCH MODULE RENDER CODE ---------------------//
//==================================================================//

echo $this->render('_search.php', ['category' => $category, 'cityDefault' => $defaultSetting['city']]);

//==================================================================//
//--------------- SEARCH MODULE RENDER CODE END---------------------//
//==================================================================//

$allAds = \frontend\models\TemplatesDesign::categoryUrl($cat, $sub_cat, $type, $sort, $near);
if ($template == '1') {
    $list = "show";
    $grid = "show";
    $bar = "hidden";
} elseif ($template == '2') {
    $list = "hidden";
    $grid = "hidden";
    $bar = "hidden";
} elseif ($template == '3') {
    $list = "show";
    $grid = "show";
    $bar = "hidden";
} elseif ($template == '4') {
    $list = "show";
    $grid = "show";
    $bar = "hidden";
} elseif ($template == '5') {
    $list = "show";
    $grid = "show";
    $bar = "hidden";
} else {
    $list = "show";
    $grid = "show";
    $bar = "show";
}

if (isset($_GET['near'])) {
    $tag = "All ads within " . $_GET['near'] . " Km range.";
} elseif (isset($_GET['sort'])) {
    if ($_GET['sort'] == "lth") {
        $result = "Display ads From Price Low to High";
    } elseif ($_GET['sort'] == "htl") {
        $result = "Display ads From Price High to Low";
    } else {
        $tag = "Display Newly Posted Ads";
    }
} else {
    $tag = "All Ads";

}
?>
<div class="main-container">
    <div class="container">
        <div class="row">

            <!-- this (.mobile-filter-sidebar) part will be position fixed in mobile version -->
            <div class="col-md-3  page-sidebar mobile-filter-sidebar">
                <aside>
                    <div class="card">
                        <div class="card-body">
                            <div class="categories-list  list-filter">
                                <h5 class="list-title">
                                    <strong> <?= Yii::t('app', 'All Categories') ?> </strong>

                                </h5>
                                <ul class=" list-unstyled">
                                    <?php
                                    foreach ($category as $listSubCat) {


                                        $url = \yii\helpers\Url::toRoute('ads/listing?category=' . $listSubCat['name']);
                                        ?>
                                        <li>
                                            <a href="<?= $url ?>">
                                            <span class="title">
                                                <?= Yii::t('app', $listSubCat['name']) ?>
                                                (<?= ($listSubCat['item']) ? $listSubCat['item'] : '0' ?>)
                                            </span>
                                            </a>
                                        </li>
                                        <?php

                                    }
                                    ?>
                                </ul>

                            </div>
                            <hr>
                            <div class="categories-list list-filter">
                                <form method="get">
                                    <input type="hidden" name="SearchForm[category]"
                                           value="<?= $filter['SearchCategory']; ?>">
                                    <input type="hidden" name="SearchForm[type]" value="<?= $filter['SearchType']; ?>">
                                    <input type="hidden" name="SearchForm[item]" value="<?= $filter['SearchItem']; ?>">

                                    <?php
                                    foreach ($filter['FilterCustom'] as $value) {
                                        $qTitle = $value['custom_title'];
                                        if ($value['custom_type'] == "textinput" or $value['custom_type'] == "text") {
                                            continue;
                                        };
                                        ?>
                                        <ul class=" list-unstyled">

                                            <h5 class="list-title">
                                                <strong style="font-size: 12px;">  <?= $value['custom_title']; ?> </strong>

                                            </h5>
                                            <li>

                                                <?php
                                                $title = $value['custom_title'];
                                                if ($value['custom_type'] == "textinput") {
                                                    echo "<input type='text' name='SearchForm[filterTitle][$title]' class='form-control'>";
                                                } elseif ($value['custom_type'] == "select") {
                                                    $option = explode(',', $value['custom_options']);
                                                    ?>
                                                    <select id="select" class="form-control"
                                                            name="SearchForm[filterValue][<?= $title; ?>]">
                                                        <?php
                                                        foreach ($option as $ops) {
                                                            ?>
                                                            <option value="<?= $ops ?>"><?= $ops ?></option>
                                                            <?php
                                                        }
                                                        ?>
                                                    </select>

                                                    <?php
                                                } elseif ($value['custom_type'] == "checkbox") {
                                                    // echo $form->field($adCustom, 'more_value[]')->checkboxList(['a' => 'Item A', 'b' => 'Item B', 'c' => 'Item C']);
                                                    $option = explode(',', $value['custom_options']);
                                                    ?>

                                                    <div class="checkbox">
                                                        <?php
                                                        foreach ($option as $key => $ops) {
                                                            ?>
                                                            <div class="checkbox">
                                                                <label><input multiple
                                                                              name='SearchForm[filterValue][<?= $title; ?>]'
                                                                              value="<?= $ops ?>"
                                                                              type="checkbox"> <?= $ops ?></label>
                                                            </div>


                                                            <?php
                                                        }
                                                        ?>

                                                    </div>

                                                    <?php
                                                } elseif ($value['custom_type'] == "radio") {
                                                    $data = $value['custom_options'];

                                                    $option = explode(',', $data);
                                                    ?>
                                                    <?php
                                                    foreach ($option as $ops) {
                                                        ?>
                                                        <div class="radio"><label><input multiple
                                                                                         name='SearchForm[filterValue][<?= $title; ?>]'
                                                                                         value="<?= $ops ?>"
                                                                                         type="radio"> <?= $ops ?>
                                                            </label></div>

                                                        <?php
                                                    }
                                                    ?>
                                                    <?php

                                                } else {
                                                    echo "<input type='text' multiple  name='SearchForm[filterValue][<?= $title; ?>]' class='form-control col-xs'>";
                                                    echo "<hr>";
                                                }

                                                ?>

                                            </li>
                                        </ul>
                                        <?php

                                    }
                                    ?>
                                    <br>
                                    <button type="submit" class="btn btn-block btn-success">
                                        <i class="fa fa-filter"></i> Filter
                                    </button>
                                </form>

                            </div>

                            <div class="locations-list  list-filter">
                                <h5 class="list-title"><strong><a href="#">Location</a></strong></h5>
                                <ul class="browse-list list-unstyled long-list">
                                    <li>
                                        <a href="#selectRegion" id="dropdownMenu1" data-toggle="modal">
                                            <?= Yii::t('app', $cityDefault); ?>
                                            <span class="caret"></span>

                                        </a>
                                    </li>

                                </ul>
                            </div>
                            <!--/.locations-list-->


                            <!--/.list-filter-->
                            <div style="clear:both"></div>
                        </div>
                    </div>

                    <!--/.categories-list-->
                </aside>
            </div>
            <!--/.page-side-bar-->
            <!--/.page-side-bar-->
            <div class="col-md-9 page-content col-thin-left">

                <div class="category-list">
                    <div class="col" style="padding: 10px;" align="center">
                        <?= \common\models\Adsense::show('top') ?>
                    </div>
                    <div class="tab-box ">

                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs add-tabs" role="tablist">
                            <li class=" nav-item">
                                <a class="nav-link " href="#all" data-url="<?= $allAds; ?>" role="tab"
                                   data-toggle="tab">
                                    <?= Yii::t('app', 'All Ads') ?>
                                    <span class="badge badge-secondary"><?= count($model);; ?></span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#business" data-url="<?= $allAds; ?>" role="tab"
                                   data-toggle="tab">
                                    <?= Yii::t('app', 'Business') ?>
                                    <span class="badge badge-primary"><?= count($business);; ?></span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#personal" data-url="<?= $allAds; ?>" role="tab"
                                   data-toggle="tab">
                                    <?= Yii::t('app', 'Personal') ?>
                                    <span class="badge badge-secondary"><?= count($personal);; ?></span>
                                </a>
                            </li>
                        </ul>


                        <div class="tab-filter">

                            <select class="selectpicker select-sort-by" onchange="window.location.href = $(this).val()"
                                    data-style="btn-select" data-width="auto">
                                <option><?= Yii::t('app', 'Sort by') ?> </option>
                                <option value="<?= \yii\helpers\Url::toRoute('ads/listing?category=' . $cat . '&sub_category=' . $sub_cat . '&type=' . $type . '&sort=new&near=' . $near); ?>">
                                    Newly Submited
                                </option>
                                <option value="<?= \yii\helpers\Url::toRoute('ads/listing?category=' . $cat . '&sub_category=' . $sub_cat . '&type=' . $type . '&sort=lth&near=' . $near); ?>">
                                    Price: Low to High
                                </option>
                                <option value="<?= \yii\helpers\Url::toRoute('ads/listing?category=' . $cat . '&sub_category=' . $sub_cat . '&type=' . $type . '&sort=htl&near=' . $near); ?>">
                                    Price: High to Low
                                </option>
                            </select>
                        </div>
                    </div>
                    <!--/.tab-box-->

                    <div class="listing-filter">
                        <div class="pull-left col-xs-6">
                            <div class="breadcrumb-list">
                                <a href="#" class="current">
                                    <span><?= $tag; ?></span>
                                </a>
                                <?= Yii::t('app', 'in'); ?>

                                <!-- cityName will replace with selected location/area from location modal -->
                                <span class="cityName">
                                    <?= $cityDefault; ?>
                                </span>
                                <a href="#selectRegion" id="dropdownMenu1" data-toggle="modal">
                                    <span class="caret"></span>
                                </a>
                            </div>
                        </div>
                        <div class="pull-right col-xs-6 text-right listing-view-action">
                            <span class="list-view active <?= $list ?>">
                                <i class="  icon-th"></i>
                            </span>
                            <span class="compact-view <?= $bar ?>">
                                <i class=" icon-th-list  "></i>
                            </span>
                            <span class="grid-view <?= $grid ?>">
                                <i class=" icon-th-large "></i>
                            </span>
                        </div>
                        <div style="clear:both"></div>
                    </div>
                    <!--/.listing-filter-->


                    <!-- Mobile Filter bar-->
                    <div class="mobile-filter-bar col-xl-12  ">
                        <ul class="list-unstyled list-inline no-margin no-padding">
                            <li class="filter-toggle">
                                <a class="">
                                    <i class="  icon-th-list"></i>
                                    <?= Yii::t('app', 'Filters') ?>
                                </a>
                            </li>
                            <li>


                                <div class="dropdown"><a data-toggle="dropdown" class="dropdown-toggle">
                                        <?= Yii::t('app', 'Sort By') ?></a>
                                    <ul class="dropdown-menu">
                                        <li class="dropdown-item"><a href="#" rel="nofollow">Relevance</a>
                                        </li>
                                        <li class="dropdown-item"><a href="#" rel="nofollow">Date</a>
                                        </li>
                                        <li class="dropdown-item"><a href="#" rel="nofollow">Company</a>
                                        </li>
                                    </ul>
                                </div>

                            </li>
                        </ul>
                    </div>
                    <div class="menu-overly-mask"></div>
                    <!-- Mobile Filter bar End-->

                    <div class="adds-wrapper">
                        <div class="tab-content">
                            <div class="tab-pane active fade-in " id="all">
                                <?= TemplatesDesign::ItemList($model, $template) ?>
                            </div>
                            <div class="tab-pane fade-in" id="business">
                                <?= TemplatesDesign::ItemList($business, $template) ?>
                            </div>
                            <div class="tab-pane fade-in" id="personal">
                                <?= TemplatesDesign::ItemList($personal, $template) ?>
                            </div>
                        </div>
                    </div>
                    <!--/.adds-wrapper-->

                    <div class="tab-box  save-search-bar text-center hidden"><a href="#"> <i
                                    class=" icon-star-empty"></i>
                            Save Search </a></div>
                </div>

                <div class="pagination-bar text-center">
                    <nav aria-label="Page navigation " class="d-inline-b">
                        <?php
                        // display pagination

                        echo \yii\widgets\LinkPager::widget([
                            'pagination' => $pages,
                            // 'options'=>['class'=>'page-item'],
                            'linkOptions' => ['class' => 'page-link'],
                            'disabledPageCssClass' => 'page-link disabled',
                            'prevPageLabel' => 'Prev',
                            'nextPageLabel' => 'Next'
                        ]);

                        ?>

                    </nav>
                </div>
                <!--/.pagination-bar -->
                <div class="col-md-12" align="center">
                    <?= \common\models\Adsense::show('bottom') ?>
                </div>
                <div class="post-promo text-center">
                    <h2> <?= Yii::t('app', 'Do you get anything for sell?') ?> </h2>
                    <h5><?= Yii::t('app', 'Sell your products online FOR FREE. Its easier than you think !'); ?></h5>
                    <a href="<?= \yii\helpers\Url::toRoute('ads/post') ?>"
                       class="btn btn-lg btn-border btn-post btn-danger">
                        <?= Yii::t('app', 'Post a Free Ad') ?>
                    </a>
                </div>
                <!--/.post-promo-->

            </div>

        </div>
    </div>
</div>
<!-- /.main-container -->
<script>
    $(document).ready(function () {

        $('#viewCarousel').click(function () {
            if ($(this).hasClass('active')) return;
            $(this).addClass("active");
            $("#viewGrid").removeClass("active");

            setCarousel();
            $('.thumbs p').eq(0).clone().addClass("floating-thumb").appendTo("#window")
            $('.thumbs').fadeOut(300);
            setTimeout(function () {
                $('.floating-thumb').addClass('animate');
                $("#carousel").delay(200).fadeIn(200, function () {
                    $('.floating-thumb').remove();
                });
            }, 150);
        });

        $('#viewGrid').click(function () {
            if ($(this).hasClass('active')) return;
            $(this).addClass("active");
            $("#viewCarousel").removeClass("active");

            $("#carousel").fadeOut(200, function () {
                resetCarousel();
            });
            $("<p class='floating-thumb animate'><span></span><span class='short'></span></p>").appendTo("#window");
            $('.thumbs').show();
            var parentpos = $('#window').offset();
            var childpos = $('.thumbs p').eq(currentSlide - 1).offset();
            $('.thumbs').hide();

            $('.floating-thumb').removeClass('animate').css({
                'top': (childpos.top - parentpos.top) - 16 + "px",
                'left': (childpos.left - parentpos.left) + "px",
                "transition": "300ms cubic-bezier(0,.93,.33,.99)",
                'width': '155px',
                'height': "60px",
                "padding-top": "121px"
            });
            $('.thumbs').delay(300).fadeIn(200, function () {
                $('.floating-thumb').remove()
            });

        });

        /* ----  Image Gallery Carousel   ---- */

        var carousel = $('#carousel .innerCarousel');
        var carouselSlideWidth = 337;
        var currentSlide = 1;
        var isAnimating = false;
        var carouselSlides = $('.innerCarousel div');


        setCarousel();

        function resetCarousel() {
            $(carouselSlides).find('p').removeClass('current').eq(0).addClass('current');
            $("#carousel .innerCarousel").css('left', '-168px');
            currentSlide = 1;
        }

        function setCarousel() {

            // building the width of the casousel
            var carouselWidth = 0;
            $('#carousel div').each(function () {
                carouselWidth += carouselSlideWidth;
            });

            $(carousel)[0].style = "";
            $(carousel).css('width', carouselWidth);
            // Load Next Image
            $(carouselSlides).eq(currentSlide).prev().find('p').unbind("click").click(function () {

                if ($(this).hasClass('current')) {
                    return;
                }
                var currentLeft = Math.abs(parseInt($(carousel).css("left")));
                var newLeft = currentLeft - carouselSlideWidth;


                if (isAnimating === true) {
                    return;
                }
                $(carousel).css({
                    'left': "-" + newLeft + "px",
                    "transition": "500ms cubic-bezier(0,.93,.33,.99)"
                });
                isAnimating = true;
                $(this).addClass("current");
                $(carouselSlides).eq(currentSlide).find('p')[0].className = "";
                setTimeout(function () {
                    isAnimating = false;
                    currentSlide--;
                    setCarousel();
                }, 500);

            });

            $(carouselSlides).eq(currentSlide).next().find('p').unbind("click").click(function () {

                if ($(this).hasClass('current')) {
                    return;
                }
                var currentLeft = Math.abs(parseInt($(carousel).css("left")));
                var newLeft = currentLeft + carouselSlideWidth;

                if (isAnimating === true) {
                    return;
                }
                $(this).addClass("current");
                $(carouselSlides).eq(currentSlide).find('p')[0].className = "";
                $(carousel).css({
                    'left': "-" + newLeft + "px",
                    "transition": "500ms cubic-bezier(0,.93,.33,.99)"
                });
                isAnimating = true;
                setTimeout(function () {
                    isAnimating = false;
                    currentSlide++;
                    setCarousel();
                }, 500);
            });

        }


    });
</script>


<script type="text/javascript">
    var setcity = localStorage.getItem("setcity");
    document.getElementById("cityy").innerHTML = setcity;
</script>
