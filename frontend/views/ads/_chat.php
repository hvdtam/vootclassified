<?php
/**
 * Created by PhpStorm.
 * User: Mayank Singh
 * Date: 10/26/2018
 * Time: 11:05 AM
 */
?>

<div style="display: none" class="chatContainer" id="ChatMsg">
    <div class="card">
        <div class="card-header">
            <b style="color: #5add3e">
                <i class="fa fa-comment-o"></i> <?= Yii::t('app', 'Message') ?>
            </b>
            <button type="button" class="close" onclick="closeChat()">
                <span aria-hidden="true">&times;</span>
                <span class="sr-only">Close</span>
            </button>
        </div>
        <div class="card-body" style="height: 229px;overflow-y: scroll" id="displayMsg">
            <br>
            <input type="hidden" name="check" id="check"
                   value="<?= count(\common\models\Message::LoadMsg($model->user_id, $model->id)) ?>">
            <?php
            foreach (\common\models\Message::LoadMsg($model->user_id, $model->id) as $chat) {
                ?>
                <div class=" <?= ($chat['sender'] == Yii::$app->user->identity->getId()) ? "sender" : "receiver"; ?>">
                    <span class="name"> <?= \common\models\User::getNameById($chat['sender']); ?> : </span>
                    <span class="msg">
                        <?= $chat['text'] ?>
                    </span>
                    <div class="clearfix"></div>
                    <div class="time">
                        <?= \common\models\Analytic::time_elapsed_string($chat['time']) ?> ago
                    </div>
                </div>

                <?php

            }

            ?>
        </div>
        <div class="card-footer">
            <div class="row" align="left">
                <input type="text" placeholder="Type Something..." id="msgboxtext" class="btn col-md-10">
                <button type="button" class="btn col-md-2" onclick="sendmsg(<?= $model->user_id ?>,<?= $model->id ?>)">
                    <i class="fa fa-send-o text-info"></i>
                </button>
            </div>

        </div>
    </div>
</div>