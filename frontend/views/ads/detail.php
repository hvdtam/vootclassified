<?php
$this->title = $model->ad_title;

use  \common\models\Message;
use \yii\bootstrap\ActiveForm;
use yii\helpers\Html;

\common\models\Ads::view($model->id);
$currency_default = common\models\Currency::default_currency();

$current_url = \yii\helpers\Url::current();
\yii\helpers\Url::remember($current_url, 'currency_p');

$session2 = Yii::$app->cache;
$default_selected = $session2->get('default_currency');
$default = (isset($default_selected)) ? $default_selected : $currency_default;
$api = \common\models\ApiKeys::find()->where(['type' => 'maps'])->andWhere(['status' => 'enable'])->one();

?>

<?php
if ($model->category == 'Jobs') {
    $view = "hidden";
    $class = "col-lg-8 col-lg-push-2";
} else {
    $view = "";
    $class = "col-lg-6";;
}
//d9534f,
$template = isset($_GET['listTPL']) ? base64_decode($_GET['listTPL']) : $template;

?>
<style>
    .tab-card {
        border: 1px solid #eee;
    }

    .tab-card-header {
        background: none;
    }

    /* Default mode */
    .tab-card-header > .nav-tabs {
        border: none;
        margin: 0px;
    }

    .tab-card-header > .nav-tabs > li {
        margin-right: 2px;
    }

    .tab-card-header > .nav-tabs > li > a {
        border: 0;
        border-bottom: 2px solid transparent;
        margin-right: 0;
        color: #737373;
        padding: 2px 15px;
    }

    .tab-card-header > .nav-tabs > li > a.show {
        border-bottom: 2px solid #007bff;
        color: #007bff;
    }

    .tab-card-header > .nav-tabs > li > a:hover {
        color: #007bff;
    }

    #vendorMap {
        width: 100%;
        height: 400px;
    }
</style>
<!-- Its Theme Control Settings only For Demo Please remove this code after purchase-->
<?php
if (Yii::$app->params['demo']) {
    ?>
    <div class="themeControll ">

        <h3 style=" color:#fff; font-size: 10px; line-height: 12px;" class="uppercase color-white text-center">
            <a target="_blank" href="#">
                All Pages
            </a>
        </h3>

        <div class="themeConntrolContent">
            <div class="card-body2">
                <!-- Theme Color Selection-->
                <h3 class="themecontrolTitle">
                    Color themes
                </h3>
                <div>
                    <span class="colorBlock" onclick="changeTheme('style.css')" title="hello"
                          style="background-color: #e55231;color: #fff">
                        <i class="fa fa-check"></i> &nbsp;
                    </span>
                    <span class="colorBlock" onclick="changeTheme('style_old.css')" data-original-title="Classic"
                          data-toggle="tooltip" data-placement="top" style="background-color: #16a085;color: #fff">
                        <i class="fa fa-check"></i> &nbsp;
                    </span>
                    <span class="colorBlock" onclick="changeTheme('style_two.css')" data-original-title="Awesome Blue"
                          data-toggle="tooltip" data-placement="top" style="background-color: #5984c7;color: #f6ea06">
                        <i class="fa fa-check"></i> &nbsp;
                    </span>
                    <span class="colorBlock" onclick="changeTheme('style_one.css')" data-original-title="Regular Blue"
                          data-toggle="tooltip" data-placement="top" style="background-color: #5984c7;color: #b3b3b3">
                        <i class="fa fa-check"></i> &nbsp;
                    </span>
                    <span class="colorBlock" onclick="changeTheme('style_three.css')"
                          data-original-title="Regular Yellow" data-toggle="tooltip" data-placement="top"
                          style="background-color: #f3e806;color: #363636">
                        <i class="fa fa-check"></i> &nbsp;
                    </span>
                    <span class="colorBlock" onclick="changeTheme('style_four.css')" data-original-title="Black Red"
                          data-toggle="tooltip" data-placement="top" style="background-color: #f6333b;color: #555555">
                        <i class="fa fa-check"></i> &nbsp;
                    </span>
                </div>
                <!-- Theme Color Selection end-->

                <!-- Display Widgets Settings-->

                <h3 class="themecontrolTitle">
                    List Style Settings
                </h3>
                <div style="display: block">
                    <ul class="list-group">
                        <li class="list-group-item">
                            <a href="<?= \yii\helpers\Url::toRoute('ads/detail/' . base64_encode($model['id']) . '-listStyle') ?>?listTPL=<?= base64_encode('_detail-view-one') ?>">
                                Style One
                                <?php
                                if ($template == '_detail-view-one') {
                                    echo '<span class="badge badge-info">ACTIVE</span>';
                                }
                                ?>

                            </a>
                        </li>
                        <li class="list-group-item">
                            <a href="<?= \yii\helpers\Url::toRoute('ads/detail/' . base64_encode($model['id']) . '-listStyle') ?>?listTPL=<?= base64_encode('_detail-view-two') ?>">
                                Style Two
                                <?php
                                if ($template == '_detail-view-two') {
                                    echo '<span class="badge badge-info">ACTIVE</span>';
                                }
                                ?>
                            </a>
                        </li>


                    </ul>
                </div>

                <!-- Display Widgets Settings end-->

            </div>
        </div>

        <p class="tbtn"><i class="fa fa-angle-double-left"></i></p>
    </div>
    <?php
}
?>

<!-- Its Theme Control Settings ( above ) only For Demo Please remove this code after purchase-->
<!--single-page-->
<div class="main-container p-0 m-0">

    <div class="container-fluid p-0">
        <div class="row no-gutters">
            <div class="col">
                <div class="position-sticky  pr-1" align="left" style="top: 0px">
                    <?= \common\models\Adsense::show('left') ?>
                </div>
            </div>
            <div class="col-9">
                <div class="row ">
                    <div class="col-md-12 mt-3">

                        <nav aria-label="breadcrumb" role="navigation" class="pull-left">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= \yii\helpers\Url::toRoute('site/index') ?>"><i
                                                class="icon-home fa"></i></a></li>
                                <li class="breadcrumb-item"><a
                                            href="<?= \yii\helpers\Url::previous('backToResult') ?>"><?= Yii::t('app', 'Listing') ?></a>
                                </li>
                                <li class="breadcrumb-item active"
                                    aria-current="page"><?= Yii::t('app', 'Search Results') ?></li>
                            </ol>
                        </nav>
                        <div class="pull-right backtolist">
                            <a href="<?= \yii\helpers\Url::previous('backToResult') ?>">
                                <i class="fa fa-angle-double-left"></i>
                                <?= Yii::t('app', 'Back to Results') ?>
                            </a>
                        </div>
                    </div>
                </div>
                <?= $this->render($template, [
                    'model' => $model, 'more' => $more, 'related' => $related, 'author' => $author
                ]) ?>
            </div>
            <div class="col">
                <div class="position-sticky pl-1" align="right" style="top: 0px">
                    <?= \common\models\Adsense::show('right') ?>
                </div>
            </div>
        </div>
        <div align="center">

            <?= \common\models\Adsense::show('bottom') ?>
        </div>
    </div>


</div>
<!-- /.main-container -->

<!--Login model-->
<div id="doLogin" class="modal fade  white-text">
    <div class="modal-dialog modal-md ">
        <div class="modal-content">
            <div class="modal-body" align="center" style="padding: 0;">
                <div class="row white white-text">
                    <div class="col-lg-12"
                         style="padding: 50px 30px;color: #fff;background-color: limegreen;border-radius: 10px;"
                         align="center">
                        <i class="fa fa-user" style="font-size: 60px;color: #fff"></i>
                        <br>
                        <h2 style="color: #fff;font-family: proxima-regular;letter-spacing: 1px;" class="white-text">
                            <?= Yii::t('app', 'Login Now'); ?>
                        </h2>
                        <p class="white-text"
                           style="color: #fff;font-family: proxima-thin;letter-spacing: 1px;line-height: 20px;font-size: 16px;">
                            <?= Yii::t('app', 'Want to contact ???'); ?> <br>
                            <?= Yii::t('app', 'Please Login/Signup for further conversation'); ?>
                        </p>
                        <a style="font-family: proxima-thin;" class="btn btn-warning btn-lg "
                           href="<?= \yii\helpers\Url::toRoute('site/login') ?>"/>
                        <i class="fa fa-bolt"></i>
                        <?= Yii::t('app', 'Login'); ?>
                        </a>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<!--login model end-->
<div id="wrong" class="modal fade">
    <div class="modal-dialog" style="border: none">
        <div class="modal-content">
            <div class="modal-header bg-success">
                <h3 class="modal-title" style="color:#fff">
                    <i class="fa fa-warning"></i>
                    <?= Yii::t('app', 'Ops. something went wong.') ?>
                </h3>
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only"><?= Yii::t('app', 'Close') ?></span>
                </button>
            </div>
            <div class="modal-body" align="center">
                <div style="font-size: 50px;color: #777;">
                    <i class="fa fa-warning"></i>
                </div>
                <p style="font-size: 18px;color: #555;">
                    <?= Yii::t('app', 'YOU CAN NOT SEND MESSAGE YOURSELF') ?>
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn" data-dismiss="modal"><?= Yii::t('app', 'Ok Close it') ?>!</button>
            </div>
        </div>
    </div>

</div>

<!-- Modal contactAdvertiser -->

<div class="modal fade" id="contactAdvertiser" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><i class=" icon-mail-2"></i> <?= Yii::t('app', 'Contact advertiser') ?> </h4>

                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                            class="sr-only">Close</span></button>
            </div>
            <div class="modal-body">
                <form role="form">
                    <div class="form-group">
                        <label for="recipient-name" class="control-label"><?= Yii::t('app', 'Name') ?>:</label>
                        <input class="form-control required" id="recipient-name"
                               placeholder="<?= Yii::t('app', 'Your name') ?>"
                               data-placement="top" data-trigger="manual"
                               data-content="Must be at least 3 characters long, and must only contain letters."
                               type="text">
                    </div>
                    <div class="form-group">
                        <label for="sender-email" class="control-label"><?= Yii::t('app', 'Email') ?>:</label>
                        <input id="sender-email" type="text"
                               data-content="Must be a valid e-mail address (user@gmail.com)" data-trigger="manual"
                               data-placement="top" placeholder="<?= Yii::t('app', 'email@you.com') ?>"
                               class="form-control email">
                    </div>
                    <div class="form-group">
                        <label for="recipient-Phone-Number" class="control-label"><?= Yii::t('app', 'Phone Number') ?>
                            :</label>
                        <input type="text" maxlength="60" class="form-control" id="recipient-Phone-Number">
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="control-label"><?= Yii::t('app', 'Message') ?> <span
                                    class="text-count">(300) </span>:</label>
                        <textarea class="form-control" id="message-text"
                                  placeholder="<?= Yii::t('app', 'Your message here') ?>.."
                                  data-placement="top" data-trigger="manual"></textarea>
                    </div>
                    <div class="form-group">
                        <p class="help-block pull-left text-danger hide" id="form-error">&nbsp; The form is not
                            valid. </p>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default"
                        data-dismiss="modal"><?= Yii::t('app', 'Cancel') ?></button>
                <button type="submit" class="btn btn-success pull-right"><?= Yii::t('app', 'Send message') ?>!</button>
            </div>
        </div>
    </div>
</div>

<!-- /.modal -->

<!-- Modal contactAdvertiser -->

<div class="frontend-master-form modal fade" id="reportAdvertiser" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><i class="fa icon-info-circled-alt"></i>
                    <span id="ad_report_form-header"><?= Yii::t('app', 'There is something wrong with this ads') ?>?</span>
                </h4>

                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only"><?= Yii::t('app', 'Close') ?></span>
                </button>
            </div>
            <?php $form = ActiveForm::begin([
                'action' => ['ajax/ad-report'],
                //'method' => 'get',
                'options' => ['id' => 'ad_report_form', 'class' => 'frontend-master-form']
            ]); ?>
            <div class="modal-body" id="ad_report_form-body">


                <?php echo $form->field($report, 'reason', [
                    'template' => '
                                <div class="form-group form-group-default">
                                    {label} {input}{error}{hint}
                                </div>'
                ])->dropDownList(
                    ['item unavailable' => 'không tồn tại sản phầm', 'fraud' => 'lỗi', 'lặp', 'wrong category' => 'sai danh mục', 'spam' => 'spam', 'khác'],
                    ['prompt' => 'Chọn...']) ?>
                <?php echo $form->field($report, 'email', [
                    'template' => '<div class="form-group form-group-default">
                                {label} {input}{error}{hint}
                                </div>'
                ])->textInput(['placeholder' => Yii::t('app', 'Your Email Id'), 'type' => 'email']);
                ?>

                <?php echo $form->field($report, 'message', [
                    'template' => '<div class="form-group form-group-default">
                                {label} {input}{error}{hint}
                                </div>'
                ])->textarea(['placeholder' => Yii::t('app', 'Describe Your Issue'), 'cols' => 3]);
                ?>
                <?php echo $form->field($report, 'ad_id', [
                    'template' => '<div class="form-group form-group-default">
                                {label} {input}{error}{hint}
                                </div>'
                ])->hiddenInput(['value' => $model->id])->label(false);
                ?>
                <?php echo $form->field($report, 'url', [
                    'template' => '<div class="form-group form-group-default">
                                {label} {input}{error}{hint}
                                </div>'
                ])->hiddenInput(['value' => \yii\helpers\Url::canonical()])->label(false);
                ?>

            </div>
            <div class="modal-footer no-bd">
                <?php echo \yii\helpers\Html::button(Yii::t('app', 'Cancel'), ['class' => 'btn', 'data-dismiss' => 'modal']) ?>
                <?php echo \yii\helpers\Html::submitButton(Yii::t('app', 'Report this Ad'), ['class' => 'btn btn-success close_modal_v', 'id' => 'report_modal']) ?>

            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>

<!-- /.modal -->
<script>

    $(document).ready(function () {

        // Slider
        var $mainImgSlider = $('.bxslider').bxSlider({
            speed: 1000,
            pagerCustom: '#bx-pager',
            controls: false,
            adaptiveHeight: true
        });

        // initiates responsive slide
        var settings = function () {
            var mobileSettings = {
                slideWidth: 80,
                minSlides: 2,
                maxSlides: 5,
                slideMargin: 5,
                adaptiveHeight: true,
                pager: false

            };
            var pcSettings = {
                slideWidth: 100,
                minSlides: 3,
                maxSlides: 5,
                pager: false,
                slideMargin: 10,
                adaptiveHeight: true
            };
            return ($(window).width() < 768) ? mobileSettings : pcSettings;
        }

        var thumbSlider;

        function tourLandingScript() {
            thumbSlider.reloadSlider(settings());
        }

        thumbSlider = $('.product-view-thumb').bxSlider(settings());
        $(window).resize(tourLandingScript);

    });
</script>
<?php
$uid = '';
if (!Yii::$app->user->isGuest)//make like !Yii::$app->user->isGuest after testing
{
    $uid = Yii::$app->user->identity->getId();
    echo $this->render('_chat.php', [
        'model' => $model, 'more' => $more, 'related' => $related
    ]);
}
?>
<script>
    function sorry() {
        alert('<?= Yii::t('app', 'You cannot send message yourself') ?>')
    }

    $('#DoLogin').modal('open');

    function openChat() {

        $('#ChatMsg').show();
        var div = $("#displayMsg");
        div.scrollTop(div.prop('scrollHeight'));
        setInterval(function () {
            newUpdate();
        }, 3000);

    }

    function closeChat() {
        $('#ChatMsg').hide();

    }

    function sendmsg(id, ads) {
        var receiver = id;
        var url = "<?= \yii\helpers\Url::toRoute('message/send-msg'); ?>?msg=" + $('#msgboxtext').val() + "&receiver=" + receiver + "&ad=" + ads;
        $.post(url, function (data) {
            $('#msgboxtext').val(null);
            // $('#displayMsg').append("Sent");
        }).fail(function () {
            $('#display-msg').append("<p>Connection Error...</p>");
        });

    }

    function newUpdate() {
        var chat = "<?= $uid ?>" + "<?= $model->id ?>" + "<?= $model->user_id ?>";
        var receiver = "<?= $model->user_id ?>";
        var current = $('#check').val();

        var check = "<?= \yii\helpers\Url::toRoute('message/check'); ?>?chatId=" + chat;

        $.post(check, function (data) {
            if (data > current) {
                console.log("chech client :" + current + " --- " + " server : " + data);
                $('#check').val(data);
                // var new_msg = getNewMsg(chat);
                var check = "<?= \yii\helpers\Url::toRoute('message/load-new-msg'); ?>?chat=" + chat;
                $.post(check, function (data) {
                    $('#displayMsg').append(data);
                    var div = $("#displayMsg");
                    div.scrollTop(div.prop('scrollHeight'));
                }).fail(function () {
                    return "fools";
                });
            }
        }).fail(function (data) {
            alert(data + " urrent:" + current);
        });
    }

    function getNewMsg(chat) {
        var check = "<?= \yii\helpers\Url::toRoute('message/load-new-msg'); ?>?chat=" + chat;
        $.post(check, function (data) {
            return data;
        }).fail(function () {
            return "fools";
        });
    }

    function CheckUpdate() {

        var chat = "<?= $uid ?>" + "<?= $model->id ?>" + "<?= $model->user_id ?>";
        var receiver = "<?= $model->user_id ?>";
        var current = $('#check').val();

        var check = "<?= \yii\helpers\Url::toRoute('message/check'); ?>?chatId=" + chat;

        var url = "<?= \yii\helpers\Url::toRoute('message/load-msg-tpl')?>?chat_id=" + chat + "&current=" + current;
        $.post(url, function (data) {
            $('#displayMsg').append(data);
            var div = $("#displayMsg");
            div.scrollTop(div.prop('scrollHeight'));

        }).done(function () {
            $('#displayMsg').append(data);
        }).fail(function () {
            $('#display-msg').append("<p>Connection Error...</p>");
        });


        $.post(check, function (data) {
            if (data > current) {
                document.getElementById("check").value = data;

            }
        }).fail(function () {
            alert(data + " urrent:" + current);
        });
    }


</script>
<script type="text/javascript">
    $(window).on('load', function () {
        //
    });
    $("#btnOpenContact").click( function()
        {
            $('#contactAdvertiser').modal('show');
        }
    );
</script>
<!--//single-page-->
<script>
    function initMap() {

        var myLatLng = {lat:  <?= $model['lat'] ?>, lng: <?= $model['lng'] ?>};
        var map = new google.maps.Map(document.getElementById('vendorMap'), {
            zoom: 10,
            center: myLatLng
        });
        var marker = new google.maps.Marker({
            position: myLatLng,
            map: map,
            title: 'Location'
        });
    }
</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=<?= $api['api_key']; ?>&callback=initMap"></script>

<!--//single-page-->