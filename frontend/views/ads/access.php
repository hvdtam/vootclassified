<?php
$this->title = "Item No Longer Available";
?>
<style>
    .error-page {
        background-color: #fff;
        padding: 50px;
        margin-top: 30px;
        text-align: center;
        border: 1px solid #ddd;
        -webkit-box-shadow: 0 3px 12px rgba(0, 0, 0, .175);
        box-shadow: 0 3px 12px rgba(0, 0, 0, .175);
    }

    .error-page-content {
        max-width: 675px;
        display: inline-block;
    }

    .error-page h2 {
        font-size: 75px;
        text-transform: capitalize;
        margin-bottom: 25px;
        color: #555;
    }

    .error-page-content .btn-primary {
        margin: 30px 0;
    }
</style>

<div class="main-container inner-page">
    <div class="container">
        <div class="error-page">
            <div class="error-page-content">
                <h2>404 <span>error</span></h2>
                <p>
                    <?= Yii::t('app', 'This is embarrassing. We didn’t find anyone'); ?>

                </p>
                <h1>
                    <?= Yii::t('app', 'This Item No Longer Available') ?>
                </h1>
                <a href="<?= \yii\helpers\Url::toRoute('ads/post') ?>"
                   class="btn btn-warning"> <?= Yii::t('app', 'Post Free Ad'); ?>   </a>
            </div>
        </div>
        <center>
            <div class="col-md-12 col-md-offset-2">
                <br><br><br><br>
                <?= \common\models\Adsense::show('bottom') ?>

            </div>
        </center>
    </div>

</div>
