<?php

use \frontend\models\TemplatesDesign;
use \yii\bootstrap\ActiveForm;

$session2 = Yii::$app->cache;
$default_selected = $session2->get('default_currency');
$default = (isset($default_selected)) ? $default_selected : $currency_default;
?>
<div class="mobile_search_wrp hidden-lappy">
    <div class="row">
        <div class="col-6">
            <button class="btn btn-block  btn-primary" data-toggle="collapse" data-target="#search">
                <i class="fa fa-search"></i> Search
            </button>

        </div>
        <div class="col-6">
            <div class="dropdown">
                <button type="button" class="btn btn-block btn-success dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-map-marker"></i> Nearby
                </button>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="?near=10">10 Km range</a>
                    <a class="dropdown-item" href="?near=20">20 Km range</a>
                    <a class="dropdown-item" href="?near=30">30 Km range</a>
                    <a class="dropdown-item" href="?near=50">50 Km range</a>
                    <a class="dropdown-item" href="?near=70">70 Km range</a>
                    <a class="dropdown-item" href="?near=100">100 Km range</a>
                    <a class="dropdown-item" href="?near=150">150 Km range</a>
                    <a class="dropdown-item" href="?near=200">200 Km range</a>
                    <a class="dropdown-item" href="?near=500">500 Km range</a>
                    <a class="dropdown-item" href="?near=1000">1000 Km range</a>


                </div>
            </div>
        </div>
        <div id="search" class="collapse">
            <div class="col-12">
                <div class="card" style="margin-top: 15px;box-shadow: 0px 0px 20px #55555540; border-radius: 8px;">
                    <div class="card-body">
                        <?php $form = ActiveForm::begin(['action' => \yii\helpers\Url::toRoute('ads/all'), 'class' => 'banner-form', 'method' => 'GET']) ?>
                        <div class="row">
                            <input type="hidden" name="SearchForm[category]"
                                   class="form-control has-icon autocomplete-category-ajax"
                                   id="autocomplete-category-ajax" value="">
                            <input type="hidden" name="SearchForm[type]"
                                   class="form-control has-icon autocomplete-type-ajax" id="autocomplete-type-ajax"
                                   value="">


                            <div class="col-md-3 marB15">
                                <select name="SearchForm[category]" class="form-control searchInput keyword">
                                    <?php
                                    foreach ($category as $list) {
                                        $name = $list['name'];
                                        echo "<option value='$name'>" . $name . "</option>";
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-md-3 marB15 hidden">
                                <?= TemplatesDesign::HomeCategoryMenu($category) ?>
                            </div>
                            <div class="col-md-3 marB15">
                                <input class="form-control searchInput keyword" name="SearchForm[item]" type="text"
                                       placeholder="<?= Yii::t('app', 'What? (e.g. Mobile Sale )'); ?>">
                            </div>
                            <div class="col-md-3 marB15">
                                <input type="text" data-target="#selectRegion" id="dropdownMenu2" data-toggle="modal"
                                       class="form-control searchInput keyword CityValue"
                                       value="<?= Yii::t('app', $cityDefault); ?>"
                                       placeholder="<?= Yii::t('app', 'Where'); ?>..?">
                            </div>
                            <div class="col-md-3 marB15">
                                <button class="btn btn-block btn-primary "><i class="fa fa-search"></i>
                                </button>
                            </div>
                        </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="search-row-wrapper hidden-sm hidden-xs position-sticky" style="top: 0;z-index: 1">
    <div class="container">
        <?php $form = ActiveForm::begin(['action' => \yii\helpers\Url::toRoute('ads/all'), 'class' => 'banner-form', 'method' => 'GET']) ?>
        <div class="row no-gutters">
            <input type="hidden" name="SearchForm[type]" class="form-control has-icon autocomplete-type-ajax"
                   id="autocomplete-type-ajax" value="cat">


            <div class="col-md-3">
                <input list="catagoryDataList" class="form-control searchInput keyword" name="SearchForm[category]"
                       type="text" placeholder="<?= Yii::t('app', 'Category'); ?>">
                <datalist id="catagoryDataList">
                    <?php
                    foreach ($category as $list) {
                        $name = $list['name'];
                        echo "<option value='$name'>$name</option>";
                    }
                    ?>
                </datalist>
            </div>

            <div class="col-md-4">
                <input class="form-control searchInput keyword" name="SearchForm[item]" type="text"
                       placeholder="<?= Yii::t('app', 'What? (e.g. Mobile Sale )'); ?>">
            </div>

            <div class="col-md-3">
                <div data-target="#selectRegion" id="dropdownMenu2" data-toggle="modal"
                     class="form-control searchInput keyword" placeholder="<?= Yii::t('app', 'Where'); ?>..?">
                    <div style="padding: 10px 5px;">
                        In <span class="CityText"><?= Yii::t('app', $cityDefault); ?></span>
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                <button class="btn btn-block searchInput btn-primary  "><i class="fa fa-search"></i> <strong><?= Yii::t('app', 'QUICK SEARCH'); ?></strong>
                </button>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
        <div class="row">
            <div class="col-md-1">
                <p style="padding-top: 35px;padding-bottom: 10px;">
                    <b> <?= Yii::t('app', 'NEAR BY'); ?></b>
                </p>
            </div>
            <div class="col-md-11">
                <p style="padding-top: 0px;padding-bottom: 10px;">

                <div class="chip" style="margin-bottom: 5px;">
                    <a href="?near=10" class="catChips">10Km</a>
                    <span class="closebtn" onclick="this.parentElement.style.display='none'">&times;</span>
                </div>
                <div class="chip" style="margin-bottom: 5px;">
                    <a href="?near=20" class="catChips">20Km</a>
                    <span class="closebtn" onclick="this.parentElement.style.display='none'">&times;</span>
                </div>
                <div class="chip" style="margin-bottom: 5px;">
                    <a href="?near=30" class="catChips">30Km</a>
                    <span class="closebtn" onclick="this.parentElement.style.display='none'">&times;</span>
                </div>
                <div class="chip" style="margin-bottom: 5px;">
                    <a href="?near=50" class="catChips">50Km</a>
                    <span class="closebtn" onclick="this.parentElement.style.display='none'">&times;</span>
                </div>
                <div class="chip" style="margin-bottom: 5px;">
                    <a href="?near=70" class="catChips">70Km</a>
                    <span class="closebtn" onclick="this.parentElement.style.display='none'">&times;</span>
                </div>
                <div class="chip" style="margin-bottom: 5px;">
                    <a href="?near=100" class="catChips">100Km</a>
                    <span class="closebtn" onclick="this.parentElement.style.display='none'">&times;</span>
                </div>
                <div class="chip" style="margin-bottom: 5px;">
                    <a href="?near=150" class="catChips">150Km</a>
                    <span class="closebtn" onclick="this.parentElement.style.display='none'">&times;</span>
                </div>
                <div class="chip" style="margin-bottom: 5px;">
                    <a href="?near=200" class="catChips">200Km</a>
                    <span class="closebtn" onclick="this.parentElement.style.display='none'">&times;</span>
                </div>
                <div class="chip" style="margin-bottom: 5px;">
                    <a href="?near=500" class="catChips">500Km</a>
                    <span class="closebtn" onclick="this.parentElement.style.display='none'">&times;</span>
                </div>


            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
