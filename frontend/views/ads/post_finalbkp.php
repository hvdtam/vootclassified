<?php
/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\file\FileInput;

$this->title = 'Classified Ads Script Php | Voot Classified';

$uid = Yii::$app->user->id;
$usrInfo = \common\models\User::findOne($uid);
$parSub = common\models\SubCategory::find()->where(['name' => $sub])->one();
$defaultSettings = \common\models\DefaultSetting::getDefaultSetting();
$mobileUrl = \yii\helpers\Url::to(['ads/verify']);
$mobileSettings = \common\models\SmsTwilio::find()->one();
echo Yii::$app->params['sid'];
//\Yii::$app->pinPayment->settings['mode'] = 'new mode';

\Yii::$app->sms->useFileTransport = false;

?>
<style>
    p {
        padding: 0 !important;
        margin-bottom: 0;
    }
</style>
<input type="hidden" id="mobileurl124" value="<?= $mobileUrl ?>">
<input type="hidden" id="otpurl" value="<?= \yii\helpers\Url::to(['ads/verify-otp']); ?>">

<input type="hidden" id="PostType7842" value="<?= \yii\helpers\Url::toRoute('search/ptype') ?>" name="HidType">
<div class="main-container">
    <div class="container">
        <div class="row">
            <div class="col-md-9 page-content">
                <div class="inner-box category-content">
                    <h2 class="title-2 uppercase">
                        <strong>
                            <i class="icon-docs"></i>
                            More Information About your ads, <span class="text-success">Step : 2 - of -2</span>
                        </strong>
                    </h2>

                    <div class="row">
                        <div class="col-md-12" style="background-color: #fff;padding: 25px 10px;line-height: 66px;"
                             align="center">
                            <h3 style="letter-spacing: 4px;text-transform: uppercase;color: #999;">Previous
                                Information</h3>
                            <div class="chip CmaainI">
                                <i class="<?= \common\models\Category::IconByName($saved['category']) ?>"></i>
                                <span class="catChips">Category > <?= $saved['category'] ?></span>
                                <span class="closebtn" onclick="this.parentElement.style.display='none'">&times;</span>
                            </div>

                            <div class="chip CmaainI">
                                <i class="pe-7s-menu"></i>
                                <span class="catChips">Sub Category > <?= $saved['sub_category'] ?></span>
                                <span class="closebtn" onclick="this.parentElement.style.display='none'">&times;</span>
                            </div>
                        </div>
                        <div class="col-sm-12">

                            <?php $form = ActiveForm::begin([
                                'layout' => 'horizontal',
                                'fieldConfig' =>
                                    [
                                        'horizontalCssClasses' =>
                                            [
                                                'label' => 'col-sm-3 col-form-label',
                                                'offset' => '',
                                                'wrapper' => 'col-sm-8 row',
                                                'error' => 'col-sm-8  col-sm-push-3 imgHintS',
                                                'hint' => 'col-sm-8  col-sm-push-3 imgHintS',
                                            ],
                                    ],
                                'options' => ['enctype' => 'multipart/form-data', 'id' => 'post1489'],
                                'enableAjaxValidation' => false,
                            ]) ?>
                            <!--                            <form class="form-horizontal">-->

                            <?php
                            $savedData = json_decode($saved['more'], true);

                            foreach ($custom as $foAds) {

                                $data = $foAds['custom_options'];
                                $options = explode(',', $data);
                                $option = array();
                                foreach ($options as $optionList) {
                                    $option[$optionList] = $optionList;
                                };
                                if (isset($savedData)) {
                                    $title = $foAds['custom_title'];
                                    $value = "";//implode(',',$savedData[$foAds['custom_title']]);
                                } else {
                                    $title = $foAds['custom_title'];
                                    $value = "";
                                };
                                //   echo $form->field($model, 'more[]')->hiddenInput(['value'=>$foAds['custom_title']])->label(false);

                                if ($foAds['custom_type'] == "textinput") {
                                    echo $form->field($model, 'more[' . $foAds['custom_title'] . '][]')->textInput(['value' => $value])->label($foAds['custom_title'])->error(['not empty' => $title . ' cannot be empty']);

                                } elseif ($foAds['custom_type'] == "select") {
                                    ?>
                                    <?= $form->field($model, 'more[' . $foAds['custom_title'] . '][]', [
                                        'template' => '<div class="form-group row">{label}<div class="col-sm-8">{input}{error}{hint}</div></div>'])
                                        ->dropDownList($option)->label($foAds['custom_title']);
                                    ?>

                                    <?php
                                } elseif ($foAds['custom_type'] == "checkbox") {

                                    echo $form->field($model, 'more[' . $foAds['custom_title'] . '][]', [
                                        'template' => '<div class="form-group row">{label}<div class="col-sm-8">{input}{error}{hint}</div></div>'])
                                        ->checkboxList($option)->label($foAds['custom_title']);
                                } elseif ($foAds['custom_type'] == "radio") {

                                    echo $form->field($model, 'more[' . $foAds['custom_title'] . '][]', [
                                        'template' => '<div class="form-group row">{label}<div class="col-sm-8">{input}{error}{hint}</div></div>'])
                                        ->radioList($option)->label($foAds['custom_title']);

                                } else {
                                    echo $form->field($model, 'more[' . $foAds['custom_title'] . '][]')->textInput(['value' => $value])->label($foAds['custom_title'])->error(['not empty' => $title . ' cannot be empty']);
                                }


                            }
                            ?>
                            <div class="content-subheading">
                                <i class="icon-user fa"></i>
                                <strong>Seller information</strong>
                            </div>


                            <!-- Text input-->
                            <?= $form->field($model, 'name', [
                                'template' => '<div class="form-group row">{label}<div class="col-sm-8">{input}{error}{hint}</div></div>'])->textInput(['placeholder' => 'What is your good name...?']);
                            ?>
                            <?php
                            if ($mobileSettings['status'] == "1") {
                                echo $form->field($model, 'mobile', [
                                    'template' => '<div class="form-group row">{beginLabel}{labelTitle}{endLabel}<div class="col-sm-8"><div class="input-group">{input}
            <span class="input-group-addon mobilevVerification btn btn-success"><i class="fa fa-send"></i> Verify Mobile Number</span></div>{error}{hint}</div></div>'
                                ]);
                            } else {
                                echo $form->field($model, 'mobile', [
                                    'template' => '<div class="form-group row">{label}<div class="col-sm-8">{input}{error}{hint}</div></div>'])->textInput(['placeholder' => 'Enter Your Mobile Number']);
                            }
                            ?>
                            <?php // $form->field($model, 'mobile',[
                            // 'template' => '<div class="form-group row">{label}<div class="col-sm-8">{input}{error}{hint}</div></div>','addon' => ['prepend' => ['content'=>'@']]])->textInput(['placeholder'=>'Your Working Mobile Numbe...r?']);
                            ?>
                            <div class="form-group field-enterotp rounded bg-light shadow-lg pt-3 pb-2 required"
                                 style="display:none">
                                <div class="form-group row" id="optvirifyresult">
                                    <label class="control-label col-sm-3 col-form-label" for="enterotp">
                                        Enter Your OTP
                                    </label>
                                    <div class="col-sm-8">
                                        <div class="row">
                                            <div class="col-lg-6 col-md-6 col-sm-12">
                                                <input type="text" id="enterotp" class="form-control" name="enterotp"
                                                       placeholder="XXXX" aria-required="true">
                                            </div>
                                            <div>
                                                <button class="btn btn-md btn-secondary otpvalidate">
                                                    Verify Now
                                                </button>
                                            </div>
                                        </div>
                                        <p class="help-block help-block-error col-sm-8  col-sm-push-3 otpHintS"></p>

                                    </div>
                                </div>
                                <div class="well bg-success p-3 text-white" style="display:none" id="otpalert">
                                    <b id="otpmsg"></b>
                                </div>
                            </div>

                            <?= $form->field($model, 'email', [
                                'template' => '<div class="form-group row">{label}<div class="col-sm-8">{input}{error}{hint}</div></div>'])->textInput(['placeholder' => 'What is your Working Email...?']);
                            ?>


                            <hr>
                            <!-- Select Basic -->


                            <div class="card bg-light card-body mb-3">
                                <h3><i class=" icon-certificate icon-color-1"></i> Make your Ad Premium
                                </h3>

                                <p>Premium ads help sellers promote their product or service by getting
                                    their ads more visibility with more
                                    buyers and sell what they want faster.
                                </p>

                                <div class="form-group row">
                                    <table class="table table-hover checkboxtable">
                                        <tr>
                                            <td>

                                                <div class="form-check">
                                                    <label class="form-check-label">
                                                        <input onclick="$('#payable').text('0')"
                                                               class="form-check-input" id="reg" type="radio"
                                                               value="regular" name="AdsForm[premium]" checked>

                                                        <?= Yii::t('app', 'Regular ads'); ?>
                                                    </label>
                                                </div>

                                            </td>
                                            <td><p>$00.00</p></td>
                                        </tr>
                                        <?php
                                        $premium = common\models\AdsPremium::find()->all();
                                        foreach ($premium as $adsList) {
                                            ?>
                                            <tr>
                                                <td>
                                                    <div class="form-check">
                                                        <label class="form-check-label">
                                                            <input onclick="$('#payable').text(<?= $adsList['price'] ?>)"
                                                                   class="form-check-input" id="<?= $adsList['id'] ?>"
                                                                   value="<?= $adsList['name'] ?>" type="radio"
                                                                   name="AdsForm[premium]">

                                                            <strong> <?= Yii::t('app', $adsList['name']); ?></strong>
                                                        </label>
                                                        <small style="font-size: 10px;">
                                                            &nbsp; <?= ($adsList['home_page'] == "yes") ? "( Front page ad )" : "" ?>
                                                        </small>
                                                    </div>

                                                </td>
                                                <td><p>$<?= $adsList['price'] ?>.00/-</p></td>
                                            </tr>
                                            <?php
                                        }
                                        ?>

                                        <tr>
                                            <td>
                                                <div class="form-group row">
                                                    <div class="col-sm-8">
                                                        <select class="form-control" name="AdsForm[method]"
                                                                id="PaymentMethod">
                                                            <?php
                                                            $pay = \common\models\Payment::find()->where(['status' => 'enable'])->all();
                                                            foreach ($pay as $method) {
                                                                ?>
                                                                <option value="<?= $method['view'] ?>"><?= $method['name'] ?></option>';
                                                                <?php
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </td>
                                            <td><p><strong>Payable Amount : $<span id="payable">00</span>.00</strong>
                                                </p></td>
                                        </tr>
                                    </table>

                                </div>
                            </div>

                            <!-- terms -->
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label p-0">Terms</label>

                                <div class="col-sm-8">

                                    <label class="custom-control custom-checkbox mb-2 mr-sm-2 mb-sm-0">
                                        <input type="checkbox" class="custom-control-input">
                                        <span class="custom-control-indicator"></span>
                                        <span class="custom-control-description"> Remember above contact information</span>
                                    </label>

                                </div>
                            </div>

                            <!-- Button  -->
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label"></label>

                                <div class="col-sm-8">
                                    <input type="button" id="button1id74568"
                                           style="display:<?= ($mobileSettings['status'] == "1") ? 'none' : 'inline' ?>"
                                           class="btn btn-success btn-lg" value="SUBMIT MY AD NOW">
                                    <button type="reset" class="btn btn-info btn-lg">
                                        Reset
                                    </button>
                                </div>
                            </div>


                            <?php ActiveForm::end(); ?>

                        </div>
                    </div>
                </div>
            </div>
            <!-- /.page-content -->

            <div class="col-md-3 reg-sidebar">
                <div class="reg-sidebar-inner text-center">
                    <div class="promo-text-box"><i class=" icon-picture fa fa-4x icon-color-1"></i>

                        <h3><strong>Post a Free Classified</strong></h3>

                        <p> Post your free online classified ads with us. Lorem ipsum dolor sit amet, consectetur
                            adipiscing elit. </p>
                    </div>

                    <div class="card sidebar-card">
                        <div class="card-header uppercase">
                            <small><strong>How to sell quickly?</strong></small>
                        </div>
                        <div class="card-content">
                            <div class="card-body text-left">
                                <ul class="list-check">
                                    <li> Use a brief title and description of the item</li>
                                    <li> Make sure you post in the correct category</li>
                                    <li> Add nice photos to your ad</li>
                                    <li> Put a reasonable price</li>
                                    <li> Check the item before publish</li>

                                </ul>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
            <!--/.reg-sidebar-->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->
</div>

<!-- /.main-container -->

