<?php
$widgets = new \common\models\Plugin();
?>
<div class="row">
    <div class="col-md-9 page-content col-thin-right">
        <div class="inner inner-box ads-details-wrapper">
            <h1 class="auto-heading">
                        <span class="auto-title left">
                            <?= $model->ad_title; ?>
                        </span>
                <span class="auto-price pull-right">
                        <?= $model->currency_symbol; ?> <?= $model->price; ?>
                    </span>
            </h1>
            <h5>
                    <span>
                        <?php
                        $widgets->for = "7896";
                        $widgets->data = $model;
                        $widgets->get();
                        ?>
                    </span>
            </h5>

            <div style="clear:both;"></div>
            <span class="info-row">
                    <span class="date">
                        <i class=" icon-clock"> </i>
                        <?= date('d/m/Y', $model->created_at) ?>
                        <small>
                            (
                            <?= \common\models\Analytic::time_elapsed_string($model->created_at) ?>
                            <?= Yii::t('app', 'Ago'); ?>
                            )
                        </small>
                    </span> -
                    <span class="item-location">
                        <?= $model->address; ?> <?= $model->city; ?>  |

                    </span>
                </span>
            <div class="row">
                <div class="col-sm-9 automobile-left-col">

                    <div class="item-image">
                        <ul class="bxslider">
                            <?php
                            $image = $model->image;
                            $imgChunck = explode(",", $image);

                            foreach ($imgChunck as $arr => $img) {
                                ?>
                                <li>
                                    <img onerror="this.src='<?= Yii::getAlias('@web') ?>/images/item/itemLight_lg.jpg'"
                                         src="<?= Yii::getAlias('@web') ?>/images/item/<?= $img ?>"
                                         alt="<?= $model['ad_title'] ?>">
                                </li>
                                <?php
                            }
                            ?>

                        </ul>

                        <div class="product-view-thumb-wrapper">


                            <ul id="bx-pager" class="product-view-thumb">
                                <?php
                                $image = $model->image;
                                $imgChunck = explode(",", $image);

                                foreach ($imgChunck as $arr => $img) {
                                    ?>
                                    <li>
                                        <a data-slide-index="<?= $arr; ?>" class="thumb-item-link" href="#">
                                            <img class="lazy"
                                                 src="<?= Yii::getAlias('@web') ?>/images/item/itemLight.jpg"
                                                 data-src="<?= Yii::getAlias('@web') ?>/images/item/<?= $img ?>"
                                                 data-srcset="<?= Yii::getAlias('@web') ?>/images/item/<?= $img ?>"
                                                 alt="<?= $model['ad_title'] ?>">

                                        </a>
                                    </li>
                                    <?php
                                }
                                ?>


                            </ul>


                        </div>

                    </div>
                    <!--ads-image-->
                </div>
                <!-- /.automobile-left-col-->

                <div class="col-sm-3 automobile-right-col">
                    <div class="inner">

                        <div class="key-features">
                            <div class="media">

                                <div class="media-body">
                                    <span class="media-heading"><?= $model->ad_type; ?></span>
                                    <span class="data-type"> <?= Yii::t('app', 'Ad Type') ?></span>
                                </div>
                            </div>

                            <div class="media">

                                <div class="media-body">
                                    <span class="media-heading"><?= $model->type; ?></span>
                                    <span class="data-type"><?= \frontend\models\TemplatesDesign::TypeDefiner($model->category) ?></span>
                                </div>
                            </div>

                            <div class="media">

                                <div class="media-body">
                                    <span class="media-heading"><?= $model->sub_category; ?></span>
                                    <span class="data-type"><?= Yii::t('app', 'Sub Category') ?></span>
                                </div>
                            </div>

                            <div class="media">

                                <div class="media-body">
                                    <span class="media-heading"><?= $model->category; ?></span>
                                    <span class="data-type"><?= Yii::t('app', 'Category') ?></span>
                                </div>
                            </div>

                            <?php


                            $data = json_decode($model['more']);
                            $data = ($data == null) ? false : $data;

                            if ($data) {
                                foreach ($data as $key => $value) {
                                    if (isset($value)) continue;
                                    $value = implode(',', $value);
                                    if (isset($key)) continue;
                                    ?>
                                    <li>
                                        <p class=" no-margin ">
                                            <strong><?= $key ?>:</strong>
                                            <?= $value; ?>
                                        </p>
                                    </li>
                                    <?php
                                }
                            }
                            //
                            ?>


                        </div>
                    </div>

                </div>

                <div class="col-lg-12">
                    <i class="fa fa-link"></i> Tags :
                    <?php
                    $tags = explode(',', $model->tags);
                    foreach ($tags as $tag) {
                        ?>
                        <a class="btn btn-light btn-md" href="">
                            <?= $tag; ?>
                        </a>
                        <?php
                    }
                    ?>

                </div>
            </div>
            <!--/.row-->


            <div class="Ads-Details">
                <h5 class="list-title"><strong>Ads Details</strong></h5>

                <div class="row">
                    <div class="ads-details-info col-md-8">
                        <?= $model->ad_description; ?>

                        <h4 class="text-uppercase">Address </h4>
                        <address>
                            <p>
                                <?= $model->address; ?>
                                <br>
                                <?= Yii::t('app', 'Phone Number') ?>: <?= $model->mobile; ?><br>

                                <?= Yii::t('app', 'Email') ?>: <?= $model->email; ?><br>
                            </p>
                        </address>


                    </div>
                    <div class="col-md-4">
                        <aside class="panel panel-body panel-details">
                            <ul>
                                <li>
                                    <p class=" no-margin "><strong>Prices:</strong>
                                        from <?= $model->currency_symbol; ?> <?= $model->price; ?> </p>
                                </li>
                                <li>
                                    <p class=" no-margin ">
                                        <strong><?= Yii::t('app', 'Category') ?> :</strong>
                                        <?= $model->type; ?>
                                        <?= $model->sub_category; ?>

                                    </p>
                                </li>
                                <li>
                                    <p class=" no-margin ">
                                        <strong><?= Yii::t('app', 'Location') ?> :</strong>
                                        <?= $model->city; ?>

                                    </p>
                                </li>


                                <?php

                                if (!empty($model['more'])) {
                                    $data = json_decode($model['more']);
                                    foreach ($data as $key => $value) {
                                        if (isset($value)) continue;
                                        $value = implode(',', $value);
                                        if (empty($key)) continue;
                                        ?>
                                        <li>
                                            <p class="no-margin ">
                                                <strong><?= $key ?>:</strong>
                                                <?= $value; ?>
                                            </p>
                                        </li>
                                        <?php
                                    }
                                }

                                //
                                ?>


                            </ul>
                        </aside>

                    </div>
                </div>

                <h5 class="list-title"><strong>Map</strong></h5>

                <div id="vendorMap"></div>
                <?php
                $widgets->for = "8459";
                $widgets->data = $model;
                $widgets->get();
                ?>

                <div class="content-footer mt-5 text-left">

                    <div style="clear: both"></div>
                    <?php
                    if (Yii::$app->user->isGuest and $model['guest'] == 0) {
                        ?>
                        <button class="btn btn-success  " data-toggle="modal" data-target="#doLogin">
                            <i class="fa fa-comment text-success"></i> <b><?= Yii::t('app', 'Send Message') ?></b>
                        </button>
                        <a class="btn  btn-info" href="tel:<?= $author->phone_number; ?>">
                            <i class=" icon-phone-1"></i> <?= Yii::t('app', 'Login To Call') ?>
                        </a>
                        <?php
                    } elseif (Yii::$app->user->id == $model['user_id'] and $model['guest'] == 0) {
                        ?>
                        <button class="btn btn-success  " data-toggle="modal" data-target="#wrong">
                            <i class="fa fa-comment"></i> <b><?= Yii::t('app', 'Send Message') ?></b>
                        </button>
                        <a class="btn  btn-info" data-toggle="modal" data-target="#wrong">
                            <i class=" icon-phone-1"></i> <?= Yii::t('app', 'Your Cannot Call himself') ?> <?= $author->phone_number; ?>
                        </a>
                        <?php
                    } elseif ($model['guest'] == 1) {
                        ?>

                        <a class="btn  btn-info" data-toggle="modal" data-target="#wrong">
                            <i class=" icon-phone-1"></i> <?= Yii::t('app', 'Contact') ?> <?= $model['mobile'] ?>
                        </a>
                        <?php
                    } else {

                        ?>
                        <button class="btn btn-success  "
                                onclick="<?= (Yii::$app->user->isGuest == $model->user_id) ? "sorry()" : "openChat()"; ?>">
                            <i class="fa fa-comment"></i> <b><?= Yii::t('app', 'Send Message') ?></b>
                        </button>
                        <a class="btn  btn-info" href="tel:<?= isset($author->phone_number); ?>">
                            <i class=" icon-phone-1"></i> <?= isset($author->phone_number); ?>
                        </a>
                        <?php
                    }
                    ?>

                    <button type="button" href="#reportAdvertiser" data-toggle="modal" class="btn" style="color: #777">
                        <i class="fa icon-info-circled-alt"></i> <b><?= Yii::t('app', 'Report') ?></b>
                    </button>
                </div>
            </div>
        </div>
        <!--/.ads-details-wrapper-->
    </div>
    <!--/.page-content-->

    <div class="col-md-3  page-sidebar-right">
        <aside>
            <div class="card sidebar-card card-contact-seller">
                <div class="card-header"><?= Yii::t('app', 'Contact Dealer') ?></div>
                <div class="card-content user-info">
                    <div class="card-body text-center">
                        <div class="seller-info">
                            <div class="company-logo-thumb">
                                <a>
                                    <?php
                                    if ($model['guest'] == 1) {
                                        ?>
                                        <img onerror="this.src='<?= Yii::getAlias('@web') ?>/images/users/profile-placeholder.jpg'"
                                             src="<?= Yii::getAlias('@web') ?>/images/users/profile-placeholder.jpg"
                                             class="" alt="<?= $model->name ?>">
                                        <?php
                                    } else {
                                        ?>
                                        <img onerror="this.src='<?= Yii::getAlias('@web') ?>/images/users/profile-placeholder.jpg'"
                                             src="<?= Yii::getAlias('@web') ?>/images/users/<?= $author->image ?>"
                                             class="" alt="<?= $author->username ?>">
                                        <?php
                                    }
                                    ?>
                                </a>
                            </div>
                            <br>

                            <?php
                            if ($model['guest'] == 1) {
                                ?>
                                <h3 class="no-margin"> <?= $model['name']; ?> </h3>
                                <p> <?= Yii::t('app', 'Email') ?>: <strong><?= $model['email'] ?></strong></p>
                                <p> <?= Yii::t('app', 'Call') ?> :
                                    <strong>

                                        <?php
                                        if (Yii::$app->user->isGuest) {
                                            ?>
                                            <a data-toggle='modal' data-target='#doLogin'> <i class='icon-phone-1'></i>
                                                XXXXXXXXX</a>
                                            <?php
                                        } else {
                                            ?>
                                            <a href='tel:<?= $model['mobile']; ?>'>
                                                <i class='icon-phone-1'></i> <?= $model['mobile']; ?>
                                            </a>
                                            <?php
                                        }
                                        ?>
                                    </strong>
                                </p>
                                <?php
                            } else {
                                ?>
                                <h3 class="no-margin"> <?= $author['first_name']; ?> <?= $author['last_name']; ?> </h3>
                                <p><?= Yii::t('app', 'Location') ?>: <strong><?= $author->city ?></strong></p>

                                <p> <?= Yii::t('app', 'Email') ?>: <strong><?= $author->email ?></strong></p>
                                <p> <?= Yii::t('app', 'Call') ?> :
                                    <strong>

                                        <?php
                                        if (Yii::$app->user->isGuest) {
                                            ?>
                                            <a data-toggle='modal' data-target='#doLogin'> <i class='icon-phone-1'></i>
                                                XXXXXXXXX</a>
                                            <?php
                                        } else {
                                            ?>
                                            <a href='tel:<?= $author->phone_number; ?>'>
                                                <i class='icon-phone-1'></i> <?= ($model->mobile) ? $model->mobile : $author->phone_number; ?>
                                            </a>
                                            <?php
                                        }
                                        ?>
                                    </strong>
                                </p>
                                <?php
                            }

                            ?>


                        </div>
                        <div class="user-ads-action">
                            <?php
                            if (Yii::$app->user->isGuest and $model['guest'] == 0) {
                                ?>
                                <button class="btn btn-success btn-block " data-toggle="modal" data-target="#doLogin">
                                    <i class="fa fa-comment text-success"></i>
                                    <b><?= Yii::t('app', 'Send Message') ?></b>
                                </button>
                                <?php
                            } elseif (Yii::$app->user->id == $model['user_id'] and $model['guest'] == 0) {
                                ?>
                                <button class="btn btn-success btn-block " data-toggle="modal" data-target="#wrong">
                                    <i class="fa fa-comment"></i> <b><?= Yii::t('app', 'Send Message') ?></b>
                                </button>
                                <?php
                            } elseif ($model['guest'] == 0) {
                                ?>
                                <button class="btn btn-success btn-block "
                                        onclick="<?= (Yii::$app->user->isGuest == $model->user_id) ? "sorry()" : "openChat()"; ?>">
                                    <i class="fa fa-comment"></i> <b><?= Yii::t('app', 'Send Message') ?></b>
                                </button>
                                <?php
                            } else {
                                echo "You Cannot Cannot Chat with this user";
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <div class="card">
                <div class="card-header">
                    <b style="color: #555">
                        <i class="fa fa-star-o"></i> <?= Yii::t('app', 'Feedback') ?>
                    </b>
                </div>
                <div class="card-body">
                    <p>
                        <i><?= Yii::t('app', 'If You like this ad you can save this ad from hit the like button,
                                anything wrong this this ad please report.') ?>
                        </i>
                    </p>
                    <div class="btn-group btn-group-justified">
                        <div class="btn-group">
                            <button id="saveAdsBtn<?= $model['id'] ?>" type="button"
                                    class="btn btn-warning saveAdsBtn make-favorite" ads="<?= $model['id'] ?>"
                                    data-del="<?= \yii\helpers\Url::toRoute('saved/remove-ads/' . base64_encode($model['id'])); ?>"
                                    data-ref="<?= \yii\helpers\Url::toRoute('saved/ads/' . base64_encode($model['id'])); ?>">
                                <i id="dil<?= $model['id'] ?>" class="fa fa-heart "></i>
                                <b>
                                    <span id="saved<?= $model['id'] ?>"><?= Yii::t('app', 'Save') ?></span>
                                </b>
                            </button>
                        </div>

                        <div class="btn-group">

                            <button type="button" href="#reportAdvertiser" data-toggle="modal" class="btn"
                                    style="color: #777">
                                <i class="fa icon-info-circled-alt"></i> <b><?= Yii::t('app', 'Report') ?></b>
                            </button>
                        </div>

                    </div>
                    <small style="color:#777;"> <?= Yii::t('app', 'Report for spam or wrong category') ?>.. </small>

                    <br>

                </div>
            </div>
            <div class="card sidebar-card">
                <div class="card-header"><?= Yii::t('app', 'Safety Tips for Buyers') ?></div>
                <div class="card-content">
                    <div class="card-body text-left">
                        <ul class="list-check">
                            <li> <?= Yii::t('app', 'Meet seller at a public place') ?></li>
                            <li><?= Yii::t('app', 'Check the item before you buy') ?> </li>
                            <li> <?= Yii::t('app', 'Pay only after collecting the item') ?></li>
                        </ul>

                    </div>
                </div>
            </div>
            <!--/.categories-list-->
        </aside>
    </div>
    <!--/.page-side-bar-->
    <div class="col-xl-12 col-12 col-lg-12 content-box ">
        <div class="row row-featured">
            <div class="col-xl-12  box-title ">
                <div class="inner">
                    <h2>
                        <i class="fa fa-soccer-ball-o"></i>
                        <span> <?= Yii::t('app', 'Related') ?>  </span>
                        <?= Yii::t('app', 'Ads') ?>

                    </h2>
                </div>
            </div>
            <div style="clear: both"></div>
            <div class=" relative  content featured-list-row  w100">

                <nav class="slider-nav has-white-bg nav-narrow-svg">
                    <a class="prev">
                        <span class="nav-icon-wrap"></span>

                    </a>
                    <a class="next">
                        <span class="nav-icon-wrap"></span>
                    </a>
                </nav>

                <div class="no-margin featured-list-slider ">
                    <?php
                    foreach ($related as $list) {
                        $imagebase = $list['image'];
                        $imageArray = explode(",", $imagebase);
                        $imgs = reset($imageArray);
                        $img = ($list['category'] == 'Jobs') ? 'QuikJobs.jpg' : $imgs;;
                        $controlHide = ($list['category'] == 'Jobs') ? 'hidden' : '';
                        $controlShow = ($list['category'] == 'Jobs') ? '' : 'hidden';
                        // $faker = Factory::create();
                        $labelClass = $list['premium']; //$faker->randomElement(['topAds','featuredAds','urgentAds']);

                        $ImageUrl = Yii::getAlias('@web') . '/images/item/' . $img; // Image Directory URL
                        $data_del = \yii\helpers\Url::toRoute('saved/remove-ads/' . base64_encode($list['id'])); // SAVE ADS URL
                        $data_ref = \yii\helpers\Url::toRoute('saved/ads/' . base64_encode($list['id'])); // REMOVE AD FROM SAVE LIST URL
                        $imgAlt = 'Image';
                        $imgTitle = '';
                        $filtrUrlFind = array('/\,/', '/\s+/', '/\?+/', '/\'/');
                        $filtrUrlReplace = array('_', '_', '_');

                        $detailUrl = \yii\helpers\Url::to([
                            'ads/detail',
                            'title' => trim(preg_replace($filtrUrlFind, $filtrUrlReplace, $list['ad_title'])),
                            'IID' => $list['id']
                        ]);
                        ?>
                        <div class="item">
                            <a href="<?= $detailUrl; ?>">
                                    <span class="item-carousel-thumb">
                                       <img height="150" class="lazy"
                                            src="<?= Yii::getAlias('@web') ?>/images/item/itemLight.jpg"
                                            data-src="<?= $ImageUrl; ?>"
                                            data-srcset="<?= $ImageUrl; ?> 2x, <?= $ImageUrl; ?> 1x"
                                            alt="I'm an image!">

                                     </span>
                                <span class="item-name">
                                        <?= substr($list['ad_title'], '0', '20'); ?>...
                                    </span>
                                <span class="price"><?= Yii::t('app', '') ?>
                                    <?= $list['currency_symbol']; ?><?= $list['price']; ?>
                                    </span>
                            </a>
                        </div>
                        <?php
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>

