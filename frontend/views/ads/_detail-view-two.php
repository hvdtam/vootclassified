<?php
$userModel = \common\models\User::findOne($model->user_id);
$ads = \common\models\Ads::find()->where(['user_id' => $userModel['id']])->all();
$premium = \common\models\Ads::find()->where(['user_id' => $userModel['id']])->andWhere('premium != ""')->count();
$like = \common\models\SavedAds::find()->where(['user_id' => $userModel['id']])->count();
$widgets = new \common\models\Plugin();

?>
<div class="container">
    <div class="row">
        <div class="col-md-9 page-content col-thin-right">
            <div class="card">
                <div class="inner card-body ads-details-wrapper">
                    <h1> <?= $model->ad_title; ?>
                        <?php if(isset($model->ad_type)):?>
                        <small class="label label-default adlistingtype"><?= $model->ad_type; ?></small>
                        <?php endif;?>

                    </h1>
                    <h5>
                        <span>
                           <?php
                           $widgets->for = "7896";
                           $widgets->data = $model;
                           $widgets->get();
                           ?>
                        </span>
                    </h5>
                    <span class="info-row">
                    <span class="date">
                        <i class=" icon-clock"></i>
                        <?= date('d/m/Y', $model->created_at) ?>
                        <small>
                            (
                            <?= \common\models\Analytic::time_elapsed_string($model->created_at) ?>
                            <?= Yii::t('app', 'Ago') ?>
                            )
                        </small>
                    </span> -
                    <span class="category"><?= $model->category; ?> </span> -
                    <span class="item-location">
                        <i class="fa fa-map-marker"></i>
                        <?= $model->city; ?>
                    </span>

                </span>


                    <div class="item-image">
                        <h1 class="pricetag"> <?= $model->currency_symbol; ?><?= $model->price; ?></h1>
                        <div style="max-width: 100%;" class="bx-wrapper">
                            <div style="width: 100%; overflow: hidden; position: relative; height: 445.333px;"
                                 aria-live="polite" class="bx-viewport">
                                <ul style="width: 3215%; position: relative; transition-duration: 0s; transform: translate3d(-790px, 0px, 0px);"
                                    class="bxslider">

                                    <?php
                                    $image = $model->image;
                                    $imgChunck = explode(",", $image);

                                    foreach ($imgChunck as $arr => $img) {
                                        ?>
                                        <li aria-hidden="true" class="bx-clone"
                                            style="float: left; list-style: outside none none; position: relative; width: 790px;">
                                            <img src="<?= Yii::getAlias('@web') ?>/images/item/<?= $img ?>" alt="img">
                                        </li>
                                        <?php
                                    }
                                    ?>

                                    <li class="hidden" aria-hidden="false"
                                        style="float: left; list-style: outside none none; position: relative; width: 790px;">
                                        <img src="<?= Yii::getAlias('@web') ?>/images/item/<?= $img ?>" alt="img">
                                    </li>

                                </ul>
                            </div>
                            <div class="bx-controls bx-has-controls-direction">
                                <div class="bx-controls-direction">
                                    <a class="bx-prev" href=""><?= Yii::t('app', 'Prev') ?></a>
                                    <a class="bx-next" href=""><?= Yii::t('app', 'Next') ?></a>
                                </div>
                            </div>
                        </div>
                        <div id="bx-pager">
                            <?php
                            $image = $model->image;
                            $imgChunck = explode(",", $image);

                            foreach ($imgChunck as $arr => $img) {
                                ?>
                                <a class="thumb-item-link <?= ($arr == '0') ? 'active' : ''; ?>active"
                                   data-slide-index="<?= $arr; ?>" href="#">
                                    <img src="<?= Yii::getAlias('@web') ?>/images/item/<?= $img ?>" alt="img">
                                </a>


                                <?php
                            }
                            ?>
                        </div>
                    </div>
                    <!--ads-image-->

                    <div class="d-none Ads-Details">


                        <h5 class="list-title"><strong><?= Yii::t('app', 'Ads Details') ?></strong></h5>

                        <div class="row">
                            <div class="ads-details-info col-md-8">
                                <?= $model->ad_description; ?>

                                <h4 class="text-uppercase"><?= Yii::t('app', 'Address') ?> </h4>
                                <address>
                                    <p>
                                        <?= $model->address; ?>
                                        <br>
                                        <?= Yii::t('app', 'Phone Number') ?>: <?= $model->mobile; ?><br>

                                        <?= Yii::t('app', 'EMail') ?> : <?= $model->email; ?><br>
                                    </p>
                                </address>


                            </div>

                        </div>

                    </div>

                </div>
                <div class="card-footer text-left">
                    <div style="clear: both"></div>

                    <div class="btn-group">
                        <button id="saveAdsBtn<?= $model['id'] ?>" type="button"
                                class="btn btn-warning saveAdsBtn make-favorite" ads="<?= $model['id'] ?>"
                                data-del="<?= \yii\helpers\Url::toRoute('saved/remove-ads/' . base64_encode($model['id'])); ?>"
                                data-ref="<?= \yii\helpers\Url::toRoute('saved/ads/' . base64_encode($model['id'])); ?>">
                            <i id="dil<?= $model['id'] ?>" class="fa fa-heart "></i>
                            <b>
                                <span id="saved<?= $model['id'] ?>"><?= Yii::t('app', 'Save') ?></span>
                            </b>
                        </button>
                    </div>


                    <div class="btn-group ">
                        <button type="button" href="#reportAdvertiser" data-toggle="modal" class="btn"
                                style="color:#555;">
                            <i class="fa icon-info-circled-alt"></i> <b><?= Yii::t('app', 'Report') ?></b>
                        </button>
                    </div>
                </div>
            </div>
            <div class="card mt-3 tab-card">
                <div class="card-header tab-card-header">
                    <ul class="nav nav-tabs card-header-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link" id="one-tab" data-toggle="tab" href="#one" role="tab"
                               aria-controls="One" aria-selected="true">
                                <?= Yii::t('app', 'Ads Details') ?>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="two-tab" data-toggle="tab" href="#two" role="tab"
                               aria-controls="Two" aria-selected="false">
                                <?= Yii::t('app', 'Description') ?>
                            </a>
                        </li>

                        <?php
                        $widgets->for = "8410";
                        $widgets->data = $model;
                        $widgets->get();
                        ?>
                        <li class="nav-item">
                            <a class="nav-link" id="map-tab" data-toggle="tab" href="#map" role="tab"
                               aria-controls="four" aria-selected="false"><?= Yii::t('app', 'Map') ?></a>
                        </li>
                    </ul>
                </div>

                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active p-3" id="one" role="tabpanel" aria-labelledby="one-tab">

                        <h2 class="page-header">
                            <?= Yii::t('app', 'Ads Detail') ?>
                        </h2>
                        <ul class="list-group">
                            <li class="list-group-item">
                                <p class=" no-margin "><strong><?= Yii::t('app', 'Prices') ?>
                                        :</strong><?= Yii::t('app', 'from') ?>   <?= $model->currency_symbol; ?> <?= $model->price; ?>
                                </p>
                            </li>
                            <li class="list-group-item">
                                <p class=" no-margin ">
                                    <strong><?= Yii::t('app', 'Category') ?> :</strong>
                                    <?= $model->type; ?>
                                    <?= $model->sub_category; ?>

                                </p>
                            </li>
                            <li class="list-group-item">
                                <p class=" no-margin ">
                                    <strong><?= Yii::t('app', 'Location') ?> :</strong>
                                    <?= $model->city; ?>

                                </p>
                            </li>
                            <?php
                            $data = json_decode($model['more']);
                            $data = ($data == null) ? false : $data;
                            if ($data) {
                                foreach ($data as $key => $value) {
                                    if (isset($value)) continue;
                                    $value = implode(',', $value);
                                    if (empty($key)) continue;
                                    ?>
                                    <li class="list-group-item">
                                        <p class=" no-margin ">
                                            <strong><?= $key ?>:</strong>
                                            <?= $value; ?>
                                        </p>
                                    </li>
                                    <?php
                                }
                            }
                            //
                            ?>


                        </ul>
                    </div>
                    <div class="tab-pane fade p-3" id="two" role="tabpanel" aria-labelledby="two-tab">
                        <h2 class="page-header">
                            <?= Yii::t('app', 'Description') ?>
                        </h2>
                        <p class="card-text"> <?= $model->ad_description; ?></p>
                        <h4 class="text-uppercase">Address </h4>
                        <address>
                            <p>
                                <?= $model->address; ?>
                                <br>
                                <?= Yii::t('app', 'Phone Number') ?>: <?= $model->mobile; ?><br>

                                <?= Yii::t('app', 'Email') ?>: <?= $model->email; ?><br>
                            </p>
                        </address>
                    </div>
                    <div class="tab-pane fade p-3" id="three" role="tabpanel" aria-labelledby="three-tab">
                        <?php
                        $widgets->for = "8459";
                        $widgets->data = $model;
                        $widgets->get();
                        ?>
                    </div>
                    <div class="tab-pane fade p-3" id="map" role="tabpanel" aria-labelledby="map-tab">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div id="vendorMap"></div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <!--/.ads-details-wrapper-->

        </div>
        <!--/.page-content-->

        <div class="col-md-3  page-sidebar-right">
            <aside>

                <div class="card card-profile card-secondary">
                    <div class="card-header"
                         style="background-image: url('<?= Yii::getAlias('@web') ?>/images/banner/bg7.jpg');background-position: center center;background-size: cover">
                        <div class="profile-picture">
                            <?php
                            if ($model['guest'] == 1) {
                                ?>
                                <img onerror="this.src='<?= Yii::getAlias('@web') ?>/images/users/profile-placeholder.jpg'"
                                     src="<?= Yii::getAlias('@web') ?>/images/users/profile-placeholder.jpg" class=""
                                     alt="<?= $model->name ?>">
                                <?php
                            } else {
                                ?>
                                <img onerror="this.src='<?= Yii::getAlias('@web') ?>/images/users/profile-placeholder.jpg'"
                                     src="<?= Yii::getAlias('@web') ?>/images/users/<?= $author['image'] ?>" class=""
                                     alt="<?= $author['username'] ?>">
                                <?php
                            }
                            ?>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="user-profile text-center">

                            <?php
                            if ($model['guest'] == 1) {
                                ?>
                                <div class="name"><?= $model['name']; ?></div>
                                <div class="desc">
                                    <i class="fa fa-phone"></i>
                                    <?= ($model->mobile) ? $model->mobile : "Mobile Number Not Avalable"; ?>
                                </div>
                                <?php
                            } else {
                                ?>
                                <div class="name"><?= $author['first_name'] . ' ' . $author['last_name'] ?></div>
                                <div class="job"><?= $author['city']; ?></div>
                                <div class="desc">
                                    <i class="fa fa-phone"></i>
                                    <?= ($model->mobile) ? $model->mobile : $author['phone_number']; ?>
                                </div>
                                <?php
                            }
                            ?>
                            <div class="view-profile">
                                <?php
                                if (Yii::$app->user->isGuest and $model['guest'] == 0) {
                                    ?>
                                    <button class="btn btn-primary btn-block " data-toggle="modal"
                                            data-target="#doLogin">
                                        <i class="fa fa-comment"></i> <b><?= Yii::t('app', 'Send Message') ?></b>
                                    </button>
                                    <?php
                                } elseif (Yii::$app->user->id == $model['user_id'] and $model['guest'] == 0) {
                                    ?>
                                    <button class="btn btn-primary btn-block " data-toggle="modal" data-target="#wrong">
                                        <i class="fa fa-comment"></i> <b><?= Yii::t('app', 'Send Message') ?></b>
                                    </button>
                                    <?php
                                } elseif ($model['guest'] == 1) {
                                    ?>
                                    <button class="btn btn-primary btn-block " id="btnOpenContact">
                                        <i class="fa fa-comment"></i> <b><?= Yii::t('app', 'Call') ?>
                                            :</b> <?= ($model->mobile) ? $model->mobile : $author->phone_number; ?>
                                    </button>
                                    <?php
                                } else {
                                    ?>
                                    <button class="btn btn-primary btn-block "
                                            onclick="<?= (Yii::$app->user->isGuest == $model->user_id) ? "sorry()" : "openChat()"; ?>">
                                        <i class="fa fa-comment"></i> <b><?= Yii::t('app', 'Send Message') ?></b>
                                    </button>
                                    <?php
                                }
                                ?>

                            </div>

                        </div>
                    </div>

                    <div class="card-footer">
                        <div class="row user-stats text-center">
                            <div class="col">
                                <div class="number"><?= count($ads); ?></div>
                                <div class="title"><?= Yii::t('app', 'Ads') ?></div>
                            </div>
                            <div class="col">
                                <div class="number"><?= $premium; ?></div>
                                <div class="title"><?= Yii::t('app', 'Premium') ?> </div>
                            </div>
                            <div class="col">
                                <div class="number"><?= $like; ?></div>
                                <div class="title"><?= Yii::t('app', 'Like Ads') ?></div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
                if ($model['guest'] == 1) {
                    ?>
                    <a class="btn btn-default btn-block text-secondary" href="tel:<?= $model->mobile; ?>">

                        <b>
                            <?= Yii::t('app', 'Call') ?> : <i class=" icon-phone-1"></i>
                        </b>
                        <button class="btn btn-primary btn-block ">
                            <i class="fa fa-comment"></i> <b><?= Yii::t('app', 'Call') ?>
                                :</b> <?= ($model->mobile) ? $model->mobile : "number Not Available"; ?>
                        </button>
                    </a>

                    <?php
                } else {
                    ?>
                    <a class="btn btn-default btn-block text-secondary" href="tel:<?= $author['phone_number']; ?>">

                        <b>
                            <?= Yii::t('app', 'Call') ?> : <i class=" icon-phone-1"></i>
                        </b>
                        <button class="btn btn-primary btn-block ">
                            <i class="fa fa-comment"></i> <b><?= Yii::t('app', 'Call') ?>
                                :</b> <?= ($model->mobile) ? $model->mobile : $author->phone_number; ?>
                        </button>
                    </a>

                    <?php
                }
                ?>

                <br>
                <div class="card">
                    <div class="card-header">
                        <b style="color: #555">
                            <i class="fa fa-star-o"></i> <?= Yii::t('app', 'Feedback') ?>
                        </b>
                    </div>
                    <div class="card-body">
                        <p>
                            <i><?= Yii::t('app', 'If You like this ad you can save this ad from hit the like button,
                                anything wrong this this ad please report.') ?>
                            </i>
                        </p>
                        <div class="btn-group btn-group-justified">
                            <div class="btn-group">
                                <button id="saveAdsBtn<?= $model['id'] ?>" type="button"
                                        class="btn btn-warning saveAdsBtn make-favorite" ads="<?= $model['id'] ?>"
                                        data-del="<?= \yii\helpers\Url::toRoute('saved/remove-ads/' . base64_encode($model['id'])); ?>"
                                        data-ref="<?= \yii\helpers\Url::toRoute('saved/ads/' . base64_encode($model['id'])); ?>">
                                    <i id="dil<?= $model['id'] ?>" class="fa fa-heart "></i>
                                    <b>
                                        <span id="saved<?= $model['id'] ?>"><?= Yii::t('app', 'Save') ?></span>
                                    </b>
                                </button>

                            </div>


                            <div class="btn-group">

                                <button type="button" href="#reportAdvertiser" data-toggle="modal" class="btn"
                                        style="color:#555;">
                                    <i class="fa icon-info-circled-alt"></i> <b><?= Yii::t('app', 'Report') ?></b>
                                </button>
                            </div>

                        </div>
                        <small style="color:#777;"> <?= Yii::t('app', 'Report for spam or wrong category') ?>.. </small>

                        <br>

                    </div>
                </div>

                <div class="card sidebar-panel">
                    <div class="card-header"><?= Yii::t('app', 'Safety Tips for Buyers') ?></div>
                    <div class="card-content">
                        <div class="card-body text-left">

                            <ul class="list-check">
                                <li> <?= Yii::t('app', 'Meet seller at a public place') ?></li>
                                <li><?= Yii::t('app', 'Check the item before you buy') ?> </li>
                                <li> <?= Yii::t('app', 'Pay only after collecting the item') ?></li>
                            </ul>


                        </div>

                    </div>
                </div>
                <!--/.categories-list-->
            </aside>
        </div>
        <!--/.page-side-bar-->


    </div>
    <hr>
    <div class="">

        <div class="card">
            <div class="card-header">
                <b style="color: #555">
                    <i class="fa fa-laptop"></i><?= Yii::t('app', 'Related Ads') ?>
                </b>
            </div>
            <div class="card-body">
                <div class=" relative  content featured-list-row  w100">

                    <nav class="slider-nav has-white-bg nav-narrow-svg">
                        <a class="prev">
                            <span class="nav-icon-wrap"></span>

                        </a>
                        <a class="next">
                            <span class="nav-icon-wrap"></span>
                        </a>
                    </nav>

                    <div class="no-margin featured-list-slider ">
                        <?php
                        foreach ($related as $list) {
                            $imagebase = $list['image'];
                            $imageArray = explode(",", $imagebase);
                            $imgs = reset($imageArray);
                            $img = ($list['category'] == 'Jobs') ? 'QuikJobs.jpg' : $imgs;;
                            $controlHide = ($list['category'] == 'Jobs') ? 'hidden' : '';
                            $controlShow = ($list['category'] == 'Jobs') ? '' : 'hidden';
                            // $faker = Factory::create();
                            $labelClass = $list['premium']; //$faker->randomElement(['topAds','featuredAds','urgentAds']);

                            $ImageUrl = Yii::getAlias('@web') . '/images/item/' . $img; // Image Directory URL
                            $data_del = \yii\helpers\Url::toRoute('saved/remove-ads/' . base64_encode($list['id'])); // SAVE ADS URL
                            $data_ref = \yii\helpers\Url::toRoute('saved/ads/' . base64_encode($list['id'])); // REMOVE AD FROM SAVE LIST URL
                            $imgAlt = 'Image';
                            $imgTitle = '';
                            $filtrUrlFind = array('/\,/', '/\s+/', '/\?+/', '/\'/');
                            $filtrUrlReplace = array('_', '_', '_');

                            $detailUrl = \yii\helpers\Url::to([
                                'ads/detail',
                                'title' => trim(preg_replace($filtrUrlFind, $filtrUrlReplace, $list['ad_title'])),
                                'IID' => $list['id']
                            ]);
                            ?>
                            <div class="item">
                                <a href="<?= $detailUrl; ?>" style="padding: 10px;">
                                    <span class="item-carousel-thumb">
                                        <img height="110" class="img-responsive" src="<?= $ImageUrl; ?>"
                                             alt="<?= $imgAlt; ?>">
                                     </span>
                                    <span class="item-name" style="min-height: 0;">
                                        <?= substr($list['ad_title'], '0', '20'); ?>...
                                    </span>
                                    <span class="price">
                                        <?= $list['currency_symbol']; ?><?= $list['price']; ?>
                                    </span>
                                </a>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>

    </div>

</div>