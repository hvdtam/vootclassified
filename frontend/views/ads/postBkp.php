<?php
/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\file\FileInput;
use dosamigos\ckeditor\CKEditor;

$this->title = 'Classified Ads Script Php | Voot Classified';

$uid = Yii::$app->user->id;
$usrInfo = \common\models\User::findOne($uid);
$defaultSettings = \common\models\DefaultSetting::getDefaultSetting();
$api = \common\models\ApiKeys::find()->where(['type' => 'maps'])->andWhere(['status' => 'enable'])->one();

?>
<?php
$session = Yii::$app->session;

$ses_country = $defaultSettings['country'];
$ses_state = $defaultSettings['state'];
$ses_city = $defaultSettings['city'];
$ses_lat = $defaultSettings['lat'];
$ses_lng = $defaultSettings['lng'];
$ses_currency = $defaultSettings['currency'];
$ses_lng = $defaultSettings['lng'];


$countryIn = ($ses_country) ? $ses_country : 'Choose Country';
$stateIn = ($ses_state) ? $ses_state : 'Choose State';
$cityIn = ($ses_city) ? $ses_city : 'Choose City';
?>
<input type="hidden" id="subcatUrl" value="<?= \yii\helpers\Url::toRoute('search/sub-category') ?>">

<input type="hidden" id="PostType7842" value="<?= \yii\helpers\Url::toRoute('search/ptype') ?>" name="HidType">
<div class="main-container">
    <div class="container">
        <div class="row">
            <div class="col-md-9 page-content">
                <div class="inner-box category-content">
                    <h2 class="title-2 uppercase">
                        <strong> <i class="icon-docs"></i>
                            <?= Yii::t('app', 'Post a Free Classified Ad') ?>
                        </strong>
                    </h2>

                    <div class="row">
                        <div class="col-sm-12">

                            <?php $form = ActiveForm::begin([
                                'layout' => 'horizontal',
                                'fieldConfig' =>
                                    [
                                        'horizontalCssClasses' =>
                                            [
                                                'label' => 'col-sm-3 col-form-label',
                                                'offset' => '',
                                                'wrapper' => 'row',
                                                'error' => 'col-sm-8  col-sm-push-3 imgHintS',
                                                'hint' => 'col-sm-8  col-sm-push-3 imgHintS',
                                            ],
                                    ],
                                'options' => ['enctype' => 'multipart/form-data', 'id' => 'post1489'],
                                'enableAjaxValidation' => false,
                            ]) ?>
                            <!--                            <form class="form-horizontal">-->


                            <div class="form-group row">
                                <label for="" class="col-sm-3 col-form-label"><?= Yii::t('app', 'Ad Type') ?></label>
                                <div class="col-sm-8">
                                    <div class="form-check form-check-inline">
                                        <label class="form-check-label">
                                            <input class="form-check-input" type="radio" name="AdsForm[ad_type]"
                                                   id="inlineRadio1" value="Private"> <?= Yii::t('app', 'Private') ?>
                                        </label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <label class="form-check-label">
                                            <input class="form-check-input" type="radio" name="AdsForm[ad_type]"
                                                   id="inlineRadio2" value="Business"> <?= Yii::t('app', 'Business') ?>
                                        </label>
                                    </div>


                                </div>
                            </div>


                            <?= $form->field($model, 'ad_title', [
                                'template' => '<div class="form-group row">{label}<div class="col-sm-8">{input}{error}{hint}</div></div>']);
                            ?>
                            <?= $form->field($model, 'ad_description', [
                                'template' => '<div class="form-group row">{label}<div class="col-sm-8">{input}{error}{hint}</div></div>'])->textarea(array('row' => '6', 'style' => 'height:150px'));
                            ?>


                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label"><?= Yii::t('app', 'Category') ?></label>
                                <div class="col-sm-8">
                                    <button type="button" href="#select-Category" data-toggle="modal"
                                            class="btn btn-primary"><?= Yii::t('app', 'Select Category') ?></button>

                                </div>

                            </div>
                            <div class="hidden">
                                <input type="text" id="category45983" value="" name="AdsForm[category]">
                                <input type="text" id="Subcategory45983" value="" name="AdsForm[sub_category]">
                            </div>
                            <div class="form-group row catChipsW hidden">
                                <label class="col-sm-3 col-form-label"></label>
                                <div class="col-sm-8" style="margin-top: 10px;margin-bottom: 10px">
                                    <div class="chip CmaainI">
                                        <i class="fa fa-user"></i>
                                        <span class="catChips"><?= Yii::t('app', 'Category') ?></span>
                                        <span class="closebtn"
                                              onclick="this.parentElement.style.display='none'">&times;</span>
                                    </div>

                                    <div class="chip subChip">
                                        <i class="fa fa-user"></i>
                                        <span id="subChipTxt"></span>
                                        <span class="closebtn"
                                              onclick="this.parentElement.style.display='none'">&times;</span>
                                    </div>
                                </div>
                            </div>

                            <?= $form->field($model, 'type', [
                                'template' => '<div class="form-group row">{label}<div class="col-sm-8">{input}{error}{hint}</div></div>'])->dropDownList(
                                [
                                    'prompt' => Yii::t('app', 'Select Type'),
                                ]);
                            ?>
                            <?php
                            $cat = '';
                            if ($cat != 'Jobs') {
                                $currency = $defaultSettings['currency'];

                                echo $form->field($model, 'price', [
                                    'template' => '<div class="form-group row">{label}<div class="col-sm-9"><div class="input-group">
                                            <span  class="input-group-addon">
                                            <i>' . $ses_currency . '</i>
                                            </span>{input}{error}{hint}
                                            </div>
                                            </div>
                                    <small id="" class="form-text text-muted">{hint}</small></div>'])->textInput(['placeholder' => 'eg. 1500']);
                            }
                            echo $form->field($model, 'currency_symbol')->hiddenInput(['value' => $ses_currency])->label(false);

                            ?>
                            <?php
                            echo $form->field($model, 'image[]', [
                                'template' => '<div class="form-group row">{label}<div class="col-sm-8">{input}{error}{hint}</div></div>'])->widget(FileInput::classname(), [
                                'options' => ['accept' => 'image/*', 'multiple' => true],
                                'pluginOptions' => [
                                    'maxFileCount' => $adsSetting['number_of_photo'],
                                    'overwriteInitial' => false,
                                    'theme' => 'fa',
                                    'initialPreviewAsData' => 'true',
                                    'cancelLabel' => '',
                                    'cancelClass' => 'hidden',
                                    'browseLabel' => Yii::t('app', 'Photos'),
                                    'browseClass' => 'btn btn-primary',
                                    'browseIcon' => '<i class="fa fa-image"></i>',
                                    'removeLabel' => '',
                                    'removeIcon' => '<i class="fa fa-trash"></i>',
                                    'uploadClass' => 'hidden',
                                    'uploadLabel' => '',
                                    'uploadIcon' => '<i class="fa fa-upload"></i>',
                                    'previewFileType' => 'image',
                                    'allowedFileExtensions' => ['jpg', 'jpeg', 'png'],
                                    'previewClass' => 'bg-light'
                                ]

                            ])->label(Yii::t('app', 'Picture'));
                            ?>


                            <?=
                            $form->field($model, 'tags', [
                                'template' => '<div class="form-group row">{label}<div class="col-sm-8">{input}{error}{hint}</div></div>'])->widget(\pudinglabs\tagsinput\TagsinputWidget::classname(), [
                                'options' => [],
                                'clientOptions' => ['tagClass' => 'bg-dark', 'maxTags' => $adsSetting['number_of_tags']],
                                'clientEvents' => []
                            ]);
                            ?>
                            <!-- Select Basic -->
                            <div class="form-group row">

                                <div class="col-12 " align="center">


                                </div>
                                <label class="col-sm-3 col-form-label" for="seller-Location">
                                    <?= Yii::t('app', 'Address') ?>
                                </label>
                                <div class="col-sm-8" style="padding-right: 28px;padding-left: 33px;">
                                    <?= $form->field($model, 'address')->textInput(['onFocus' => 'geolocate()', 'id' => 'autocomplete'])->label(false); ?>
                                    <blockquote class="blocInput">
                                        <?= Yii::t('app', 'It`s seems like you don`t choose location property. Please Fill correct Address') ?>

                                    </blockquote>
                                </div>

                            </div>

                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label" for="seller-Location">
                                    <?= Yii::t('app', 'City') ?> & <?= Yii::t('app', 'States') ?>
                                </label>
                                <div class="col-sm-8 " align="left">
                                    <div class="chip">
                                        <i class="fa fa-map-marker"></i>
                                        <span>
                                                <?php
                                                echo "<span id='CountryCip745'> " . $countryIn . " </span> >";
                                                echo " <span id='StateCip754'> " . $stateIn . "</span> >";
                                                echo " <span id='CityCip754'> " . $cityIn . "</span>";
                                                ?>
                                            </span>
                                        <span class="closebtn"
                                              onclick="this.parentElement.style.display='none'">&times;</span>
                                    </div>
                                    <a href="#selectRegion" data-toggle="modal"
                                       style="font-size: 12px;color: rgb(52, 225,109);padding: 12px;display: inline-block;position: absolute;font-weight: 600;">
                                        <i class="fa fa-edit"></i><?= Yii::t('app', 'Change') ?>
                                    </a>
                                    <div style="display: none">
                                        <?= $form->field($model, 'latitude')->hiddenInput(['value' => $ses_lat])->label(false); ?>
                                        <?= $form->field($model, 'longitude')->hiddenInput(['value' => $ses_lng])->label(false); ?>

                                        <?= $form->field($model, 'country')->hiddenInput(['id' => 'hiddenInputCountry', 'value' => $ses_country])->label(false); ?>
                                        <?= $form->field($model, 'states')->hiddenInput(['id' => 'hiddenInputState', 'value' => $ses_state])->label(false); ?>
                                        <?= $form->field($model, 'city')->hiddenInput(['id' => 'hiddenInputCity', 'value' => $ses_city])->label(false); ?>


                                    </div>

                                </div>
                            </div>
                            <hr>
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label"></label>

                                <div class="col-sm-8">
                                    <input type="button" id="button1id74568" class="btn btn-success btn-lg"
                                           value="<?= Yii::t('app', 'SAVE AND NEXT') ?>">
                                    <button type="reset" class="btn btn-info btn-lg">
                                        <?= Yii::t('app', 'Reset') ?>
                                    </button>
                                </div>
                            </div>
                            <?php ActiveForm::end(); ?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.page-content -->
            <div class="col-md-3 reg-sidebar">
                <div class="reg-sidebar-inner text-center">
                    <div class="promo-text-box"><i class=" icon-picture fa fa-4x icon-color-1"></i>

                        <h3><strong><?= Yii::t('app', 'Post a Free Classified') ?></strong></h3>

                        <p><?= Yii::t('app', 'Post your free online classified ads with us. Lorem ipsum dolor sit amet, consectetur
                            adipiscing elit.') ?>  </p>
                    </div>

                    <div class="card sidebar-card">
                        <div class="card-header uppercase">
                            <small><strong><?= Yii::t('app', 'How to sell quickly') ?>?</strong></small>
                        </div>
                        <div class="card-content">
                            <div class="card-body text-left">
                                <ul class="list-check">
                                    <li><?= Yii::t('app', 'Use a brief title and description of the item') ?> </li>
                                    <li> <?= Yii::t('app', 'Make sure you post in the correct category') ?></li>
                                    <li> <?= Yii::t('app', 'Add nice photos to your ad') ?></li>
                                    <li> <?= Yii::t('app', 'Put a reasonable price') ?></li>
                                    <li> <?= Yii::t('app', 'Check the item before publish') ?></li>

                                </ul>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
            <!--/.reg-sidebar-->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->
</div>
<input type="hidden" class="field" id="locality" disabled="true" value="24.578941" name="city"/>
<input type="hidden" class="field" id="administrative_area_level_1" value="24.5879" name="state" disabled="true"/>
<input type="hidden" class="field" id="country" name="country" disabled="true"/>

<!-- /.main-container -->
<script>
    var placeSearch, autocomplete;

    var componentForm = {
        locality: 'long_name',
        administrative_area_level_1: 'long_name',
        country: 'long_name'
    };
    //    var componentForm = {
    //        hiddenInputCity: 'long_name'
    //       // hiddenInputState: 'short_name',
    //       // hiddenInputCountry: 'long_name',
    //    };
    function initAutocomplete() {
        // Create the autocomplete object, restricting the search predictions to
        // geographical location types.
        autocomplete = new google.maps.places.Autocomplete(
            document.getElementById('autocomplete'), {types: ['geocode']});

        // Avoid paying for data that you don't need by restricting the set of
        // place fields that are returned to just the address components.
        autocomplete.setFields('address_components');

        // When the user selects an address from the drop-down, populate the
        // address fields in the form.
        autocomplete.addListener('place_changed', fillInAddress);
    }

    function fillInAddress() {
        // Get the place details from the autocomplete object.
        var place = autocomplete.getPlace();
        var lat = place.geometry.location.lat(),
            lng = place.geometry.location.lng();
        $('#adsform-longitude').val(lng);
        $('#adsform-latitude').val(lat);

        for (var component in componentForm) {

            document.getElementById(component).value = '';
            document.getElementById(component).disabled = false;
        }

        // Get each component of the address from the place details,
        // and then fill-in the corresponding field on the form.
        for (var i = 0; i < place.address_components.length; i++) {

            var addressType = place.address_components[i].types[0];

            if (componentForm[addressType]) {
                var val = place.address_components[i][componentForm[addressType]];
                document.getElementById(addressType).value = val;
            }
            if (addressType == 'country') {
                var val = place.address_components[i][componentForm[addressType]];
                $('#hiddenInputCountry').val(val);
                $('#CountryCip745').text(val);
            }

            if (addressType == 'administrative_area_level_1') {
                var val = place.address_components[i][componentForm[addressType]];
                $('#hiddenInputState').val(val);
                $('#StateCip754').text(val);
            }
            if (addressType == 'locality') {
                var val = place.address_components[i][componentForm[addressType]];
                $('#hiddenInputCity').val(val);
                $('#CityCip754').text(val);
            }
        }
    }

    // Bias the autocomplete object to the user's geographical location,
    // as supplied by the browser's 'navigator.geolocation' object.
    function geolocate() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {
                var geolocation = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };
                var circle = new google.maps.Circle(
                    {center: geolocation, radius: position.coords.accuracy});
                autocomplete.setBounds(circle.getBounds());
            });
        }
    }
</script>
<div class="modal fade " id="select-Category" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><i class="fa fa-bars"></i>
                    <?= Yii::t('app', 'Select Category') ?>
                </h4>

                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="post-from-modal-cat" class="row">
                    <?= \frontend\models\TemplatesDesign::PostFormCategoryLoop($categoryList, 'icon'); ?>
                </div>
                <div id="post-from-modal-sub" class="row hidden">
                    <div class="col-lg-12" id="post-modal-waiting">
                        <div class="card card-default bg-inverse" style="padding: 30px 10px">
                            <div class="card-body" align="center">
                                <p style="font-size: 60px;color: #fff">
                                    <i id="waiting-icon" class="fa fa-circle-o-notch fa-spin"></i>
                                </p>
                                <p style="font-size: 16px;color: #fff" id="waiting-text">
                                    <?= Yii::t('app', 'Loading Sub Category') ?>...
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 hidden" id="post-modal-data">
                        <div class="col-sm-12 bg-light"
                             style="margin-top: 10px;margin-bottom: 10px;padding: 17px 14px;border-radius: 4px;">
                            <div class="chip">
                                <i class="fa fa-user "></i>
                                <span class="catChips"><?= Yii::t('app', 'Category') ?></span>
                            </div>


                            <button type="button" class="btn btn-success pull-right"
                                    onclick="$('#post-from-modal-sub').addClass('hidden');$('#post-from-modal-cat').removeClass('hidden');"><?= Yii::t('app', 'Back to Main Category') ?>
                                <i class="fa fa-arrow-up"></i></button>
                        </div>
                        <ul class="list pst-modal-subC" id="post-modal-data-list">


                        </ul>
                    </div>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default cat-model" data-dismiss="modal">
                    <i class=""></i>
                    <span><?= Yii::t('app', 'Close') ?></span>
                </button>
            </div>
        </div>
    </div>
</div>
<script src="https://maps.googleapis.com/maps/api/js?key=<?= $api['api_key']; ?>&libraries=places&callback=initAutocomplete"
        async defer></script>