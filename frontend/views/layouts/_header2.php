<?php
/**
 * Created by PhpStorm.
 * User: Mayank Singh
 * Date: 3/3/2020
 * Time: 12:30 PM
 */

use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;

?>
<div class="container-fluid p-0 bg-light border-bottom ">
    <div class="containers p-0 pr-lg-5 pl-lg-5">

        <div class="d-flex justify-content-between p-2">
            <div class="align-self-center col-xs-2">
                <div class="brand">
                    <a href="<?php echo Yii::$app->homeUrl ?>">
                        <img style="max-width: <?php echo $siteSettings['logo_width'] ?>;max-height:<?php echo $siteSettings['logo_height'] ?>"
                             src="<?php echo Yii::getAlias('@web') ?>/images/site/logo/<?php echo $siteSettings['logo']; ?>">
                    </a>
                </div>
            </div>
            <?php $form = ActiveForm::begin(['action' => \yii\helpers\Url::toRoute('ads/all'), 'class' => 'banner-form', 'method' => 'GET']) ?>

            <div class="align-self-center d-none d-lg-block">
                <input type="hidden" name="fdfd" class="form-control has-icon autocomplete-category-ajax"
                       id="autocomplete-category-ajax" value="">
                <input type="hidden" name="SearchForm[type]" class="form-control has-icon autocomplete-type-ajax"
                       id="autocomplete-type-ajax" value="">

                <div class="location-search-group">
                    <button type="button" class="btn" data-target="#selectRegion" data-toggle="modal"><i
                                class="fa fa-map-pin"></i></button>
                    <input class="main-search-loc CityValue" name="SearchForm[city]" type="text" placeholder="Locaton">

                </div>

                <div class="main-search">
                    <input type="search" id="dosearch7458"
                           placeholder="<?php echo Yii::t('app', 'What? (e.g. Car, Mobile Sale )'); ?>"
                           name="SearchForm[item]" class="text-input">
                    <div id="searchresult784259" class="suggetion bg-green"></div>
                </div>

            </div>
            <?php ActiveForm::end(); ?>
            <div class="align-self-center d-block">
                    <span class="dropdown no-arrow loginTrue">
                        <span class="btn bg-transparent dropdown-toggle" data-toggle="dropdown">
                            <?php
                            if (!Yii::$app->user->isGuest) {
                                $img = Yii::$app->user->identity->image;
                                $name = Yii::$app->user->identity->first_name . " " . Yii::$app->user->identity->last_name;
                                ?>
                                <span class="user-image round-img header-img">
                                    <img width="33"
                                         onerror="<?php echo Yii::getAlias('@web') ?>/images/users/default.jpg"
                                         src="<?php echo Yii::getAlias('@web') ?>/images/users/<?php echo $img; ?>">
                                </span>
                                <?php
                            };
                            ?>

                        </span>
                        <ul class="dropdown-menu dropdown-menu-right user-menu" role="menu">

                            <li class="active dropdown-item">
                                <a href="<?php echo \yii\helpers\Url::toRoute('user/index') ?>">
                                    <i class="icon-home"></i>
                                    <?php echo Yii::t('site_header', 'Personal Home') ?>
                                </a>
                            </li>
                            <li class="dropdown-item">
                                <a href="<?php echo \yii\helpers\Url::toRoute('user/my-ads') ?>"><i
                                            class="icon-th-thumb"></i><?php echo Yii::t('site_header', 'My ads') ?>  </a>
                            </li>
                            <li class="dropdown-item">
                                <a href="<?php echo \yii\helpers\Url::toRoute('user/favourite-ads') ?>">
                                    <i class="icon-heart"></i> <?php echo Yii::t('site_header', 'Favourite ads') ?>
                                </a>
                            </li>
                            <li class="dropdown-item hidden">
                                <a href="account-saved-search.html">
                                    <i class="icon-star-circled"></i> <?php echo Yii::t('site_header', 'Saved search') ?>

                                </a>
                            </li>
                            <li class="dropdown-item hidden">
                                <a href="account-archived-ads.html">
                                    <i class="icon-folder-close"></i>
                                    Archived ads

                                </a>
                            </li>
                            <li class="dropdown-item">
                                <a href="<?php echo \yii\helpers\Url::toRoute('user/pending-ads') ?>">
                                    <i class="icon-hourglass"></i>
                                    <?php echo Yii::t('site_header', 'Pending approval') ?>
                                </a>
                            </li>

                            <li class="dropdown-item">
                                <a data-method="get" href="<?php echo \yii\helpers\Url::toRoute('site/logout') ?>"><i
                                            class=" icon-logout "></i> <?php echo Yii::t('site_header', 'Log out') ?> </a>
                            </li>
                        </ul>
                    </span>

                <a href="<?php echo \yii\helpers\Url::toRoute('site/login') ?>"
                   class="btn text-dark btn-lg text-uppercase loginFalse">
                    <i class="icon-login"></i>
                    <?php echo Yii::t('site_header', 'join') ?>
                </a>

                <a href="<?php echo \yii\helpers\Url::toRoute('ads/post') ?>"
                   class="homeBtn btn-lg d-inline d-lg-none d-sm-inline">
                    <i class="fa fa-cart-plus"></i>
                </a>
                <a href="<?php echo \yii\helpers\Url::toRoute('ads/post') ?>" class="homeBtn btn-lg d-none d-lg-inline">
                    <i class="fa fa-cart-plus"></i>
                    <?php echo Yii::t('site_header', 'POST ADS') ?>
                </a>
            </div>

        </div>
    </div>
</div>