<?php

/* @var $this \yii\web\View */

/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\VootAsset;
use common\widgets\Alert;

use kartik\depdrop;
use kartik\select2;

//use kartik\ipinfo\IpInfo;
//use kartik\popover\PopoverX;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use common\models\Cities;

$defaultSettings = \common\models\DefaultSetting::getDefaultSetting();

if ($defaultSettings['direction'] == 'rtl') {
    \frontend\assets\RtlAsset::register($this);

} else {
    \frontend\assets\VootAsset::register($this);

}

\frontend\assets\TopAsset::register($this);
\frontend\assets\CloudAsset::register($this);
$siteSettings = \common\models\SiteSettings::find()->one();
$select_location_form = new \frontend\models\SelectCityForm();
$defaultSettings = \common\models\DefaultSetting::getDefaultSetting();
//        'themes/assets/css/style.css',
common\models\Track::track();
//Yii::$app->request->cookies['language'];
$theme = $defaultSettings['themes'] . '.css';
$this->registerCssFile("@web/themes/assets/css/" . $theme, ['id' => 'themeCss', 'depends' => [\yii\bootstrap\BootstrapAsset::className()]]);
$this->registerCssFile("@web/css/demo/themecontrol.css", ['depends' => [\yii\bootstrap\BootstrapAsset::className()]]);
$this->registerJsFile("@web/js/themecontrol.js", ['depends' => [\yii\bootstrap\BootstrapAsset::className()]]);
$widgets = new \common\models\Plugin();

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" dir="<?= $defaultSettings['direction'] ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php // Html::csrfMetaTags() ?>
    <?= Html::csrfMetaTags() ?>
    <!-- Fav and touch icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144"
          href="<?= Yii::getAlias('@web') ?>/themes/assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114"
          href="<?= Yii::getAlias('@web') ?>/themes/assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72"
          href="<?= Yii::getAlias('@web') ?>/themes/assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed"
          href="<?= Yii::getAlias('@web') ?>/themes/assets/ico/apple-touch-icon-57-precomposed.png">
    <link rel="shortcut icon" href="<?= Yii::getAlias('@web/images/site/fav/' . $siteSettings['fav_icon']) ?>">
    <title><?= Html::encode($this->title) ?></title>
    <!-- Just for debugging purposes. -->
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <!--    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>-->

    <!-- include pace script for automatic web page progress bar  -->
    <script>
        paceOptions = {
            elements: true
        };
    </script>

    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div id="wrapper">
    <div class="hidden">
        <input type="hidden" name="default_flag" value="<?= $defaultSettings['flag'] ?>" id="dFlagBtn">
        <input type="hidden" name="default_city" value="<?= $defaultSettings['CurrCity'] ?>" id="dCityBtn">
        <input type="hidden" name="default_currency" value="<?= $defaultSettings['currency'] ?>" id="dCityBtn">
        <input type="hidden" name="default_theme"
               value="<?= Yii::getAlias('@web') . '/themes/assets/css/' . $defaultSettings['themes'] ?>.css"
               id="dthemeBtn">
        <input type="hidden" name="default_theme_url" value="<?= Yii::getAlias('@web') . '/themes/assets/css/' ?>"
               id="dthemeUrlDemo">
        <input type="hidden" name="searchUrl" class="hidden"
               value="<?php echo \yii\helpers\Url::toRoute('search/index'); ?>" id="searchUrl">

        <input type="hidden" name="default_bg" data-url="<?= Yii::getAlias('@web') ?>/themes/assets/img/"
               value="<?= $defaultSettings['background'] ?>" id="dBgBtn">

        <input type="hidden" name="webUrl" class="hidden" value="<?= Yii::getAlias('@web') ?>" id="webUrl">
        <input type="hidden" name="HomeUrl" class="hidden" value="<?= \yii\helpers\Url::home(); ?>" id="HomeUrl">
        <input type="hidden" name="IsLogin" class="hidden"
               value="<?= (!Yii::$app->user->isGuest) ? 'login' : 'guest'; ?>" id="IsLogin">
        <input type="hidden" name="DLat" class="hidden" value="26.9124" id="DLat">
        <input type="hidden" name="DLong" class="hidden" value="75.7873" id="DLong">
    </div>


    <!-- /.header -->

    <?= $this->render('_header', [
        'siteSettings' => $siteSettings, 'widgets' => $widgets, 'defaultSettings' => $defaultSettings
    ]) ?>
    <!-- /.header -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <?= Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ]) ?>
            </div>
        </div>
    </div>
    <?php
    $alert = Alert::widget();
    if ($alert) {
        echo "<div style='margin-top: 0px;margin-bottom: -16px;'><div class='row'><div class='col-lg-12'>" . $alert . "</div></div></div>";
    }
    ?>
    <?= $content ?>
    <!-- /.main-container -->
    <footer class="main-footer">
        <div class="footer-content">
            <div class="container">
                <div class="row">
                    <?php

                    $column = \common\models\FooterColumn::find()->all();

                    foreach ($column as $col) {
                        $content = \common\models\FooterContent::find()->where(['column_id' => $col['id']])->all();

                        ?>
                        <div class="<?= $col['column_class'] ?> ">
                            <div class="footer-col">
                                <h4 class="footer-title"><?= $col['name'] ?></h4>
                                <ul class="list-unstyled footer-nav">
                                    <?php
                                    foreach ($content as $link) {
                                        if ($link['type'] == 'customUrl') {
                                            $url = '//' . $link['url'];
                                        } else {
                                            $url = \yii\helpers\Url::toRoute($link['url']);
                                        }
                                        ?>
                                        <li>
                                            <a target="<?= $link['target'] ?>" href="<?= $url; ?>">
                                                <?= Yii::t('site_footer', $link['name']) ?>
                                            </a>
                                        </li>
                                        <?php
                                    }
                                    ?>

                                </ul>
                            </div>
                        </div>
                        <?php
                    }
                    ?>


                    <div class=" col-xl-4 col-xl-4 col-md-4 col-12">
                        <div class="footer-col row">

                            <div class="col-sm-12 col-xs-6 col-xxs-12 no-padding-lg">
                                <div class="mobile-app-content">
                                    <h4 class="footer-title"><?= Yii::t('site_footer', 'Mobile Apps') ?></h4>
                                    <div class="row ">
                                        <div class="col-6  ">
                                            <a class="app-icon" target="_blank" href="https://itunes.apple.com/">
                                                <span class="hide-visually"><?= Yii::t('site_footer', 'iOS app') ?></span>
                                                <img src="<?= Yii::getAlias('@web') . '/themes/' ?>images/site/app_store_badge.svg"
                                                     alt="Available on the App Store">
                                            </a>
                                        </div>
                                        <div class="col-6  ">
                                            <a class="app-icon" target="_blank" href="https://play.google.com/store/">
                                                <span class="hide-visually"><?= Yii::t('site_footer', 'Android App') ?></span>
                                                <img src="<?= Yii::getAlias('@web') . '/themes/' ?>images/site/google-play-badge.svg"
                                                     alt="Available on the App Store">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-12 col-xs-6 col-xxs-12 no-padding-lg">
                                <div class="hero-subscribe">
                                    <h4 class="footer-title no-margin"><?= Yii::t('site_footer', 'Follow us on') ?></h4>
                                    <ul class="list-unstyled list-inline footer-nav social-list-footer social-list-color footer-nav-inline">
                                        <?php
                                        $contact = \common\models\Contact::getSocial();
                                        ?>

                                    </ul>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div style="clear: both"></div>

                    <div class="col-xl-12">
                        <div class=" text-center paymanet-method-logo">

                            <img src="<?= Yii::getAlias('@web') . '/themes/' ?>images/site/payment/master_card.png"
                                 alt="img">
                            <img alt="img"
                                 src="<?= Yii::getAlias('@web') . '/themes/' ?>images/site/payment/visa_card.png">
                            <img alt="img"
                                 src="<?= Yii::getAlias('@web') . '/themes/' ?>images/site/payment/paypal.png">
                            <img alt="img"
                                 src="<?= Yii::getAlias('@web') . '/themes/' ?>images/site/payment/american_express_card.png">
                            <img alt="img"
                                 src="<?= Yii::getAlias('@web') . '/themes/' ?>images/site/payment/discover_network_card.png">
                            <img alt="img"
                                 src="<?= Yii::getAlias('@web') . '/themes/' ?>images/site/payment/google_wallet.png">
                        </div>

                        <div class="copy-info text-center">
                            <p class="pull-left">
                                &copy; <?= Html::encode($siteSettings['site_name']); ?> <?= date('Y') ?></p>

                        </div>

                    </div>

                </div>
            </div>
        </div>
    </footer>
    <!-- /.footer -->
</div>
<!-- Modal Change City -->


<!-- Modal Change City -->
<div class="modal fade modalHasList" id="selectRegion" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg modal-sm">
        <div class="modelBox modal-content" align="center">

            <h1>
              <span style="font-size: 70px;">
                   <i class="pe-7s-map-marker"></i>
              </span>
                <br><br>
                <span id="c-11" class="show col-lg-12"><?= Yii::t('home', 'Choose Your Location') ?> </span>
                <span id="c-12" class="hidden col-lg-12">
                <div class="coast">
                    <div class="wave-rel-wrap">
                        <div class="wave"></div>
                    </div>
                </div>
                <div class="coast delay">
                    <div class="wave-rel-wrap">
                        <div class="wave delay"></div>
                    </div>
                </div>
                <div class="text text-s">S</div>
                <div class="text text-e">e</div>
                <div class="text text-a">a</div>
                <div class="text text-r">r</div>
                <div class="text text-c">c</div>
                <div class="text text-h">h</div>
                <div class="text text-i">i</div>
                <div class="text text-n">n</div>
                <div class="text text-g">g</div>

            </span>
            </h1>


            <div class="row search-row animated fadeInUp">
                <div class="col-xl-9 col-sm-9 search-col relative locationicon" align="left">
                    <i class="icon-location-2 icon-append"></i>
                    <input type="text" pattern=".{3,}" data-ref-set="<?= \yii\helpers\Url::toRoute('search/set') ?>"
                           data-ref="<?= \yii\helpers\Url::toRoute('search/city') ?>" id="citySearchInput"
                           class="form-control locinput input-rel searchtag-input has-icon"
                           placeholder="<?= Yii::t('home', 'Enter City, State, Zip') ?>" required>
                    <ul id="cityWaiting" class="list-group hidden"></ul>
                </div>


                <div class="col-xl-3 col-sm-3 search-col">
                    <button class="btn btn-warning btn-search btn-block" onclick="citySearch()" id="citySearch">
                        <i class="icon-search"></i><strong> <?= Yii::t('home', 'FIND') ?> </strong>
                    </button>
                </div>
                <div class="col-xl-12 col-sm-12 ">
                    <br><br><br>
                    <h3>
                        <?= Yii::t('home', 'CURRENT LOCATION') ?> : <i class="pe-7s-map-marker"></i> <span id="dCity">Hà Nội</span>
                    </h3>
                    <br>
                </div>
            </div>
            <br>

        </div>
    </div>
</div>


<!-- /.modal -->


<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
