<?php
/**
 * Created by PhpStorm.
 * User: Mayank Singh
 * Date: 3/3/2020
 * Time: 12:30 PM
 */
?>
<div class="header ">
    <nav class="navbar navbar-site navbar-light bg-header navbar-expand-md" role="navigation">

        <div class="container-fluid">
            <div class="navbar-identity">
                <a href="<?= Yii::$app->homeUrl ?>" class="navbar-brand logo logo-title">
        			    <span class="logo-img">
<!--                    <i class="icon icon-search-1 ln-shadow-logo "></i>-->
                        <img style="max-width: <?= $siteSettings['logo_width'] ?>;max-height:<?= $siteSettings['logo_height'] ?>"
                             src="<?= Yii::getAlias('@web') ?>/images/site/logo/<?= $siteSettings['logo']; ?>">
                        </span>
                </a>
                <button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggler pull-right"
                        type="button">
                    <svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 30 30" width="30" height="30"
                         focusable="false"><title>Menu</title>
                        <path stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-miterlimit="10"
                              d="M4 7h22M4 15h22M4 23h22"/>
                    </svg>
                </button>
                <button class="flag-menu country-flag d-block d-md-none btn btn-secondary hidden pull-right"
                        href="#selectRegion" data-toggle="modal">
                    <span id="dflag1" class="dflag flag-icon flag-icon-us"></span>
                    <span class="caret"></span>
                </button>
            </div>
            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-left">
                    <li class="flag-menu country-flag tooltipHere hidden-xs nav-item" data-toggle="tooltip"
                        data-placement="bottom" title="Select Country"><a href="#selectRegion" data-toggle="modal"
                                                                          class="nav-link">
                            <span id="dflag2" class="flag-icon flag-icon-us"></span>
                            <span class="caret"></span>
                        </a>
                    </li>
                </ul>
                <ul>
                    <li>

                    </li>
                </ul>
                <ul class="nav navbar-nav ml-auto navbar-right">

                    <li class="nav-item loginFalse">
                        <a href="<?= \yii\helpers\Url::toRoute('site/login') ?>" class="nav-link">
                            <i class="icon-login"></i>
                            <?= Yii::t('site_header', 'Login') ?>
                        </a>
                    </li>
                    <li class="nav-item loginFalse">
                        <a href="<?= \yii\helpers\Url::toRoute('site/signup') ?>" class="nav-link">
                            <i class="icon-user"></i>
                            <?= Yii::t('site_header', 'Register') ?>
                        </a>
                    </li>
                    <li class="dropdown no-arrow nav-item loginTrue">
                        <a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown">
                                <span>
                                    <?php
                                    if (!Yii::$app->user->isGuest) {
                                        $name = Yii::$app->user->identity->first_name . " " . Yii::$app->user->identity->last_name;
                                        echo Yii::t('site_header', $name);
                                    };
                                    ?>
                                </span>
                            <i class="icon-user fa"></i>
                            <i class=" icon-down-open-big fa"></i>
                        </a>
                        <ul class="dropdown-menu user-menu dropdown-menu-right">
                            <li class="active dropdown-item">
                                <a href="<?= \yii\helpers\Url::toRoute('user/index') ?>">
                                    <i class="icon-home"></i>
                                    <?= Yii::t('site_header', 'Personal Home') ?>
                                </a>
                            </li>
                            <li class="dropdown-item">
                                <a href="<?= \yii\helpers\Url::toRoute('user/my-ads') ?>"><i
                                            class="icon-th-thumb"></i><?= Yii::t('site_header', 'My ads') ?>  </a>
                            </li>
                            <li class="dropdown-item">
                                <a href="<?= \yii\helpers\Url::toRoute('user/favourite-ads') ?>">
                                    <i class="icon-heart"></i> <?= Yii::t('site_header', 'Favourite ads') ?>
                                </a>
                            </li>
                            <li class="dropdown-item hidden">
                                <a href="account-saved-search.html">
                                    <i class="icon-star-circled"></i> <?= Yii::t('site_header', 'Saved search') ?>

                                </a>
                            </li>
                            <li class="dropdown-item hidden">
                                <a href="account-archived-ads.html">
                                    <i class="icon-folder-close"></i>
                                    Archived ads

                                </a>
                            </li>
                            <li class="dropdown-item">
                                <a href="<?= \yii\helpers\Url::toRoute('user/pending-ads') ?>">
                                    <i class="icon-hourglass"></i>
                                    <?= Yii::t('site_header', 'Pending approval') ?>
                                </a>
                            </li>

                            <li class="dropdown-item">
                                <a data-method="get" href="<?= \yii\helpers\Url::toRoute('site/logout') ?>"><i
                                            class=" icon-logout "></i> <?= Yii::t('site_header', 'Log out') ?> </a>
                            </li>
                        </ul>
                    </li>
                    <li class="postadd nav-item">
                        <a class="btn btn-block btn-primary post-btn-color nav-link"
                           href="<?= \yii\helpers\Url::toRoute('ads/post') ?>">
                            <?= Yii::t('site_header', 'Post Free Ad') ?>
                        </a>
                    </li>
                    <?php
                    $widgets->for = "0248";
                    $widgets->data = $defaultSettings['language'];
                    $widgets->get();
                    ?>

                </ul>
            </div>
            <!--/.nav-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>
</div>