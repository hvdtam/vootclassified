<?php

/* @var $this \yii\web\View */

/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\VootAsset;
use common\widgets\Alert;

use kartik\depdrop;
use kartik\select2;

//use kartik\ipinfo\IpInfo;
//use kartik\popover\PopoverX;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use common\models\Cities;

$defaultSettings = \common\models\DefaultSetting::getDefaultSetting();

if ($defaultSettings['direction'] == 'rtl') {
    \frontend\assets\RtlAsset::register($this);

} else {
    \frontend\assets\VootAsset::register($this);

}
\frontend\assets\TopAsset::register($this);
\frontend\assets\CloudAsset::register($this);
$siteSettings = \common\models\SiteSettings::find()->one();
$select_location_form = new \frontend\models\SelectCityForm();
$defaultSettings = \common\models\DefaultSetting::getDefaultSetting();
$session = Yii::$app->session;
$session->set('defaultSettings', $defaultSettings);
$theme = $defaultSettings['themes'] . '.css';
$this->registerCssFile("@web/themes/assets/css/" . $theme, ['id' => 'themeCss', 'depends' => [\yii\bootstrap\BootstrapAsset::className()]]);
$this->registerCssFile("@web/css/demo/themecontrol.css", ['depends' => [\yii\bootstrap\BootstrapAsset::className()]]);
$this->registerJsFile("@web/js/themecontrol.js", ['depends' => [\yii\bootstrap\BootstrapAsset::className()]]);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" dir="<?= $defaultSettings['direction'] ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php // Html::csrfMetaTags() ?>
    <?= Html::csrfMetaTags() ?>
    <!-- Fav and touch icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144"
          href="<?= Yii::getAlias('@web') ?>/themes/assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114"
          href="<?= Yii::getAlias('@web') ?>/themes/assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72"
          href="<?= Yii::getAlias('@web') ?>/themes/assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed"
          href="<?= Yii::getAlias('@web') ?>/themes/assets/ico/apple-touch-icon-57-precomposed.png">
    <link rel="shortcut icon" href="<?= Yii::getAlias('@web/images/site/fav/' . $siteSettings['fav_icon']) ?>">
    <title><?= Html::encode($this->title) ?></title>
    <!-- Just for debugging purposes. -->
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <!--    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>-->

    <!-- include pace script for automatic web page progress bar  -->
    <script>
        paceOptions = {
            elements: true
        };
    </script>

    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div id="wrapper">
    <div class="hidden">
        <input type="hidden" name="default_flag" value="<?= $defaultSettings['flag'] ?>" id="dFlagBtn">
        <input type="hidden" name="default_city" value="<?= $defaultSettings['CurrCity'] ?>" id="dCityBtn">
        <input type="hidden" name="default_currency" value="<?= $defaultSettings['currency'] ?>" id="dCityBtn">
        <input type="hidden" name="default_theme"
               value="<?= Yii::getAlias('@web') . '/themes/assets/css/' . $defaultSettings['themes'] ?>.css"
               id="dthemeBtn">
        <input type="hidden" name="default_theme_url" value="<?= Yii::getAlias('@web') . '/themes/assets/css/' ?>"
               id="dthemeUrlDemo">
        <input type="hidden" name="searchUrl" class="hidden"
               value="<?php echo \yii\helpers\Url::toRoute('search/index'); ?>" id="searchUrl">

        <input type="hidden" name="default_bg" data-url="<?= Yii::getAlias('@web') ?>/themes/assets/img/"
               value="<?= $defaultSettings['background'] ?>" id="dBgBtn">

        <input type="hidden" name="webUrl" class="hidden" value="<?= Yii::getAlias('@web') ?>" id="webUrl">
        <input type="hidden" name="HomeUrl" class="hidden" value="<?= \yii\helpers\Url::home(); ?>" id="HomeUrl">
        <input type="hidden" name="IsLogin" class="hidden"
               value="<?= (!Yii::$app->user->isGuest) ? 'login' : 'guest'; ?>" id="IsLogin">
        <input type="hidden" name="DLat" class="hidden" value="26.9124" id="DLat">
        <input type="hidden" name="DLong" class="hidden" value="75.7873" id="DLong">
    </div>


    <!-- /.header -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <?= Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ]) ?>
            </div>
        </div>
    </div>
    <?php
    $alert = Alert::widget();
    if ($alert) {
        echo "<div style='margin-top: 0px;margin-bottom: -16px;'><div class='row'><div class='col-lg-12'>" . $alert . "</div></div></div>";
    }
    ?>
    <?= $content ?>
</div>


<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
