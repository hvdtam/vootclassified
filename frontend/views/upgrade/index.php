<?php
/**
 * Created by PhpStorm.
 * User: Mayank Singh
 * Date: 3/5/2020
 * Time: 11:33 AM
 */

use yii\helpers\Url;

?>
<div class="container">
    <div class="row d-flex justify-content-center">
        <div class="col-6 mt-5">

            <div class="card mt-3">
                <div class="card-header">
                    <h2 class="card-title">
                        Voot Classified CMS Version 2.5
                    </h2>
                    <p class="card-subtitle text-secondary">
                        upgrade v2.4 to v2.5
                    </p>
                </div>
                <div class="card-body">
                    <h3 class="text-secondary mb-1">
                        This Module Only Work for Voot 2.4, if you using older version Please moves file manually
                    </h3>
                    <h5 class="text-secondary mb-3">
                        This Modules moves file automatically, but create backup of your files. in case any data loss
                    </h5>

                    <p>
                        <strong>
                            Instruction :
                        </strong>
                        Please Backup all your images files, Database, API keys
                    </p>

                    <p class="card-text">
                        1. Backup All images from <code>roo-folder/frontend/web/images/items</code>
                    </p>
                    <p class="card-text">
                        2. Backup All images from <code>roo-folder/frontend/web/images/site</code>
                    </p>
                    <p class="card-text">
                        3. Backup All images from <code>roo-folder/frontend/web/images/users</code>
                    </p>

                    <p class="card-text">
                        4. Create a backup files of current database, in case any data loss you can retrieve your data
                        back.
                    </p>
                </div>
                <div class="card-footer" align="center">
                    <a class="btn btn-success btn-lg border-none" href="<?= Url::toRoute(['upgrade/move-files']); ?>">
                        Yes i saved all my data
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>