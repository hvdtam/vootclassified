<?php
/**
 * Created by PhpStorm.
 * User: Mayank Singh
 * Date: 3/5/2020
 * Time: 11:33 AM
 */

use yii\helpers\Url;

?>
<div class="container">
    <div class="row d-flex justify-content-center">
        <div class="col-6 mt-5">

            <div class="card mt-3">
                <div class="card-header">
                    <h2>
                        Voot Classified CMS Version 2.5
                    </h2>
                </div>
                <div class="card-body" align="center">
                    <h1>
                        <?= $step;; ?>
                    </h1>
                    <h2 class="card-title">
                        <?= $title; ?>
                    </h2>
                    <h5 class="card-subtitle">
                        <?= $SubTitle; ?>
                    </h5>
                    <?php
                    if ($error) {
                        echo "<div class='alert alert-danger'>" . $error . "</div>";
                    }
                    ?>
                </div>
                <div class="card-footer" align="center">
                    <p>
                        <?= ($Skip) ? $Skip : ''; ?>
                    </p>
                    <a class="btn btn-success btn-lg border-none" href="<?= $url; ?>">
                        <?= $btn; ?>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>