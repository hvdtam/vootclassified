<?php
/**
 * Created by PhpStorm.
 * User: Asus
 * Date: 17-03-2016
 * Time: 14:58
 */

use common\models\Payment;

$this->title = "Paystack Payment";
$configJson = $payment['config'];
$config = json_decode($configJson, true);

$notifyUrl = $return;

use yii\di\ServiceLocator;

$services = new ServiceLocator();
$services->setComponents([
    'Paystack' => [
        'class' => 'smladeoye\paystack\Paystack',
        'environment' => $config['environment'],
        'testPublicKey' => $config['testPublicKey'],
        'testSecretKey' => $config['testSecretKey'],
        'livePublicKey' => $config['livePublicKey'],
        'liveSecretKey' => $config['liveSecretKey'],
    ],
    // ...
]);
// Retrieving the defined components:
$paystack = $services->get('Paystack');
$paystack = $services->Paystack;
$transaction = $paystack->transaction();
$transaction->initialize(['email' => $config['email'], 'amount' => $amount, 'currency' => $config['currency'], 'callback_url' => $return]);

// check if an error occured during the operation
if (!$transaction->hasError) {
    //response property for response gotten for any operation
    $response = $transaction->getResponse();
    // redirect the user to the payment page gotten from the initialization
    $transaction->redirect();
} else {
    // display message
    echo $transaction->message;
    // get all the errors information regarding the operation from paystack
    $error = $transaction->getError();
}
?>
<style type="text/css">
    <!--
    .style1 {
        font-size: 14px;
        font-family: Verdana, Arial, Helvetica, sans-serif;
    }

    -->
</style>


<div class="main-container">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="d-flex align-items-center">
                            <h4 class="card-title">
                                <?= $this->title; ?>
                            </h4>

                        </div>
                    </div>
                    <div class="card-body" align="center">
                        <div class="row">
                            <div class="col-md-6" align="center">
                                <img src="<?= Yii::getAlias('@web') ?>/images/site/payment/paystck.png">
                                <img style="width: 60%"
                                     src="<?= Yii::getAlias('@web') ?>/images/site/payment/cards.png">

                            </div>
                            <div class="col-md-6">
                                <h4 style="color:#999">
                                    Paystack Payment
                                </h4>
                                <div style="color:#555;display: inline">
                                    <?= $config['currency_code']; ?>/-
                                </div>
                                <div style="color:#777;font-size: 80px;display: inline">
                                    <?= $amount; ?>/-
                                </div>

                                <br>
                                <div class="form-control col-8">
                                    <?= 'Ads Type <b>' . $type . '</b>' ?>
                                </div>
                                <br>
                                <a href="javascript:document.paypal.submit();" class="btn btn-md btn-info">Pay With
                                    Paypal</a>
                            </div>
                        </div>


                    </div>
                    <div class="card-footer" align="center">
                        <strong>
                            Transfering you to the Paypal.com Secure payment system, if you are not forwarded
                            <a href="javascript:document.paypal.submit();">click here</a>
                        </strong>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

