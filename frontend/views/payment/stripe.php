<?php
$this->title = "Stripe Payment - " . Yii::$app->name;

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use ruskid\stripe\StripeCheckout;
use ruskid\stripe\StripeCheckoutCustom;
use \yii\web\JsExpression;
use ruskid\stripe\StripeForm;

$siteSettings = \common\models\SiteSettings::find()->one();

$logoUrl = Yii::getAlias('@web') . "/images/site/logo/" . $siteSettings['logo'];

$configJson = $payment['config'];
$config = json_decode($configJson, true);


$key = isset($config['data-key']) ? $config['data-key'] : false;
if (!$key) {
    Yii::$app->session->setFlash('danger', 'Stripe key is invalid or empty, please check your Stripe API key');

}
?>
<div class="main-container">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="d-flex align-items-center">
                            <h4 class="card-title">
                                <?= $this->title; ?>
                            </h4>

                        </div>
                    </div>
                    <div class="card-body" align="center">
                        <div class="row">
                            <div class="col-md-6" align="center">
                                <img src="<?= Yii::getAlias('@web') ?>/images/site/payment/stripe.png">
                                <img style="width: 60%"
                                     src="<?= Yii::getAlias('@web') ?>/images/site/payment/cards.png">

                            </div>
                            <div class="col-md-6">
                                <h4 style="color:#999">
                                    Stripe Payment
                                </h4>
                                <div style="color:#555;display: inline">
                                    <?= $config['currency_code']; ?>/-
                                </div>
                                <div style="color:#777;font-size: 80px;display: inline">
                                    <?= $amount; ?>/-
                                </div>

                                <br>
                                <div class="form-control col-8">
                                    <?= 'Ads Type <b>' . $type . '</b>' ?>
                                </div>
                                <br>
                                <form action="<?= $return; ?>" method="POST">
                                    <script
                                            src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                                            data-key="<?= $key; ?>"
                                            data-amount="<?= $amount; ?>00"
                                            data-name="<?= $siteSettings['site_name'] ?>"
                                            data-description="<?= $siteSettings['site_title'] ?>"
                                            data-image="<?= $logoUrl; ?>"
                                            data-locale="auto">
                                    </script>
                                </form>
                            </div>
                        </div>


                    </div>
                    <div class="card-footer ">

                    </div>
                </div>
            </div>

            <div class="col-md-12">
                <?php
                //                    StripeCheckout::widget([
                //                        'action' => '/',
                //                        'name' => 'Demo test',
                //                        'description' => '2 widgets ($20.00)',
                //                        'amount' => 2000,
                //                        'image' => 'https://stripe.com/img/documentation/checkout/marketplace.png',
                //                    ]);


                ?>
                <?php
                //                    StripeCheckoutCustom::widget([
                //                        'action' => '/',
                //                        'name' => 'Demo test',
                //                        'description' => '2 widgets ($20.00)',
                //                        'amount' => 2000,
                //                        'image' => '/128x128.png',
                //                        'buttonOptions' => [
                //                            'class' => 'btn btn-lg btn-success',
                //                        ],
                //                        'tokenFunction' => new JsExpression('function(token) {
                //                alert("Here you should control your token.");
                //    }'),
                //                        'openedFunction' => new JsExpression('function() {
                //                alert("Model opened");
                //    }'),
                //                        'closedFunction' => new JsExpression('function() {
                //                alert("Model closed");
                //    }'),
                //                    ]);
                ?>


            </div>
        </div>
    </div>
</div>
