<?php
// for more info and support
/* visit paytm website @url : https://developer.paytm.com/ */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
//use app\helpers\AppHelper;
use common\models\Crypto;
use yii\helpers\Url;

$this->title = 'Merchant Check Out Page';

$configJson = $payment['config'];
$config = json_decode($configJson);
foreach ($config as $key => $value) {
    define($key, $value);
}


define("txnAmount", $price);// @txnAmount : payable amount, you can use 100.12 for testing
define("orderId", "OR" . time());// @orderId : use unique Id for paytm Order
define("callbackUrl", $return, true);// @callbackUrl : return URl , paytm redirect after transaction
$paytmParams = array();
$paytmParams["MID"] = merchantMid;// @merchantMid : Key in your staging and production MID available in your dashboard
$paytmParams["ORDER_ID"] = orderId;
$paytmParams["CUST_ID"] = custId; // @custId : is provide by paytm
$paytmParams["MOBILE_NO"] = mobileNo; // @mobileNo : Contact paytm for more information
$paytmParams["EMAIL"] = email; // @email : Contact paytm for more information
$paytmParams["CHANNEL_ID"] = channelId; // @channelId : provided by paytm
$paytmParams["TXN_AMOUNT"] = txnAmount;
$paytmParams["WEBSITE"] = website; // @website : merchant website url, This is the staging value. Production value is available in your dashboard;
$paytmParams["INDUSTRY_TYPE_ID"] = industryTypeId; // @industryTypeId : This is the staging value. Production value is available in your dashboard
$paytmParams["CALLBACK_URL"] = callbackUrl;
$data = new Crypto();
$paytmChecksum = $data->getChecksumFromArray($paytmParams, merchantKey); // @merchantKey : Key in your staging and production merchant key available in your dashboard
$transactionURL = "https://securegw-stage.paytm.in/theia/processTransaction";
// $transactionURL = "https://securegw.paytm.in/theia/processTransaction"; // for production
$transactionURL = "https://securegw-stage.paytm.in/theia/processTransaction";

?>

<div class="container">
    <div class="row">

        <?= Yii::$app->session->getFlash('success') ?>
        <?= Yii::$app->session->getFlash('error') ?>

        <div class="col-sm-12">

            <div class="panel panel-default">
                <div class="panel-body">
                    <center><h1>Please do not refresh this page...</h1></center>
                    <form method='post' action='<?php echo $transactionURL; ?>' name='f1'>
                        <?php
                        foreach ($paytmParams as $name => $value) {
                            echo '<input type="hidden" name="' . $name . '" value="' . $value . '">';
                        }
                        ?>
                        <input type="hidden" name="CHECKSUMHASH" value="<?php echo $paytmChecksum ?>">
                    </form>
                    <script type="text/javascript">
                        document.f1.submit();
                    </script>
                </div>
            </div>


        </div>


    </div>
</div>



