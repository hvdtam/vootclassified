<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */

/* @var $exception Exception */

use yii\helpers\Html;

$this->title = "Payment Success";
?>
<style>
    .error-page {
        background-color: #fff;
        padding: 70px;
        margin-top: 30px;
        text-align: center;
        border: 1px solid #ddd;
        -webkit-box-shadow: 0 3px 12px rgba(0, 0, 0, .175);
        box-shadow: 0 3px 12px rgba(0, 0, 0, .175);
    }

    .error-page-content {
        max-width: 675px;
        display: inline-block;
    }

    .error-page h2 {
        font-size: 55px;
        text-transform: capitalize;
        margin-bottom: 25px;
        margin-top 25px;
        color: #555;
        line-height: 1;
    }

    .error-page-content .btn-primary {
        margin: 30px 0;
    }
</style>
<div class="main-container inner-page">
    <div class="container">
        <div class="error-page">
            <div class="error-page-content">
                <br>
                <h2>Payment Transaction Success</h2>
                <p style="padding: 25px;color: #f6333b;font-size: 20px;">
                    <?= Html::encode($this->title) ?>
                </p>
                <p>
                    <?= Yii::t('app', 'Your Ads is posted Successfully') ?>
                </p>

                <?php
                $adsSetting = \common\models\AdsSettings::find()->one();
                if ($adsSetting['login_required'] == 0 and Yii::$app->user->isGuest) {
                    ?>
                    <a href="<?= \yii\helpers\Url::toRoute('site/index') ?>"
                       class="btn btn-success"> <?= Yii::t('app', 'Back To Home'); ?>   </a>
                    <?php
                } else {
                    ?>
                    <a href="<?= \yii\helpers\Url::toRoute('user/my-ads') ?>"
                       class="btn btn-success"> <?= Yii::t('app', 'My Ads'); ?>   </a>
                    <?php
                };
                ?>
                <span style="padding: 0px 20px;color: #777;display: inline-block;"> -- OR -- </span>
                <a href="<?= \yii\helpers\Url::toRoute('ads/post') ?>"
                   class="btn btn-warning"> <?= Yii::t('app', 'Post Free Ad'); ?>   </a>
            </div>
        </div>
        <center>
            <div class="col-md-12 col-md-offset-2">
                <br><br><br><br>
                <?= \common\models\Adsense::show('bottom') ?>

            </div>
        </center>
    </div>

</div>

