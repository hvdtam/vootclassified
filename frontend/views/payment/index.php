<?php
/**
 * Created by PhpStorm.
 * User: Asus
 * Date: 17-03-2016
 * Time: 14:58
 */

use common\models\Payment;

$this->title = "Paypal Payment - " . Yii::$app->name;
$payment = Payment::find()->where(['name' => 'Paypal'])->one();
$configJson = $payment['config'];
$config = json_decode($configJson, true);

$notifyUrl = $rerunUrl;
?>
<style type="text/css">
    <!--
    .style1 {
        font-size: 14px;
        font-family: Verdana, Arial, Helvetica, sans-serif;
    }

    -->
</style>
<form name="paypal" action="https://www.paypal.com/cgi-bin/webscr" method="post">
    <input type="hidden" name="cmd" value="_xclick">
    <input type="hidden" name="business" value="<?= $config['business']; ?>">
    <input type="hidden" name="item_name" value="Deposit into <?= $item_name; ?>">
    <input type="hidden" name="amount" value="<?= $amount; ?>">
    <input type="hidden" name="custom" value="<?= $type; ?>">
    <input type="hidden" name="return" value="<?= $rerunUrl ?>">
    <input type="hidden" name="notify_url" value="<?= $notifyUrl; ?>">
    <input type="hidden" name="currency_code" value="<?= $config['currency_code']; ?>">
</form>

<div class="main-container">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="d-flex align-items-center">
                            <h4 class="card-title">
                                <?= $this->title; ?>
                            </h4>

                        </div>
                    </div>
                    <div class="card-body" align="center">
                        <div class="row">
                            <div class="col-md-6" align="center">
                                <img src="<?= Yii::getAlias('@web') ?>/images/site/payment/paypal.png">
                                <img style="width: 60%"
                                     src="<?= Yii::getAlias('@web') ?>/images/site/payment/cards.png">

                            </div>
                            <div class="col-md-6">
                                <h4 style="color:#999">
                                    Paypal Payment
                                </h4>
                                <div style="color:#555;display: inline">
                                    <?= $config['currency_code']; ?>/-
                                </div>
                                <div style="color:#777;font-size: 80px;display: inline">
                                    <?= $amount; ?>/-
                                </div>

                                <br>
                                <div class="form-control col-8">
                                    <?= 'Ads Type <b>' . $type . '</b>' ?>
                                </div>
                                <br>
                                <a href="javascript:document.paypal.submit();" class="btn btn-md btn-info">Pay With
                                    Paypal</a>
                            </div>
                        </div>


                    </div>
                    <div class="card-footer" align="center">
                        <strong>
                            Transfering you to the Paypal.com Secure payment system, if you are not forwarded
                            <a href="javascript:document.paypal.submit();">click here</a>
                        </strong>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

