<?php
/**
 * Created by PhpStorm.
 * User: Mayank Singh
 * Date: 3/4/2019
 * Time: 6:38 PM
 */
?>
<div class="container">
    <div class="row  mt-5">


        <div class="col-lg-6 col-sm-12 m-auto">

            <div class="card">
                <div class="card-header" align="center">
                    Your ads is Posted
                </div>
                <div class="card-body" align="center">
                    <h2>Bank Detail</h2>
                    <i class="fa fa-bank fa-5x"></i>
                    <br>
                    <hr>
                    <?php
                    foreach ($config as $key => $list) {
                        echo "<h5>" . $key . " : " . $list . "</h5>";
                    }
                    ?>
                    <a class="btn btn-success mt-5" href="<?= \yii\helpers\Url::toRoute('site/index') ?>">
                        BACK TO HOME
                    </a>
                </div>
            </div>


        </div>


    </div>
</div>
