<?php
/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;

$this->title = $model['title'];
?>
<div class="main-container">
    <div class="container">
        <div class="row">

            <div class="col-md-9 page-content col-thin-right">
                <div class="card">
                    <div class="inner card-body ads-details-wrapper">
                        <h2> <?= Yii::t('app', $model['title']) ?>
                        </h2>
                    </div>
                    <div class="card-body text-left">
                        <?= Yii::t('app', $model['content']) ?>
                    </div>

                </div>
            </div>
            <div class="col-md-3 page-content col-thin-left">
                <?= \common\models\Adsense::show('left') ?>

            </div>
        </div>
    </div>
</div>
