<?php
$this->title = "Change Ads Location- " . Yii::$app->name;

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\file\FileInput;

$defaultSettings = \common\models\DefaultSetting::getDefaultSetting();
$api = \common\models\ApiKeys::find()->where(['type' => 'maps'])->andWhere(['status' => 'enable'])->one();

$session = Yii::$app->session;

$ses_country = $defaultSettings['country'];
$ses_state = $defaultSettings['state'];
$ses_city = $defaultSettings['city'];
$ses_lat = $defaultSettings['lat'];
$ses_lng = $defaultSettings['lng'];
$ses_currency = $defaultSettings['currency'];
$ses_lng = $defaultSettings['lng'];


$countryIn = ($ses_country) ? $ses_country : 'Choose Country';
$stateIn = ($ses_state) ? $ses_state : 'Choose State';
$cityIn = ($ses_city) ? $ses_city : 'Choose City';
?>
<style>
    p {
        padding: 0 !important;
        margin-bottom: 0;
    }

    label {
        font-weight: 600;
    }
</style>
<div class="main-container">

    <div class="container">
        <div class="row">
            <?= $this->render('_side_bar', ['adsId' => $adsId]) ?>
            <!--/.page-sidebar-->


            <div class="col-md-6">


                <div class="inner-box">
                    <h2 class="text-dark text-capitalize font-weight-bold">
                        Change Location : <?= substr($model->ad_title, 0, 35) ?>...
                    </h2>
                    <div class="alert alert-success rounded">
                        <small class="font-weight-bold">
                            You last Changes in at: <?= date("d/m/Y", $model->updated_at) ?>
                        </small>
                    </div>
                    <div class="inner-box-content mt-5">

                        <?php $form = ActiveForm::begin([
                            'layout' => 'horizontal',
                            'fieldConfig' =>
                                [
                                    'horizontalCssClasses' =>
                                        [
                                            'label' => 'col-sm-3 col-form-label',
                                            'offset' => '',
                                            'wrapper' => 'row',
                                            'error' => 'col-sm-8  col-sm-push-3 imgHintS',
                                            'hint' => 'col-sm-8  col-sm-push-3 imgHintS',
                                        ],
                                ],
                            'options' => ['enctype' => 'multipart/form-data', 'id' => 'editpost1489'],
                            // 'enableAjaxValidation' => false,
                        ]) ?>

                        <div class="form-group row">

                            <div class="col-12 " align="center">


                            </div>
                            <label class="col-sm-3 col-form-label" for="seller-Location">
                                <?= Yii::t('app', 'Address') ?>
                            </label>
                            <div class="col-sm-8" style="padding-right: 28px;padding-left: 33px;">
                                <?= $form->field($model, 'address')->textInput(['onFocus' => 'geolocate()', 'id' => 'autocomplete'])->label(false); ?>
                                <blockquote class="blocInput">
                                    <?= Yii::t('app', 'It`s seems like you don`t choose location property. Please Fill correct Address') ?>

                                </blockquote>
                            </div>

                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label" for="seller-Location">
                                <?= Yii::t('app', 'City') ?> & <?= Yii::t('app', 'States') ?>
                            </label>
                            <div class="col-sm-8 " align="left">
                                <div class="chip">
                                    <i class="fa fa-map-marker"></i>
                                    <span>
                                                <?php
                                                echo "<span id='CountryCip745'> " . $model->country . " </span> >";
                                                echo " <span id='StateCip754'> " . $model->states . "</span> >";
                                                echo " <span id='CityCip754'> " . $model->city . "</span>";
                                                ?>
                                            </span>
                                    <span class="closebtn"
                                          onclick="this.parentElement.style.display='none'">&times;</span>
                                </div>
                                <a href="#selectRegion" data-toggle="modal"
                                   style="font-size: 12px;color: rgb(52, 225,109);padding: 12px;display: inline-block;position: absolute;font-weight: 600;">
                                    <i class="fa fa-edit"></i><?= Yii::t('app', 'Change') ?>
                                </a>
                                <div style="display: none">
                                    <?= $form->field($model, 'lat')->hiddenInput(['value' => $model->lat])->label(false); ?>
                                    <?= $form->field($model, 'lng')->hiddenInput(['value' => $model->lng])->label(false); ?>

                                    <?= $form->field($model, 'country')->hiddenInput(['id' => 'hiddenInputCountry', 'value' => $model->country])->label(false); ?>
                                    <?= $form->field($model, 'states')->hiddenInput(['id' => 'hiddenInputState', 'value' => $model->states])->label(false); ?>
                                    <?= $form->field($model, 'city')->hiddenInput(['id' => 'hiddenInputCity', 'value' => $model->city])->label(false); ?>


                                </div>

                            </div>
                        </div>
                        <hr>

                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label"></label>

                            <div class="col-sm-8">
                                <input type="submit" class="btn btn-success btn-lg"
                                       value="<?= Yii::t('app', 'SAVE AND NEXT') ?>">

                            </div>
                        </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                    <!--/.row-box End-->

                </div>
            </div>
            <!--/.page-content-->
            <div class="col-md-3">
                <?= \common\models\Adsense::show('left') ?>

            </div>
        </div>
        <!--/.row-->
    </div>
    <!--/.container-->
</div>
<!-- /.main-container -->
<script>
    var placeSearch, autocomplete;

    var componentForm = {
        locality: 'long_name',
        administrative_area_level_1: 'long_name',
        country: 'long_name'
    };
    //    var componentForm = {
    //        hiddenInputCity: 'long_name'
    //       // hiddenInputState: 'short_name',
    //       // hiddenInputCountry: 'long_name',
    //    };
    function initAutocomplete() {
        // Create the autocomplete object, restricting the search predictions to
        // geographical location types.
        autocomplete = new google.maps.places.Autocomplete(
            document.getElementById('autocomplete'), {types: ['geocode']});

        // Avoid paying for data that you don't need by restricting the set of
        // place fields that are returned to just the address components.
        autocomplete.setFields('address_components');

        // When the user selects an address from the drop-down, populate the
        // address fields in the form.
        autocomplete.addListener('place_changed', fillInAddress);
    }

    function fillInAddress() {
        // Get the place details from the autocomplete object.
        var place = autocomplete.getPlace();
        var lat = place.geometry.location.lat(),
            lng = place.geometry.location.lng();
        $('#adsform-longitude').val(lng);
        $('#adsform-latitude').val(lat);

        for (var component in componentForm) {

            document.getElementById(component).value = '';
            document.getElementById(component).disabled = false;
        }

        // Get each component of the address from the place details,
        // and then fill-in the corresponding field on the form.
        for (var i = 0; i < place.address_components.length; i++) {

            var addressType = place.address_components[i].types[0];

            if (componentForm[addressType]) {
                var val = place.address_components[i][componentForm[addressType]];
                document.getElementById(addressType).value = val;
            }
            if (addressType == 'country') {
                var val = place.address_components[i][componentForm[addressType]];
                $('#hiddenInputCountry').val(val);
                $('#CountryCip745').text(val);
            }

            if (addressType == 'administrative_area_level_1') {
                var val = place.address_components[i][componentForm[addressType]];
                $('#hiddenInputState').val(val);
                $('#StateCip754').text(val);
            }
            if (addressType == 'locality') {
                var val = place.address_components[i][componentForm[addressType]];
                $('#hiddenInputCity').val(val);
                $('#CityCip754').text(val);
            }
        }
    }

    // Bias the autocomplete object to the user's geographical location,
    // as supplied by the browser's 'navigator.geolocation' object.
    function geolocate() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {
                var geolocation = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };
                var circle = new google.maps.Circle(
                    {center: geolocation, radius: position.coords.accuracy});
                autocomplete.setBounds(circle.getBounds());
            });
        }
    }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=<?= $api['api_key']; ?>&libraries=places&callback=initAutocomplete"
        async defer></script>