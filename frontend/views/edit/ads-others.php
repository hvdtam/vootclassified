<?php
$this->title = "Change Ads Location- " . Yii::$app->name;

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\file\FileInput;

$defaultSettings = \common\models\DefaultSetting::getDefaultSetting();
$api = \common\models\ApiKeys::find()->where(['type' => 'maps'])->andWhere(['status' => 'enable'])->one();

$session = Yii::$app->session;

$ses_country = $defaultSettings['country'];
$ses_state = $defaultSettings['state'];
$ses_city = $defaultSettings['city'];
$ses_lat = $defaultSettings['lat'];
$ses_lng = $defaultSettings['lng'];
$ses_currency = $defaultSettings['currency'];
$ses_lng = $defaultSettings['lng'];


$countryIn = ($ses_country) ? $ses_country : 'Choose Country';
$stateIn = ($ses_state) ? $ses_state : 'Choose State';
$cityIn = ($ses_city) ? $ses_city : 'Choose City';
?>
<style>
    p {
        padding: 0 !important;
        margin-bottom: 0;
    }

    label {
        font-weight: 600;
    }
</style>
<div class="main-container">

    <div class="container">
        <div class="row">
            <?= $this->render('_side_bar', ['adsId' => $adsId]) ?>
            <!--/.page-sidebar-->


            <div class="col-md-6">


                <div class="inner-box">
                    <h2 class="text-dark text-capitalize font-weight-bold">
                        Change Location : <?= substr($model->ad_title, 0, 35) ?>...
                    </h2>
                    <div class="alert alert-success rounded">
                        <small class="font-weight-bold">
                            You last Changes in at: <?= date("d/m/Y", $model->updated_at) ?>
                        </small>
                    </div>
                    <div class="inner-box-content mt-5">

                        <?php $form = ActiveForm::begin([
                            'layout' => 'horizontal',
                            'fieldConfig' =>
                                [
                                    'horizontalCssClasses' =>
                                        [
                                            'label' => 'col-sm-3 col-form-label',
                                            'offset' => '',
                                            'wrapper' => 'row',
                                            'error' => 'col-sm-8  col-sm-push-3 imgHintS',
                                            'hint' => 'col-sm-8  col-sm-push-3 imgHintS',
                                        ],
                                ],
                            'options' => ['enctype' => 'multipart/form-data', 'id' => 'editpost1489'],
                            // 'enableAjaxValidation' => false,
                        ]) ?>

                        <?php
                        $savedData = json_decode($saved['more'], true);

                        foreach ($custom as $foAds) {

                            $data = $foAds['custom_options'];
                            $options = explode(',', $data);
                            $option = array();
                            foreach ($options as $optionList) {
                                $option[$optionList] = $optionList;
                            };
                            if (isset($savedData)) {
                                $title = $foAds['custom_title'];
                                $value = implode(',', $savedData[$foAds['custom_title']]);
                            } else {
                                $title = $foAds['custom_title'];
                                $value = "";
                            };
                            //   echo $form->field($model, 'more[]')->hiddenInput(['value'=>$foAds['custom_title']])->label(false);

                            if ($foAds['custom_type'] == "textinput") {
                                echo $form->field($model, 'more[' . $foAds['custom_title'] . '][]', [
                                    'template' => '<div class="form-group row">{label}<div class="col-sm-8">{input}{error}{hint}</div></div>'])->textInput(['value' => $value])->label($foAds['custom_title'])->error(['not empty' => $title . ' cannot be empty']);

                            } elseif ($foAds['custom_type'] == "select") {
                                ?>
                                <?= $form->field($model, 'more[' . $foAds['custom_title'] . '][]', [
                                    'template' => '<div class="form-group row">{label}<div class="col-sm-8">{input}{error}{hint}</div></div>'])
                                    ->dropDownList($option)->label($foAds['custom_title']);
                                ?>

                                <?php
                            } elseif ($foAds['custom_type'] == "checkbox") {

                                echo $form->field($model, 'more[' . $foAds['custom_title'] . '][]', [
                                    'template' => '<div class="form-group row">{label}<div class="col-sm-8">{input}{error}{hint}</div></div>'])
                                    ->checkboxList($option)->label($foAds['custom_title']);
                            } elseif ($foAds['custom_type'] == "radio") {

                                echo $form->field($model, 'more[' . $foAds['custom_title'] . '][]', [
                                    'template' => '<div class="form-group row">{label}<div class="col-sm-8">{input}{error}{hint}</div></div>'])
                                    ->radioList($option)->label($foAds['custom_title']);

                            } else {
                                echo $form->field($model, 'more[' . $foAds['custom_title'] . '][]', [
                                    'template' => '<div class="form-group row">{label}<div class="col-sm-8">{input}{error}{hint}</div></div>'])->textInput(['value' => $value])->label($foAds['custom_title'])->error(['not empty' => $title . ' cannot be empty']);
                            }


                        }
                        ?>
                        <hr>

                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label"></label>

                            <div class="col-sm-8">
                                <input type="submit" class="btn btn-success btn-lg"
                                       value="<?= Yii::t('app', 'SAVE AND NEXT') ?>">

                            </div>
                        </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                    <!--/.row-box End-->

                </div>
            </div>
            <!--/.page-content-->
            <div class="col-md-3">
                <?= \common\models\Adsense::show('left') ?>

            </div>
        </div>
        <!--/.row-->
    </div>
    <!--/.container-->
</div>
<!-- /.main-container -->
<script>
    var placeSearch, autocomplete;

    var componentForm = {
        locality: 'long_name',
        administrative_area_level_1: 'long_name',
        country: 'long_name'
    };
    //    var componentForm = {
    //        hiddenInputCity: 'long_name'
    //       // hiddenInputState: 'short_name',
    //       // hiddenInputCountry: 'long_name',
    //    };
    function initAutocomplete() {
        // Create the autocomplete object, restricting the search predictions to
        // geographical location types.
        autocomplete = new google.maps.places.Autocomplete(
            document.getElementById('autocomplete'), {types: ['geocode']});

        // Avoid paying for data that you don't need by restricting the set of
        // place fields that are returned to just the address components.
        autocomplete.setFields('address_components');

        // When the user selects an address from the drop-down, populate the
        // address fields in the form.
        autocomplete.addListener('place_changed', fillInAddress);
    }

    function fillInAddress() {
        // Get the place details from the autocomplete object.
        var place = autocomplete.getPlace();
        var lat = place.geometry.location.lat(),
            lng = place.geometry.location.lng();
        $('#adsform-longitude').val(lng);
        $('#adsform-latitude').val(lat);

        for (var component in componentForm) {

            document.getElementById(component).value = '';
            document.getElementById(component).disabled = false;
        }

        // Get each component of the address from the place details,
        // and then fill-in the corresponding field on the form.
        for (var i = 0; i < place.address_components.length; i++) {

            var addressType = place.address_components[i].types[0];

            if (componentForm[addressType]) {
                var val = place.address_components[i][componentForm[addressType]];
                document.getElementById(addressType).value = val;
            }
            if (addressType == 'country') {
                var val = place.address_components[i][componentForm[addressType]];
                $('#hiddenInputCountry').val(val);
                $('#CountryCip745').text(val);
            }

            if (addressType == 'administrative_area_level_1') {
                var val = place.address_components[i][componentForm[addressType]];
                $('#hiddenInputState').val(val);
                $('#StateCip754').text(val);
            }
            if (addressType == 'locality') {
                var val = place.address_components[i][componentForm[addressType]];
                $('#hiddenInputCity').val(val);
                $('#CityCip754').text(val);
            }
        }
    }

    // Bias the autocomplete object to the user's geographical location,
    // as supplied by the browser's 'navigator.geolocation' object.
    function geolocate() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {
                var geolocation = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };
                var circle = new google.maps.Circle(
                    {center: geolocation, radius: position.coords.accuracy});
                autocomplete.setBounds(circle.getBounds());
            });
        }
    }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=<?= $api['api_key']; ?>&libraries=places&callback=initAutocomplete"
        async defer></script>