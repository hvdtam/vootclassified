<?php
$this->title = "Change Ads Category- " . Yii::$app->name;

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\file\FileInput;

$defaultSettings = \common\models\DefaultSetting::getDefaultSetting();
$api = \common\models\ApiKeys::find()->where(['type' => 'maps'])->andWhere(['status' => 'enable'])->one();

?>
<style>
    p {
        padding: 0 !important;
        margin-bottom: 0;
    }

    label {
        font-weight: 600;
    }
</style>
<input type="hidden" id="subcatUrl" value="<?= \yii\helpers\Url::toRoute('search/sub-category') ?>">

<input type="hidden" id="PostType7842" value="<?= \yii\helpers\Url::toRoute('search/ptype') ?>" name="HidType">
<div class="main-container">

    <div class="container">
        <div class="row">
            <?= $this->render('_side_bar', ['adsId' => $adsId]) ?>
            <!--/.page-sidebar-->


            <div class="col-md-6">


                <div class="inner-box">
                    <h2 class="text-dark text-capitalize font-weight-bold">
                        Change Category : <?= substr($model->ad_title, 0, 35) ?>...
                    </h2>
                    <div class="alert alert-success rounded">
                        <small class="font-weight-bold">
                            You last Changes in at: <?= date("d/m/Y", $model->updated_at) ?>
                        </small>
                    </div>
                    <div class="inner-box-content mt-5">
                        <div class="row">
                            <div class="col-md-12" style="background-color: #fff;padding: 25px 10px;line-height: 66px;"
                                 align="center">
                                <h3 style="letter-spacing: 4px;text-transform: uppercase;color: #999;">Previous
                                    Information</h3>
                                <div class="chip CmaainI">
                                    <i class="<?= \common\models\Category::IconByName($model['category']) ?>"></i>
                                    <span class="catChips">Category > <?= $model['category'] ?></span>
                                    <span class="closebtn"
                                          onclick="this.parentElement.style.display='none'">&times;</span>
                                </div>

                                <div class="chip CmaainI">
                                    <i class="pe-7s-menu"></i>
                                    <span class="catChips">Sub Category > <?= $model['sub_category'] ?></span>
                                    <span class="closebtn"
                                          onclick="this.parentElement.style.display='none'">&times;</span>
                                </div>
                            </div>
                        </div>
                        <?php $form = ActiveForm::begin([
                            'layout' => 'horizontal',
                            'fieldConfig' =>
                                [
                                    'horizontalCssClasses' =>
                                        [
                                            'label' => 'col-sm-3 col-form-label',
                                            'offset' => '',
                                            'wrapper' => 'row',
                                            'error' => 'col-sm-8  col-sm-push-3 imgHintS',
                                            'hint' => 'col-sm-8  col-sm-push-3 imgHintS',
                                        ],
                                ],
                            'options' => ['enctype' => 'multipart/form-data', 'id' => 'editpost1489'],
                            // 'enableAjaxValidation' => false,
                        ]) ?>

                        <div class="form-group row font-weight-bold">
                            <label class="col-sm-1 col-form-label light-text-1">
                            </label>
                            <div class="col-sm-11 d-flex justify-content-start">
                                <button type="button" href="#select-Category" data-toggle="modal"
                                        class="btn rounded-0 btn-success"><?php echo Yii::t('app', 'Select Category') ?></button>
                                <div class="mr-5 bg-light p-3 " style="min-width: 250px;">
                                    <div class=" CmaainI d-inline light-text-1">
                                        <i class="fa fa-times"></i>
                                        <span class="catChips ml-2"><?php echo Yii::t('app', 'Category') ?></span>
                                    </div>
                                    <div class=" subChip d-inline light-text-1 ml-3">
                                        <i class="fa fa-time"></i>
                                        <span id="subChipTxt" class="ml-2"></span>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="hidden">
                            <input type="text" id="category45983" value="<?= $model['category'] ?>"
                                   name="Ads[category]">
                            <input type="text" id="Subcategory45983" value="<?= $model['sub_category'] ?>"
                                   name="Ads[sub_category]">
                        </div>

                        <div class="form-group row pt-3 pb-3 rounded bg-light">
                            <label class="col-sm-3 col-form-label"></label>

                            <div class="col-sm-8">
                                <input type="submit" class="btn btn-success btn-lg"
                                       value="<?= Yii::t('app', 'SAVE AND NEXT') ?>">

                            </div>
                        </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                    <!--/.row-box End-->

                </div>
            </div>
            <!--/.page-content-->
            <div class="col-md-3">
                <?= \common\models\Adsense::show('left') ?>

            </div>
        </div>
        <!--/.row-->
    </div>
    <!--/.container-->
</div>
<input type="hidden" class="field" id="locality" disabled="true" value="24.578941" name="city"/>
<input type="hidden" class="field" id="administrative_area_level_1" value="24.5879" name="state" disabled="true"/>
<input type="hidden" class="field" id="country" name="country" disabled="true"/>

<!-- /.main-container -->
<script>
    var placeSearch, autocomplete;

    var componentForm = {
        locality: 'long_name',
        administrative_area_level_1: 'long_name',
        country: 'long_name'
    };
    //    var componentForm = {
    //        hiddenInputCity: 'long_name'
    //       // hiddenInputState: 'short_name',
    //       // hiddenInputCountry: 'long_name',
    //    };
    function initAutocomplete() {
        // Create the autocomplete object, restricting the search predictions to
        // geographical location types.
        autocomplete = new google.maps.places.Autocomplete(
            document.getElementById('autocomplete'), {types: ['geocode']});

        // Avoid paying for data that you don't need by restricting the set of
        // place fields that are returned to just the address components.
        autocomplete.setFields('address_components');

        // When the user selects an address from the drop-down, populate the
        // address fields in the form.
        autocomplete.addListener('place_changed', fillInAddress);
    }

    function fillInAddress() {
        // Get the place details from the autocomplete object.
        var place = autocomplete.getPlace();
        var lat = place.geometry.location.lat(),
            lng = place.geometry.location.lng();
        $('#adsform-longitude').val(lng);
        $('#adsform-latitude').val(lat);

        for (var component in componentForm) {

            document.getElementById(component).value = '';
            document.getElementById(component).disabled = false;
        }

        // Get each component of the address from the place details,
        // and then fill-in the corresponding field on the form.
        for (var i = 0; i < place.address_components.length; i++) {

            var addressType = place.address_components[i].types[0];

            if (componentForm[addressType]) {
                var val = place.address_components[i][componentForm[addressType]];
                document.getElementById(addressType).value = val;
            }
            if (addressType == 'country') {
                var val = place.address_components[i][componentForm[addressType]];
                $('#hiddenInputCountry').val(val);
                $('#CountryCip745').text(val);
            }

            if (addressType == 'administrative_area_level_1') {
                var val = place.address_components[i][componentForm[addressType]];
                $('#hiddenInputState').val(val);
                $('#StateCip754').text(val);
            }
            if (addressType == 'locality') {
                var val = place.address_components[i][componentForm[addressType]];
                $('#hiddenInputCity').val(val);
                $('#CityCip754').text(val);
            }
        }
    }

    // Bias the autocomplete object to the user's geographical location,
    // as supplied by the browser's 'navigator.geolocation' object.
    function geolocate() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {
                var geolocation = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };
                var circle = new google.maps.Circle(
                    {center: geolocation, radius: position.coords.accuracy});
                autocomplete.setBounds(circle.getBounds());
            });
        }
    }
</script>
<div class="modal fade " id="select-Category" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><i class="fa fa-bars"></i>
                    <?= Yii::t('app', 'Select Category') ?>
                </h4>

                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="post-from-modal-cat" class="row">
                    <?= \frontend\models\TemplatesDesign::PostFormCategoryLoop($categoryList, 'icon'); ?>
                </div>
                <div id="post-from-modal-sub" class="row hidden">
                    <div class="col-lg-12" id="post-modal-waiting">
                        <div class="card card-default bg-inverse" style="padding: 30px 10px">
                            <div class="card-body" align="center">
                                <p style="font-size: 60px;color: #fff">
                                    <i id="waiting-icon" class="fa fa-circle-o-notch fa-spin"></i>
                                </p>
                                <p style="font-size: 16px;color: #fff" id="waiting-text">
                                    <?= Yii::t('app', 'Loading Sub Category') ?>...
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 hidden" id="post-modal-data">
                        <div class="col-sm-12 bg-light"
                             style="margin-top: 10px;margin-bottom: 10px;padding: 17px 14px;border-radius: 4px;">
                            <div class="chip">
                                <i class="fa fa-user "></i>
                                <span class="catChips"><?= Yii::t('app', 'Category') ?></span>
                            </div>


                            <button type="button" class="btn btn-success pull-right"
                                    onclick="$('#post-from-modal-sub').addClass('hidden');$('#post-from-modal-cat').removeClass('hidden');"><?= Yii::t('app', 'Back to Main Category') ?>
                                <i class="fa fa-arrow-up"></i></button>
                        </div>
                        <ul class="list-group " id="post-modal-data-list">


                        </ul>
                    </div>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default cat-model" data-dismiss="modal">
                    <i class=""></i>
                    <span><?= Yii::t('app', 'Close') ?></span>
                </button>
            </div>
        </div>
    </div>
</div>
<script src="https://maps.googleapis.com/maps/api/js?key=<?= $api['api_key']; ?>&libraries=places&callback=initAutocomplete"
        async defer></script>
