<div class="col-md-3 page-sidebar">
    <aside>
        <div class="inner-box">
            <div class="user-panel-sidebar">
                <a href="<?= \yii\helpers\Url::toRoute('user/my-ads') ?>" class="btn btn-block btn-lg btn-primary">
                    <i class="fa fa-angle-double-left"></i> <b><?= Yii::t('app', 'My Ads') ?></b>

                </a>
                <!-- /.collapse-box  -->
                <div class="collapse-box">

                    <h5 class="collapse-title">
                        <?= Yii::t('app', 'Edit Ads') ?>
                        <a href="#MyAds" aria-expanded="true" data-toggle="collapse" class="pull-right">
                            <i class="fa fa-angle-down"></i>
                        </a>
                    </h5>

                    <div class="panel-collapse collapse show" id="MyAds">
                        <ul class="acc-list">
                            <li><a href="<?= \yii\helpers\Url::to(['edit/index', 'id' => $adsId]) ?>">
                                    <i class="icon-docs"></i>
                                    <?= Yii::t('app', 'Details and Price') ?>
                                </a>
                            </li>
                            <li>
                                <a href="<?= \yii\helpers\Url::to(['edit/ads-images', 'id' => $adsId]) ?>">
                                    <i class="icon-picture"></i>
                                    <?= Yii::t('app', 'Change Image') ?>
                                </a>
                            </li>
                            <li>
                                <a href="<?= \yii\helpers\Url::to(['edit/ads-category', 'id' => $adsId]) ?>">
                                    <i class="icon-star-circled"></i>
                                    <?= Yii::t('app', 'Change Ads Category') ?>
                                </a>
                            </li>
                            <li>
                                <a href="<?= \yii\helpers\Url::to(['edit/ads-location', 'id' => $adsId]) ?>">
                                    <i class="icon-location"></i>
                                    <?= Yii::t('app', 'Location And Address') ?>
                                </a>
                            </li>
                            <li>
                                <a href="<?= \yii\helpers\Url::to(['edit/ads-others', 'id' => $adsId]) ?>">
                                    <i class="icon-hourglass"></i>
                                    <?= Yii::t('app', 'Others') ?>
                                </a>
                            </li>

                        </ul>
                    </div>
                </div>
                <!-- /.collapse-box  -->


            </div>
        </div>
        <!-- /.inner-box  -->

    </aside>
</div>