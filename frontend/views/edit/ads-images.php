<?php
$this->title = "Change Ads Images- " . Yii::$app->name;

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\file\FileInput;

$defaultSettings = \common\models\DefaultSetting::getDefaultSetting();

?>
<style>
    p {
        padding: 0 !important;
        margin-bottom: 0;
    }

    label {
        font-weight: 600;
    }
</style>
<div class="main-container">

    <div class="container">
        <div class="row">
            <?= $this->render('_side_bar', ['adsId' => $adsId]) ?>
            <!--/.page-sidebar-->


            <div class="col-md-6">


                <div class="inner-box">
                    <h2 class="text-dark text-capitalize font-weight-bold">
                        Change Images : <?= substr($model->ad_title, 0, 35) ?>...
                    </h2>
                    <div class="alert alert-success rounded">
                        <small class="font-weight-bold">
                            You last Changes in at: <?= date("d/m/Y", $model->updated_at) ?>
                        </small>
                    </div>
                    <div class="inner-box-content mt-5">

                        <?php $form = ActiveForm::begin([
                            'layout' => 'horizontal',
                            'fieldConfig' =>
                                [
                                    'horizontalCssClasses' =>
                                        [
                                            'label' => 'col-sm-3 col-form-label',
                                            'offset' => '',
                                            'wrapper' => 'row',
                                            'error' => 'col-sm-8  col-sm-push-3 imgHintS',
                                            'hint' => 'col-sm-8  col-sm-push-3 imgHintS',
                                        ],
                                ],
                            'options' => ['enctype' => 'multipart/form-data', 'id' => 'editpost1489'],
                            // 'enableAjaxValidation' => false,
                        ]) ?>

                        <?php
                        echo $form->field($model, 'image[]', [
                            'template' => '<div class="form-group row">{label}<div class="col-sm-8">{input}{error}{hint}</div></div>'])->widget(FileInput::classname(), [
                            'options' => ['accept' => 'image/*', 'multiple' => true],
                            'pluginOptions' => [
                                'maxFileCount' => $adsSetting['number_of_photo'],
                                'overwriteInitial' => false,
                                'theme' => 'fa',
                                'initialPreviewAsData' => 'true',
                                'cancelLabel' => '',
                                'cancelClass' => 'hidden',
                                'browseLabel' => Yii::t('app', 'Photos'),
                                'browseClass' => 'btn btn-primary',
                                'browseIcon' => '<i class="fa fa-image"></i>',
                                'removeLabel' => '',
                                'removeIcon' => '<i class="fa fa-trash"></i>',
                                'uploadClass' => 'hidden',
                                'uploadLabel' => '',
                                'uploadIcon' => '<i class="fa fa-upload"></i>',
                                'previewFileType' => 'image',
                                'allowedFileExtensions' => ['jpg', 'jpeg', 'png'],
                                'previewClass' => 'bg-light'
                            ]

                        ])->label(Yii::t('app', 'Picture'));
                        ?>

                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label"></label>

                            <div class="col-sm-8">
                                <input type="submit" class="btn btn-success btn-lg"
                                       value="<?= Yii::t('app', 'SAVE AND NEXT') ?>">

                            </div>
                        </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                    <!--/.row-box End-->

                </div>
            </div>
            <!--/.page-content-->
            <div class="col-md-3">
                <div class="card">
                    <div class="card-header bg-white">
                        <p class="card-title"> Image Preview</p>
                    </div>
                    <div class="card-body">
                        <div class="item-image">
                            <ul class="bxslider">
                                <?php
                                $image = $model->image;
                                $imgChunck = explode(",", $image);

                                foreach ($imgChunck as $arr => $img) {
                                    ?>
                                    <li>
                                        <img onerror="this.src='<?= Yii::getAlias('@web') ?>/images/item/itemLight_lg.jpg'"
                                             src="<?= Yii::getAlias('@web') ?>/images/item/<?= $img ?>"
                                             alt="<?= $model['ad_title'] ?>">
                                    </li>
                                    <?php
                                }
                                ?>

                            </ul>

                            <div class="product-view-thumb-wrapper">


                                <ul id="bx-pager" class="product-view-thumb">
                                    <?php
                                    $image = $model->image;
                                    $imgChunck = explode(",", $image);

                                    foreach ($imgChunck as $arr => $img) {
                                        ?>
                                        <li>
                                            <a data-slide-index="<?= $arr; ?>" class="thumb-item-link" href="#">
                                                <img class="lazy"
                                                     src="<?= Yii::getAlias('@web') ?>/images/item/itemLight.jpg"
                                                     data-src="<?= Yii::getAlias('@web') ?>/images/item/<?= $img ?>"
                                                     data-srcset="<?= Yii::getAlias('@web') ?>/images/item/<?= $img ?>"
                                                     alt="<?= $model['ad_title'] ?>">

                                            </a>
                                        </li>
                                        <?php
                                    }
                                    ?>


                                </ul>


                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--/.row-->
    </div>
    <!--/.container-->
</div>
<script>

    $(document).ready(function () {

        // Slider
        var $mainImgSlider = $('.bxslider').bxSlider({
            speed: 1000,
            pagerCustom: '#bx-pager',
            controls: false,
            adaptiveHeight: true
        });

        // initiates responsive slide
        var settings = function () {
            var mobileSettings = {
                slideWidth: 80,
                minSlides: 2,
                maxSlides: 5,
                slideMargin: 5,
                adaptiveHeight: true,
                pager: false

            };
            var pcSettings = {
                slideWidth: 100,
                minSlides: 3,
                maxSlides: 5,
                pager: false,
                slideMargin: 10,
                adaptiveHeight: true
            };
            return ($(window).width() < 768) ? mobileSettings : pcSettings;
        }

        var thumbSlider;

        function tourLandingScript() {
            thumbSlider.reloadSlider(settings());
        }

        thumbSlider = $('.product-view-thumb').bxSlider(settings());
        $(window).resize(tourLandingScript);

    });
</script>
