<?php
$this->title = "Edit Ads - " . Yii::$app->name;

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$defaultSettings = \common\models\DefaultSetting::getDefaultSetting();
$ses_currency = $defaultSettings['currency'];

?>
<style>
    p {
        padding: 0 !important;
        margin-bottom: 0;
    }

    label {
        font-weight: 600;
    }
</style>
<div class="main-container">

    <div class="container">
        <div class="row">
            <?= $this->render('_side_bar', ['adsId' => $adsId]) ?>
            <!--/.page-sidebar-->

            <div class="col-md-6 ">


                <div class="inner-box">
                    <h2 class="text-dark text-capitalize font-weight-bold">
                        Change Details and Price : <?= substr($model->ad_title, 0, 35) ?>...
                    </h2>
                    <div class="alert alert-success rounded">
                        <small class="font-weight-bold">
                            You last Changes in at: <?= date("d/m/Y", $model->updated_at) ?>
                        </small>
                    </div>
                    <div class="inner-box-content mt-5">

                        <?php $form = ActiveForm::begin([
                            'layout' => 'horizontal',
                            'fieldConfig' =>
                                [
                                    'horizontalCssClasses' =>
                                        [
                                            'label' => 'col-sm-3 col-form-label',
                                            'offset' => '',
                                            'wrapper' => 'row',
                                            'error' => 'col-sm-8  col-sm-push-3 imgHintS',
                                            'hint' => 'col-sm-8  col-sm-push-3 imgHintS',
                                        ],
                                ],
                            'options' => ['enctype' => 'multipart/form-data', 'id' => 'editpost1489'],
                            // 'enableAjaxValidation' => false,
                        ]) ?>

                        <?= $form->field($model, 'ad_type', [
                            'template' => '<div class="form-group row">{label}<div class="col-sm-8">{input}{error}{hint}</div></div>'])
                            ->radioList(['Private' => 'Private', 'Business' => 'Business']);
                        ?>
                        <?= $form->field($model, 'ad_title', [
                            'template' => '<div class="form-group row">{label}<div class="col-sm-8">{input}{error}{hint}</div></div>']);
                        ?>
                        <?= $form->field($model, 'ad_description', [
                            'template' => '<div class="form-group row">{label}<div class="col-sm-8">{input}{error}{hint}</div></div>'])->textarea(array('row' => '6', 'style' => 'height:150px'));
                        ?>
                        <?php
                        $cat = $model->category;
                        if ($cat != 'Jobs') {
                            $currency = $defaultSettings['currency'];

                            echo $form->field($model, 'price', [
                                'template' => '<div class="form-group row">{label}<div class="col-sm-9"><div class="input-group">
                                            <span  class="input-group-addon">
                                            <i>' . $ses_currency . '</i>
                                            </span>{input}{error}{hint}
                                            </div>
                                            </div>
                                    <small id="" class="form-text text-muted">{hint}</small></div>'])->textInput(['placeholder' => 'eg. 1500']);
                        }
                        echo $form->field($model, 'currency_symbol')->hiddenInput(['value' => $ses_currency])->label(false);

                        ?>

                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label"></label>

                            <div class="col-sm-8">
                                <input type="submit" class="btn btn-success btn-lg"
                                       value="<?= Yii::t('app', 'SAVE AND NEXT') ?>">
                                <button type="reset" class="btn btn-info btn-lg">
                                    <?= Yii::t('app', 'Reset') ?>
                                </button>
                            </div>
                        </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                    <!--/.row-box End-->

                </div>
            </div>
            <!--/.page-content-->
            <div class="col-md-3">
                <?= \common\models\Adsense::show('left') ?>

            </div>
        </div>
        <!--/.row-->
    </div>
    <!--/.container-->
</div>