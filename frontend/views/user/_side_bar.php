<div class="col-md-3 page-sidebar">
    <aside>
        <div class="inner-box">
            <div class="user-panel-sidebar">
                <div class="collapse-box">
                    <h5 class="collapse-title no-border"> <?= Yii::t('app', 'My Classified') ?>
                        <a href="#MyClassified" aria-expanded="true" data-toggle="collapse" class="pull-right">
                            <i class="fa fa-angle-down"></i>
                        </a>
                    </h5>

                    <div class="panel-collapse collapse show" id="MyClassified">
                        <ul class="acc-list">
                            <li>
                                <a class="active" href="<?= \yii\helpers\Url::toRoute('user/index') ?>">
                                    <i class="icon-home"></i>
                                    <?= Yii::t('app', 'Personal Home') ?>
                                </a>
                            </li>

                        </ul>
                    </div>
                </div>
                <!-- /.collapse-box  -->
                <div class="collapse-box">
                    <h5 class="collapse-title">
                        <?= Yii::t('app', 'My Ads') ?>
                        <a href="#MyAds" aria-expanded="true" data-toggle="collapse" class="pull-right">
                            <i class="fa fa-angle-down"></i>
                        </a>
                    </h5>

                    <div class="panel-collapse collapse show" id="MyAds">
                        <ul class="acc-list">
                            <li><a href="<?= \yii\helpers\Url::toRoute('user/my-ads') ?>">
                                    <i class="icon-docs"></i>
                                    <?= Yii::t('app', 'My ads') ?>
                                    <span class="badge"><?= $myadsCount; ?></span>
                                </a>
                            </li>
                            <li>
                                <a href="<?= \yii\helpers\Url::toRoute('user/favourite-ads') ?>">
                                    <i class="icon-heart"></i>
                                    <?= Yii::t('app', 'Favourite ads') ?>
                                    <span class="badge badge-secondary"><?= $savedAds; ?></span>
                                </a>
                            </li>
                            <li class="hidden">
                                <a href="<?= \yii\helpers\Url::toRoute('user/favourite-ads') ?>">
                                    <i class="icon-star-circled"></i>
                                    <?= Yii::t('app', 'Saved search') ?>
                                    <span class="badge badge-secondary">42</span>
                                </a>
                            </li>
                            <li class="hidden">
                                <a href="account-archived-ads.html">
                                    <i class="icon-folder-close"></i>
                                    Archived ads
                                    <span class="badge badge-secondary">42</span>
                                </a>
                            </li>
                            <li>
                                <a href="<?= \yii\helpers\Url::toRoute('user/pending-ads') ?>">
                                    <i class="icon-hourglass"></i>
                                    <?= Yii::t('app', 'Pending approval') ?>
                                    <span class="badge"><?= $pending ?></span>
                                </a>
                            </li>
                            <li>
                                <a href="<?= \yii\helpers\Url::toRoute('user/chats') ?>">
                                    <i class="icon-chat"></i>
                                    <?= Yii::t('app', 'Chat') ?>
                                    <span class="badge"><?= $msg ?></span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- /.collapse-box  -->

                <div class="collapse-box">
                    <h5 class="collapse-title"> <?= Yii::t('app', 'Terminate Account') ?> <a href="#TerminateAccount"
                                                                                             aria-expanded="true"
                                                                                             data-toggle="collapse"
                                                                                             class="pull-right"><i
                                    class="fa fa-angle-down"></i></a></h5>

                    <div class="panel-collapse collapse show" id="TerminateAccount">
                        <ul class="acc-list">
                            <li><a href="<?= \yii\helpers\Url::toRoute('user/delete') ?>"><i
                                            class="icon-cancel-circled "></i>
                                    <?= Yii::t('app', 'Close account') ?> </a></li>
                        </ul>
                    </div>
                </div>
                <!-- /.collapse-box  -->
            </div>
        </div>
        <!-- /.inner-box  -->

    </aside>
</div>