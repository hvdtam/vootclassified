<?php
$this->title = "Favourite Ads - " . Yii::$app->name;

use yii\helpers\Html; ?>
<div class="main-container">
    <div class="container">
        <div class="row">
            <?= $this->render('_side_bar',
                [
                    'savedAds' => $savedAds,
                    'myadsCount' => $myadsCount,
                    'pending' => $pending,
                    'myAds' => $myAds,
                    'msg' => $msg
                ]) ?>
            <!--/.page-sidebar-->

            <div class="col-md-9 page-content">
                <div class="inner-box">
                    <h2 class="title-2"><i class="icon-heart-empty"></i> <?= Yii::t('app', 'Favourite Ads') ?> </h2>
                    <input type="hidden" name="ads-delete-url" id="ads-delete-url"
                           value="<?= \yii\helpers\Url::toRoute('ads/delete-ajax/') ?>">
                    <div class="table-responsive">

                        <div class="table-action">
                            <label for="checkAll">
                                <input type="checkbox" id="checkAll">
                                <?= Yii::t('app', 'Select: All') ?> |
                                <button type="button" name="delete-all" id="allDel_btn" class="btn btn-sm btn-danger">
                                    <?= Yii::t('app', 'Delete') ?>
                                    <i class="fa fa-remove"></i>
                                </button>
                            </label>

                            <div class="table-search pull-right col-sm-7">
                                <div class="form-group">
                                    <div class="row">

                                    </div>
                                </div>
                            </div>
                        </div>
                        <table id="addManageTable"
                               class="table table-striped table-bordered add-manage-table table demo"
                               data-filter="#filter" data-filter-text-only="true">
                            <thead>
                            <tr>
                                <th data-type="numeric" data-sort-initial="true"></th>
                                <th> <?= Yii::t('app', 'Photo') ?></th>
                                <th data-sort-ignore="true"> <?= Yii::t('app', 'Ads Details') ?> </th>
                                <th data-type="numeric"><?= Yii::t('app', 'Price') ?> </th>
                                <th><?= Yii::t('app', 'Option') ?> </th>
                            </tr>
                            </thead>
                            <tbody>
                            <?= \frontend\models\TemplatesDesign::ProfileAds($myAds, false, true); ?>
                            </tbody>
                        </table>

                    </div>
                    <!--/.row-box End-->

                </div>
            </div>
            <!--/.page-content-->
        </div>
        <!--/.row-->
    </div>
    <!--/.container-->
</div>
<!-- /.main-container -->
