<?php
$this->title = "User Home - " . Yii::$app->name;

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>
<div class="main-container">

    <div class="container">
        <div class="row">
            <?= $this->render('_side_bar',
                [
                    'savedAds' => $savedAds,
                    'myadsCount' => $myadsCount,
                    'pending' => $pending,
                    'myAds' => $myAds,
                    'msg' => $msg
                ]) ?>
            <!--/.page-sidebar-->

            <div class="col-md-9 ">
                <div class="inner-box">
                    <div class="row">
                        <div class="col-md-5 col-xs-4 col-xxs-12">
                            <h3 class="no-padding text-center-480 useradmin">
                                <a href="#">
                                    <img src="<?= Yii::getAlias('@web') ?>/images/users/<?= $author->image ?>"
                                         class="userImg" alt="<?= $author->username ?> image">
                                    <?= $author->first_name . ' ' . $author->last_name ?>
                                </a>
                            </h3>
                        </div>
                        <div class="col-md-7 col-xs-8 col-xxs-12">
                            <div class="header-data text-center-xs">

                                <!-- Traffic data -->
                                <div class="hdata hidden">
                                    <div class="mcol-left">
                                        <!-- Icon with red background -->
                                        <i class="fa fa-eye ln-shadow"></i>
                                    </div>
                                    <div class="mcol-right">
                                        <!-- Number of visitors -->
                                        <p><a href="#">7000</a> <em>visits</em></p>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>

                                <!-- revenue data -->
                                <div class="hdata">
                                    <div class="mcol-left">
                                        <!-- Icon with green background -->
                                        <i class="icon-th-thumb ln-shadow"></i></div>
                                    <div class="mcol-right">
                                        <!-- Number of visitors -->
                                        <p><a href="#"><?= $myadsCount; ?></a><em>Ads</em></p>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>

                                <!-- revenue data -->
                                <div class="hdata">
                                    <div class="mcol-left">
                                        <!-- Icon with blue background -->
                                        <i class="fa fa-user ln-shadow"></i></div>
                                    <div class="mcol-right">
                                        <!-- Number of visitors -->
                                        <p><a href="#"><?= $savedAds; ?></a> <em><?= Yii::t('app', 'Favorites') ?> </em>
                                        </p>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="inner-box">
                    <div class="welcome-msg">
                        <h3 class="page-sub-header2 clearfix no-padding">
                            <?= $author->first_name . ' ' . $author->last_name ?>
                        </h3>
                        <span class="page-sub-header-sub small hidden">
                            You last logged in at: 01-01-2014 12:40 AM [UK time (GMT + 6:00hrs)]
                        </span>
                    </div>
                    <div id="accordion" class="panel-group">
                        <div class="card card-default">
                            <div class="card-header">
                                <h4 class="card-title">
                                    <a href="#collapseB1" aria-expanded="true" data-toggle="collapse">
                                        <?= Yii::t('app', 'My details') ?>
                                    </a>
                                </h4>
                            </div>
                            <div class="panel-collapse collapse show" id="collapseB1">
                                <div class="card-body" align="left">
                                    <?php $form = ActiveForm::begin([

                                        'options' => ['enctype' => 'multipart/form-data'],
                                        'enableAjaxValidation' => false,

                                    ]) ?>

                                    <?= $form->field($author, 'username') ?>

                                    <?= $form->field($author, 'first_name') ?>
                                    <?= $form->field($author, 'last_name') ?>

                                    <?= $form->field($author, 'email') ?>
                                    <?= $form->field($author, 'phone_number')->widget(\borales\extensions\phoneInput\PhoneInput::className(), [
                                        'jsOptions' => [
                                            'preferredCountries' => ['in', 'us', 'au', 'uk'],
                                            'nationalMode' => false
                                        ]
                                    ]); ?>
                                    <?= $form->field($author, 'about_you')->textarea(['row' => 12, 'style' => 'height:auto', 'placeholder' => 'About You..']) ?>
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label" for="textarea">User Image</label>
                                        <div class="col-lg-12">
                                            <div class="mb10">
                                                <?php
                                                echo \kartik\file\FileInput::widget([
                                                    'model' => $author,
                                                    'attribute' => 'image',
                                                    'id' => 'userImage',
                                                    'options' => [
                                                        'multiple' => false
                                                    ],
                                                    'pluginOptions' => [
                                                        'uploadUrl' => \yii\helpers\Url::to(['/site/file-upload']),
                                                        'uploadExtraData' => [
                                                            'album_id' => 44,
                                                            'cat_id' => 'User'
                                                        ],

                                                        'maxFileCount' => 1,
                                                        'overwriteInitial' => false,
                                                        'theme' => 'fa',
                                                        'initialPreviewAsData' => 'true',
                                                        'cancelLabel' => '',
                                                        'cancelClass' => 'hidden',
                                                        'browseLabel' => 'User Image',
                                                        'browseClass' => 'btn btn-success',
                                                        'browseIcon' => '<i class="fa fa-image"></i>',
                                                        'removeLabel' => '',
                                                        'removeIcon' => '<i class="fa fa-trash"></i>',
                                                        'uploadLabel' => '',
                                                        'uploadIcon' => '<i class="fa fa-upload"></i>',
                                                        'previewFileType' => 'image',
                                                        'allowedFileExtensions' => ['jpg', 'jpeg', 'png'],
                                                        'previewClass' => 'bg-light'


                                                    ]
                                                ]);
                                                ?>
                                            </div>

                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <?= Html::submitButton(Yii::t('app', 'Update'), ['class' => 'btn btn-success', 'name' => 'user-update']) ?>
                                    </div>

                                    <?php ActiveForm::end(); ?>
                                </div>
                            </div>
                        </div>
                        <div class="card card-default">
                            <div class="card-header">
                                <h4 class="card-title">
                                    <a href="#collapseB2" aria-expanded="true" data-toggle="collapse">
                                        <?= Yii::t('app', 'Account Settings') ?>
                                    </a>
                                </h4>
                            </div>
                            <div class="panel-collapse collapse" id="collapseB2">
                                <div class="card-body">
                                    <?php $form = ActiveForm::begin([

                                        'options' => ['enctype' => 'multipart/form-data'],
                                        'enableAjaxValidation' => false,

                                    ]) ?>

                                    <?= $form->field($account, 'username') ?>
                                    <?= $form->field($account, 'password_input')->passwordInput() ?>

                                    <div class="form-group">
                                        <?= Html::submitButton(Yii::t('app', 'Update'), ['class' => 'btn btn-success', 'name' => 'user-update']) ?>
                                    </div>

                                    <?php ActiveForm::end(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--/.row-box End-->

                </div>
            </div>
            <!--/.page-content-->
        </div>
        <!--/.row-->
    </div>
    <!--/.container-->
</div>
