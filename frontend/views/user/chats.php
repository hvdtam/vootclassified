<?php
$this->title = "My Chats - " . Yii::$app->user->identity->first_name;

use yii\helpers\Html; ?>
<div class="main-container">
    <div class="container">
        <div class="row">
            <?= $this->render('_side_bar',
                [
                    'savedAds' => $savedAds,
                    'myadsCount' => $myadsCount,
                    'pending' => $pending,
                    'myAds' => $myAds,
                    'msg' => $msg
                ]) ?>
            <!--/.page-sidebar-->

            <div class="col-md-9">
                <div class="inner-box">
                    <h2 class="title-2"><i class="icon-chat"></i> My Chats </h2>
                    <div class="row">
                        <div class="col-md-3">
                            <ul class="list">
                                <?php

                                foreach ($chatList as $list) {
                                    ?>
                                    <li onclick="openChat('<?= $list['chat_id']; ?>','<?= $list['ad_id']; ?>','<?= $list['sender']; ?>')"
                                        style="cursor: pointer;padding: 10px 20px;background-color: #6A6C75;display: block;border-bottom: 1px solid #a4abb5">
                                        <img width="30" class="pull-left img-circle"
                                             src="<?= Yii::getAlias('@web') ?>/images/users/<?= $list['image'] ?>"
                                             alt="image">
                                        <div style="padding-left: 10px;" class="pull-left">
                                            <strong style="color: #f2f5f8">
                                                <?= \common\models\User::getNameById($list['sender']) ?>
                                            </strong>
                                            <br>
                                            <small style="color: #d4d6d9">
                                                <?= $list['text']; ?>
                                            </small>
                                        </div>
                                        <div class="clearfix"></div>
                                    </li>
                                    <?php
                                }
                                ?>
                            </ul>

                        </div>
                        <div class="col-md-9 ">
                            <input type="hidden" value="" id="messageReciever">
                            <input type="hidden" value="" id="AdsId">
                            <input type="hidden" name="check" id="check" value="">
                            <div style="display: block" class="" align="center" id="ChatPreview">
                                <br>
                                <img width="120px" src="<?= Yii::getAlias('@web') ?>/images/site/chatIcon.png">
                                <br><br><br>

                                <span style="color: #5add3e;font-size: 18px;font-weight: 800;">
                                    Chat Message
                                </span>
                                <br><br>
                                <span style="color: #777">
                                    Your Conversation between user will show here.
                                    click the chat list for start conversation
                                </span>
                                <br><br><br><br><br><br>
                            </div>
                            <div style="display: none" class="" id="ChatMsg">
                                <div class="card">
                                    <div class="card-header">
                                        <b style="color: #5add3e">
                                            <i class="fa fa-comment-o"></i> All Message
                                        </b>
                                        <button type="button" class="close" onclick="closeChat()">
                                            <span aria-hidden="true">&times;</span>
                                            <span class="sr-only">Close</span>
                                        </button>
                                    </div>
                                    <div class="card-body" style="height: 329px;overflow-y: scroll" id="displayMsg">

                                    </div>
                                    <div class="card-footer">
                                        <div class="row" align="left">
                                            <input align="left" type="text" placeholder="Type Something..."
                                                   id="msgboxtext" class="form-control col-md-10">
                                            <button type="button" class="btn col-md-2" onclick="sendmsg()">
                                                <i class="fa fa-send-o text-info"></i>
                                            </button>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--/.page-content-->
        </div>
        <!--/.row-->
    </div>
    <!--/.container-->
</div>
<!-- /.main-container -->
<script>
    function openChat(chatId, adId, reciever) {
        $('#displayMsg').html('<b>Loading Message...</b>');
        $('#messageReciever').val(reciever);
        $('#AdsId').val(adId);
        localStorage.removeItem('chatId');

        localStorage.setItem('chatId', chatId);

        //set initial check value
        var count_url = "<?= \yii\helpers\Url::toRoute('ajax/tbt'); ?>?chatId=" + chatId;

        $.post(count_url, function (data) {
            $('#check').val(data);
        }).fail(function (data) {
            alert("Fail  to load message this time");
        });

        //================set initial check value==================

        //================load initial Message==================
        var url = "<?= \yii\helpers\Url::toRoute('message/load-all-msg')?>?chat_id=" + chatId;
        $.post(url, function (data) {
            $('#displayMsg').html('');
            //  $('#displayMsg').append(data);
            var div = $("#displayMsg");
            div.scrollTop(div.prop('scrollHeight'));

        }).done(function (data) {
            $('#displayMsg').append(data);
            var div = $("#displayMsg");
            div.scrollTop(div.prop('scrollHeight'));
        }).fail(function () {
            $('#display-msg').append("<p>Connection Error...</p>");
        });
        //================load initial Message end==================
        $('#ChatPreview').hide();

        $('#ChatMsg').show();
        //
        var div = $("#displayMsg");
        div.scrollTop(div.prop('scrollHeight'));
        setInterval(function () {
            newUpdate();
        }, 3000);


    }

    function closeChat() {
        $('#ChatMsg').hide();
        $('#ChatPreview').show();

    }

    function sendmsg() {
        var receiver = $('#messageReciever').val();
        var ads = $('#AdsId').val();
        var ChatId = localStorage.getItem('chatId');
        var url = "<?= \yii\helpers\Url::toRoute('message/send-msg'); ?>?msg=" + $('#msgboxtext').val() + "&receiver=" + receiver + "&ad=" + ads + "&advertiser=yes";
        $.post(url, function (data) {
            $('#msgboxtext').val(null);
            // $('#displayMsg').append("Sent");
        }).fail(function () {
            $('#display-msg').append("<p>Connection Error...</p>");
        });

    }

    function newUpdate() {
        var chat = localStorage.getItem('chatId');
        var receiver = "<?= $model->id ?>";
        var current = $('#check').val();

        var check = "<?= \yii\helpers\Url::toRoute('message/check'); ?>?chatId=" + chat;

        $.post(check, function (data) {
            if (data > current) {
                console.log("chech client :" + current + " --- " + " server : " + data);
                $('#check').val(data);
                // var new_msg = getNewMsg(chat);
                var check = "<?= \yii\helpers\Url::toRoute('message/load-new-msg'); ?>?chat=" + chat;
                $.post(check, function (data) {
                    $('#displayMsg').append(data);
                    var div = $("#displayMsg");
                    div.scrollTop(div.prop('scrollHeight'));
                }).fail(function () {
                    return "fools";
                });
            }
        }).fail(function (data) {
            alert(data + " urrent:" + current);
        });
    }

    function getNewMsg(chat) {
        var check = "<?= \yii\helpers\Url::toRoute('message/load-new-msg'); ?>?chat=" + chat;
        $.post(check, function (data) {
            return data;
        }).fail(function () {
            return "fools";
        });
    }

    function CheckUpdate() {

        var chat = localStorage.getItem('chatId');
        ;
        var receiver = "<?= $model->id ?>";
        var current = $('#check').val();

        var check = "<?= \yii\helpers\Url::toRoute('message/check'); ?>?chatId=" + chat;

        var url = "<?= \yii\helpers\Url::toRoute('message/load-msg-tpl')?>?chat_id=" + chat + "&current=" + current;
        $.post(url, function (data) {
            $('#displayMsg').append(data);
            var div = $("#displayMsg");
            div.scrollTop(div.prop('scrollHeight'));

        }).done(function () {
            $('#displayMsg').append(data);
        }).fail(function () {
            $('#display-msg').append("<p>Connection Error...</p>");
        });


        $.post(check, function (data) {
            if (data > current) {
                document.getElementById("check").value = data;

            }
        }).fail(function () {
            alert(data + " urrent:" + current);
        });
    }

</script>