<?php

namespace frontend\controllers;


use common\models\Ads;
use common\models\AdsPremium;
use common\models\AdsSettings;
use common\models\Crypto;
use common\models\Payment;
use common\models\Transactions;
use common\models\User;
use Yii;
use yii\helpers\Url;
use yii\rest\CreateAction;
use yii\web\Controller;
use Riazxrazor\Payumoney;

/**
 * payment controller
 */
class PaymentController extends Controller
{
    public $layout = 'main';
    public $enableCsrfValidation = false;

    /**
     * @inheritdoc
     */
    public function actionIndex($type, $pid, $price, $service = false)
    {
        $this->layout = "main";

        if ($service) {
            //$type as a service label
            $payment = Payment::find()->where(['name' => 'Paypal'])->one();
            $rerunUrl = Url::toRoute('payment/success', true) . "?type=$type&pid=" . $pid . "&service=true";
            // return $this->redirect(Url::toRoute('payment/success?type='.$type.'&pid='.$pid));

            return $this->render('index', [
                'type' => $type,
                'paypalAddress' => $payment['email'],
                'payment' => $payment,
                'amount' => $price,
                'rerunUrl' => $rerunUrl,
                'item_name' => "Boost Your profile"

            ]);
        } else {
            $payment = Payment::find()->where(['status' => 'enable'])->one();
            // return $this->redirect(Url::toRoute('payment/success?type='.$type.'&pid='.$pid));
            $rerunUrl = Url::toRoute('payment/success', true) . "?type=$type&pid=" . $pid . "&paymentMethod=Paypal&payment_gateway=paypal&price=" . $price;

            return $this->render('index', [
                'type' => $type,
                'paypalAddress' => $payment['email'],
                'payment' => $payment,
                'amount' => $price,
                'rerunUrl' => $rerunUrl,
                'item_name' => $type

            ]);
        }


    }

    public function actionStripe($type, $pid, $price, $service = false)
    {
        $this->layout = "main";

        $payment = Payment::find()->where(['id' => '4'])->one();
        $return = Url::toRoute('payment/success', true) . "?type=$type&pid=" . $pid . "&paymentMethod=credit card&payment_gateway=stripe&price=" . $price;
        // return $this->redirect(Url::toRoute('payment/success?type='.$type.'&pid='.$pid));

        return $this->render('stripe', [
            'type' => $type,
            'payment' => $payment,
            'amount' => $price,
            'return' => $return,
            'item_name' => "Boost Your profile"

        ]);
    }

    public function actionOffline($type, $pid, $price, $service = false)
    {
        $payment = Payment::find()->where(['id' => '7'])->one();
        $configJson = $payment['config'];
        $config = json_decode($configJson, true);
        $adsSetting = AdsSettings::find()->one();
        if ($adsSetting['login_required'] == 0) {
            $userId = null;
        } else {
            $userId = Yii::$app->user->identity->getId();
        }
        $ads = Ads::find()->where(['id' => $pid])->one();
        $ads->premium = $type;;
        $ads->save(false);
        Yii::$app->session->setFlash('info', 'your premium as has been posted');

        //Add($user,$ads,$AdsType,$payment_gateway,$paymentMethod,$status,$amount=false)
        Transactions::Add($userId, $pid, $type, "offline", "offline", 'pending', $price);
        return $this->render('offline', [
            'type' => $type,
            'payment' => $payment,
            'amount' => $price,
            'config' => $config

        ]);
    }

    public function actionPayuMoney($type, $pid, $price, $service = false)
    {
        $ads = Ads::find()->where(['id' => $pid])->one();
        $return = Url::toRoute('payment/success', true) . "?type=$type&pid=" . $pid . "&paymentMethod=Payu Money&payment_gateway=PayuMoney&price=" . $price;

        $payment = Payment::find()->where(['id' => '5'])->one();
        $configJson = $payment['config'];
        $config = json_decode($configJson, true);


        $key = isset($config['KEY']) ? $config['KEY'] : false;
        $salt = isset($config['SALT']) ? $config['SALT'] : false;
        $testMode = ($config['TEST_MODE'] == "true") ? true : false;
        $debug = ($config['DEBUG'] == "true") ? true : false;


        if (!$key) {
            Yii::$app->session->setFlash('danger', 'Stripe key is invalid or empty, please check your Stripe API key');

        }

        $payumoney = new Payumoney\Payumoney([
            'KEY' => $key,
            'SALT' => $salt,
            'TEST_MODE' => $testMode, // optional default to true
            'DEBUG' => $debug // optional default to false
        ]);


        // All of these parameters are required!
        $params = [
            'txnid' => time(),
            'amount' => $price,
            'productinfo' => $type,
            'firstname' => $ads['name'],
            'email' => $ads['email'],
            'phone' => $ads['mobile'],
            'surl' => $return,
            'furl' => $return,
        ];

// Redirects to PayUMoney
        $payumoney->pay($params)->send();

        //$type as a service label
        $payment = Payment::find()->where(['name' => 'PayuMoney'])->one();
        return $this->render('payumoney', [
            'type' => $type,
            'paypalAddress' => $payment['email'],
            'payment' => $payment,
            'amount' => $price,
            'rerunUrl' => $return,
            'item_name' => "Boost Your profile"

        ]);
    }

    public function actionPaytm($type, $pid, $price, $service = false)
    {

        $this->layout = 'main';

        $request = Yii::$app->request->bodyParams;
        $return = Url::toRoute('payment/success', true) . "?type=$type&pid=" . $pid . "&paymentMethod=credit card&payment_gateway=paytm&price=" . $price;
        $payment = Payment::find()->where(['name' => 'paytm'])->one();

        if (isset($request['subscribe'])) {

            $params = [
                'ORDERID' => 'OR7845796587125',
                'CUST_ID' => '123',
                'TXN_AMOUNT' => '30',
                'EMAIL' => 'demo@gmail.com',
                'MOBILE_NO' => '7844759887',
            ];

            \common\components\Paytm::configPaytm($params, 'live');
        }

        return $this->render('paytm', [
            'type' => $type,
            'payment' => $payment,
            'amount' => $price,
            'return' => $return,
            'item_name' => $type

        ]);
    }

    public function actionPaystack($type, $pid, $price, $service = false)
    {

        $this->layout = 'main';

        $request = Yii::$app->request->bodyParams;
        $return = Url::toRoute('payment/success', true) . "?type=$type&pid=" . $pid . "&paymentMethod=paystack&payment_gateway=paystack&price=" . $price;
        $payment = Payment::find()->where(['name' => 'paystack'])->one();

        return $this->render('paystack', [
            'type' => $type,
            'payment' => $payment,
            'amount' => $price,
            'return' => $return,
            'item_name' => $type

        ]);
    }

    public function actionSecure($method, $adsId, $adsType, $amount)
    {


        $session = Yii::$app->session;
        $session->set('adsType', $adsType);
        $session->set('pid', $adsId);
        $session->set('amount', $amount);
        $rerunUrl = Url::to([$method . '/index', 'amount' => $amount]);
        return $this->redirect($rerunUrl);
    }

    public function actionTransaction($tid, $payment_gateway)
    {

        $session = Yii::$app->session;
        $type = $session->get('adsType');
        $pid = $session->get('pid');
        $rerunUrl = Url::toRoute('payment/success', true) . "?type=$type&pid=" . $pid . "&service=true";

        return $this->redirect($rerunUrl);
        //add money in wallet
    }

    public function actionSuccess($type, $pid, $paymentMethod = false, $payment_gateway = false, $price = false)
    {
        $this->layout = "main";


        if ($payment_gateway == "PayuMoney") {
            $payment = Payment::find()->where(['id' => '5'])->one();
            $payumoney = new Payumoney\Payumoney([
                'KEY' => $payment['public_key'],
                'SALT' => $payment['private_key'],
                'TEST_MODE' => true, // optional default to true
                'DEBUG' => true // optional default to false
            ]);

            $result = $payumoney->completePay($_POST);

            $result = $payumoney->completePay($_POST);

// Returns true if the checksum is correct
            $result->checksumIsValid();
            if ($result->checksumIsValid() && $result->isSuccess()) {
                $ads = Ads::find()->where(['id' => $pid])->one();
                $ads->premium = $type;;
                $ads->save(false);
                Yii::$app->session->setFlash('info', 'your premium as has been posted');
                Transactions::Add(Yii::$app->user->identity->getId(), $pid, $type, $paymentMethod, $payment_gateway, 'success', $price);
                $this->redirect(Url::toRoute('user/my-ads'));

            } else {

                Yii::$app->session->setFlash('danger', 'your transaction is fail');
                Yii::$app->session->setFlash('success', 'Ads posted as regular ads');
                return $this->redirect(Url::toRoute('user/my-ads'));
            }


        } elseif ($payment_gateway == "paytm") {
            $toto = new Crypto();
            $paytmParams = $_POST;
            $merchantKey = "7@92&QgCA74g@AzF";
            $paytmChecksum = isset($_POST["CHECKSUMHASH"]) ? $_POST["CHECKSUMHASH"] : "";
            $isValidChecksum = $toto->verifychecksum_e($paytmParams, $merchantKey, $paytmChecksum);
            if ($isValidChecksum == "TRUE") {
                $ads = Ads::find()->where(['id' => $pid])->one();
                $ads->premium = $type;;
                $ads->save(false);
                Yii::$app->session->setFlash('info', 'Your Ads is now featured');
                Yii::$app->session->setFlash('success', 'your premium as has been posted');
                Transactions::Add(Yii::$app->user->identity->getId(), $pid, $type, $paymentMethod, $payment_gateway, 'success', $price);
                $this->redirect(Url::toRoute('user/my-ads'));
            } else {
                Yii::$app->session->setFlash('danger', 'your transaction is fail');
                Yii::$app->session->setFlash('success', 'Ads posted as regular ads');
                return $this->redirect(Url::toRoute('user/my-ads'));
            }
        } else {
            $ads = Ads::find()->where(['id' => base64_decode($pid)])->one();
            $ads->premium = $type;;
            $ads->save(false);
            Yii::$app->session->setFlash('info', 'your premium as has been posted');
            //$this->redirect(Url::toRoute('user/my-ads'));
            $adsSetting = AdsSettings::find()->one();
            if ($adsSetting['login_required'] == 0 and Yii::$app->user->isGuest) {
                $userId = null;
            } else {
                $userId = Yii::$app->user->identity->getId();
            };
            Transactions::Add($userId, $pid, $type, $paymentMethod, $payment_gateway, 'success', $price);
            return $this->render('success');

        }

    }


    public function actionDemo()
    {
        return $this->render('demo');
    }

    public function actionUpto()
    {

//        die();
//        if ($_POST['STATUS'] === 'Success') {
//            if(isset($_POST['GATEWAYNAME']) && ($_POST['GATEWAYNAME']=='WALLET')){
//                $TXN_AMOUNT = $_POST['TXNAMOUNT'];
//                $TXN_ID = $_POST['TXN_ID'];
//            }
//            Yii::$app->session->setFlash('success', '<br>Thank you for shopping with us. Your credit card has been charged and your transaction is successful. We will be shipping your order to you soon.');
//            return $this->redirect(['payment']);
//        }
        $toto = new Crypto();
        $paytmParams = $_POST;
        $merchantKey = "7@92&QgCA74g@AzF";
        $paytmChecksum = isset($_POST["CHECKSUMHASH"]) ? $_POST["CHECKSUMHASH"] : "";
        $isValidChecksum = $toto->verifychecksum_e($paytmParams, $merchantKey, $paytmChecksum);
        if ($isValidChecksum == "TRUE") {
            echo "<pre>";
            var_dump($_POST);
            echo "<pre>";
            echo "<b>Checksum Matched</b>";
        } else {
            echo "<pre>";
            var_dump($_POST);
            echo "<pre>";
            echo "<b>Checksum MisMatch</b>";
        }
    }


}