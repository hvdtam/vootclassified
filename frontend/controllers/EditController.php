<?php

namespace frontend\controllers;

use common\models\Category;
use common\models\CustomFields;
use common\models\SubCategory;
use Yii;
use common\models\Ads;
use yii\web\NotAcceptableHttpException;
use common\models\AdReport;
use common\models\AdsMore;
use common\models\AdsPremium;
use common\models\AdsSettings;
use yii\web\UploadedFile;

define('SCREENSHOT', Yii::getAlias('@webroot') . '/images/item/');

class EditController extends \yii\web\Controller
{
    public function actionIndex($id)
    {
        if (\Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $saved = Ads::findOne($id);

        $model = Ads::findOne($id);
        $this->exep($model);
        if ($model->load(Yii::$app->request->post())) {
            Yii::$app->session->setFlash('success', '<b>Success!!!</b> Ads edited successfully...');

            $model->save(false);
        }
        return $this->render('index', ['model' => $model, 'adsId' => $id]);
    }

    public function actionAdsImages($id)
    {
        if (\Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $saved = Ads::findOne($id);

        $model = Ads::findOne($id);
        $this->exep($model);
        $adsSetting = AdsSettings::find()->one();

        if ($model->load(Yii::$app->request->post())) {
            if (UploadedFile::getInstances($model, 'image')) {
                $model->image = UploadedFile::getInstances($model, 'image');
                $screen = $model->ScreenShot();
                $model->image = $screen;
            } else {
                $model->image = $saved->image;
            };
            Yii::$app->session->setFlash('success', '<b>Success!!!</b> Ads edited successfully...');

            $model->save(false);
        }
        return $this->render('ads-images', ['model' => $model, 'adsSetting' => $adsSetting, 'adsId' => $id]);
    } // for images upload

    public function actionAdsCategory($id)
    {
        if (\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = Ads::findOne($id);
        $this->exep($model);
        $categoryList = Category::find()->all();

        if ($model->load(Yii::$app->request->post())) {
            Yii::$app->session->setFlash('success', '<b>Success!!!</b> Ads edited successfully...');

            $model->save(false);
        }
        return $this->render('ads-category', ['model' => $model, 'categoryList' => $categoryList, 'adsId' => $id]);
    } // for change ads category

    public function actionAdsLocation($id)
    {
        if (\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = Ads::findOne($id);
        $this->exep($model);
        $categoryList = Category::find()->all();

        if ($model->load(Yii::$app->request->post())) {
            Yii::$app->session->setFlash('success', '<b>Success!!!</b> Ads edited successfully...');

            $model->save(false);
        }
        return $this->render('ads-location', ['model' => $model, 'categoryList' => $categoryList, 'adsId' => $id]);
    } // for change ads location

    public function actionAdsOthers($id)
    {
        if (\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = Ads::findOne($id);
        $saved = Ads::findOne($id);
        $this->exep($model);
        $categoryList = Category::find()->all();
        $form1 = $model->load(Yii::$app->request->post());

        if ($form1) {
            //final value save for ads modal
            $ads = Ads::findOne($id);
            //======================= ================================//

            $array1 = $model['more'];
            $ads->more = json_encode($array1);
            $ads->save(false);
        }
        $SubId = SubCategory::find()->where(['name' => $model['sub_category']])->one();
        $custom = CustomFields::find()->select(['custom_title', 'custom_type', 'custom_options'])->distinct()->where(['custom_subcatid' => $SubId['id']])->all();;

        return $this->render('ads-others', ['model' => $model, 'saved' => $saved, 'custom' => $custom, 'adsId' => $id]);
    } // for change More settings


    public function exep($data)
    {
        $uid = \Yii::$app->user->identity->getId();
        if (!$data) {
            Yii::$app->session->setFlash('error', 'No ads found');
            throw new NotAcceptableHttpException('Something going wrong');
            return false;

        }
        if ($uid != $data->user_id) {
            Yii::$app->session->setFlash('error', 'Something going wrong');
            throw new NotAcceptableHttpException('Something going wrong');
            return false;
        } else {
            return true;
        }
    }


}
