<?php

namespace frontend\controllers;

use common\models\AdReport;
use common\models\Ads;
use common\models\AdsMore;
use common\models\AdsPremium;
use common\models\AdsSettings;
use common\models\Category;
use common\models\Cities;
use common\models\CommentReply;
use common\models\Currency;
use common\models\CustomFields;
use common\models\Message;
use common\models\MetaTags;
use common\models\Reviews;
use common\models\SmsTwilio;
use common\models\SubCategory;
use common\models\Type;
use common\models\User;
use common\models\Verify;
use common\models\Widgets;
use frontend\models\AdsForm;
use frontend\models\ContactSallerForm;
use frontend\models\SearchForm;
use frontend\models\TemplatesDesign;
use Yii;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use yii\base\InvalidParamException;
use yii\data\Pagination;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\UploadedFile;

/**
 * Ads controller
 */
define('SCREENSHOT', Yii::getAlias('@webroot') . '/images/item/');
define('BUSINESSLOGO', Yii::getAlias('@webroot') . '/images/item/logo/');

class ReviewsController extends Controller
{

    public $enableCsrfValidation = false;


    public function actionIndex($token)
    {
        $ads = $token;
        $model = Ads::findOne(['id' => $ads]);
        if (!$model) {
            return $this->render('access');
        }
        //Ads::find()->innerJoin('ads_more','ads.id = ads_more.ads_id');

        $query = new Query;
        $query->select(['user.image', 'reviews.name', 'reviews.rating', 'reviews.comment', 'reviews.created_at'])
            ->from('reviews')
            ->leftJoin('user', 'user.id = reviews.user_id');

        $command = $query->createCommand();
        $reviews = $command->queryAll();
        //$reviews = Reviews::find()->innerJoin('user','reviews.user_id = user.id','user.image')->where(['ad_id'=>$ads])->all();
        //$reviews = Reviews::find()->where(['ad_id'=>$ads])->all();
        return $this->render('index', ['reviews' => $reviews, 'model' => $model]);
    }


    //delete code end
    public function actionWrite($title = false, $token)
    {
        if (\Yii::$app->user->isGuest) {
            Yii::$app->session->setFlash('danger', 'you must be login/register before write a review');
            return $this->goHome();
        };

        $ads = $token;//base64_decode($IID);
        $model = Ads::findOne(['id' => $ads]);
        $Uid = Yii::$app->user->identity->getId();
        $filtrUrlFind = array('/\,/', '/\s+/', '/\?+/', '/\'/');
        $filtrUrlReplace = array('_', '_', '_');

        $backUrl = \yii\helpers\Url::to([
            'ads/detail',
            'title' => trim(preg_replace($filtrUrlFind, $filtrUrlReplace, $model['ad_title'])),
            'IID' => $model['id']
        ]);
        if ($Uid == $model['user_id']) {
            Yii::$app->session->setFlash('danger', 'you cannot write a review for yourself');
            //return $this->redirect($backUrl);
        }
        $Uname = Yii::$app->user->identity->username;

        if (!$model) {
            return $this->render('access');
        }


        $author = User::findOne($model->user_id);
        $WriteReview = new Reviews();
        $review = Reviews::find()->where(['ad_id' => $ads])->all();


        if ($WriteReview->load(Yii::$app->request->post())) {
            $WriteReview->user_id = $Uid;
            $WriteReview->name = $Uname;
            $WriteReview->created_at = time();

            $WriteReview->save(false);

            $overall = $WriteReview->calculate();
            $model->rating = $overall['rating'];
            $model->reviews = $overall['reviews'];
            $model->save(false);
            Yii::$app->session->setFlash('success', 'Thank you for Write a review us.');
            return $this->redirect($backUrl);
        }
        return $this->render('write', [
            'model' => $model,
            'author' => $author,
            'WriteReview' => $WriteReview,
            'review' => $review
        ]);
    }


    public function actionReply()
    {
        $model = new CommentReply();
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

            if ($model->load(Yii::$app->request->post())) {
                $model->created_at = time();
                if ($model->save(false)) {
                    return [
                        'data' => [
                            'success' => true,
                            'model' => $model,
                            'title' => 'Reply added',
                            'message' => 'Your Reply is added. thank you for your feedback',
                        ],
                        'code' => 0,
                    ];
                } else {
                    return [
                        'data' => [
                            'success' => false,
                            'model' => $model->getErrors(),
                            'title' => 'Reply Submission Failed',
                            'message' => 'Your Reply against this comment are not submitted yet. Please try again.',
                        ],
                        'code' => 1, // Some semantic codes that you know them for yourself
                    ];
                }

            } else {
                return [
                    'data' => [
                        'success' => false,
                        'model' => $model->getErrors(),
                        'title' => 'Reply Submission Failed',
                        'message' => 'Your Reply against this comment are not submitted yet. Please try again.',
                    ],
                    'code' => 1, // Some semantic codes that you know them for yourself
                ];
            }
        }


    }

}
