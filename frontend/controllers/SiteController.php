<?php

namespace frontend\controllers;

use common\models\Ads;
use common\models\Category;
use common\models\DefaultSetting;
use common\models\Faq;
use common\models\Languages;
use common\models\MetaTags;
use common\models\Plugin;
use common\models\SeoTools;
use common\models\User;
use common\models\Verify;
use frontend\models\VerifyForm;
use Yii;
use yii\base\InvalidParamException;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use yii\web\Cookie;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public $successUrl = "success";

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['get']

                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
            'auth' => [
                'class' => 'yii\authclient\AuthAction',
                'successCallback' => [$this, 'successCallback'],
            ],
        ];
    }

    public function successCallback($client)
    {
        // get user data from client
        $userAttributes = $client->getUserAttributes();

        $user = User::find()->where(['email' => $userAttributes['email']])->one();
        if (!empty($user)) {
            Yii::$app->user->login($user);
        } else {
            $session = Yii::$app->session;
            $session['attribute'] = $userAttributes;
            $this->successUrl = Url::to(['signup']);
        }
        // do some thing with user data. for example with $userAttributes['email']
    }

    /**
     * Settings Language variable.
     *
     * @return mixed
     */

    public function beforeAction($action)
    {
        $user = new User();
        /*if (!$user->validateServer(true)) {
            $this->redirect(Url::toRoute('site/verify'));
        };*/

        return parent::beforeAction($action);
    }

    public function actionLanguage()
    {
        // die;
        $modal = Languages::findOne(base64_decode($_GET['language']));
        $language = $modal['name'];
        $ISO = $modal['iso_code'];
        $direction = $modal['direction'];
        $session = Yii::$app->session;
        $session->set('language', $ISO);
        $session->set('iso', $ISO);
        $session->set('direction', $direction);

        Yii::$app->language = $ISO;
        Yii::$app->response->cookies->remove('language');
        $languageCookie = new Cookie([
            'name' => 'language',
            'value' => $ISO,
            'expire' => time() + 60 * 60 * 24 * 30, // 30 days
        ]);
        Yii::$app->response->cookies->add($languageCookie);
        $this->redirect(base64_decode($_GET['conical']));

    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $metaTag = MetaTags::find()->where(['page' => 'home page'])->one();
        if ($metaTag) {
            \Yii::$app->view->title = $metaTag->title;

            \Yii::$app->view->registerMetaTag([
                'name' => 'description',
                'content' => $metaTag->description
            ]);
            \Yii::$app->view->registerMetaTag([
                'name' => 'keyword',
                'content' => $metaTag->keyword
            ]);
        }
        $seoTools = SeoTools::find()->one();
        if ($seoTools['google'] != null) {
            \Yii::$app->view->registerMetaTag([
                'name' => 'google-site-verification',
                'content' => $seoTools->google
            ]);
        } //for google site verification
        if ($seoTools['alexa'] != null) {
            \Yii::$app->view->registerMetaTag([
                'name' => 'alexa-site-verification',
                'content' => $seoTools->alexa
            ]);
        } //for alexa site verification
        if ($seoTools['bing'] != null) {
            \Yii::$app->view->registerMetaTag([
                'name' => 'bing-site-verification',
                'content' => $seoTools->bing
            ]);
        } //for bing site verification
        if ($seoTools['yandex'] != null) {
            \Yii::$app->view->registerMetaTag([
                'name' => 'yandex-site-verification',
                'content' => $seoTools->yandex
            ]);
        } //for yandex site verification

        $widgets = new Plugin();
        $session = Yii::$app->session;
        $default = $session->get('defaultSettings');
        if (!$default) {
            $default = DefaultSetting::getDefaultSetting();
        }
        $category = Category::find()->all();
        $featured = Ads::find()->where('premium' != "")->all();
        return $this->render('index',
            [
                'category' => $category,
                'widgets' => $widgets,
                'default' => $default,
                'bannerOption' => null,
            ]
        );
    }

    public function actionFaq()
    {
        $modal = Faq::find()->all();
        return $this->render('faq',
            ['modal' => $modal]
        );
    }

    public function actionCountryList($id)
    {
        $count = \common\models\States::find()
            ->where(['country_id' => $id,])
            ->count();

        $cities = \common\models\States::find()
            ->where(['country_id' => $id])
            ->orderBy('id DESC')
            ->all();

        if ($count > 0) {
            foreach ($cities as $city) {
                echo "<option value='" . $city->id . "'>" . $city->name . "</option>";
            }
        } else {
            echo "<option>-</option>";
        }

    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $metaTag = MetaTags::find()->where(['page' => 'login'])->one();
        if ($metaTag) {
            \Yii::$app->view->title = $metaTag->title;

            \Yii::$app->view->registerMetaTag([
                'name' => 'description',
                'content' => $metaTag->description
            ]);
            \Yii::$app->view->registerMetaTag([
                'name' => 'keyword',
                'content' => $metaTag->keyword
            ]);
        }
        $model = new LoginForm();


        if (isset($_POST['Type']) == 'ajax') {
            $model->username = $_POST['login_username'];
            $model->password = $_POST['login_password'];
            if ($model->login()) {
                return true;
            } else {
                return false;
            }
        } else {
            $model->password = '';

            return $this->render('login', [
                'model' => $model,
            ]);
        }

    }

    /**
     * Ajax Logs in a user.
     *
     * @return mixed
     */
    public function actionAjax()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();

        if ($_POST['Type'] == 'ajax') {
            $model->username = $_POST['username'];
            $model->password = $_POST['password'];
            if ($model->login()) {
                return true;
            } else {
                return false;
            }
        }
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            $model->password = '';

            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        $metaTag = MetaTags::find()->where(['page' => 'contact'])->one();
        if ($metaTag) {
            $this->title = $metaTag->title;
            \Yii::$app->view->registerMetaTag([
                'name' => 'description',
                'content' => $metaTag->description
            ]);
            \Yii::$app->view->registerMetaTag([
                'name' => 'keyword',
                'content' => $metaTag->keyword
            ]);
        }
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending your message.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    /**
     * Displays User Found.
     *
     * @return mixed
     */
    public function actionUserFound($id)
    {
        $model = User::findOne($id);
        // return $this->render('user-found',['model'=>$model]);
        return $this->render('user-found', [
            'model' => $model,
        ]);
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */

    public function actionSignup()
    {

        $metaTag = MetaTags::find()->where(['page' => 'register'])->one();
        if ($metaTag) {
            $this->title = $metaTag->title;
            \Yii::$app->view->registerMetaTag([
                'name' => 'description',
                'content' => $metaTag->description
            ]);
            \Yii::$app->view->registerMetaTag([
                'name' => 'keyword',
                'content' => $metaTag->keyword
            ]);
        }
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {

                    Yii::$app->mailer->compose('signup', ['model' => $model])
                        ->setTo($model->email)
                        ->setFrom([Yii::$app->params['adminEmail'] => Yii::$app->name])
                        ->setSubject('welcome to voot')
                        ->send();
                    return $this->goBack();
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    public function actionUpdate()
    {


        return $this->render('update', [
            'model' => false,
        ]);
    }

    public function actionVerify()
    {

        $this->layout = "main";
        return false;
        $cheksds = User::LiveStatus();
        $msg = "$cheksds";
        $urlD = Url::home();
        if (!$cheksds) {

            $msg = "you cannot use this script until you have full right to use this. please purchase this script from <a href='https://codecanyon.net/krishnainfosoftindia'>Here</a>";
        }


        $model = new VerifyForm();
        if ($model->load(Yii::$app->request->post())) {
            //step1
            $cSession = curl_init();
            $url = "http://localhost/check/validating.php?itemId=" . $model['itemId'] .
                '&purchaseCode=' . $model['purchaseCode'] .
                '&username=' . $model['username'] .
                '&email=' . $model['email'] .
                '&item=Voot' .
                '&url=' . $urlD;

            // $url = "http://xyx.xyz/verify/request.php?itemId=".$model['itemId'].'&purchaseCode='.$model['purchaseCode'].'&username='.$model['username'].'&email='.$model['email'];
//step2
            curl_setopt($cSession, CURLOPT_URL, $url);
            curl_setopt($cSession, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($cSession, CURLOPT_HEADER, false);
//step3
            $result = curl_exec($cSession);
//step4
            // die($result);
            curl_close($cSession);
//step5
            Verify::addUnique($model['purchaseCode'], false);


            Yii::$app->getSession()->setFlash('success', $result);
            return $this->render('verify', [
                'model' => $model,
                'msg' => $result
            ]);
            // return $this->redirect(Url::toRoute('settings/dashboard'));
        } else {
            return $this->render('verify', [
                'model' => $model,
                'msg' => $msg
            ]);
        }
    }

    public function actionVerifyMe()
    {
        $unique = Verify::userVerify();
        if ($unique) {
            $this->redirect(Url::toRoute('site/index'));
        };

        $model = new VerifyForm();
        if ($model->load(Yii::$app->request->post())) {
            $cSession = curl_init();
            // $url = "http://krishnainfosoft.com/verify/otp.php?otp=".$model['otp'];
            $url = "http://localhost/check/otp.php?otp=" . $model['otp'];
//step2
            curl_setopt($cSession, CURLOPT_URL, $url);
            curl_setopt($cSession, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($cSession, CURLOPT_HEADER, false);
//step3
            $result = curl_exec($cSession);
//step4
            curl_close($cSession);
//step5
            //var_dump($result);die;
            if ($result) {
                Verify::addUnique(false, $model['otp']);
                Verify::rightP();
                $msg = "Your successfully identify your identity ";
                Yii::$app->getSession()->setFlash('danger', $msg);
                $this->redirect(Url::toRoute('site/index'));

            } else {
                $msg = "OTP already claim, Please purchase your script from codecanyon";
                Verify::addUnique(false, $model['otp']);
                Yii::$app->getSession()->setFlash('danger', $msg);

            }


            return $this->render('verify_me', [
                'model' => $model,
            ]);
        } else {
            return $this->render('verify_me', [
                'model' => $model,
            ]);
        }
    }


    public function actionPayment()
    {
        $this->layout = 'main';
        $request = Yii::$app->request->bodyParams;
        if (isset($request['subscribe'])) {
            $params = [
                'ORDER_ID' => 'demo',
                'CUST_ID' => '457887',
                'TXN_AMOUNT' => '30',
                'EMAIL' => 'demo@gmail.com',
                'MOBILE_NO' => '7844759887',
            ];

            \common\components\Paytm::configPaytm($params, 'live');
        }
        return $this->render('payment', [
        ]);
    }

    public function actionPaymentSuccess()
    {

        if ($response['order_status'] === 'Success') {
            if (isset($_POST['GATEWAYNAME']) && ($_POST['GATEWAYNAME'] == 'WALLET')) {
                $TXN_AMOUNT = $_POST['TXNAMOUNT'];
                $TXN_ID = $_POST['TXN_ID'];
            }
            Yii::$app->session->setFlash('success', '<br>Thank you for shopping with us. Your credit card has been charged and your transaction is successful. We will be shipping your order to you soon.');
            return $this->redirect(['payment']);
        } else {
            Yii::$app->session->setFlash('error', "<br>Security Error. Illegal access detected");
            return $this->redirect(['payment']);
        }
    }
}
