<?php

namespace frontend\controllers;

use common\models\Ads;
use common\models\Message;
use common\models\SavedAds;
use common\models\User;
use common\models\Version;
use frontend\models\AdsForm;
use yii\filters\AccessControl;
use Yii;
use common\models\Ad;
use yii\data\ActiveDataProvider;
use yii\helpers\Inflector;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * AdController implements the CRUD actions for Ad model.
 */
define('SCREENSHOT', Yii::getAlias('@webroot') . '/images/ads/');
define('PROFILE_DIR', Yii::getAlias('@webroot') . '/images/user/');
define('THUMB_SIZE', 300);

class DemoController extends Controller
{
    /**
     * @inheritdoc
     */

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['delete', 'edit'],
                'rules' => [
                    [
                        'actions' => ['edit'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],


                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index');


    }
    /**
     * set cities.
     *
     * @return mixed
     */
    public function actionString()
    {
        //String
        //Việc_Làm -> Viec_Lam
        $msg = "Việc Làm"; //Viec_Lam
        $msg = Inflector::slug($msg);
        $msg = str_replace('-', '_', $msg);
        dd($msg);

    }
}
