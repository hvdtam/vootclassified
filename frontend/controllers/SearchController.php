<?php

namespace frontend\controllers;

use common\models\AdConfiguration;
use common\models\Ads;
use common\models\Category;
use common\models\Cities;
use common\models\Countries;
use common\models\DefaultSetting;
use common\models\PropertyFeatures;
use common\models\States;
use common\models\SubCategory;
use frontend\models\PhotoForm;
use frontend\models\SearchForm;
use PharIo\Manifest\Type;
use Yii;
use common\models\Ad;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * AdController implements the CRUD actions for Ad model.
 */
define('SCREENSHOT', Yii::getAlias('@webroot') . '/images/ads/');

class SearchController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex($q)
    {
        $default = DefaultSetting::getDefaultSetting();

        $category = Category::find()->where(['LIKE', 'name', $q])->all();
        $subcategory = SubCategory::find()->where(['LIKE', 'name', $q])->all();
        $type = \common\models\Type::find()->where(['LIKE', 'name', $q])->all();

        $model = Ads::find()
            ->where(['city' => $default['city']])
            ->andWhere(['like', 'ad_title', $q])->all();
        return $this->renderPartial('index', [
            'model' => $model,
            'category' => $category,
            'subcategory' => $subcategory,
            'type' => $type,
            'default' => $default

        ]);

    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionHome()
    {
        $model = new SearchForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($model->type) {
                $result = $model->searchHome();
            } else {

                $result = $model->searchSideBar();

            }
            if ($result != null) {
                return $this->render('index', [
                    'ads' => $result,
                    'model' => $model,
                ]);
            } else {
                return $this->render('not-found', [
                    'model' => $model,
                ]);
            }

        }
        return $this->render('not-found', [
            'model' => $model,
        ]);
    }

    /**
     * set cities.
     *
     * @return mixed
     */
    public function actionSet($country, $state = false, $city = false, $type, $flag)
    {
        $session = Yii::$app->session;
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        if ($type == 'country') {
            $session->set('country', $country);
            $session->set('state', null);
            $session->set('city', null);
            $session->set('CurrCity', $country);
            $session->set('locationtype', $type);
            $session->set('flag', $flag);
            $currCity = $country;
        } elseif ($type == 'states') {
            $session->set('country', $country);
            $session->set('state', $state);
            $session->set('city', null);
            $session->set('CurrCity', $state);
            $session->set('locationtype', $type);
            $session->set('flag', $flag);
            $currCity = $state;
        } else {
            $session->set('country', $country);
            $session->set('state', $state);
            $session->set('city', $city);
            $session->set('CurrCity', $city);
            $session->set('flag', $flag);
            $session->set('locationtype', $type);
            $currCity = $city;
        }
        $session->set('locationUpdate', time());

        return [
            'data' => [
                'city' => $currCity,
                'flag' => $flag,
            ],
            'code' => 0,
        ];
    }

    /**
     * return type.
     *
     * @return mixed
     */
    //

    public function actionPtype($id)
    {
        $modal = \common\models\Type::find()->where(['parent' => $id])->all();

        return $this->renderPartial('type', [
            'modal' => $modal
        ]);
    }
    /**
     * return sub category.
     *
     * @return mixed
     */
    //Ptype

    public function actionSubCategory($id)
    {
        $modal = SubCategory::find()->where(['parent' => $id])->all();
        return $this->renderPartial('sub-cat', [
            'modal' => $modal
        ]);
    }

    /**
     *
     * @return mixed
     */
    public function actionCity($city)
    {
        $countryCount = Countries::find()->where(['like', 'name', $city . '%', false])->count();
        $statesCount = States::find()->where(['like', 'name', $city . '%', false])->count();
        $citiesCount = Cities::find()->where(['like', 'name', $city . '%', false])->count();
        //Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        return $this->renderPartial('city', [
            'countryCount' => $countryCount,
            'statesCount' => $statesCount,
            'citiesCount' => $citiesCount,
            'city' => $city

        ]);


        // return $result;
    }
}
