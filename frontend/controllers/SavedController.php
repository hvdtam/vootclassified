<?php

namespace frontend\controllers;

use common\models\SavedAds;
use common\models\SavedAgents;
use common\models\SavedProperty;
use frontend\models\ContactForm;
use Yii;
use common\models\Ad;
use common\models\User;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * SavedController implements the CRUD actions for User model.
 */
define('USER_IMG', Yii::getAlias('@webroot') . '/images/user/');

class SavedController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $agents = User::find()->all();
        return $this->redirect(Url::toRoute('search/not-found'));
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     */
//    agent detail profile displayed  below########
    public function actionAds($ref)
    {
        $model = SavedAds::add($ref);
        return $model;
    }

    public function actionRemoveAds($ref)
    {
        $model = SavedAds::remove($ref);
        return $model;
    }


    //save agents start here

//    public function actionAgents($ref)
//    {
//        $model = SavedAgents::add($ref);
//        return  $model;
//    }
//    public function actionRemoveAgents($ref)
//    {
//        $model = SavedAgents::remove($ref);
//        return $model;
//    }


}
