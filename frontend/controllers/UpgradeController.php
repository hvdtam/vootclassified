<?php

namespace frontend\controllers;


use common\models\Crypto;
use common\models\DefaultSetting;
use common\models\PaymentPaypal;
use common\models\PaymentUpgradee;
use common\models\Plugin;
use common\models\States;
use common\models\Transactions;
use common\models\User;
use Yii;
use yii\base\InvalidArgumentException;
use yii\helpers\Url;
use yii\rest\CreateAction;
use yii\web\Controller;

/**
 * Upgrade controller
 */
class UpgradeController extends Controller
{
    public $enableCsrfValidation = false;

    /**
     * @inheritdoc
     */

    public $layout = "plain";

    public function actionIndex()
    {


        return $this->render('index', [
            'model' => false,
        ]);
    }

    public function actionMoveFiles()
    {


        $error = false;

        $url = Url::toRoute(['upgrade/move']);
        $skipurl = Url::toRoute(['upgrade/update-database']);
        $title = "Moves Files";
        $SubTitle = "Moves Files From Old Folder to New Destination";
        $Skip = "If you using version 2.5 or higher then you can skip this step <a href='$skipurl'> Skip to next</a>";
        return $this->render('phases', [
            'error' => $error,
            'url' => $url,
            'title' => $title,
            'SubTitle' => $SubTitle,
            'Skip' => $Skip,
            'step' => '1/3',
            'btn' => 'Yes i saved all my data'
        ]);
    }

    public function actionMove()
    {

        $error = false;
        try {
            $siteDir = Yii::getAlias('@frontend') . '/web/images/site/';
            $NewSiteDir = str_replace('frontend', '', Yii::getAlias('@frontend')) . 'images/site/';
            $this->deleteDir($NewSiteDir);//
            $site = $this->MoveFiles($siteDir, $NewSiteDir);//

            $BannerDir = Yii::getAlias('@frontend') . '/web/images/banner/';
            $NewBannerDir = str_replace('frontend', '', Yii::getAlias('@frontend')) . 'images/banner/';
            $this->deleteDir($NewBannerDir);//
            $banner = $this->MoveFiles($BannerDir, $NewBannerDir);//

            $itemDir = Yii::getAlias('@frontend') . '/web/images/item/';
            $newItemDir = str_replace('frontend', '', Yii::getAlias('@frontend')) . 'images/item/';
            $this->deleteDir($newItemDir);//
            $item = $this->MoveFiles($itemDir, $newItemDir);//

            $usersDir = Yii::getAlias('@frontend') . '/web/images/users/';
            $newUsersDir = str_replace('frontend', '', Yii::getAlias('@frontend')) . 'images/users/';
            $this->deleteDir($newUsersDir);//
            $users = $this->MoveFiles($usersDir, $newUsersDir);//
            $error = false;

        } catch (\Throwable $e) {
            $error = 'Error Message: ' . $e->getMessage();

        };


        $backendControllers = Yii::getAlias('@backend') . '/controllers';
        $backendViews = Yii::getAlias('@backend') . '/views';

        $FrontendControllers = Yii::getAlias('@frontend') . '/controllers';
        $FrontendViews = Yii::getAlias('@frontend') . '/views';

        $common = Yii::getAlias('@common') . '/models';

        $dir = [
            $backendViews . '/language/',
            $backendViews . '/payment/',//backend
            $FrontendViews . '/payment/',
            $FrontendViews . '/reviews/',
        ];
        $deleteAll = array(
            $backendControllers . '/LanguageController.php',
            $backendControllers . '/PaymentController.php',
            $FrontendControllers . '/PaymentController.php',
            $FrontendControllers . '/ReviewsController.php',
            //views
            $backendViews . '/language/index.php',
            $backendViews . '/language/translate.php',
            $backendViews . '/language/translate2.php',
            $backendViews . '/language/translator.php',
            //translate
            $backendViews . '/payment/ads_payment_setting.php',//backend
            $backendViews . '/payment/ads_payment_setting_edit.php',//backend
            $backendViews . '/payment/edit.php',//backend
            $backendViews . '/payment/index.php',//backend
            $backendViews . '/payment/paypal.php',//backend

            $FrontendViews . '/payment/',

            $FrontendViews . '/reviews/',
            //common
            $common . '/Payment.php',
            $common . '/Reviews.php',
            $common . '/Language.php',
            $common . '/I18nMessage.php',
            $common . '/I18nTranslation.php',
            $common . '/I18nTranslator.php',

        );
        foreach ($deleteAll as $deletable => $data) {
            echo "File Deleted " . $data . "<br>";

            if (is_dir($data)) {
                self::deleteDir($data);
            } else {
                if (file_exists($data)) {
                    unlink($data);
                }
            }
        }
        if ($error) {
            Yii::$app->session->setFlash('success', 'Files Moved Successfully');
        }
        $this->redirect(Url::to(['upgrade/update-database']));

    }

    public function actionXyz()
    {

        try {
            $siteDir = Yii::getAlias('@frontend') . '/web/images/site/';
            $NewSiteDir = str_replace('frontend', '', Yii::getAlias('@frontend')) . 'images/site/';
            $this->deleteDir($NewSiteDir);//
            $site = $this->MoveFiles($siteDir, $NewSiteDir);//

            $BannerDir = Yii::getAlias('@frontend') . '/web/images/banner/';
            $NewBannerDir = str_replace('frontend', '', Yii::getAlias('@frontend')) . 'images/banner/';
            $this->deleteDir($NewBannerDir);//
            $banner = $this->MoveFiles($BannerDir, $NewBannerDir);//

            $itemDir = Yii::getAlias('@frontend') . '/web/images/item/';
            $newItemDir = str_replace('frontend', '', Yii::getAlias('@frontend')) . 'images/item/';
            $this->deleteDir($newItemDir);//
            $item = $this->MoveFiles($itemDir, $newItemDir);//

            $usersDir = Yii::getAlias('@frontend') . '/web/images/users/';
            $newUsersDir = str_replace('frontend', '', Yii::getAlias('@frontend')) . 'images/users/';
            $this->deleteDir($newUsersDir);//
            $users = $this->MoveFiles($usersDir, $newUsersDir);//
            $error = false;

        } catch (\Throwable $e) {
            $error = 'Error Message: ' . $e->getMessage();

        };

        echo $error;


    }

    //
    public function actionUpdateDatabase($skip = false)
    {

        $error = false;

        $url = Url::toRoute(['upgrade/update-note', 'skip' => true]);
        $title = "Update Database ";
        $SubTitle = "Update Tables";
        return $this->render('phases', [
            'error' => $error,
            'url' => $url,
            'title' => $title,
            'SubTitle' => $SubTitle,
            'Skip' => false,
            'step' => '2/3',
            'btn' => 'Update Database'
        ]);
    }

    public function actionUpdateNote($skip = false)
    {

        $error = false;
        if ($skip) {
            // $sqlData[] = ""; SQL SAMPLE
            $sqlData[] = "ALTER TABLE `site_settings` ADD `script_version` VARCHAR(255) NULL DEFAULT NULL AFTER `meta_description`;";
            $sqlData[] = "ALTER TABLE `default_setting` ADD `url_key` TEXT NOT NULL AFTER `active`;";
            $sqlData[] = "ALTER TABLE `default_setting` CHANGE `themes` `themes` ENUM('style','style_old','style_one','style_two','style_three','style_four','style_five','style_six') CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL;";
            $sqlData[] = "ALTER TABLE `track` ADD PRIMARY KEY(`id`);";
            $sqlData[] = "ALTER TABLE `track` ADD INDEX(`id`);";
            $sqlData[] = "TRUNCATE `track`";
            $sqlData[] = "DELETE FROM `api_keys` WHERE `api_keys`.`id` = 1;";
            $sqlData[] = "DELETE FROM `api_keys` WHERE `api_keys`.`id` = 2;";
            $sqlData[] = "DELETE FROM `api_keys` WHERE `api_keys`.`id` = 3;";
            $sqlData[] = "DELETE FROM `api_keys` WHERE `api_keys`.`id` = 4;";
            $sqlData[] = "DROP TABLE `i18n_message`";
            $sqlData[] = "DROP TABLE `i18n_translation`";
            $sqlData[] = "DROP TABLE `i18n_translator`";
            $sqlData[] = "DROP TABLE `payment";
            $sqlData[] = "DROP TABLE `languages`";
            $sqlData[] = "DELETE FROM `widgets` WHERE `widgets`.`id` = 6";
            $sqlData[] = "CREATE TABLE IF NOT EXISTS `plugin` ( `id` INT(11) NOT NULL AUTO_INCREMENT , `plugin_id` INT(11) NOT NULL ,`plugin_icon` VARCHAR(255) NOT NULL , `plugin_name` VARCHAR(255) NOT NULL , `plugin_version` VARCHAR(255) NOT NULL , `plugin_admin_url` VARCHAR(255) NOT NULL , `plugin_url` VARCHAR(255) NOT NULL , `plugin_type` ENUM('module','widgets','system','payment') NOT NULL , `plugin_template` VARCHAR(255) NOT NULL , `plugin_key` VARCHAR(255) NOT NULL , `purchase_code` VARCHAR(255) NOT NULL , `codecanyon_username` VARCHAR(255) NOT NULL , `status` ENUM('0','1') NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;";
            $sqlData[] = "INSERT INTO `plugin` (`plugin_id`, `plugin_icon`, `plugin_name`, `plugin_version`, `plugin_admin_url`, `plugin_url`, `plugin_type`, `plugin_template`, `plugin_key`, `purchase_code`, `codecanyon_username`, `status`) VALUES ('001', 'fa fa-paypal', 'paypal', '1.0', 'uninstalled', 'uninstalled', 'system', 'uninstalled', '', '', '', '0');";
            $sqlData[] = "INSERT INTO `plugin` (`plugin_id`, `plugin_icon`, `plugin_name`, `plugin_version`, `plugin_admin_url`, `plugin_url`, `plugin_type`, `plugin_template`, `plugin_key`, `purchase_code`, `codecanyon_username`, `status`) VALUES ('002', 'fa fa-cc-stripe', 'stripe', '1.0', 'uninstalled', 'uninstalled', 'system', 'uninstalled', '', '', '', '0');";
            $sqlData[] = "INSERT INTO `plugin` (`plugin_id`, `plugin_icon`, `plugin_name`, `plugin_version`, `plugin_admin_url`, `plugin_url`, `plugin_type`, `plugin_template`, `plugin_key`, `purchase_code`, `codecanyon_username`, `status`) VALUES ('003', 'fa fa-dollar', 'paystack', '1.0', 'uninstalled', 'uninstalled', 'system', 'uninstalled', '', '', '', '0');";
            $sqlData[] = "INSERT INTO `plugin` (`plugin_id`, `plugin_icon`, `plugin_name`, `plugin_version`, `plugin_admin_url`, `plugin_url`, `plugin_type`, `plugin_template`, `plugin_key`, `purchase_code`, `codecanyon_username`, `status`) VALUES ('004', 'fa fa-user-o', 'user-reviews', '1.0', 'user-reviews', 'uninstalled', 'system', 'uninstalled', '', '', '', '0');";
            $sqlData[] = "INSERT INTO `plugin` (`plugin_id`, `plugin_icon`, `plugin_name`, `plugin_version`, `plugin_admin_url`, `plugin_url`, `plugin_type`, `plugin_template`, `plugin_key`, `purchase_code`, `codecanyon_username`, `status`) VALUES ('005', 'fa fa-language', 'multi language support', '1.0', '', '', 'system', '', '', '', '', '0');";
            $sqlData[] = "INSERT INTO `plugin` (`plugin_id`, `plugin_icon`, `plugin_name`, `plugin_version`, `plugin_admin_url`, `plugin_url`, `plugin_type`, `plugin_template`, `plugin_key`, `purchase_code`, `codecanyon_username`, `status`) VALUES ('006', 'fa fa-rss', 'blog-post-plugin', '1.0', 'uninstalled', 'uninstalled', 'system', 'uninstalled', '', '', '', '0');";

            $sqlData[] = "UPDATE `category` SET `fa_icon` = 'furniture.jpg' WHERE `category`.`id` = 1;";
            $sqlData[] = "UPDATE `category` SET `fa_icon` = 'jobs.png' WHERE `category`.`id` = 2;";
            $sqlData[] = "UPDATE `category` SET `fa_icon` = 'mobiles.png' WHERE `category`.`id` = 3;";
            $sqlData[] = "UPDATE `category` SET `fa_icon` = 'service.png' WHERE `category`.`id` = 4;";
            $sqlData[] = "UPDATE `category` SET `fa_icon` = 'cars.jpg' WHERE `category`.`id` = 5;";
            $sqlData[] = "UPDATE `category` SET `fa_icon` = 'homes.png' WHERE `category`.`id` = 6;";
            $sqlData[] = "UPDATE `category` SET `fa_icon` = 'bikes.jpg' WHERE `category`.`id` = 7;";
            $sqlData[] = "UPDATE `category` SET `fa_icon` = 'pets.jpg' WHERE `category`.`id` = 8;";
            $sqlData[] = "UPDATE `category` SET `fa_icon` = 'kids.jpg' WHERE `category`.`id` = 9;";
            $sqlData[] = "UPDATE `category` SET `fa_icon` = 'fasion.jpg' WHERE `category`.`id` = 10;";
            $sqlData[] = "UPDATE `category` SET `fa_icon` = 'device.png' WHERE `category`.`id` = 11;";
            $sqlData[] = "UPDATE `category` SET `fa_icon` = 'books.png' WHERE `category`.`id` = 12;";

            foreach ($sqlData as $sql) {

                try {
                    $connection = \Yii::$app->getDb();
                    $command = $connection->createCommand($sql);
                    $result = $command->query();
                    Yii::$app->session->setFlash('success', 'Database is updated');

                    $error = false;
                } catch (\Throwable $e) {
                    $error = 'Error Message: ' . $e->getMessage();

                }
            }

        }

        $this->redirect(Url::to(['upgrade/success']));
    }

    public function actionSuccess($skip = false)
    {

        $error = false;
        Yii::$app->session->setFlash('success', 'Version is updated');

        $url = Url::toRoute(['site/index']);
        $skipurl = Url::toRoute(['upgrade/update-database']);
        $title = "Update is Installed Successfully";
        $SubTitle = "Voot is updated, Thanks For Purchasing Voot";
        return $this->render('phases', [
            'error' => $error,
            'url' => $url,
            'title' => $title,
            'SubTitle' => $SubTitle,
            'Skip' => false,
            'step' => '3/3',
            'btn' => 'Back To Home Page'
        ]);
    }

    public function MoveFiles($OldDir, $NewDir)
    {
        if (!rename($OldDir, $NewDir)) {
            if (copy($OldDir, $NewDir)) {
                unlink($OldDir);
                return TRUE;
            }
            return FALSE;
        }
        return TRUE;
    }

    public static function deleteDir($dirPath)
    {
        //InvalidArgumentException
        if (!is_dir($dirPath)) {
            throw new InvalidArgumentException("$dirPath must be a directory");
        }
        if (substr($dirPath, strlen($dirPath) - 1, 1) != '/') {
            $dirPath .= '/';
        }
        $files = glob($dirPath . '*', GLOB_MARK);
        foreach ($files as $file) {
            if (is_dir($file)) {
                self::deleteDir($file);
            } else {
                unlink($file);
            }
        }
        rmdir($dirPath);
    }
}