<?php

namespace frontend\controllers;

use common\models\AdminNotification;
use common\models\AdReport;
use common\models\Ads;
use common\models\Message;
use common\models\SavedAds;
use common\models\User;
use common\models\Version;
use yii\filters\AccessControl;
use Yii;
use common\models\Ad;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * AdController implements the CRUD actions for Ad model.
 */
define('SCREENSHOT', Yii::getAlias('@webroot') . '/images/ads/');
define('PROFILE_DIR', Yii::getAlias('@webroot') . '/images/user/');
define('THUMB_SIZE', 300);

class AjaxController extends Controller
{
    /**
     * @inheritdoc
     */

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['delete', 'edit'],
                'rules' => [
                    [
                        'actions' => ['edit'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $this->redirect(Url::toRoute('site/index'));
    }

    /**
     * Ad report.
     *
     * @return mixed
     */
    public function actionAdReport()
    {
        $model = new AdReport();
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

            if ($model->load(Yii::$app->request->post()) && $model->save(false)) {

                AdminNotification::add("Ad Report Found", "Someone report an ads.", "reports/index");
                return [
                    'data' => [
                        'success' => true,
                        'model' => $model,
                        'title' => 'Report Submitted',
                        'message' => 'Your Report against this ad successfully submitted. thank you for your feedback',
                    ],
                    'code' => 0,
                ];
            } else {
                return [
                    'data' => [
                        'success' => false,
                        'model' => $model->getErrors(),
                        'title' => 'Report Submission Failed',
                        'message' => 'Your Report against this ad are not submitted yet. Please try again.',
                    ],
                    'code' => 1, // Some semantic codes that you know them for yourself
                ];
            }
        }
    }

    /**
     * Count Message.
     *
     * @return mixed
     */
    public function actionTbt()
    {
        $chatId = $_GET['chatId'];
        if (\Yii::$app->user->isGuest) {
            return false;
        } else {
            $chat = Message::find()->where(['chat_id' => $chatId])->orderBy(['time' => SORT_ASC])->count();
            return $chat;
        }
    }

    /**
     * Count Message.
     *
     * @return mixed
     */
    public static function LoadInbox($chatId)
    {
        if (\Yii::$app->user->isGuest) {
            return false;
        } else {
            $chat = Message::find()->where(['chat_id' => $chatId])->orderBy(['time' => SORT_ASC])->all();
            return $chat;
        }
    }


}
