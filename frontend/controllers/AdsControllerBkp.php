<?php

namespace frontend\controllers;

use common\models\AdReport;
use common\models\Ads;
use common\models\AdsMore;
use common\models\AdsPremium;
use common\models\BuyServices;
use common\models\Category;
use common\models\Cities;
use common\models\City;
use common\models\Currency;
use common\models\CustomFields;
use common\models\Message;
use common\models\QnewCustomFields;
use common\models\SubCategory;
use common\models\Type;
use common\models\User;
use common\models\Verify;
use common\models\Widgets;
use frontend\models\AdsForm;
use frontend\models\SearchForm;
use frontend\models\SearchTypeForm;
use frontend\models\SortByForm;
use frontend\models\TemplatesDesign;
use Yii;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use yii\base\InvalidParamException;
use yii\data\Pagination;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\UploadedFile;

/**
 * Ads controller
 */
define('SCREENSHOT', Yii::getAlias('@webroot') . '/images/item/');

class AdsControllerBkp extends Controller
{


    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionPostAds($category = false, $kyind = false)
    {
        // echo $user_id = Yii::$app->user->id;die;
        $categoryList = Category::find()->all();
        if (\Yii::$app->user->isGuest) {
            Yii::$app->session->setFlash('danger', 'you must be login/register before posting you Ads');
            return $this->goHome();
        }

        $currency = $currency_default = Currency::default_currency();

        $model = new AdsForm();
        $adCustom = new AdsMore();
        if ($model->load(Yii::$app->request->post())) {
            if (UploadedFile::getInstances($model, 'image')) {
                $model->image = UploadedFile::getInstances($model, 'image');
                $screen = $model->ScreenShot();
                $model->image = $screen;
            } else {
                $model->image = false;
            }

            $adsId = $model->post($model->image, true);
            Yii::$app->session->setFlash('success', 'your ads almost done please complete second step.');
            return $this->redirect(Url::toRoute('ads/final/' . base64_encode($adsId)));
        } else {
            //  die("working");
            $SubId = SubCategory::find()->where(['name' => $kyind])->one();
            $custom = CustomFields::find()->where(['custom_subcatid' => $SubId['id']])->all();;
//die;
            return $this->render('post', [
                'adCustom' => $adCustom,
                'custom' => $custom,
                'model' => $model,
                'currency' => $currency,
                'cat' => $category,
                'sub' => $kyind,
                'categoryList' => $categoryList

            ]);
        }
        //return $this->render('index');
    }

    public function actionFinal($ads)
    {
        $adsId = base64_decode($ads);
        $categoryList = Category::find()->all();
        if (\Yii::$app->user->isGuest) {
            Yii::$app->session->setFlash('info', 'you must be login/register before posting you Ads');
            return $this->goHome();
        }

        $currency = $currency_default = Currency::default_currency();

        $model = new AdsForm();
        $Saved = Ads::findOne($adsId);

        $adCustom = new AdsMore();
        $form1 = $model->load(Yii::$app->request->post());
        $form2 = $adCustom->load(Yii::$app->request->post());

        if ($form1) {

            if ($model->premium == "regular") {
                //final value save for ads modal
                $ads = Ads::findOne($adsId);
                $ads->name = $model->name;
                $ads->mobile = $model->mobile;
                $ads->email = $model->email;


                $array1 = $model['more'];
                $ads->more = json_encode($array1);
                $ads->save(false);

                return $this->redirect(Url::toRoute('user/my-ads'));

            } else {
                $ads = Ads::findOne($adsId);
                $ads->name = $model->name;
                $ads->mobile = $model->category;
                $ads->email = $model->category;
                $array1 = $model['more'];
                $ads->more = json_encode($array1);
                $ads->save(false);

                return $this->redirect(Url::toRoute('payment/' . $model->method . '/' . $model->premium . '/' . $adsId . '/' . AdsPremium::getPriceByName($model->premium)));

            }
            Yii::$app->session->setFlash('success', 'your ads is under review...');
        } else {

            $SubId = SubCategory::find()->where(['name' => $Saved['sub_category']])->one();
            $custom = CustomFields::find()->select(['custom_title', 'custom_type', 'custom_options'])->distinct()->where(['custom_subcatid' => $SubId['id']])->all();;
//die;
            return $this->render('post_final', [
                'adCustom' => $adCustom,
                'custom' => $custom,
                'model' => $model,
                'currency' => $currency,
                'cat' => $model['sub_category'],
                'sub' => $model['category'],
                'categoryList' => $categoryList,
                'saved' => $Saved,
                'more' => false

            ]);
        }
        //return $this->render('index');
    }

    /**
     * Displays buy-service page.
     *
     * @return mixed
     */
    public function actionCustomField()
    {
        $kyind = $_GET['sub_cat'];

        $SubId = SubCategory::find()->where(['name' => $kyind])->one();
        $custom = CustomFields::find()->where(['custom_subcatid' => $SubId['id']])->all();
        foreach ($custom as $list) {
            echo "list";
        }
    }

    public function actionIndex()
    {
        // echo $user_id = Yii::$app->user->id;die;


        return $this->render('access');

        //return $this->render('index');
    }

    //edit ads code start here

    public function actionEditAds($id)
    {
        // echo $user_id = Yii::$app->user->id;die;
        if (\Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $adsId = $id;
        $categoryList = Category::find()->all();
        if (\Yii::$app->user->isGuest) {
            Yii::$app->session->setFlash('info', 'you must be login/register before posting you Ads');
            return $this->goHome();
        }

        $currency = $currency_default = Currency::default_currency();

        $model = Ads::findOne($adsId);;
        $saved = Ads::findOne($adsId);;

        $adCustom = new AdsMore();
        if ($model->load(Yii::$app->request->post())) {
            if (UploadedFile::getInstances($model, 'image')) {
                $model->image = UploadedFile::getInstances($model, 'image');
                $screen = $model->ScreenShot();
                echo $model->image = $screen;
            } else {
                $model->image = $saved->image;
            };
            if (empty($model->price)) {
                $model->price = "0";
                $model->currency_symbol = $saved->currency_symbol;

            }
            $adsId = $model->save(false);
            Yii::$app->session->setFlash('success', 'your ads almost done please complete second step.');
            return $this->redirect(Url::toRoute('ads/final/' . base64_encode($adsId)));
        } else {
            //  die("working");

//die;
            return $this->render('post', [
                'adCustom' => $adCustom,
                'model' => $model,
                'currency' => $currency,
                'categoryList' => $categoryList

            ]);
        }

        //return $this->render('index');
    }
    //edit code end here

    //delete code here
    public function actionDelete($id)
    {
        $model = Ads::find()->where(['id' => $id])->one();

        $control = Yii::$app->params['demo'];
        if ($control == true) {
            Yii::$app->session->setFlash('danger', 'Its Demo version. You Cannot Perform delete/edit Action...');
            return false;
        } else {
            $model->delete();
            Yii::$app->session->setFlash('info', 'Ads deleted successfully');
        }
        return $this->redirect(Url::toRoute('site/profile'));
    }

    //delete code here {Main Working Code }
    public function actionDeleteAjax($id)
    {
        $control = Yii::$app->params['demo'];
        if ($control == true) {
            Yii::$app->session->setFlash('danger', 'Its Demo version. You Cannot Perform delete/edit Action...');
            return false;
        } else {
            $model = Ads::find()->where(['id' => $id])->one();
            Category::removeCounter($model->category);
            SubCategory::removeCounter($model->sub_category);
            if ($model->delete()) {
                return true;
            } else {
                return false;

            }
        }
    }

    //delete code end
    public function actionDetail($ads, $title = false)
    {
        $similar = Ads::find()->orderBy(['id' => SORT_DESC])->limit(4)->all();
        $model = Ads::findOne(['id' => base64_decode($ads)]);
        if (!$model) {
            return $this->render('access');
        }
        $author = User::findOne($model->user_id);
        $widget = Widgets::find()->where(['name' => 'item detail'])->one();
        $template = $widget['template'];
        $related = Ads::find()->where(['city' => $model['city']])->andWhere(['category' => $model['category']])
            ->andWhere(['sub_category' => $model['sub_category']])->limit(5)->all();
        $report = new AdReport();

        $more = AdsMore::find()->where(['ads_id' => $model['id']])->one();
        return $this->render('detail', [
            'model' => $model,
            'similar' => $similar,
            'more' => $more,
            'author' => $author,
            'related' => $related,
            'template' => $template,
            'report' => $report
        ]);
    }


    public function actionAll()
    {
        $searchForm = new \frontend\models\SearchForm();
        $all = Category::find()->all();


        $defaultSettings = \common\models\DefaultSetting::getDefaultSetting();
        $locationType = $defaultSettings['l_type'];;
        if ($locationType == '') {
            $cityDefault = $defaultSettings['city'];
            $locationType = 'city';
        } elseif ($locationType == 'city') {
            $cityDefault = $defaultSettings['city'];

        } elseif ($locationType == 'states') {
            $cityDefault = $defaultSettings['state'];
        } else {
            $cityDefault = $defaultSettings['country'];

        }


        $cat = isset($_GET['category']) ? $_GET['category'] : '';
        $sub_cat = isset($_GET['sub_category']) ? $_GET['sub_category'] : '';
        $type = isset($_GET['type']) ? $_GET['type'] : '';
        $sort = isset($_GET['sort']) ? $_GET['sort'] : '';
        $near = isset($_GET['near']) ? $_GET['near'] : false;
        $page_size = 5;

        $widget = Widgets::find()->where(['name' => 'item list'])->one();
        $template = $widget['template'];
        $model = Ads::find()->andWhere([$locationType => $cityDefault])->orderBy(['id' => SORT_DESC])->limit(15)->all();

        $category = Category::find()->all();
        $sub = SubCategory::find()->where(['parent' => Category::findId($cat)])->all();
        $typeList = Type::find()->where(['parent' => SubCategory::findId($sub_cat)])->all();

        if ($searchForm->load(Yii::$app->request->get())) {
            if ($searchForm->type == "cat") {
                $param = 'category';
                $FilterCat = $searchForm->category;
                $FilterCustom = CustomFields::find()->where(['custom_catid' => Category::findId($searchForm->category)])->all();
                // $FilterType = Type::find()->where(['parent'=>''])->all();
                $filterTypeDefiner = TemplatesDesign::TypeDefiner($FilterCat);
            } else {
                $param = 'sub_category';
                $parentName = SubCategory::findParent($searchForm->category);;
                $filterTypeDefiner = TemplatesDesign::TypeDefiner($parentName);
                $cat = $parentName;
                $FilterCat = false;

                $FilterCustom = CustomFields::find()->where(['custom_subcatid' => SubCategory::findId($searchForm->category)])->all();
                // $FilterType = Type::find()->where(['parent'=>''])->all();
            };


            $filter = ['SearchCategory' => $searchForm->category, 'SearchType' => $searchForm->type, 'SearchItem' => $searchForm->item, 'FilterCat' => $FilterCat, 'FilterCustom' => $FilterCustom, 'filterTypeDefiner' => $filterTypeDefiner];
            //filter data options array
            if ($searchForm->filterValue) {
                //==============================================//
                //=============== model query ===================//
                //==============================================//
                $model = Ads::find()->where(['like', 'ad_title', $searchForm->item])->andWhere(['=', $param, $searchForm->category])->andWhere([$locationType => $cityDefault]);
                $AdsData = ArrayHelper::map($model->all(), 'id', 'id');
                $modelFilter = $model;

                foreach ($searchForm->filterValue as $key => $value) {
                    $filterValue = '"' . $key . '":"[' . $value . ']"';//json_encode(array($key=>$value));
                    $modelFilter->andWhere(['like', 'more', $value]);
                }

                $modelFilter->groupBy('id');
                $countModalQuery = clone $modelFilter;
                $modalPages = new Pagination(['totalCount' => $countModalQuery->count(), 'pageSize' => $page_size]);
                $model = $modelFilter->offset($modalPages->offset)
                    ->limit($modalPages->limit)
                    ->all();// for all ads

                //==============================================//
                //=============== business query ===================//
                //==============================================//
                $business = Ads::find()->where(['like', 'ad_title', $searchForm->item])->andWhere(['!=', 'premium', ''])->andWhere(['=', $param, $searchForm->category])->andWhere([$locationType => $cityDefault]);
                $BusinessAdsData = ArrayHelper::map($business->all(), 'id', 'id');
                $businessFilter = Ads::find()->innerJoin('ads_more', 'ads.id = ads_more.ads_id');
                foreach ($BusinessAdsData as $filterData) {
                    $businessFilter->andWhere(['ads.id' => $filterData]);

                    foreach ($searchForm->filterValue as $filterValue) {
                        $businessFilter->andWhere(['ads_more.more_value' => $filterValue]);
                    }

                    //->andWhere(['ads_more.more_value'=>'2005'])->all();
                }
                $businessFilter->groupBy('ads.id');
                $countBUsinessQuery = clone $businessFilter;
                $businessPages = new Pagination(['totalCount' => $countBUsinessQuery->count(), 'pageSize' => $page_size]);
                $business = $businessFilter->offset($businessPages->offset)
                    ->limit($businessPages->limit)
                    ->all();// for all ads

                //==============================================//
                //=============== personal query ===================//
                //==============================================//
                $personal = Ads::find()->where(['like', 'ad_title', $searchForm->item])->andWhere(['!=', 'premium', ''])->andWhere(['=', $param, $searchForm->category])->andWhere([$locationType => $cityDefault]);
                $PersonalAdsData = ArrayHelper::map($personal->all(), 'id', 'id');
                $PersonalFilter = Ads::find()->innerJoin('ads_more', 'ads.id = ads_more.ads_id');
                foreach ($PersonalAdsData as $filterData) {
                    $PersonalFilter->andWhere(['ads.id' => $PersonalAdsData]);

                    foreach ($searchForm->filterValue as $filterValue) {
                        $PersonalFilter->andWhere(['ads_more.more_value' => $filterValue]);
                    }

                    //->andWhere(['ads_more.more_value'=>'2005'])->all();
                }
                $PersonalFilter->groupBy('ads.id');
                $countPersonalQuery = clone $PersonalFilter;
                $PersonalPages = new Pagination(['totalCount' => $countPersonalQuery->count(), 'pageSize' => $page_size]);
                $personal = $PersonalFilter->offset($PersonalPages->offset)
                    ->limit($PersonalPages->limit)
                    ->all();// for all ads


            } else {

                $model = Ads::find()->where(['like', 'ad_title', $searchForm->item])->andWhere(['=', $param, $searchForm->category])->andWhere([$locationType => $cityDefault]);
                $business = Ads::find()->where(['like', 'ad_title', $searchForm->item])->andWhere([$param => $searchForm->category])->andWhere([$locationType => $cityDefault])->andWhere(['!=', 'premium', '']);
                $personal = Ads::find()->where(['like', 'ad_title', $searchForm->item])->andWhere([$param => $searchForm->category])->andWhere([$locationType => $cityDefault])->andWhere(['=', 'premium', '']);

                $countModalQuery = clone $model;
                $modalPages = new Pagination(['totalCount' => $countModalQuery->count(), 'pageSize' => $page_size]);
                $model = $model->offset($modalPages->offset)
                    ->limit($modalPages->limit)
                    ->all();// for all ads

                $countBusinessQuery = clone $business;
                $BusinessPages = new Pagination(['totalCount' => $countBusinessQuery->count(), 'pageSize' => $page_size]);
                $business = $business->offset($BusinessPages->offset)
                    ->limit($BusinessPages->limit)
                    ->all();// for all ads

                $countPersonalQuery = clone $personal;
                $personalPages = new Pagination(['totalCount' => $countPersonalQuery->count(), 'pageSize' => $page_size]);
                $personal = $personal->offset($personalPages->offset)
                    ->limit($personalPages->limit)
                    ->all();// for all ads
            }

            if ($cat == false and $sub_cat == false and $type == false) {

                $catsection = 'visible';
                $subcatsection = 'hidden';
                $typetsection = 'hidden';

            } elseif ($cat == true and $sub_cat == false and $type == false) {
                $catsection = 'hidden';
                $subcatsection = 'visible';
                $typetsection = 'hidden';

            } elseif ($cat == true and $sub_cat == true and $type == false) {
                $catsection = 'hidden';
                $subcatsection = 'visible';
                $typetsection = 'visible';

            } else {
                $catsection = 'hidden';
                $subcatsection = 'hidden';
                $typetsection = 'visible';

            };
            return $this->render('all', [
                'category' => $all,
                'model' => $model,

                'cat' => $cat,
                'sub_cat' => $sub_cat,
                'subList' => $sub,
                'typeList' => $typeList,
                'type' => $type,
                'sort' => $sort,

                'catsection' => $catsection,
                'subcatsection' => $subcatsection,
                'typetsection' => $typetsection,
                'near' => $near,
                'business' => $business,
                'personal' => $personal,
                'pages' => $modalPages,
                'template' => $template,
                'cityDefault' => $cityDefault,
                'filter' => $filter
            ]);
        }

        $this->redirect(Url::toRoute('ads/listing'));

    }

    public function actionContentCat($cat = false, $sub_cat = false, $type = false, $sort = false, $near = false)
    {

        $session = Yii::$app->session;
        $city = $session->get('cityset');//die;;

        $cityDefault = ($city == null) ? "jodhpur" : $city;

        if ($cat == true and $sub_cat == false) {

            if ($sort) {
                switch ($sort) {
                    case "new":
                        $model = Ads::find()->where(['category' => $cat])->andWhere(['city' => $cityDefault])->orderBy(['id' => SORT_DESC])->all();
                        break;
                    case "htl":
                        $model = Ads::find()->where(['category' => $cat])->andWhere(['city' => $cityDefault])->orderBy(['price' => SORT_DESC])->all();
                        break;
                    case "lth":
                        $model = Ads::find()->where(['category' => $cat])->andWhere(['city' => $cityDefault])->orderBy(['id' => SORT_ASC])->all();
                        break;
                }

            } elseif ($near) {
                $lat1 = 26.9124;
                $lon1 = 78.7873;
                $center_lat = 26.9124;
                $center_lng = 78.7873;
                $radius = $near;//25;
                //78.7873
                $query = sprintf("SELECT * ,( 3959 * acos( cos( radians('%s') ) * cos( radians(lat) ) * cos( radians( lng ) - radians('%s') ) + sin( radians('%s') ) * sin( radians( lat ) ) ) )  AS distance FROM ads  where `category` = '" . $cat . "' HAVING distance < '%s' ORDER BY distance LIMIT 0 , 20",
                    //\Yii::$app->db->quoteValue($value); ,
                    $center_lat,
                    $center_lng,
                    $center_lat,
                    $radius);
                $connection = \Yii::$app->db;
                $command = $connection->createCommand($query);
                $model = $command->queryAll();
            } else {
                $model = Ads::find()->where(['category' => $cat])->orderBy(['id' => SORT_DESC])->all();
            }

        } elseif ($sub_cat == true and $type == false) {
            if ($sort) {
                switch ($sort) {
                    case "new":
                        $model = Ads::find()->where(['sub_category' => $sub_cat])->andWhere(['city' => $cityDefault])->orderBy(['id' => SORT_DESC])->all();
                        break;
                    case "htl":
                        $model = Ads::find()->where(['sub_category' => $sub_cat])->andWhere(['city' => $cityDefault])->orderBy(['price' => SORT_DESC])->all();
                        break;
                    case "lth":
                        $model = Ads::find()->where(['sub_category' => $sub_cat])->andWhere(['city' => $cityDefault])->orderBy(['id' => SORT_ASC])->all();
                        break;
                }

            } elseif ($near) {
                $lat1 = 26.9124;
                $lon1 = 78.7873;

                $center_lat = 26.9124;
                $center_lng = 78.7873;
                $radius = $near;//25;
                //78.7873
                $query = sprintf("SELECT * ,( 3959 * acos( cos( radians('%s') ) * cos( radians(lat) ) * cos( radians( lng ) - radians('%s') ) + sin( radians('%s') ) * sin( radians( lat ) ) ) )  AS distance FROM ads  where `sub_category` = '" . $sub_cat . "' HAVING distance < '%s' ORDER BY distance LIMIT 0 , 20",
                    //\Yii::$app->db->quoteValue($value); ,
                    $center_lat,
                    $center_lng,
                    $center_lat,
                    $radius);

                $connection = \Yii::$app->db;
                $command = $connection->createCommand($query);
                $model = $command->queryAll();
            } else {
                $model = Ads::find()->where(['sub_category' => $sub_cat])->andWhere(['city' => $cityDefault])->orderBy(['id' => SORT_DESC])->all();

            }
        } elseif ($type) {
            if ($sort) {
                switch ($sort) {
                    case "new":
                        $model = Ads::find()->where(['type' => $type])->andWhere(['city' => $cityDefault])->orderBy(['id' => SORT_DESC])->all();
                        break;
                    case "htl":
                        $model = Ads::find()->where(['type' => $type])->andWhere(['city' => $cityDefault])->orderBy(['price' => SORT_DESC])->all();
                        break;
                    case "lth":
                        $model = Ads::find()->where(['type' => $type])->andWhere(['city' => $cityDefault])->orderBy(['id' => SORT_ASC])->all();
                        break;

                }

            } elseif ($near) {
                $lat1 = 26.9124;
                $lon1 = 78.7873;

                $center_lat = 26.9124;
                $center_lng = 78.7873;
                $radius = $near;//25;
                //78.7873
                $query = sprintf("SELECT * ,( 3959 * acos( cos( radians('%s') ) * cos( radians(lat) ) * cos( radians( lng ) - radians('%s') ) + sin( radians('%s') ) * sin( radians( lat ) ) ) )  AS distance FROM ads  where `type` = '" . $type . "' HAVING distance < '%s' ORDER BY distance LIMIT 0 , 20",
                    //\Yii::$app->db->quoteValue($value); ,
                    $center_lat,
                    $center_lng,
                    $center_lat,
                    $radius);

                $connection = \Yii::$app->db;
                $command = $connection->createCommand($query);
                $model = $command->queryAll();
            } else {
                $model = Ads::find()->where(['type' => $type])->andWhere(['city' => $cityDefault])->orderBy(['id' => SORT_DESC])->all();

            }

        } else {
            $model = Ads::find()->orderBy(['id' => SORT_DESC])->andWhere(['city' => $cityDefault])->limit(20)->all();

        }
        \frontend\models\TemplatesDesign::ItemList($model);

    }


    /*demo code for testing*/


    public function actionListing()
    {
        $defaultSettings = \common\models\DefaultSetting::getDefaultSetting();


        $locationType = $defaultSettings['l_type'];;
        if ($locationType == '') {
            $cityDefault = $defaultSettings['city'];
            $locationType = 'city';
        } elseif ($locationType == 'city') {
            $cityDefault = $defaultSettings['city'];

        } elseif ($locationType == 'states') {
            $cityDefault = $defaultSettings['state'];
        } else {
            $cityDefault = $defaultSettings['country'];

        }
        $widget = Widgets::find()->where(['name' => 'item list'])->one();
        $template = $widget['template'];
        /* ==============  VERIABLE DECLAIRE =================*/

        $cat = isset($_GET['category']) ? $_GET['category'] : '';
        $sub_cat = isset($_GET['sub_category']) ? $_GET['sub_category'] : '';
        $type = isset($_GET['type']) ? $_GET['type'] : '';
        $sort = isset($_GET['sort']) ? $_GET['sort'] : '';
        $near = isset($_GET['near']) ? $_GET['near'] : false;
        $page_size = 5;

        $category = Category::find()->all();
        $sub = SubCategory::find()->where(['parent' => Category::findId($cat)])->all();
        $typeList = Type::find()->where(['parent' => SubCategory::findId($sub_cat)])->all();

        if ($cat == true and $sub_cat == false and $type == false) {
            $model = Ads::find()->where(['category' => $cat])->andWhere([$locationType => $cityDefault]);
            $business = Ads::find()->where(['category' => $cat])->andWhere([$locationType => $cityDefault])->andWhere(['!=', 'premium', '']);
            $personal = Ads::find()->where(['category' => $cat])->andWhere([$locationType => $cityDefault])->andWhere(['=', 'premium', '']);

        } elseif ($cat == true and $sub_cat == true and $type == false) {
            $model = Ads::find()->where(['category' => $cat])->andWhere(['sub_category' => $sub_cat])->andWhere([$locationType => $cityDefault]);
            $business = Ads::find()->where(['category' => $cat])->andWhere(['sub_category' => $sub_cat])->andWhere([$locationType => $cityDefault])->andWhere(['!=', 'premium', null]);
            $personal = Ads::find()->where(['category' => $cat])->andWhere(['sub_category' => $sub_cat])->andWhere([$locationType => $cityDefault])->andWhere(['=', 'premium', null]);
        } elseif ($cat == true and $sub_cat == true and $type == true) {
            $model = Ads::find()
                ->where(['category' => $cat])
                ->andWhere(['sub_category' => $sub_cat])
                ->andWhere(['type' => $type])
                ->andWhere([$locationType => $cityDefault]);
            $business = Ads::find()
                ->where(['category' => $cat])
                ->andWhere(['sub_category' => $sub_cat])
                ->andWhere(['type' => $type])
                ->andWhere([$locationType => $cityDefault])->andWhere(['!=', 'premium', null]);
            $personal = Ads::find()
                ->where(['category' => $cat])
                ->andWhere(['sub_category' => $sub_cat])
                ->andWhere(['type' => $type])
                ->andWhere([$locationType => $cityDefault])->andWhere(['=', 'premium', null]);
        } else {
            $model = Ads::find()
                ->where([$locationType => $cityDefault]);
            $business = Ads::find()
                ->where([$locationType => $cityDefault])->andWhere(['!=', 'premium', null]);
            $personal = Ads::find()
                ->where([$locationType => $cityDefault])->andWhere(['=', 'premium', null]);
        };
        if ($sort) {
            //  $model = $model->orderBy(['price'=>SORT_ASC])->all();

            switch ($sort) {
                case "new":
                    $countModalQuery = clone $model;
                    $modalPages = new Pagination(['totalCount' => $countModalQuery->count(), 'pageSize' => $page_size]);
                    $model = $model->offset($modalPages->offset)
                        ->limit($modalPages->limit)->orderBy(['id' => SORT_DESC])
                        ->all();


                    $countBusinessQuery = clone $business;
                    $businessPages = new Pagination(['totalCount' => $countBusinessQuery->count(), 'pageSize' => $page_size]);
                    $business = $business->offset($businessPages->offset)
                        ->limit($businessPages->limit)->orderBy(['id' => SORT_DESC])
                        ->all();

                    $countPersonalQuery = clone $personal;
                    $personalPages = new Pagination(['totalCount' => $countPersonalQuery->count(), 'pageSize' => $page_size]);
                    $personal = $personal->offset($personalPages->offset)
                        ->limit($personalPages->limit)->orderBy(['id' => SORT_DESC])
                        ->all();

                    break;
                case "htl":
                    $countModalQuery = clone $model;
                    $modalPages = new Pagination(['totalCount' => $countModalQuery->count(), 'pageSize' => $page_size]);
                    $model = $model->offset($modalPages->offset)
                        ->limit($modalPages->limit)->orderBy(['price' => SORT_DESC])
                        ->all();


                    $countBusinessQuery = clone $business;
                    $businessPages = new Pagination(['totalCount' => $countBusinessQuery->count(), 'pageSize' => $page_size]);
                    $business = $business->offset($businessPages->offset)
                        ->limit($businessPages->limit)->orderBy(['price' => SORT_DESC])
                        ->all();

                    $countPersonalQuery = clone $personal;
                    $personalPages = new Pagination(['totalCount' => $countPersonalQuery->count(), 'pageSize' => $page_size]);
                    $personal = $personal->offset($personalPages->offset)
                        ->limit($personalPages->limit)->orderBy(['price' => SORT_DESC])
                        ->all();
                    break;
                case "lth":

                    $countModalQuery = clone $model;
                    $modalPages = new Pagination(['totalCount' => $countModalQuery->count(), 'pageSize' => $page_size]);
                    $model = $model->offset($modalPages->offset)
                        ->limit($modalPages->limit)->orderBy(['price' => SORT_ASC])
                        ->all();


                    $countBusinessQuery = clone $business;
                    $businessPages = new Pagination(['totalCount' => $countBusinessQuery->count(), 'pageSize' => $page_size]);
                    $business = $business->offset($businessPages->offset)
                        ->limit($businessPages->limit)->orderBy(['price' => SORT_ASC])
                        ->all();

                    $countPersonalQuery = clone $personal;
                    $personalPages = new Pagination(['totalCount' => $countPersonalQuery->count(), 'pageSize' => $page_size]);
                    $personal = $personal->offset($personalPages->offset)
                        ->limit($personalPages->limit)->orderBy(['price' => SORT_ASC])
                        ->all();

            }
        } elseif ($near != '') {
            $lat1 = 26.9124;
            $lon1 = 78.7873;

            $center_lat = 26.900;
            $center_lng = 78.700;
            $radius = $near;//25;
            //78.7873


            $query = sprintf("SELECT * ,( 3959 * acos( cos( radians('%s') ) * cos( radians(lat) ) * cos( radians( lng ) - radians('%s') ) + sin( radians('%s') ) * sin( radians( lat ) ) ) )  AS distance FROM ads  HAVING distance < '%s' ORDER BY distance ",
                //\Yii::$app->db->quoteValue($value); ,
                $center_lat,
                $center_lng,
                $center_lat,
                $radius);
            $connection = \Yii::$app->db;
            $command = $connection->createCommand($query);
            $countModalQuery = clone $command->query();
            $modalPages = new Pagination(['totalCount' => $countModalQuery->count(), 'pageSize' => $page_size]);


            $query_modal = sprintf("SELECT * ,( 3959 * acos( cos( radians('%s') ) * cos( radians(lat) ) * cos( radians( lng ) - radians('%s') ) + sin( radians('%s') ) * sin( radians( lat ) ) ) )  AS distance FROM ads  HAVING distance < '%s' ORDER BY distance LIMIT " . $modalPages->limit . " OFFSET " . $modalPages->limit,
                $center_lat,
                $center_lng,
                $center_lat,
                $radius);
            $command_modal = $connection->createCommand($query_modal);
            $model = $command_modal->queryAll();

            $business = $model;
            $personal = $model;
        } else {
            // $business = $business->all();;
            //  $personal = $personal->all();;
            // $model = $model->all();

            $countModalQuery = clone $model;
            $modalPages = new Pagination(['totalCount' => $countModalQuery->count(), 'pageSize' => $page_size]);
            $model = $model->offset($modalPages->offset)
                ->limit($modalPages->limit)
                ->all();// for all ads

            $countBusinessQuery = clone $business;
            $BusinessPages = new Pagination(['totalCount' => $countBusinessQuery->count(), 'pageSize' => $page_size]);
            $business = $business->offset($BusinessPages->offset)
                ->limit($BusinessPages->limit)
                ->all();// for all ads

            $countPersonalQuery = clone $personal;
            $personalPages = new Pagination(['totalCount' => $countPersonalQuery->count(), 'pageSize' => $page_size]);
            $personal = $personal->offset($personalPages->offset)
                ->limit($personalPages->limit)
                ->all();// for all ads
        };
        if ($cat == false and $sub_cat == false and $type == false) {
            $catsection = 'visible';
            $subcatsection = 'hidden';
            $typetsection = 'hidden';

        } elseif ($cat == true and $sub_cat == false and $type == false) {
            $catsection = 'hidden';
            $subcatsection = 'visible';
            $typetsection = 'hidden';

        } elseif ($cat == true and $sub_cat == true and $type == false) {
            $catsection = 'hidden';
            $subcatsection = 'visible';
            $typetsection = 'visible';

        } else {
            $catsection = 'hidden';
            $subcatsection = 'hidden';
            $typetsection = 'visible';

        };
        dd($cityDefault);
        return $this->render('category', [
            'category' => $category,
            'catsection' => $catsection,
            'subcatsection' => $subcatsection,
            'typetsection' => $typetsection,
            'cat' => $cat,
            'sub_cat' => $sub_cat,
            'subList' => $sub,
            'typeList' => $typeList,
            'type' => $type,
            'sort' => $sort,
            'near' => $near,
            'model' => $model,
            'business' => $business,
            'personal' => $personal,
            'pages' => $modalPages,
            'template' => $template,
            'cityDefault' => $cityDefault
        ]);
    }


    public function actionNearby($radius)
    {
        $session = Yii::$app->session;
        $city = "jodhpur";//$session->get('cityset');//die;;
        $cityDefault = ($city == null) ? "jodhpur" : $city;
        $widget = Widgets::find()->where(['name' => 'item list'])->one();
        $template = $widget['template'];
        $lat1 = 26.9124;
        $lon1 = 78.7873;

        $center_lat = 26.9124;
        $center_lng = 78.7873;
        $radius = $radius;//25;
        //78.7873
        $query = sprintf("SELECT * ,( 3959 * acos( cos( radians('%s') ) * cos( radians(lat) ) * cos( radians( lng ) - radians('%s') ) + sin( radians('%s') ) * sin( radians( lat ) ) ) )  AS distance FROM ads  HAVING distance < '%s' ORDER BY distance LIMIT 0 , 20",
            //\Yii::$app->db->quoteValue($value); ,
            $center_lat,
            $center_lng,
            $center_lat,
            $radius);

        $connection = \Yii::$app->db;
        $command = $connection->createCommand($query);
        $model = $command->queryAll();


        return $this->render('near', [
            'model' => $model,
            'business' => $model,
            'personal' => $model,

        ]);
    }

    public function actionList($near)
    {


        $session = Yii::$app->session;
        $cityDefault = $session->get('cityset');//die;

        $lat1 = 26.9124;
        $lon1 = 78.7873;

        $center_lat = 26.9124;
        $center_lng = 78.7873;
        $radius = $near;//25;
        //78.7873
        $query = sprintf(" SELECT * ,( 3959 * acos( cos( radians('%s') ) * cos( radians(lat) ) * cos( radians( lng ) - radians('%s') ) + sin( radians('%s') ) * sin( radians( lat ) ) ) )  AS distance FROM ads  HAVING distance < '%s' ORDER BY distance LIMIT 0 , 20",
            //\Yii::$app->db->quoteValue($value); ,
            $center_lat,
            $center_lng,
            $center_lat,
            $radius);

        $connection = \Yii::$app->db;
        $command = $connection->createCommand($query);
        $model = $command->queryAll();


        return $this->render('list', [
            'model' => $model
        ]);


        //return $this->render('list');
    }

    public function actionSearch()
    {
        $category = "";
        $session = Yii::$app->session;
        $cityDefault = $session->get('cityset');//die;
        $search = new SearchForm();
        $searchType = new SearchTypeForm();
        if ($search->load(Yii::$app->request->post())) {
            $search->city;
            $model = Ads::find()->where(['LIKE', 'ad_title', $search->item])->andWhere(['category' => $search->category])->andWhere(['city' => $search->city])->all();
            $parent = SubCategory::findId($category);
            $type = Type::find()->where(['parent' => $parent])->all();
            return $this->render('list', [
                'model' => $model,
                'search' => $search,
                'type' => $type,
                'searchType' => $searchType,
            ]);
        }

        //search type end
        $searchType = new SearchTypeForm();
        if ($searchType->load(Yii::$app->request->post())) {
            $searchType->type;
            $model = Ads::find()->where(['type' => $searchType->type])->andWhere(['city' => $searchType->city])->all();
            $parent = SubCategory::findId($category);
            $type = Type::find()->where(['parent' => $parent])->all();
            return $this->render('list', [
                'model' => $model,
                'search' => $search,
                'type' => $type,
                'searchType' => $searchType,

            ]);
        }

        //sortby start
        $Sortby = new SortByForm();
        if ($Sortby->load(Yii::$app->request->post())) {
            // echo  $Sortby->sort;die;
            if ($Sortby->sort == "low") {
                $model = Ads::find()->where(['sub_category' => $category])->andWhere(['city' => $cityDefault])->orderBy(['price' => SORT_DESC])
                    ->all();
            } elseif ($Sortby->sort == "high") {
                $model = Ads::find()->where(['sub_category' => $category])->andWhere(['city' => $cityDefault])->orderBy(['price' => SORT_ASC])
                    ->all();
            } else {
                $model = Ads::find()->where(['sub_category' => $category])->andWhere(['city' => $cityDefault])->orderBy(['created_at' => SORT_DESC])
                    ->all();
            }

//            $model = Ads::find()
//                ->where(['sub_category'=>$category])
//                ->andWhere(['city'=>$cityDefault])
//                ->orderBy([
//                    'price'=>$Sortby->sort
//                ])->limit(10)
//                ->all();
            $parent = SubCategory::findId($category);
            $type = Type::find()->where(['parent' => $parent])->all();
            return $this->render('list', [
                'model' => $model,
                'search' => $search,
                'type' => $type,
                'searchType' => $searchType,

            ]);
        }
        //sortby end

        $model = Ads::find()->where(['sub_category' => $category])->andWhere(['city' => $cityDefault])->all();

        $parent = SubCategory::findId($category);
        $type = Type::find()->where(['parent' => $parent])->all();
        return $this->render('list', [
            'model' => $model,
            'search' => $search,
            'type' => $type,
            'searchType' => $searchType,

        ]);
    }

    public function actionScity($param)
    {
        $session = Yii::$app->session;
        $session->set('cityset', isset($param) ? $param : 'Jodhpur');
        Url::previous('currency_p');
        return $this->redirect(Url::previous('currency_p'));
        // return " you choose ".$param." as deault";

    }

    public function actionSearchtype($search, $category)
    {
        ///die;
        //SearchTypeForm
        $searchType = new SearchTypeForm();
        if ($searchType->load(Yii::$app->request->post())) {
            $searchType->type;
            $model = Ads::find()->where(['type' => $search->type])->andWhere(['city' => $searchType->city])->all();
            $parent = SubCategory::findId($category);
            $type = Type::find()->where(['parent' => $parent])->all();
            return $this->render('list', [
                'model' => $model,
                'search' => $search,
                'type' => $type,
            ]);
        }
    }

    // ###############################  ajax function is start here ################################################
    public function actionCat($id)
    {
        $p_id = Category::find()->where(['name' => $id])->one();
        $count = SubCategory::find()
            ->where(['parent' => $p_id->id])
            ->All();
        $subCat = SubCategory::find()
            ->where(['parent' => $p_id->id])
            ->All();
        if ($count >= 1) {
            echo "<option value='other'> Other</option>";
            foreach ($subCat as $name) {
                echo "<option value='" . $name->name . "'>" . $name->name . "</option>";
            }

        } else {
            echo "<option value='other'> Other</option>";
        }
    }

    public function actionFormtype($name)
    {
        $p_id = SubCategory::find()->where(['name' => $name])->one();
        $count = Type::find()
            ->where(['parent' => $p_id->id])
            ->All();
        $city = Type::find()
            ->where(['parent' => $p_id->id])
            ->All();
        if ($count > 0) {
            echo "<option value='other'> Other</option>";
            foreach ($city as $name) {
                echo "<option value='" . $name->name . "'>" . $name->name . "</option>";
            }

        } else {
            echo "<option value='other'> other </option>";
        }
    }

    public function actionCustom($name)
    {
        $SubId = SubCategory::find()->where(['name' => $name])->one();
        $custom = CustomFields::find()->where(['custom_subcatid' => $SubId['id']])->all();;
        $adCustom = new AdsMore();
        return $this->render('custom', [
            'adCustom' => $adCustom,
            'custom' => $custom,

        ]);
    }


//    public function actionState($id)
//    {
//        $count = Cities::find()
//            ->where(['state_id'=>$id])
//            ->All();
//        $city = Cities::find()
//            ->where(['state_id'=>$id])
//            ->All();
//        if($count > 0) {
//            foreach ($city as $name) {
//                echo "<option value='" . $name->city . "'>" . $name->city . "</option>";
//            }
//        }
//        else
//        {
//            echo "<option></option>";
//        }
//    }

}
