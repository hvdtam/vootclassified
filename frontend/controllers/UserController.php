<?php

namespace frontend\controllers;

use common\models\Ads;
use common\models\Category;
use common\models\Message;
use common\models\SavedAds;
use common\models\SubCategory;
use common\models\User;
use common\models\Version;
use yii\db\Query;
use yii\filters\AccessControl;
use Yii;
use common\models\Ad;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * AdController implements the CRUD actions for Ad model.
 */
define('SCREENSHOT', Yii::getAlias('@webroot') . '/images/item/');
define('PROFILE_DIR', Yii::getAlias('@webroot') . '/images/users/');
define('THUMB_SIZE', 300);

class UserController extends Controller
{
    /**
     * @inheritdoc
     */

//    public function behaviors()
//    {
//        return [
//            'access' => [
//                'class' => AccessControl::className(),
//                'only' => ['delete', 'edit','index'],
//                'rules' => [
//                    [
//                        'actions' => ['index'],
//                        'allow' => true,
//                        'roles' => ['?'],
//                    ],
//                ],
//            ],
//            'verbs' => [
//                'class' => VerbFilter::className(),
//                'actions' => [
//                    'delete' => ['POST'],
//                ],
//            ],
//        ];
//    }

    /**
     * Displays user Dashboard.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        if (\Yii::$app->user->isGuest) {
            Yii::$app->getSession()->setFlash('error', '<b>Error: </b> Login for access this page');
            return $this->redirect(Url::toRoute('site/login'));
        };

        $id = Yii::$app->user->identity->id;
        $myadsCount = Ads::find()->where(['user_id' => $id])->count();
        $myadspending = Ads::find()->where(['user_id' => $id])->andWhere(['active' => 'pending'])->count();
        $savedAds = SavedAds::find()->where(['user_id' => $id])->count();

        $msg = Message::find()->where(['receiver' => $id])->orderBy(['time' => SORT_ASC])->count();

        $myAds = Ads::find()->where(['user_id' => $id])->orderBy(['id' => SORT_DESC])->limit(10)->all();
        $author = User::find()->where(['id' => $id])->one();
        $default = User::findOne($id);
        $account = User::find()->where(['id' => $id])->one();

        //$author = User::find()->where(['id'=>$id])->one();

        //save Form
        if ($author->load(Yii::$app->request->post())) {
            if (Version::isDemo()) {
                Yii::$app->session->setFlash('danger', 'Its Demo version. You Cannot Perform delete/edit Action...');
                return $this->render('index', [
                    'savedAds' => $savedAds,
                    'myadsCount' => $myadsCount,
                    'pending' => $myadspending,
                    'myAds' => $myAds,
                    'author' => $author,
                    'msg' => $msg,
                    'account' => $account
                ]);
            }

            if (UploadedFile::getInstance($author, 'image')) {

                $author->image = UploadedFile::getInstance($author, 'image');
                $ext = substr(strrchr($author->image, '.'), 1);
                $desiredExt = 'jpg';
                $fileNameNew = rand(137, 999) . time() . ".$desiredExt";
                $path[0] = $author->image->tempName;
                $path[1] = PROFILE_DIR . $fileNameNew;
                if (empty($path[0])) {
                    Yii::$app->getSession()->setFlash('error', '<b>Error: </b> Image may be currepted or too big file size. try another image');
                    return $this->render('index', [
                        'savedAds' => $savedAds,
                        'myadsCount' => $myadsCount,
                        'pending' => $myadspending,
                        'myAds' => $myAds,
                        'author' => $author,
                        'msg' => $msg,
                        'account' => $account
                    ]);
                } else {
                    User::createThumb($path[0], $path[1], $ext, "150", "150", THUMB_SIZE);
                    $author->image = $fileNameNew;
                    $author->save(false);

                }


            } else {
                if (Version::isDemo()) {
                    Yii::$app->session->setFlash('danger', 'Its Demo version. You Cannot Perform delete/edit Action...');
                    return $this->render('index', [
                        'savedAds' => $savedAds,
                        'myadsCount' => $myadsCount,
                        'pending' => $myadspending,
                        'myAds' => $myAds,
                        'author' => $author,
                        'msg' => $msg,
                        'account' => $account
                    ]);
                } else {
                    //die($author->username);
                    $author->image = $default->image;
                    $run = $author->save(false);
                    if ($run) {
                        Yii::$app->getSession()->setFlash('success', '<b>Success</b> Account detail successfully updated');
                    } else {
                        Yii::$app->getSession()->setFlash('info', '<b>Info</b> Account detail successfully updated');

                        echo $author->errors;
                    }
                }

                // $model->save(false);
                //  Yii::$app->session->setFlash('danger', 'Its Demo version. You Cannot Perform delete/edit Action...');

            }


            //return $this->redirect(Url::toRoute('edit/account'));

        }
        if ($account->load(Yii::$app->request->post())) {
            if (Version::isDemo()) {

                Yii::$app->session->setFlash('danger', 'Its Demo version. You Cannot Perform delete/edit Action...');

            } else {
                // die($account->password_input);
                $account->password_hash = Yii::$app->security->generatePasswordHash($account->password_input);
                $run = $account->save(false);
                if ($run) {
                    Yii::$app->getSession()->setFlash('success', '<b>Success</b> Account detail successfully updated');
                } else {
                    Yii::$app->getSession()->setFlash('info', '<b>Info</b> Account detail successfully updated');

                    echo $author->errors;
                }
            }
        }
        //$city = City::find()->all();
        return $this->render('index', [
            'savedAds' => $savedAds,
            'myadsCount' => $myadsCount,
            'pending' => $myadspending,
            'myAds' => $myAds,
            'author' => $author,
            'msg' => $msg,
            'account' => $account
        ]);

    }

    /**
     * set cities.
     *
     * @return mixed
     */
    public function actionMyAds()
    {
        if (\Yii::$app->user->isGuest) {
            Yii::$app->getSession()->setFlash('error', '<b>Error: </b> Login for access this page');
            return $this->redirect(Url::toRoute('site/login'));
        };

        $id = Yii::$app->user->identity->id;
        $myadsCount = Ads::find()->where(['user_id' => $id])->count();
        $myadspending = Ads::find()->where(['user_id' => $id])->andWhere(['active' => 'pending'])->count();
        $savedAds = SavedAds::find()->where(['user_id' => $id])->count();

        $msg = Message::find()->where(['receiver' => $id])->orderBy(['time' => SORT_ASC])->count();

        $myAds = Ads::find()->where(['user_id' => $id])->orderBy(['id' => SORT_DESC])->all();


        return $this->render('my-ads', [
            'savedAds' => $savedAds,
            'myadsCount' => $myadsCount,
            'pending' => $myadspending,
            'myAds' => $myAds,
            'msg' => $msg,
        ]);

    }

    public function actionFavouriteAds()
    {
        if (\Yii::$app->user->isGuest) {
            Yii::$app->getSession()->setFlash('error', '<b>Error: </b> Login for access this page');
            return $this->redirect(Url::toRoute('site/login'));
        };

        $id = Yii::$app->user->identity->id;
        $myadsCount = Ads::find()->where(['user_id' => $id])->count();
        $myadspending = Ads::find()->where(['user_id' => $id])->andWhere(['active' => 'pending'])->count();
        $savedAds = SavedAds::find()->where(['user_id' => $id])->count();
        $msg = Message::find()->where(['receiver' => $id])->orderBy(['time' => SORT_ASC])->count();
        $query = new Query;
        $query->select('*')
            ->from('ads')->where(['ads.user_id' => $id])
            ->join('LEFT JOIN', 'saved_ads',
                'saved_ads.user_id = ads.user_id')
            ->LIMIT(5);

        $command = $query->createCommand();
        $data = $command->queryAll();


        return $this->render('favourite-ads', [
            'savedAds' => $savedAds,
            'myadsCount' => $myadsCount,
            'pending' => $myadspending,
            'myAds' => $data,
            'msg' => $msg,
        ]);

    }

    public function actionPendingAds()
    {
        if (\Yii::$app->user->isGuest) {
            Yii::$app->getSession()->setFlash('error', '<b>Error: </b> Login for access this page');
            return $this->redirect(Url::toRoute('site/login'));
        };

        $id = Yii::$app->user->identity->id;
        $myadsCount = Ads::find()->where(['user_id' => $id])->count();
        $myadspending = Ads::find()->where(['user_id' => $id])->andWhere(['active' => 'pending'])->count();
        $savedAds = SavedAds::find()->where(['user_id' => $id])->count();
        $msg = Message::find()->where(['receiver' => $id])->orderBy(['time' => SORT_ASC])->count();
        $data = Ads::find()->where(['user_id' => $id])->andWhere(['active' => 'pending'])->all();


        return $this->render('pending-ads', [
            'savedAds' => $savedAds,
            'myadsCount' => $myadsCount,
            'pending' => $myadspending,
            'myAds' => $data,
            'msg' => $msg,
        ]);

    }

    public function actionChats()
    {
        if (\Yii::$app->user->isGuest) {
            Yii::$app->getSession()->setFlash('error', '<b>Error: </b> Login for access this page');
            return $this->redirect(Url::toRoute('site/login'));
        };

        $id = Yii::$app->user->identity->id;

        $query = sprintf("SELECT `message`.*, `user`.`image` FROM `message` INNER JOIN `user` ON message.sender = user.id WHERE `message`.`receiver`=" . $id . " GROUP BY message.chat_id");
        $connection = \Yii::$app->db;
        $command = $connection->createCommand($query);
        $chatList = $command->queryAll();

        $myadsCount = Ads::find()->where(['user_id' => $id])->count();
        $myadspending = Ads::find()->where(['user_id' => $id])->andWhere(['active' => 'pending'])->count();
        $savedAds = SavedAds::find()->where(['user_id' => $id])->count();
        $msg = Message::find()->where(['receiver' => $id])->orderBy(['time' => SORT_ASC])->groupBy('chat_id')->count();
        $data = Ads::find()->where(['user_id' => $id])->andWhere(['active' => 'pending'])->all();
        $model = User::findOne($id);
        return $this->render('chats', [
            'chatList' => $chatList,
            'savedAds' => $savedAds,
            'myadsCount' => $myadsCount,
            'pending' => $myadspending,
            'myAds' => $data,
            'msg' => $msg,
            'model' => $model
        ]);

    }

    public function actionDelete()
    {
        if (\Yii::$app->user->isGuest) {
            Yii::$app->getSession()->setFlash('error', '<b>Error: </b> Login for access this page');
            return $this->redirect(Url::toRoute('site/login'));
        };

        $id = Yii::$app->user->identity->id;
        $user = User::find()->where(['id' => $id])->one();
        $ads = Ads::find()->where(['user_id' => $id])->all();
        foreach ($ads as $list) {
            Category::removeCounter($list['category']);
            SubCategory::removeCounter($list['sub_category']);
        }
        Ads::deleteAll(['user_id' => $id]);
        SavedAds::deleteAll(['user_id' => $id]);
        Message::deleteAll(['sender' => $id]);
        Message::deleteAll(['receiver' => $id]);

        $user->delete();
        Yii::$app->user->logout();

        return $this->goHome();
    }
}
