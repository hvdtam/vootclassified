<?php

namespace frontend\helpers;

use yii\helpers\Inflector;

class AdsHelper
{

    public static function getTemplateShow($template)
    {
        switch ($template) {
            case '1':
                $list = "show";
                $grid = "show";
                $bar = "hidden";
                break;
            case '2':
                $list = "hidden";
                $grid = "hidden";
                $bar = "hidden";
                break;
            case '3':
                $list = "show";
                $grid = "show";
                $bar = "hidden";
                break;
            case '4':
                $list = "show";
                $grid = "show";
                $bar = "hidden";
                break;
            case '5':
                $list = "show";
                $grid = "show";
                $bar = "hidden";
                break;
            default:
                $list = "show";
                $grid = "show";
                $bar = "show";
                break;
        }

        return [$list, $grid, $bar];
    }
}