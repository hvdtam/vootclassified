<?php

namespace frontend\helpers;

use yii\helpers\Inflector;

class TextHelper
{
    public static function getTextLocate($text, $locate = 'vi')
    {
        $textShow = $text;
        $locate = Yii::$app->language;
        if ($locate == 'vi') $textShow = '';

        return $textShow;
    }

    public static function getCategoryText($text, $locate = 'vi')
    {
        $text = Inflector::slug($text);
        $text = str_replace('-', '_', $text);

        return $text;
    }
}