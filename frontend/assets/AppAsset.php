<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'themes/assets/bootstrap/css/bootstrap.css',
        'themes/assets/css/style.css',
        'themes/assets/plugins/owl-carousel/owl.carousel.css',
        'themes/assets/plugins/owl-carousel/owl.theme.css',
        'themes/assets/plugins/bxslider/jquery.bxslider.css',
        'css/blog/blog.css',
    ];
    public $js = [
        'themes/assets/bootstrap/js/bootstrap.min.js',
        'themes/assets/js/vendors.min.js',
        'themes/assets/js/script.js',

    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
