<?php

namespace frontend\assets;

use common\models\DefaultSetting;
use yii\web\AssetBundle;
use yii\web\View;
$theme = DefaultSetting::find()->select(['themes'])->one();
$theme = 'themes/assets/css/'.$theme['themes'].'.css';

/**
 * Main frontend application asset bundle.
 */
class VootAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'themes/assets/bootstrap/css/bootstrap_old.css',
       // ['themes/assets/css/.css', 'id' => 'themeCss'],
        'themes/assets/plugins/owl-carousel/owl.carousel.css',
        'themes/assets/plugins/owl-carousel/owl.theme.css',
        'themes/assets/plugins/bxslider/jquery.bxslider.css',
        'themes/assets/pe-icon-7-stroke/css/pe-icon-7-stroke.css',
        'themes/assets/pe-icon-7-stroke/css/helper.css',
        'themes/assets/css/item_temp_one.css',
        'themes/assets/css/common.css',
        'css/blog/blog.css',

    ];
//    public $cssOptions = [
//        'id' => 'theme',
//    ];
    public $js = [
        'themes/assets/js/vendors.min.js',
        'themes/assets/js/bootstrap-notify.js',
        'themes/assets/js/custom.js',
        'themes/assets/js/mapSelect.js',
        'themes/assets/js/script.js',
        'themes/assets/js/jquery-popup.js',
        'themes/assets/plugins/bxslider/jquery.bxslider.min.js',


    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
//    public $publishOptions = [
//        'forceCopy'=>true,
//    ];
};

class TopAsset extends AssetBundle {
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $jsOptions = ['position' => View::POS_HEAD];
    public $js = [
        //'themes/assets/js/pace.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
};

class CloudAsset extends AssetBundle {
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $js = [
        'themes/cloud/ajax/libs/popper.js/1.12.3/umd/popper.min.js',
        ['themes/cloud/ajax/libs/popper.js/1.12.3/umd/popper.min.js','integrity'=>'sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh', 'crossorigin' => 'anonymous'],
        'themes/assets/bootstrap/js/bootstrap.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
