<?php
namespace frontend\components;
use common\models\Plugin;
use common\models\PluginLanguage;
use yii\base\BootstrapInterface;
class SetLanguage implements BootstrapInterface
{
    public $supportedLanguages = ['hi-IN'];

    public function bootstrap($app)
    {
        $preferredLanguage = isset($app->request->cookies['i18nlanguage']) ? (string)$app->request->cookies['i18nlanguage'] : Plugin::getLang();
// or in case of database:
        if (empty($preferredLanguage)) {
            $preferredLanguage = $app->request->getPreferredLanguage($this->supportedLanguages);
        }

        $app->language = $preferredLanguage;
    }

}
