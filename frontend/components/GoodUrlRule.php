<?php

namespace frontend\components;

use yii\web\UrlRule;
use yii\web\UrlRuleInterface;
use yii\base\BaseObject;

class GoodUrlRule extends UrlRule implements UrlRuleInterface
{

    public function createUrl($manager, $route, $params)
    {
        if ($route === 'ads/listing') {
            if (isset($params['cat']) && !isset($params['sub_cat'])) {
                $params['cat'] = str_replace(' ', '_', $params['cat']);

                return $params['cat'] . '-in-' . $params['location'];
            }
        }

        return false; // this rule does not apply
    }


    public function parseRequest($manager, $request)
    {
        $pathInfo = $request->getPathInfo();
        if (preg_match('%^(\w+)(/(\w+))?$%', $pathInfo, $matches)) {
            // check $matches[1] and $matches[3] to see
            // if they match a manufacturer and a model in the database.
            // If so, set $params['manufacturer'] and/or $params['model']
            // and return ['car/index', $params]
        }

        return false; // this rule does not apply
    }

}