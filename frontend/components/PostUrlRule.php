<?php
namespace frontend\components;

use common\models\MetaTags;
use common\models\SeoTools;
use yii\web\UrlRule;
use yii\web\UrlRuleInterface;
use yii\base\BaseObject;

class PostUrlRule extends UrlRule implements UrlRuleInterface
{

    public function createUrl($manager, $route, $params)
    {
        $phermaUrl = SeoTools::find()->one();
        //$phermaUrl['pherma_link'] = '{slug}-{id}';
        //$phermaUrl['pherma_extension'] = 'html';
        if($phermaUrl)
        {
            $postUrl = $phermaUrl['pherma_link'];

            if ($route === 'ads/detail') {
                if($postUrl == "{slug}-{id}")
                {

                    return 'item/'.$params['title'].'-IID'.$params['IID'].'.'. $postUrl = $phermaUrl['pherma_extension'];
                }
                elseif($postUrl == "{slug}/{id}")
                {
                    return 'item/'.$params['title'].'/IID'.$params['IID'].'.'. $postUrl = $phermaUrl['pherma_extension'];

                }
                elseif($postUrl == "{slug},{id}")
                {
                    return 'item/'.$params['title'].'/IID'.$params['IID'].'.'. $postUrl = $phermaUrl['pherma_extension'];

                }
                else
                {
                    return 'item/'.$params['title'].'_IID'.$params['IID'].'.'. $postUrl = $phermaUrl['pherma_extension'];

                }
            }
        }
        return false; // this rule does not apply
    }


    public function parseRequest($manager, $request)
    {
        $pathInfo = $request->getPathInfo();
        if (preg_match('%^(\w+)(/(\w+))?$%', $pathInfo, $matches)) {
            // check $matches[1] and $matches[3] to see
            // if they match a manufacturer and a model in the database.
            // If so, set $params['manufacturer'] and/or $params['model']
            // and return ['car/index', $params]
        }
        return false; // this rule does not apply
    }

}