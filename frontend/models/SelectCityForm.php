<?php

namespace frontend\models;

use yii\base\Model;
use common\models\User;
use borales\extensions\phoneInput\PhoneInputValidator;

/**
 * Signup form
 */
class SelectCityForm extends Model
{
    public $country;
    public $state;
    public $city;
    public $select_which;

//    public $reCaptcha;


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [


            ['country', 'required'],
            ['country', 'string', 'min' => 1],

            ['state', 'required'],
            ['state', 'string', 'min' => 1],

            ['city', 'required'],
            ['city', 'string', 'min' => 1],

            ['select_which', 'safe'],
            ['select_which', 'string', 'min' => 1],

        ];
    }


}
