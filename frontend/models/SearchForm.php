<?php

namespace frontend\models;

use common\models\Ads;
use common\models\User;
use yii\base\Model;
use Yii;
use yii\web\UploadedFile;


class SearchForm extends Model
{
    public $city;
    public $category;
    public $item;
    public $type;
    public $filterTitle;
    public $filterValue;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['city', 'safe'],

            ['category', 'safe'],
            ['type', 'safe'],
            ['filterTitle', 'safe'],
            ['filterValue', 'safe'],

            ['item', 'required'],

        ];
    }

    public function searchHome()
    {
        return false;
    }

    public function searchSideBar()
    {
        return false;
    }

}
