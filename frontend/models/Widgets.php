<?php

namespace frontend\models;

use common\models\Category;
use common\models\Plugin;
use common\models\SubCategory;
use Faker\Factory;
use Yii;
use yii\base\Model;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * ContactForm is the model behind the contact form.
 */
//define('CATEGORY_IMG_URL',Yii::getAlias('@web').'/themes/images/category');
class Widgets extends Model
{
    public $temp;
    public $page;
    public $plugin_type;

    // public $display;

    public function dem()
    {
        $model = \common\models\Widgets::find(['page' => $this->page])->orderBy(['priority' => SORT_ASC])->all();

    }

    public static function content($page, $data, $position)
    {
        $model = \common\models\Widgets::find()->where(['page' => $page])->andWhere(['position' => $position])->andWhere(['status' => 'enable'])->orderBy(['priority' => SORT_ASC])->all();
        foreach ($model as $widget) {

            $template = $widget['template'];
            $type = $widget['type'];
            $limit = ($widget['number_of_item'] == "0") ? false : $widget['number_of_item'];
            if ($type == "category") {
                $get = TemplatesDesign::homeCategoryLoop($data, $template);
            } elseif ($type == "item_list") {
                $get = TemplatesDesign::ItemList($data, $template);//pending
            } elseif ($type == "featured") {
                $get = TemplatesDesign::featured();//pending
            } elseif ($type == "latest") {
                $get = TemplatesDesign::lat();//pending
            } elseif ($type == "others") {

                $get = TemplatesDesign::PopularCategory($template);//pending
            } else {
                echo "";
            }
            echo $get;

        }
    }


    public static function full_top_panel($page, $data)
    {
        $position = "full_top_panel";
        static::content($page, $data, $position);
    }

    public static function mid_right_panel($page, $data)
    {
        $position = "right_panel";
        echo static::content($page, $data, $position);
    }

    public static function mid_left_panel($page, $data)
    {
        $position = "left_panel";
        echo static::content($page, $data, $position);
    }

    public static function mid_panel($page, $data)
    {
        ?>
        <div class="row">
            <div class="col-md-9 page-content col-thin-right">
                <?php
                $position = "right_panel";
                echo static::content($page, $data, $position);
                ?>
            </div>
            <div class="col-md-3 page-sidebar col-thin-left">
                <aside>
                    <?php
                    $position = "left_panel";
                    echo static::content($page, $data, $position);
                    ?>
                </aside>
            </div>
        </div>
        <?php
    }

    public static function full_bottom_panel($page, $data)
    {
        $position = "full_bottom_panel";
        static::content($page, $data, $position);
    }

    public static function listing($all, $business, $personal)
    {
        $position = "item_listing";
        static::content($page, $data, $position);
        ?>
        <div class="tab-content">
            <div class="tab-pane active fade-in " id="all">
                <?= \frontend\models\TemplatesDesign::content($model, 3) ?>
            </div>
            <div class="tab-pane fade-in" id="business">
                <?= \frontend\models\TemplatesDesign::ItemList($model, 1) ?>
            </div>
            <div class="tab-pane fade-in" id="personal">
                <?= \frontend\models\TemplatesDesign::ItemList($model, 4) ?>
            </div>
        </div>
        <?php
    }

    public static function layout($page, $data)
    {
        static::full_top_panel($page, $data);
        static::mid_panel($page, $data);
        static::full_bottom_panel($page, $data);
    }

    public function plugin()
    {
        if ($this->plugin_type == "payment") {
            $plug = Plugin::find()->where(['plugin_type' => 'payment'])->andWhere(['status' => '1'])->all();
            foreach ($plug as $method) {
                ?>
                <option value="<?= $method['plugin_template'] ?>"><?= $method['plugin_name'] ?></option>';
                <?php
            }
        }
    }
}
