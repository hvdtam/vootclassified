<?php

namespace frontend\models;

use common\models\Ads;
use common\models\AdsSettings;
use common\models\Category;
use common\models\SubCategory;
use common\models\User;
use yii\base\Model;
use Yii;
use yii\web\UploadedFile;

define('WATERMARK_PATH', Yii::getAlias('@webroot') . '/images/site/watermark/');
$adsSetting = AdsSettings::findOne(1);
define('MAX_FILE', $adsSetting['number_of_photo']);
define('MAX_TAGS', $adsSetting['number_of_tags']);
define('AUTO_APPROVE', $adsSetting['ads_approval']);


class AdsForm extends Model
{
    public $user_id;
    public $category;
    public $sub_category;
    public $type;
    public $ad_type;
    public $ad_title;
    public $ad_description;
    public $image;
    public $name;
    public $states;
    public $premium;
    public $tags;
    public $city;
    public $country;
    public $mobile;
    public $email;
    public $currency_symbol;
    public $method;
    public $price;
    public $address;
    public $longitude;
    public $latitude;
    public $more;
//AIzaSyBu-916DdpKAjTmJNIgngS6HL_kDIKU0aU

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],

            ['category', 'required'],
            ['category', 'string', 'max' => 255],

            ['sub_category', 'safe'],

            ['sub_category', 'string', 'max' => 255],

            [['price', 'currency_symbol', 'more'], 'safe'],
            ['type', 'safe'],
            ['country', 'safe'],
            ['country', 'string'],
            ['states', 'required'],
            ['city', 'required'],
            ['ad_title', 'required'],
            ['ad_title', 'string', 'max' => 255],

            ['ad_description', 'required'],
            ['ad_description', 'string', 'max' => 500],


//            [['image'], 'image','extensions' => 'png, jpg, gif','minWidth' => 300, 'maxWidth' => 300,
//                'minHeight' => 800, 'maxHeight' =>800,'message'=>'min image size 300x300 pixel'],
//
            [['image'], 'image', 'skipOnEmpty' => false, 'extensions' => ['jpg', 'jpeg', 'png'], 'minWidth' => 300, 'maxWidth' => 1000, 'minHeight' => 300, 'maxHeight' => 1000, 'maxFiles' => MAX_FILE],


            ['name', 'required'],
            ['name', 'string', 'max' => 55],

            ['mobile', 'required'],
            ['mobile', 'string', 'max' => 255],
            ['premium', 'required'],
            ['premium', 'string', 'max' => 255],
            ['tags', 'required'],
            ['tags', 'string'],

            ['address', 'safe'],
            ['address', 'string', 'max' => 500],
            ['longitude', 'safe'],
            ['longitude', 'string'],
            ['latitude', 'safe'],
            ['latitude', 'string'],
            ['method', 'safe'],


            ['ad_type', 'safe'],
            ['ad_type', 'string'],


        ];
    }

    public function attributeLabels()
    {
        return [
            'email' => Yii::t('app', 'Email'),
            'category' => Yii::t('app', 'Category'),
            'sub_category' => Yii::t('app', 'Sub Category'),
            'price' => Yii::t('app', 'Price'),
            'type' => Yii::t('app', 'For'),
            'ad_title' => Yii::t('app', 'Ad Title'),
            'ad_description' => Yii::t('app', 'Ad Description'),
            'image' => Yii::t('app', 'Image'),
            'name' => Yii::t('app', 'Name'),
            'mobile' => Yii::t('app', 'Mobile Number'),
            'tags' => Yii::t('app', 'Tags Your Ad'),


        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function post($image, $approve = false, $adsSetting)
    {
        $uid = Yii::$app->user->id;
        $ads = new Ads();
        if ($adsSetting['login_required'] == 0) {
            $ads->user_id = $uid;
        } else {
            $ads->user_id = null;
            $ads->guest = 1;
        }

        $ads->category = $this->category;
        $ads->sub_category = $this->sub_category;
        $ads->ad_title = $this->ad_title;
        $ads->ad_description = $this->ad_description;

        //screenshot upload
        $ads->image = $image;

        $ads->lat = $this->latitude;
        $ads->lng = $this->longitude;
        $ads->country = $this->country;
        $ads->states = $this->states;
        $ads->city = $this->city;
        $ads->tags = $this->tags;

        $ads->address = $this->address;
        $ads->active = AUTO_APPROVE;
        if (empty($this->price)) {
            $ads->price = "0";
            $ads->currency_symbol = $this->currency_symbol;

        } else {
            $ads->price = $this->price;
            $ads->currency_symbol = $this->currency_symbol;

        };

        //
        $ads->type = $this->type;
        $ads->ad_type = $this->ad_type;
        if ($ads->save(false)) {
            Category::addCounter($this->category);
            SubCategory::addCounter($this->sub_category);
            if ($adsSetting['login_required'] == 0) {
                return $ads->id;
            } else {
//                Yii::$app->mailer->compose('adSubmission',['model'=>$ads])
//                    ->setTo($ads->email)
//                    ->setFrom([Yii::$app->params['adminEmail'] => Yii::$app->name] )
//                    ->setSubject('Your ad is under review.')
//                    ->send();
//                Yii::$app->mailer->compose('adApprove',['model'=>$ads])
//                    ->setTo($ads->email)
//                    ->setFrom([Yii::$app->params['adminEmail'] => Yii::$app->name] )
//                    ->setSubject('Your ad is approve. Thanks for submission')
//                    ->send();
                return $ads->id;

            }


        } else {
            return false;
        }

    }

    //screenshot upload funtion
    public function ScreenShot()
    {
        foreach ($this->image as $file) {
            $name = rand(137, 999) . time();
            $screen[] = $name . '.' . $file->extension;
            $file->saveAs(SCREENSHOT . $name . '.' . $file->extension);
            self::watermark(SCREENSHOT . $name . '.' . $file->extension);
        }
        return $ScreenChunk = implode(",", $screen);

    }

    public function watermark($image)
    {
        //================================================
        //============== image watermark code ============
        //================================================
        $adsSettings = AdsSettings::findOne(1);
        $active = ($adsSettings['watermark_status'] == "enable") ? true : false;
        if ($active) {

            $stamp = imagecreatefrompng(WATERMARK_PATH . $adsSettings['watermark_image']);
            $path = $image;
            switch (mime_content_type($path)) {
                case 'image/png':
                    $img = imagecreatefrompng($path);
                    break;
                case 'image/gif':
                    $img = imagecreatefromgif($path);
                    break;
                case 'image/jpeg':
                    $img = imagecreatefromjpeg($path);
                    break;
                case 'image/bmp':
                    $img = imagecreatefrombmp($path);
                    break;
                default:
                    $img = null;
            }
            $im = $img;

// Set the margins for the stamp and get the height/width of the stamp image
            $marge_right = 10;
            $marge_bottom = 10;
            $sx = imagesx($stamp);
            $sy = imagesy($stamp);

// Copy the stamp image onto our photo using the margin offsets and the photo
// width to calculate positioning of the stamp.
            imagecopy($im, $stamp, imagesx($im) - $sx - $marge_right, imagesy($im) - $sy - $marge_bottom, 0, 0, imagesx($stamp), imagesy($stamp));

// Output and free memory
            //  header('Content-type: image/png');
            imagepng($im, $image);
            imagedestroy($im);
            return true;
        } else {
            return true;
        }

    }
}
