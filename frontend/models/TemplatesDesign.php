<?php

namespace frontend\models;

use common\models\Ads;
use common\models\AdsPremium;
use common\models\Category;
use common\models\DefaultSetting;
use common\models\Reviews;
use common\models\SubCategory;
use common\models\User;
use Faker\Factory;
use frontend\helpers\TextHelper;
use Yii;
use yii\base\Model;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * ContactForm is the model behind the contact form.
 */
define('CATEGORY_IMG_URL', Yii::getAlias('@web') . '/themes/images/category');
$default = \common\models\DefaultSetting::getDefaultSetting();
define('D_CITY', $default['CurrCity']);
define('D_STATE', $default['state']);
define('D_COUNTRY', $default['country']);


class TemplatesDesign extends Model
{

    public $temp;
    public $detailUrl;
    public $lazy;

    public static function homeCategoryLoop($data, $type = false)
    {
        ?>
        <div class="col-xl-12 content-box">
            <div class="row row-featured row-featured-category">
                <div class="col-xl-12  box-title no-border">
                    <div class="inner">
                        <h2>
                            <span> <?= Yii::t('home', 'Browse by'); ?></span>
                            <?= Yii::t('home', 'Categories'); ?>
                            <a href="<?= Url::toRoute('ads/listing') ?>" class="sell-your-item">
                                <?= Yii::t('home', 'View more'); ?>
                                <i class="  icon-th-list"></i>
                            </a>
                        </h2>
                    </div>
                </div>

                <?php
                foreach ($data as $list) {
                    //$url = Url::toRoute('ads/listing/').'?category='.$list['name'];
                    //$list['name'] ->  Việc_Làm -> viec_lam
                    $url = \yii\helpers\Url::to(['ads/listing', 'cat' => TextHelper::getCategoryText($list['name']), 'location' => TextHelper::getCategoryText(D_CITY)]);
                    $icon_padding = "22px";
                    $icon_font = "55px";
                    $icon_font_color = "#555";
                    //Todo get name vi
                    if ($type == 'icon') {
                        ?>
                        <div class="col-xl-2 col-md-3 col-sm-3 col-xs-4 f-category">
                            <a href="<?= $url; ?>">
                                <img src="<?= Yii::getAlias('@web/images/icon') ?>/<?= $list['fa_icon'] ?>">
                                <h6> <?php echo $list['name'] ?>  </h6>
                            </a>
                        </div>
                        <?php
                    } elseif ($type == 'image') {
                        ?>
                        <div class="col-xl-2 col-md-3 col-sm-3 col-xs-4 f-category">
                            <a href="<?= $url; ?>">
                                <i class="<?= $list['fa_icon'] ?>"
                                   style="padding: <?= $icon_padding ?>;font-size: <?= $icon_font ?>;color:<?= $icon_font_color; ?>"></i>
                                <!--                    <img src="/car-2.jpg" class="img-responsive" alt="img">-->
                                <h6> <?= Yii::t('category', $list['name']); ?> </h6>
                            </a>
                        </div>
                        <?php
                    } elseif ($type == 'withSub') {
                        ?>
                        <div class="col-md-4 col-sm-4 ">
                            <div class="cat-list">

                                <h3 class="cat-title">
                                    <a href="<?= $url ?>" class="pl-0">
                                        <img class="img-fluid" width="30"
                                             src="<?= Yii::getAlias('@web/images/icon') ?>/<?= $list['fa_icon'] ?>"
                                             alt="">

                                        <?php
                                        //echo Yii::t('category', $list['name']);
                                        echo $list['name'];
                                        ?>
                                        <span class="count"> <?= ($list['item']) ? $list['item'] : "Zero" ?> <?= Yii::t('home', 'Ads') ?>. </span>
                                    </a>
                                    <span data-target=".cat-id-1" aria-expanded="true" data-toggle="collapse"
                                          class="btn-cat-collapsed collapsed">
                                <span class="icon-down-open-big"></span>
                            </span>
                                </h3>
                                <ul id="cat-id-<?= $list['id'] ?>" class="cat-collapse">
                                    <?php
                                    static::SubCategoryFetchList($list['id'], 'ForHomeCategory', $list['name'])
                                    ?>

                                </ul>
                                <small class="btn_more" style="color:#777;cursor: pointer"
                                       id="loadMore_<?= $list['id'] ?>">
                                    <b>
                                        <?= Yii::t('home', 'Load More'); ?> <i class="fa fa-angle-double-down"></i>
                                    </b>
                                </small>

                                <small style="color:#dd1d0b;cursor: pointer;display: none"
                                       id="showLess_<?= $list['id'] ?>">
                                    <b>
                                        <?= Yii::t('home', 'Show Less') ?> <i class="fa fa-angle-double-up"></i>
                                    </b>
                                </small>
                                <script type="application/javascript">
                                    $(document).ready(function () {
                                        var listSize = $("ul#cat-id-<?= $list['id'];?> li").length;
                                        if (listSize < "4") {
                                            $("small#loadMore_<?= $list['id'];?>").css('display', 'none');
                                            // $("small#loadMore_<?= $list['id'];?>").text(listSize);
                                            Z
                                        }
                                        ;
                                        $("ul#cat-id-<?= $list['id'];?>").children('li:gt(3)').hide();
                                        $("small#loadMore_<?= $list['id'];?>").click(function () {
                                            $("ul#cat-id-<?= $list['id'];?>").children().show(500);
                                            $("small#loadMore_<?= $list['id'];?>").css('display', 'none');
                                            $("small#showLess_<?= $list['id'];?>").css('display', 'block');

                                        });
                                        $("small#showLess_<?= $list['id'];?>").click(function () {
                                            $("ul#cat-id-<?= $list['id'];?>").children('li:gt(2)').hide(500);
                                            $("small#loadMore_<?= $list['id'];?>").css('display', 'block');
                                            $("small#showLess_<?= $list['id'];?>").css('display', 'none');

                                        });
                                    });
                                </script>
                            </div>

                        </div>
                        <?php
                    } else {
                        return null;
                    }
                }

                ?>
            </div>
        </div>
        <?php
    }

    public static function PostFormCategoryLoop($data, $type = false)
    {
        foreach ($data as $list) {
            $url = Url::toRoute('category/index');
            $icon_padding = "0px";
            $icon_font = "41px";
            $icon_font_color = "#f44";

            ?>
            <div class="col-3" align="center">
                <div class="card bg-transparent shadow-none mt-3">
                    <div class="card-header text-dark">
                        <strong><?= $list['name'] ?></strong>
                    </div>
                    <div class="card-body">
                        <h5 class="card-title light-text-1 mt-2">
                            <i class="<?= $list['fa_icon'] ?> fa-3x"></i>
                        </h5>
                        <p class="text-secondary">

                        </p>
                        <a href="javascript:void(0)" class="postFormCat_list btn btn-outline-success btn-md card-link"
                           data-postForm-cat="<?= $list['id'] ?>" data-postForm-cat-name="<?= $list['name'] ?>"
                           data-postForm-cat-icon="<?= $list['fa_icon'] ?>">
                            <b>
                                Select
                            </b>
                        </a>
                    </div>
                </div>
            </div>
            <?php
        }
    }


    public static function SubCategoryFetchList($cat_id, $type = false, $parent = false)
    {
        $data = SubCategory::find()->where(['parent' => $cat_id])->all();
        // $total = SubCategory::find()->where(['parent'=>$cat_id])->count();
        foreach ($data as $key => $SubCatList) {

            $url = \yii\helpers\Url::to(['ads/listing', 'cat' => TextHelper::getCategoryText($parent), 'sub_cat' => TextHelper::getCategoryText($SubCatList['name']), 'location' => TextHelper::getCategoryText(D_CITY)]);

            //$url = Url::toRoute('ads/listing')."?category=".$parent."&sub_category=".$SubCatList['name'];
            $list_padding = "2px";
            $list_font = "12px";
            $list_font_color = "#555";

            if ($type == 'ForHomeCategory') {
                ?>
                <li>
                    <a href="<?= $url; ?>"
                       style="padding: <?= $list_padding ?>;font-size: <?= $list_font ?>;color:<?= $list_font_color; ?>">
                        <?php
                        echo $SubCatList['name'];
                        ?>

                        <sup title=" <?= $SubCatList['item'] ?> Items "> ( <?= $SubCatList['item'] ?> )</sup>
                    </a>
                </li>
                <?php

            } else {
                return null;
            }


        }
    }

    public static function HomeCategoryMenu($data)
    {
        $set = 0;
        if ($set == 0) {
            ?>
            <div class="dropdown position-relative form-control locinput input-rel searchtag-input has-icon category-dropdown">
                <a data-toggle="dropdown" href="#" data-flip="false" data-boundary="window" aria-expanded="true">
                    <i class="icon-menu "></i>
                    <span class="change-text">
                     <?= Yii::t('home', 'Select a Category') ?>

                </span>
                </a>
                <ul class="dropdown-menu category-change" id="category-change" style="left: -16px!important;">

                    <li>
                        <a href="#" class="formCatSelect" data-search-type="cat" data-cat="All">
                            <i class="pe-7s-global"></i>
                            <?= Yii::t('home', 'All Categories') ?>
                        </a>
                    </li>
                    <?php
                    foreach ($data as $list) {
                        $url = Url::toRoute('listing/index');
                        $data = SubCategory::find()->where(['parent' => $list['id']])->all();

                        ?>
                        <li>
                            <a href="javascript:void()" class="formCatSelect" data-search-type="cat"
                               data-cat="<?= $list['name']; ?>">
                                <i class="<?= $list['fa_icon'] ?>"></i>
                                <?= Yii::t('category', $list['name']) ?>

                            </a>
                            <ul>
                                <?php
                                foreach ($data as $SubCatList) {
                                    $url = Url::toRoute('category/subcategory');
                                    ?>
                                    <li>
                                        <a href="javascript:void" class="formCatSelect" data-search-type="subCat"
                                           data-cat="<?= $list['name']; ?>" data-sub-cat="<?= $SubCatList['name']; ?>">
                                            <?= Yii::t('sub_category', $SubCatList['name']); ?>
                                        </a>
                                    </li>
                                    <?php
                                }
                                ?>

                            </ul>
                        </li>
                        <?php
                    }
                    ?>
                </ul>
            </div>
            <?php
        } else {
            ?>
            <div class="btn-group">
                <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                        aria-expanded="false">
                    Action
                </button>
                <div class="dropdown-menu ">
                    <a class="dropdown-item" href="#">Action</a>
                    <a class="dropdown-item" href="#">Another action</a>
                    <a class="dropdown-item" href="#">Something else here</a><a class="dropdown-item" href="#">Something
                        else here</a><a class="dropdown-item" href="#">Something else here</a><a class="dropdown-item"
                                                                                                 href="#">Something else
                        here</a><a class="dropdown-item" href="#">Something else here</a><a class="dropdown-item"
                                                                                            href="#">Something else
                        here</a><a class="dropdown-item" href="#">Something else here</a><a class="dropdown-item"
                                                                                            href="#">Something else
                        here</a><a class="dropdown-item" href="#">Something else here</a><a class="dropdown-item"
                                                                                            href="#">Something else
                        here</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#">Separated link</a>
                </div>
            </div>
            <?php
        }
    }

    public static function ItemList($data, $temp = false)
    {
        $default = DefaultSetting::find()->select(['lazy_load'])->one();
        $default = $default['lazy_load'];
        $widgets = new \common\models\Plugin();
        $widgets->for = "7856";
        /**
         * listing loop start here
         * */
        echo "<div class='row'>";
        if (!$data) {
            $imgURl = Yii::getAlias('@web') . '/images/site/rocket.png';
            $postUrl = Url::toRoute('ads/post');
            $title = Yii::t('app', 'Didn\'t find anything');
            echo $tmp = '<div class="col-12"><div class="card"><div class="card-body" align="center"><img class="mt-5 mb-4" src="' . $imgURl . '"> <h1 class="card-title">' . $title . '</h1><p class="card-text">' . Yii::t("app", "Please Search with different keywords, and you can post an ads in this category") . '</p><a href="' . $postUrl . '" class="btn btn-success mb-3 mt-3">' . Yii::t("app", "POST FREE AD") . '</a> </div></div></div>';

        }
        foreach ($data as $list) {
            $imagebase = $list['image'];
            $imageArray = explode(",", $imagebase);
            $imgs = reset($imageArray);
            $img = ($list['category'] == 'Jobs') ? 'VootJobs.jpg' : $imgs;;
            $controlHide = ($list['category'] == 'Jobs') ? 'hidden' : '';
            $controlShow = ($list['category'] == 'Jobs') ? '' : 'hidden';
            $widgets->data = $list;
            // $faker = Factory::create();
            $labelClass = $list['premium'];//$faker->randomElement(['topAds','featuredAds','urgentAds']);

            //$detailUrlc =  \yii\helpers\Url::toRoute('item/'.$list['ad_title'].'/'.$list['id']); // URl of detail page
            $filtrUrlFind = array('/\,/', '/\s+/', '/\?+/', '/\'/');
            $filtrUrlReplace = array('_', '_', '_');

            $detailUrl = \yii\helpers\Url::to([
                'ads/detail',
                'title' => trim(preg_replace($filtrUrlFind, $filtrUrlReplace, $list['ad_title'])),
                'IID' => $list['id']
            ]);
            //$detailUrl = preg_replace('/\s+/','-',$detailUrlc);
            $ImageUrl = Yii::getAlias('@web') . '/images/item/' . $img; // Image Directory URL
            $data_del = \yii\helpers\Url::toRoute('saved/remove-ads/' . base64_encode($list['id'])); // SAVE ADS URL
            $data_ref = \yii\helpers\Url::toRoute('saved/ads/' . base64_encode($list['id'])); // REMOVE AD FROM SAVE LIST URL
            $imgAlt = Yii::t('app', 'Item Image');
            $imgTitle = "";

            if ($default == 'off') {
                $lazy = "";
                $onError = Yii::getAlias('@web') . "/images/item/itemLight.jpg";
                $dataSrc = $ImageUrl;
                $dataSrcset = $ImageUrl;
                $src = $ImageUrl;
            } else {
                $lazy = "lazy";
                $onError = Yii::getAlias('@web') . "/images/item/itemLight.jpg";
                $dataSrc = $ImageUrl;
                $dataSrcset = $ImageUrl;
                $src = $onError;
            }
            if ($temp == false or $temp == 0) {
                $hidd = ($labelClass == false) ? 'hidden' : 'show';
                ?>
                <div style="" class="item-list make-list">

                    <div class="cornerRibbons <?= $labelClass . ' ' . $hidd; ?>">
                        <a href="#"> <?= $labelClass; ?></a>
                    </div>
                    <div class="row">
                        <div class="col-md-2 no-padding photobox">
                            <div class="add-image">
                                <span class="photo-count">
                                    <i class="fa fa-camera"></i>
                                    <?= count($imageArray) ?>
                                </span>
                                <a href="<?= $detailUrl ?>">
                                    <img class="thumbnail no-margin <?= $lazy; ?>" data-src="<?= $dataSrc ?>"
                                         src="<?= $src; ?>" data-srcset="<?= $dataSrcset ?>" alt="<?= $imgAlt ?>"
                                         title="<?= $imgTitle ?>">
                                </a>
                            </div>
                        </div>
                        <!--/.photobox-->
                        <div class="col-sm-7 add-desc-box col-md-7">
                            <div class="ads-details">
                                <h5 class="add-title">
                                    <a href="<?= $detailUrl; ?>">
                                        <?= $list['ad_title']; ?>
                                    </a>
                                </h5>
                                <h6 class="card-subtitle text-muted">
                                    <?php $widgets->get(); ?>
                                </h6>
                                <span class="info-row">
                                    <span class="add-type business-ads tooltipHere" data-toggle="tooltip"
                                          data-placement="right" title="<?= $list['ad_type']; ?> Ads">
                                        <?= substr($list['ad_type'], 0, 1) ?>
                                    </span>
                                    <span class="date">
                                        <i class=" icon-clock"> </i>
                                        <?php echo Yii::$app->formatter->asDate($list['created_at']) ?>
                                    </span>
                                    -
                                    <span class="category"><?= Yii::t('sub_category', $list['sub_category']) ?> </span>
                                    -
                                    <span class="item-location">
                                        <i class="fa fa-map-marker"></i>
                                        <?= Yii::t('app', $list['city']); ?>
                                    </span>
                                </span>
                            </div>
                        </div>
                        <!--/.add-desc-box-->
                        <div class="col-md-3 text-right  price-box">
                            <div class="item-price">
                                <?= $list['currency_symbol']; ?> <?= $list['price'] ?>
                            </div>
                            <a class="btn btn-success  btn-sm make-favorite">
                                <i class="fa fa-certificate"></i>
                                <span><?= $labelClass; ?> </span>
                            </a>
                            <a id="saveAdsBtn<?= $list['id'] ?>" class="btn btn-default saveAdsBtn btn-sm make-favorite"
                               ads="<?= $list['id'] ?>" data-del="<?= $data_del; ?>" data-ref="<?= $data_ref ?>">
                                <i id="dil<?= $list['id'] ?>" class="fa fa-heart"></i>
                                <span id="saved<?= $list['id'] ?>"><?= Yii::t('app', 'Save') ?></span>
                            </a>

                        </div>
                        <!--/.add-desc-box-->

                    </div>

                </div>
                <?php
            } elseif ($temp == '1') {
                ?>
                <div class="temp1Wrap item-list make-list">
                    <div class="tmp_one_grid">
                        <article class="card-wrapper">
                            <div class="image-holder">
                                <a href="<?= $detailUrl; ?>" class="image-holder__link"></a>
                                <div class="image-liquid image-holder--original">
                                    <img class="img-fluid no-margin <?= $lazy; ?>" data-src="<?= $dataSrc ?>"
                                         src="<?= $src; ?>" data-srcset="<?= $dataSrcset ?>" alt="<?= $imgAlt ?>"
                                         title="<?= $imgTitle ?>">

                                </div>
                            </div>

                            <div class="product-description">
                                <!-- title -->
                                <h1 class="product-description__title">
                                    <a href="<?= $detailUrl; ?>">
                                        <?= $list['ad_title']; ?>
                                    </a>
                                </h1>
                                <h6 class="card-subtitle text-muted">
                                    <?php $widgets->get(); ?>
                                </h6>
                                <!-- category and price -->
                                <div class="row">
                                    <div class="col-xs-12 col-sm-8 product-description__category secondary-text">
                                        <i class="fa fa-clock-o"></i>
                                        <?= \common\models\Analytic::time_elapsed_string($list['created_at']) ?>
                                        <?= Yii::t('app', 'Ago'); ?>
                                    </div>
                                    <div style="cursor: pointer" class="col-xs-12 col-sm-4 product-description__price"
                                         data-toggle="tooltip" data-placement="top"
                                         title="Price is <?= $list['currency_symbol']; ?> <?= $list['price'] ?>/-">
                                        <?= \common\models\Ads::price($list['price']); ?>

                                    </div>
                                </div>
                                <!-- divider -->
                                <hr/>
                                <!-- sizes -->
                                <div class="sizes-wrapper">

                                <span class="secondary-text text-uppercase">
                                    <ul class="list-inline">
                                        <li>
                                            <i style="font-size: 15px; border-radius: 50px; padding: 4px; color: rgb(255, 255, 255); background-color: rgb(153, 153, 153);"
                                               class="<?= Category::IconByName($list['category']) ?>"
                                               title="<?= $list['category'] ?>"></i>
                                        </li>
                                        <li><span class="fa fa-angle-right"></span></li>
                                        <li><?= Yii::t('sub_category', $list['sub_category']) ?></li>
                                        <li><span class="fa fa-angle-right"></span></li>
                                        <li><?= Yii::t('type', $list['type']) ?></li>

                                    </ul>

                                </span>
                                </div>
                                <!-- colors -->
                                <div class="color-wrapper">
                                    <div class="btn-group btn-group-justified">
                                        <div class="btn-group">
                                            <button class="btn btn-md btn btn ">
                                                <i class="fa fa-certificate"></i>
                                                <?= $labelClass; ?>
                                            </button>
                                        </div>
                                        <div class="btn-group">
                                            <button class="btn saveAdsBtn btn-md btn-success"
                                                    data-del="<?= $data_del; ?>" data-ref="<?= $data_ref ?>"
                                                    ads="<?= $list['id'] ?>">
                                                <i id="dil<?= $list['id'] ?>" class="fa fa-heart"></i>
                                                <span id="saved<?= $list['id'] ?>"><?= Yii::t('app', 'Save'); ?></span>
                                            </button>
                                        </div>
                                    </div>

                                </div>
                            </div>

                        </article>
                    </div>
                    <div class="row tmp_one_list">
                        <div class="col-lg-3">
                            <a href="<?= $detailUrl; ?>"></a>
                            <img class="img-responsive <?= $lazy; ?>" data-src="<?= $dataSrc ?>" src="<?= $src; ?>"
                                 data-srcset="<?= $dataSrcset ?>" alt="<?= $imgAlt ?>" title="<?= $imgTitle ?>">

                        </div>
                        <div class="col-lg-5">
                            <br>
                            <h4 style="font-family: proxima-semiBold">
                                <a href="<?= $detailUrl; ?>">
                                    <?= $list['ad_title']; ?>
                                </a>
                            </h4>

                            <ul class="list-inline" style="font-family: proxima-semiBold;color: #777">
                                <li>
                                    <i style="font-size: 15px; border-radius: 50px; padding: 4px; color: rgb(255, 255, 255); background-color: rgb(153, 153, 153);"
                                       class="<?= Category::IconByName($list['category']) ?>"
                                       title="<?= $list['category'] ?>"></i>
                                </li>
                                <li><span class="fa fa-angle-right"></span></li>
                                <li><?= Yii::t('sub_category', $list['sub_category']) ?></li>
                                <li><span class="fa fa-angle-right"></span></li>
                                <li><?= Yii::t('type', $list['type']) ?> </li>
                                <li>
                                    <?= Yii::t('app', 'in') ?> <i class="fa fa-map-marker"></i> <?= $list['city'] ?>
                                </li>
                            </ul>
                        </div>
                        <div class="col-lg-4 " align="right">
                            <br>
                            <h3>
                                <?= $list['currency_symbol'] ?> <?= $list['price'] ?>/-
                            </h3>
                            <div>
                                <button class="btn btn-md btn btn  ">
                                    <i class="fa fa-certificate"></i>
                                    <?= $labelClass; ?>
                                </button>
                                <button class="btn btn saveAdsBtn btn-md btn-success" data-del="<?= $data_del; ?>"
                                        data-ref="<?= $data_ref ?>" ads="<?= $list['id'] ?>">
                                    <i id="dil<?= $list['id'] ?>" class="fa fa-heart"></i>
                                    <span id="saved<?= $list['id'] ?>"><?= Yii::t('app', 'Save'); ?></span>
                                </button>
                            </div>
                            <small style="font-family: proxima-semiBold;color: #777">
                                <i class="fa fa-clock-o"></i>
                                <?= \common\models\Analytic::time_elapsed_string($list['created_at']) ?>
                                <?= Yii::t('app', 'Ago'); ?>
                            </small>
                        </div>

                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="col-lg-4 hidden">
                    <article class="card-wrapper">
                        <div class="image-holder">
                            <a href="<?= $detailUrl; ?>" class="image-holder__link"></a>
                            <div class="image-liquid image-holder--original"
                                 style="background-image: url('<?= $ImageUrl ?>')">
                            </div>
                        </div>

                        <div class="product-description">
                            <!-- title -->
                            <h1 class="product-description__title">
                                <a href="<?= $detailUrl; ?>">
                                    <?= Yii::t('app', $list['ad_title']); ?>
                                </a>
                            </h1>
                            <!-- category and price -->
                            <div class="row">
                                <div class="col-xs-12 col-sm-8 product-description__category secondary-text">
                                    <?= Yii::t('type', $list['type']) ?>
                                    <?= Yii::t('sub_category', $list['sub_category']) ?>
                                    in <?= Yii::t('app', $list['city']) ?>
                                </div>
                                <div class="col-xs-12 col-sm-4 product-description__price">
                                    <?= $list['currency_symbol']; ?> <?= $list['price'] ?>
                                </div>
                            </div>
                            <!-- divider -->
                            <hr/>
                            <!-- sizes -->
                            <div class="sizes-wrapper">

                                <span class="secondary-text text-uppercase">
                                    <ul class="list-inline">
                                        <li>
                                            <i style="font-size: 15px; border-radius: 50px; padding: 4px; color: rgb(255, 255, 255); background-color: rgb(153, 153, 153);"
                                               class="<?= Category::IconByName($list['category']) ?>"
                                               title="<?= $list['category'] ?>"></i>
                                        </li>
                                        <li><span class="fa fa-angle-right"></span></li>
                                        <li><?= Yii::t('sub_category', $list['sub_category']) ?></li>
                                        <li><span class="fa fa-angle-right"></span></li>
                                        <li><?= Yii::t('type', $list['type']) ?></li>
                                    </ul>
                                </span>
                            </div>
                            <!-- colors -->
                            <div class="color-wrapper">

                                <div class="btn-group-justified">
                                    <button class="btn-group btn-md btn btn-success ">
                                        <i class="fa fa-certificate"></i>
                                        <?= $labelClass; ?>
                                    </button>
                                    <button class="btn-group btn saveAdsBtn btn-md btn-info"
                                            data-del="<?= $data_del; ?>" data-ref="<?= $data_ref ?>"
                                            ads="<?= $list['id'] ?>">
                                        <i id="dil<?= $list['id'] ?>" class="fa fa-heart"></i>
                                        <span id="saved<?= $list['id'] ?>"><?= Yii::t('app', 'Save'); ?></span>
                                    </button>

                                </div>


                                <ul class="list-inline hidden">
                                    <li class="color-list__item color-list__item--red fa fa-user"></li>
                                    <li class="color-list__item color-list__item--blue"></li>
                                    <li class="color-list__item color-list__item--green"></li>
                                    <li class="color-list__item color-list__item--orange"></li>
                                    <li class="color-list__item color-list__item--purple"></li>
                                </ul>
                            </div>
                        </div>

                    </article>
                </div>
                <?php
            } elseif ($temp == '2') {
                ?>

                <div class="col-md-4">

                    <div class="card">

                        <div class="card-image">
                            <img class="img-responsive <?= $lazy; ?>" data-src="<?= $dataSrc ?>" src="<?= $src; ?>"
                                 data-srcset="<?= $dataSrcset ?>" alt="<?= $imgAlt ?>" title="<?= $imgTitle ?>">
                        </div>
                        <div class="card-body">
                            <small style="color:#777">
                                <b>
                                    <i class="fa fa-clock-o"></i>
                                    <?= \common\models\Analytic::time_elapsed_string($list['created_at']) ?>
                                    <?= Yii::t('app', 'Ago'); ?>,
                                    <i class="fa fa-eye"></i>
                                    <?= $list['view'] ?>
                                    <?= Yii::t('app', 'views'); ?>,
                                    <i class="fa fa-map-marker"></i>
                                    <?= Yii::t('app', $list['city']) ?>

                                </b>
                            </small>
                            <hr>
                            <p style="color:#555;font-weight: 700;font-size: 14px;">
                                <b>
                                    <a href="<?= $detailUrl ?>">
                                        <?= $list['ad_title'] ?>
                                    </a>
                                    <label class="badge badge-danger"><?= $labelClass; ?></label>
                                </b>
                            <p>
                            <h6 class="card-subtitle text-muted">
                                <?= Reviews::show($list) ?>
                            </h6>


                        </div>
                        <div class="card-footer">
                            <div>
                                <button class="btn btn-md btn ">
                                    <span>
                                        <i class="fa fa-shopping-cart"></i>
                                    <strong> <?= $list['currency_symbol'] ?> <?= $list['price']; ?> /- </strong>
                                    </span>
                                </button>
                                <button class="btn btn saveAdsBtn btn-md btn-warning pull-right"
                                        data-del="<?= $data_del; ?>" data-ref="<?= $data_ref ?>"
                                        ads="<?= $list['id'] ?>">
                                    <i id="dil<?= $list['id'] ?>" class="fa fa-heart"></i>
                                    <span id="saved<?= $list['id'] ?>"><?= Yii::t('app', 'Save'); ?></span>
                                </button>

                            </div>
                        </div>
                    </div>
                </div>


                <?php
            } elseif ($temp == '3') {
                ?>

                <div class="temp3Wrap item-list make-list">
                    <div class="project">
                        <div class="add-image project-imgWrap">
                            <a href="<?= $detailUrl ?>">
                                <div class="cover-tmp3">
                                    <img class="img-fluid <?= $lazy; ?>" data-src="<?= $dataSrc ?>" src="<?= $src; ?>"
                                         data-srcset="<?= $dataSrcset ?>" alt="<?= $imgAlt ?>" title="<?= $imgTitle ?>">

                                </div>
                            </a>

                            <span class="photo-count">
                                <i class="fa fa-camera"></i>
                                <?= count($imageArray) ?>
                            </span>
                        </div>
                        <div class="project-content">
                            <div class="title">
                                <a href="<?= $detailUrl; ?>">
                                    <?= substr($list['ad_title'], 0, 25); ?>...
                                </a>
                            </div>
                            <h6 class="card-subtitle text-muted">
                                <?= Reviews::show($list) ?>
                            </h6>
                            <div class="loc">
                                <i class="fa fa-map-marker"></i> <?= Yii::t('app', $list['city']) ?>,
                                <i class="icon-clock"></i> <?= date('d-m-Y', $list['created_at']) ?>

                            </div>
                            <ul class="list-inline">
                                <li>
                                    <?= Yii::t('category', $list['category']) ?>
                                </li>
                                <li><span class="fa fa-angle-right"></span></li>
                                <li><?= Yii::t('sub_category', $list['sub_category']) ?></li>
                                <li><span class="fa fa-angle-right"></span></li>
                                <li><?= Yii::t('type', $list['type']); ?></li>
                            </ul>
                            <button class="btn btn-sm hideInGrid">
                                <i class="fa fa-clock-o"></i>
                                <?= \common\models\Analytic::time_elapsed_string($list['created_at']) ?>
                                <?= Yii::t('app', 'Ago'); ?>
                            </button>
                            <div class="hideInList ">
                                <button class="btn btn-clipboard hidden-sm hidden-xs">
                                    <i class="fa fa-pie-chart"></i>
                                    <?= Yii::t('app', $list['ad_type']) ?>
                                </button>
                                <button class="btn btn-clipboard ">
                                    <i id="dil<?= $list['id'] ?>"></i>
                                    <strong><?= $list['currency_symbol'] ?></strong> <?= $list['price'] ?>
                                </button>
                                <button class="btn saveAdsBtn btn-md btn-clipboard btn-success"
                                        data-del="<?= $data_del; ?>" data-ref="<?= $data_ref ?>"
                                        ads="<?= $list['id'] ?>">
                                    <i id="dil<?= $list['id'] ?>" class="fa fa-heart"></i>
                                </button>

                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="forListContent">
                            <div>
                                <button class="btn btn-clipboard ">
                                    <i id="dil<?= $list['id'] ?>" class="fa fa-dollar"></i>
                                    <?= $list['currency_symbol'] ?> <?= $list['price'] ?>
                                </button>
                                <button class="btn saveAdsBtn btn-md btn-clipboard btn-success"
                                        data-del="<?= $data_del; ?>" data-ref="<?= $data_ref ?>"
                                        ads="<?= $list['id'] ?>">
                                    <i id="dil<?= $list['id'] ?>" class="fa fa-heart"></i>
                                </button>
                            </div>
                            <br>
                            <div class="badge badge-secondary">
                                <i class="fa fa-pie-chart"></i>
                                <?= Yii::t('app', $list['ad_type']) ?>
                            </div>

                        </div>
                    </div>
                </div>
                <?php
            } elseif ($temp == '4') {
                ?>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 p-1">
                    <div class="card p-0" style="box-shadow:none;border: 1px solid #eee">
                        <div class="card-img "
                             style="display:table-cell;vertical-align:middle;text-align:center;;height: 180px;overflow-y: hidden">
                            <img style="margin: auto" onerror="$(this).attr('src','<?= $onError; ?>')"
                                 class="card-img <?= $lazy; ?>" data-src="<?= $dataSrc ?>" src="<?= $src; ?>"
                                 data-srcset="<?= $dataSrcset ?>" alt="<?= $imgAlt ?>" title="<?= $imgTitle ?>">
                        </div>
                        <div class="card-body">

                            <h3 class="card-title mb-0"><?= substr($list['ad_title'], 0, 25); ?>...</h3>
                            <h6 class="card-subtitle text-muted">
                                <?php $widgets->get(); ?>
                            </h6>
                            <h6 class="card-subtitle text-muted">
                                <?= Yii::t('category', $list['category']) ?>,
                                <i class="fa fa-map-marker"></i> <?= Yii::t('app', $list['city']) ?>,
                                <i class="icon-clock"></i> <?= date('d-m-Y', $list['created_at']) ?>
                            </h6>
                            <p class="card-text">
                                <b>
                                    <?= $list['currency_symbol'] ?></strong> <?= $list['price'] ?>
                                </b>
                            </p>
                            <a href="<?= $detailUrl ?>" class="card-link">
                                <?= $list['currency_symbol'] ?></strong> <?= $list['price'] ?>,
                                <strong>Get Details</strong>
                            </a>

                        </div>
                    </div>
                </div>
                <?php
            } elseif ($temp == '5') {
                ?>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 p-1">
                    <div class="card p-0" style="box-shadow:none;border: 1px solid #eee">
                        <div class="card-img "
                             style="display:table-cell;vertical-align:middle;text-align:center;;height: 180px;overflow-y: hidden">
                            <img style="margin: auto" onerror="$(this).attr('src','<?= $onError; ?>')"
                                 class="card-img <?= $lazy; ?>" data-src="<?= $dataSrc ?>" src="<?= $src; ?>"
                                 data-srcset="<?= $dataSrcset ?>" alt="<?= $imgAlt ?>" title="<?= $imgTitle ?>">
                        </div>
                        <div class="card-body">

                            <h3 class="card-title mb-0"><?= substr($list['ad_title'], 0, 25); ?>...</h3>
                            <h6 class="card-subtitle text-muted">
                                <?php $widgets->get(); ?>
                            </h6>
                            <h6 class="card-subtitle text-muted">
                                <?= Yii::t('category', $list['category']) ?>,
                                <i class="fa fa-map-marker"></i> <?= Yii::t('app', $list['city']) ?>,
                                <i class="icon-clock"></i> <?= date('d-m-Y', $list['created_at']) ?>
                            </h6>
                            <p class="card-text">
                                <b>
                                    <?= $list['currency_symbol'] ?></strong> <?= $list['price'] ?>
                                </b>
                            </p>
                            <a href="<?= $detailUrl ?>" class="card-link">
                                <?= $list['currency_symbol'] ?></strong> <?= $list['price'] ?>,
                                <strong>Get Details</strong>
                            </a>

                        </div>
                    </div>
                </div>
                <?php
            } else {

                $userImg = User::getImg($list['user_id']);
                if ($userImg) {
                    $userImgPath = Yii::getAlias('@web') . "/images/users/" . User::getImg($list['user_id']);
                } else {
                    $userImgPath = Yii::getAlias('@web') . "/images/users/profile-placeholder.jpg";

                }
                $itemIId = $list['id'];;
                ?>
                <!--                col-xs-12 col-sm-6 col-md-4-->
                <div class="temp4Wrap item-list make-list">
                    <div class="col-item">
                        <div class="post-img-content">
                            <div class="itemImgWrap">
                                <img class="itemImg <?= $lazy; ?>" data-src="<?= $dataSrc ?>" src="<?= $src; ?>"
                                     data-srcset="<?= $dataSrcset ?>" alt="<?= $imgAlt ?>" title="<?= $imgTitle ?>">
                            </div>
                            <span class="post-title <?= ($labelClass) ? '' : 'hidden' ?>">
                                <b><?= $labelClass ?> </b><br>
                            </span>
                            <span class="round-tag" id="block_<?= $itemIId ?>"
                                  style="background-size: cover;background-position: center;background-image: url('<?= $userImgPath; ?>')">
                            <img class="img-fluid"
                                 onerror="$('#block_<?= $itemIId ?>').css('background-image','url('<?= $onError; ?>')')"
                                 src="<?= $userImgPath; ?>"
                                 style="border: none; border-radius: 50px;width: 0;height: 0;opacity: 0">

                            </span>
                        </div>
                        <div class="info">
                            <div class="row">
                                <div class="price col-md-12">
                                    <h5 class="titleTmp4Grid" style="font-family: proxima-semiBold;font-weight: 800;">
                                        <a href="<?= $detailUrl ?>">
                                            <?= substr($list['ad_title'], 0, 23); ?>...
                                        </a>
                                    </h5>
                                    <h5 class="titleTmp4List">
                                        <a href="<?= $detailUrl ?>">
                                            <?= $list['ad_title']; ?>
                                        </a>
                                    </h5>
                                    <h5 class="price-text-color">
                                        <?= Yii::t('app', 'in') ?>  <?= Yii::t('category', $list['category']) ?>,
                                        <?= Yii::t('app', 'at') ?> <?= Yii::t('app', $list['city']) ?>
                                    </h5>

                                </div>


                            </div>
                            <div class="separator clear-left">
                                <p class="btn-add">

                                    <a href="#" class="badge text-secondary btn">
                                        <strong>
                                            <?= $list['currency_symbol'] ?>
                                            <?= $list['price'] ?>
                                            <i class="fa fa-tag"></i>
                                        </strong>
                                    </a>
                                </p>
                                <p class="btn-details">
                                    <button class="btn saveAdsBtn btn-md btn-clipboard btn-primary"
                                            data-del="<?= $data_del; ?>" data-ref="<?= $data_ref ?>"
                                            ads="<?= $list['id'] ?>">
                                        <i id="dil<?= $list['id'] ?>"
                                           class="fa fa-heart"></i> <?= Yii::t('app', 'Save'); ?>
                                    </button>

                                </p>

                            </div>
                            <div class="clearfix">
                            </div>
                        </div>
                        <div class="temp4type">
                            <div class="row">
                                <div class="price col-md-12">
                                    <h5 style="font-family: proxima-semiBold">
                                        &nbsp;
                                    </h5>

                                    <h5 class="price-text-color">
                                        <button class="btn btn-sm"> <?= Yii::t('app', $list['ad_type']); ?></button>
                                    </h5>
                                    <h5 class="price-text-color">
                                        <small>
                                            <?= \common\models\Analytic::time_elapsed_string($list['created_at']) ?>
                                            <?= Yii::t('app', 'Ago'); ?>
                                        </small>
                                    </h5>
                                </div>


                            </div>
                            <div class="clearfix">
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="row ">
                            <div class="col-8">
                                <a href="<?= $detailUrl ?>">
                                    <h2>
                                        <?= substr($list['ad_title'], 0, 23); ?>...
                                    </h2>
                                </a>
                            </div>
                            <div class="col-4">
                                <button class="btn saveAdsBtn btn-block btn-clipboard btn-primary"
                                        data-del="<?= $data_del; ?>" data-ref="<?= $data_ref ?>"
                                        ads="<?= $list['id'] ?>">
                                    <i id="dil<?= $list['id'] ?>" class="fa fa-heart"></i> <?= Yii::t('app', 'Save'); ?>
                                </button>
                            </div>
                            <div class="col-12">
                                <p>
                                    <?= Yii::t('app', 'in') ?>  <?= Yii::t('category', $list['category']) ?>,
                                    <?= Yii::t('app', 'at') ?> <?= Yii::t('app', $list['city']) ?>
                                </p>
                                <h3>
                                    <?= $list['currency_symbol'] ?>
                                    <?= $list['price'] ?>
                                    <i class="fa fa-tag"></i>
                                </h3>

                            </div>
                        </div>
                    </div>
                </div>
                <?php
            }
        }
        echo "</div>";
    }

    public function item($list)
    {
        $imagebase = $list['image'];
        $imageArray = explode(",", $imagebase);
        $imgs = reset($imageArray);
        $img = ($list['category'] == 'Jobs') ? 'placeholder.jpg' : $imgs;
        $ImageUrl = Yii::getAlias('@web') . '/images/item/' . $img; // Image Directory URL
        $data_del = \yii\helpers\Url::toRoute('saved/remove-ads/' . base64_encode($list['id'])); // SAVE ADS URL
        $data_ref = \yii\helpers\Url::toRoute('saved/ads/' . base64_encode($list['id'])); // REMOVE AD FROM SAVE LIST URL
        $imgAlt = "Item Image";
        $imgTitle = "";
        $detailUrl = $this->detailUrl;
        $time = $this->detailUrl;
        //$premium = ($list['premium'] != '0')?true:false;
        $isPremium = ($list['premium']) ? '' : 'd-none';
        if ($this->lazy == 'off') {
            $lazy = "";
            $onError = Yii::getAlias('@web') . "/images/item/itemLight.jpg";
            $dataSrc = $ImageUrl;
            $dataSrcset = $ImageUrl;
            $src = $ImageUrl;
        } else {
            $lazy = "lazy";
            $onError = Yii::getAlias('@web') . "/images/item/itemLight.jpg";
            $dataSrc = $ImageUrl;
            $dataSrcset = $ImageUrl;
            $src = $onError;
        }
        ?>
        <hr>
        <a class="link text-dark" href="<?= $detailUrl; ?>">
            <li class="media border-bottom mb-2">
                <div class="pull-left p-2">
                    <img style="width: 60px;" onerror="$(this).attr('src','<?= $onError; ?>')" src="<?= $src; ?>"
                         class="media-object w100 <?= $lazy; ?>" data-src="<?= $dataSrc ?>"
                         data-srcset="<?= $dataSrcset ?>" alt="<?= $imgAlt ?>" title="<?= $imgTitle ?>">

                </div>
                <div class="media-body">

                    <h4 class="media-heading pb-0 text-capitalize">
                        <?= $list['ad_title']; ?>

                    </h4>
                    <span class="label label-default <?= $isPremium; ?>">
                <?= Yii::t('app', 'PREMIUM'); ?>
                </span>
                    <span class="ml-2 text-secondary">
                    <?= $list['sub_category'] ?> - <?= $list['type'] ?>
                </span>
                    <h6 class=" pb-1">
                        <i class="fa fa-map-marker"></i> in <?= $list['city'] ?>,
                        <?= Yii::t('app', 'ad posted on'); ?>  <?= date('d/m/Y', $list['created_at']) ?>
                        <small>
                            (
                            <?= \common\models\Analytic::time_elapsed_string($list['created_at']) ?>
                            <?= Yii::t('app', 'Ago'); ?>
                            )
                        </small> -
                    </h6>

                    <p class="pb-1 m-0">
                   <span class="text-green">
                            <?= $list['currency_symbol'] ?>
                        </span>
                        <?= $list['price'] ?>/-
                    </p>

                </div>
            </li>
        </a>
        <?php
    }

    public static function DropDownCategory()
    {
        $data = Category::find()->select(['fa_icon', 'name'])->asArray()->all();
        foreach ($data as $list) {
            $name = $list['name'];
            $url = \yii\helpers\Url::to(['ads/listing', 'cat' => TextHelper::getCategoryText($list['name']), 'location' => TextHelper::getCategoryText(D_CITY)]);
            $icon_padding = "22px";
            $font_size = "14px";
            $font_weight = "900";
            $marginRight = "10px";
            $icon_font_color = "#555";
            $icon = Yii::getAlias('@web/images/icon') . '/' . $list['fa_icon'];
            echo $list = '<li class="dropdown-item p-2">
                    <a href="' . $url . '" class="light-text-1 text-uppercase" style="font-size:' . $font_size . '">
                        <img width="35" src="' . $icon . '"> <strong>' . $name . '</strong>
                    </a>
                </li>';
        }
    }

    public static function categoryUrl($cat, $sub_cat, $type, $sort, $near)
    {
        if ($cat == true and $sub_cat == false and $type == false and $sort == false and $near == false) {
            return $allAds = \yii\helpers\Url::toRoute('ads/content-cat/' . $cat);
        } elseif ($cat == true and $sub_cat == true and $type == false and $sort == false and $near == false) {
            return $allAds = \yii\helpers\Url::toRoute('ads/content-cat/' . $cat . '/' . $sub_cat);
        } elseif ($cat == true and $sub_cat == true and $type == true and $sort == false and $near == false) {
            return $allAds = \yii\helpers\Url::toRoute('ads/content-cat/' . $cat . '/' . $sub_cat . '/' . $type);
        } elseif ($cat == true and $sub_cat == true and $type == true and $sort == false and $near == true) {
            return $allAds = \yii\helpers\Url::toRoute('ads/content-cat/' . $cat . '/' . $sub_cat . '/' . $type . '/' . false . '/' . $near);
        } else {
            return "wrong";
        }

    }

    public static function TypeDefiner($cat)
    {
        switch ($cat) {
            case "Furniture":
                return 'Condition';
                break;
            case "Jobs":
                return 'Type';
                break;
            case "Mobiles":
                return 'Brand';
                break;
            case "Services":
                return 'Service Type';
                break;
            case "Vehicle":
                return 'Brand';
                break;
            case "Real Estate":
                return 'For';
                break;
            case "Bikes":
                return 'Brand';
                break;
            case "Pets":
                return 'Pets Bread';
                break;
            case "Kids":
                return 'Type';
                break;
            case "Fashion":
                return 'For';
                break;
            case "Electronics and Appliances":
                return 'Type';
                break;
            case "Books, Sports and Hobbies":
                return 'For ';
                break;
            default :
                return 'Type';


        }
    }

    public static function ProfileAds($data, $temp = false, $fav = false)
    {
        if (!$data) {
            ?>
            <tr>
                <td colspan="5" style="padding: 10px 20px">

                    <h4>
                        <?= Yii::t('app', 'No Item Found') ?>
                    </h4>
                </td>
            </tr>
            <?php
        } else {
            foreach ($data as $list) {
                $imagebase = $list['image'];
                $imageArray = explode(",", $imagebase);
                $imgs = reset($imageArray);
                $img = ($list['category'] == 'Jobs') ? 'VootJobs.jpg' : $imgs;
                $controlHide = ($list['category'] == 'Jobs') ? 'hidden' : '';
                $controlShow = ($list['category'] == 'Jobs') ? '' : 'hidden';
                $faker = Factory::create();
                $labelClass = $faker->randomElement(['topAds', 'featuredAds', 'urgentAds']);

                $filtrUrlFind = array('/\,/', '/\s+/', '/\?+/', '/\'/');
                $filtrUrlReplace = array('-', '-', '-');

                $detailUrl = \yii\helpers\Url::to([
                    'ads/detail',
                    'title' => preg_replace($filtrUrlFind, $filtrUrlReplace, $list['ad_title']),
                    'IID' => $list['id']
                ]);
                $ImageUrl = Yii::getAlias('@web') . '/images/item/' . $img; // Image Directory URL
                $data_del = \yii\helpers\Url::toRoute('saved/remove-ads/' . base64_encode($list['id'])); // SAVE ADS URL
                $data_ref = \yii\helpers\Url::toRoute('saved/ads/' . base64_encode($list['id'])); // REMOVE AD FROM SAVE LIST URL
                $imgAlt = 'Item Image';
                $imgTitle = '';
                $favAds = ($fav) ? "d-none hidden" : "";

                ?>
                <tr id="list_<?= $list['id'] ?>">
                    <td style="width:2%" class="add-img-selector">
                        <div class="checkbox <?= $favAds; ?>">
                            <label>
                                <input type="checkbox" name="ads-delete" value="<?= $list['id'] ?>">
                            </label>
                        </div>
                    </td>
                    <td style="width:14%" class="add-img-td">
                        <a href="<?= $detailUrl ?>">
                            <img class="thumbnail  img-responsive" src="<?= $ImageUrl ?>" alt="<?= $imgAlt ?>"
                                 title="<?= $imgTitle ?>">
                        </a>
                    </td>
                    <td style="width:58%" class="ads-details-td">
                        <div>
                            <p>
                                <strong>
                                    <a href="<?= $detailUrl; ?>" title="<?= $list['ad_title'] ?>">
                                        <?= Yii::t('app', $list['ad_title']) ?>
                                    </a>
                                </strong>
                            </p>

                            <p>
                                <strong> <?= Yii::t('app', 'Posted On') ?> </strong>:
                                <?= date('d-M-Y', $list['created_at']) ?>  <?= date('h:s', $list['created_at']) ?>
                            </p>
                            <p>
                                <strong><?= Yii::t('app', 'view') ?> </strong>
                                : <?= $list['view'] ?> <strong>Located In:</strong>
                                <?= Yii::t('app', $list['city']); ?>
                            </p>
                        </div>
                    </td>
                    <td style="width:16%" class="price-td">
                        <div><strong> <?= $list['currency_symbol']; ?> <?= $list['price'] ?></strong></div>
                    </td>
                    <td style="width:10%" class="action-td">
                        <div>
                            <p>
                                <a href="<?= \yii\helpers\Url::to(['edit/index', 'id' => $list['id']]) ?>"
                                   class="btn btn-success btn-sm <?= $favAds; ?>"> <i
                                            class="fa fa-edit"></i> <?= Yii::t('app', 'Edit'); ?> </a>
                            </p>

                            <p>

                                <a id="saveAdsBtn<?= $list['id'] ?>"
                                   class="btn btn-warning saveAdsBtn btn-sm make-favorite" ads="<?= $list['id'] ?>"
                                   data-del="<?= $data_del; ?>" data-ref="<?= $data_ref ?>">
                                    <i id="dil<?= $list['id'] ?>" class="fa fa-heart"></i>
                                    <span id="saved<?= $list['id'] ?>"><?= Yii::t('app', 'Save') ?></span>
                                </a>
                            </p>
                            <p>
                                <a onclick="adsDelete('<?= $list['id'] ?>','list_<?= $list['id'] ?>','allDel_btn');"
                                   class="btn btn-danger <?= $favAds; ?> btn-sm">
                                    <i class=" fa fa-trash"></i>
                                    <?= Yii::t('app', 'Delete') ?>
                                </a>

                            </p>
                        </div>
                    </td>
                </tr>
                <?php
            }
        }

    }

    public static function featured()
    {
        $premium = AdsPremium::find()->where(['id' => '2'])->one();
        if ($premium['display_in'] = "city") {
            $model = Ads::find()->where(['premium' => 'featured'])->andWhere(['city' => D_CITY])->all();

        } elseif ($premium['display_in'] = "state") {

            $model = Ads::find()->where('premium' != '')->andWhere(['state' => D_STATE])->all();
        } else {
            $model = Ads::find()->where('premium' != '')->andWhere(['city' => D_COUNTRY])->all();
        };

        $show = ($model == true) ? "show" : "hidden";
        ?>
        <div class="col-xl-12 content-box <?= $show; ?>">
            <div class="row row-featured">
                <div class="col-xl-12  box-title ">
                    <div class="inner">
                        <h2>
                            <span><?= Yii::t('app', 'Sponsored') ?></span>
                            <?= Yii::t('app', 'Products') ?>
                            <a href="<?= Url::toRoute('ads/listing') ?>" class="sell-your-item">
                                <?= Yii::t('app', 'View more') ?>
                                <i class="icon-th-list"></i>
                            </a>
                        </h2>
                    </div>
                </div>
                <div style="clear: both"></div>
                <div class="relative  content featured-list-row  w100">

                    <nav class="slider-nav has-white-bg nav-narrow-svg">
                        <a class="prev">
                            <span class="nav-icon-wrap"></span>

                        </a>
                        <a class="next">
                            <span class="nav-icon-wrap"></span>
                        </a>
                    </nav>

                    <div class="no-margin featured-list-slider ">
                        <?php
                        foreach ($model as $list) {
                            $imagebase = $list['image'];
                            $imageArray = explode(",", $imagebase);
                            $imgs = reset($imageArray);
                            $img = ($list['category'] == 'Jobs') ? 'QuikJobs.jpg' : $imgs;;
                            $controlHide = ($list['category'] == 'Jobs') ? 'hidden' : '';
                            $controlShow = ($list['category'] == 'Jobs') ? '' : 'hidden';
                            // $faker = Factory::create();
                            $labelClass = $list['premium']; //$faker->randomElement(['topAds','featuredAds','urgentAds']);

                            $filtrUrlFind = array('/\,/', '/\s+/', '/\?+/', '/\'/');
                            $filtrUrlReplace = array('-', '-', '-');

                            $detailUrl = \yii\helpers\Url::to([
                                'ads/detail',
                                'title' => preg_replace($filtrUrlFind, $filtrUrlReplace, $list['ad_title']),
                                'IID' => $list['id']
                            ]);
                            $ImageUrl = Yii::getAlias('@web') . '/images/item/' . $img; // Image Directory URL
                            $data_del = \yii\helpers\Url::toRoute('saved/remove-ads/' . base64_encode($list['id'])); // SAVE ADS URL
                            $data_ref = \yii\helpers\Url::toRoute('saved/ads/' . base64_encode($list['id'])); // REMOVE AD FROM SAVE LIST URL
                            $imgAlt = 'Voot Image';
                            $imgTitle = '';
                            ?>
                            <div class="item">
                                <a href="<?= $detailUrl; ?>" class="p-1">
                                    <span class="item-carousel-thumb">
                                        <img height="150" class="img-responsive" src="<?= $ImageUrl; ?>"
                                             alt="<?= $imgAlt; ?>">
                                     </span>
                                    <span class="item-name" style="min-height: 1px">
                                        <?= Yii::t('app', substr($list['ad_title'], '0', '20')); ?>...
                                    </span>
                                    <span class="price">
                                        <?= $list['currency_symbol']; ?> <?= $list['price']; ?>
                                    </span>
                                </a>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <?php


    }

    public static function lat()
    {
        $defaultSettings = \common\models\DefaultSetting::getDefaultSetting();
        $adsSetting = \common\models\Widgets::findOne(['id' => 8]);
        //number_of_item
        $locationType = $defaultSettings['l_type'];;
        if ($locationType == '') {
            $cityDefault = (Isset($defaultSettings['city'])) ? $defaultSettings['city'] : $location;
            $locationType = 'city';

        } elseif ($locationType == 'city') {
            $cityDefault = Isset($defaultSettings['CurrCity']) ? $defaultSettings['CurrCity'] : $location;

        } elseif ($locationType == 'states') {
            $cityDefault = $defaultSettings['state'];
        } else {
            $cityDefault = $defaultSettings['country'];

        }
        $model = Ads::find()->where([$locationType => $cityDefault])->andWhere(['active' => 'yes'])->orderBy(['created_at' => SORT_DESC])->limit($adsSetting['number_of_item'])->all();
        $show = ($model == true) ? "show" : "hidden";
        ?>
        <div class="col-xl-12 col-lg-12 content-box <?= $show; ?>">
            <div class="row row-featured">
                <div class="col-xl-12  box-title ">
                    <div class="inner">
                        <h2>
                            <span><?= Yii::t('app', 'Latest Ads') ?></span>
                            <a href="<?= Url::toRoute('ads/listing') ?>" class="sell-your-item">
                                <?= Yii::t('app', 'View more') ?>
                                <i class="icon-th-list"></i>
                            </a>
                        </h2>
                    </div>
                </div>
                <div style="clear: both"></div>

            </div>
            <div class="row no-gutters p-2">
                <?= TemplatesDesign::ItemList($model, 4); ?>
                <div class="col-12 mt-5 mb-5" align="center">
                    <h2>
                        <?= Yii::t('app', 'Want to see more Latest Ads') ?>
                    </h2>
                    <p>
                        <?= Yii::t('app', 'Click below to see all latest ads') ?>
                    </p>
                    <a href="<?= Url::toRoute('ads/listing') ?>" class="btn btn-default">
                        <?= Yii::t('app', 'View more') ?>
                    </a>
                </div>
            </div>

        </div>
        <?php

    }

    public static function PopularCategory($template)
    {
        $modal = \common\models\Category::find()->where(['!=', 'item', '0'])->orderBy(['item' => SORT_DESC])->all();

        ?>
        <div class="inner-box">
            <h2 class="title-2"><?= Yii::t('home', 'Popular Categories') ?> </h2>

            <div class="inner-box-content">
                <ul class="cat-list arrow">
                    <?php
                    foreach ($modal as $list) {
                        ?>
                        <li>
                            <a href="<?= \yii\helpers\Url::to(['ads/listing', 'cat' => TextHelper::getCategoryText($list['name']), 'location' => TextHelper::getCategoryText(D_CITY)]) ?>">
                                <?= Yii::t('category', $list['name']) ?>
                                (<?= $list['item'] ?> )
                            </a>
                        </li>
                        <?php
                    }
                    ?>
                </ul>
            </div>
        </div>
        <?php
    }

    public static function Zero()
    {
        $tmp = "<div class='col-12'><div class='card'><div class='card-body'><h1 class='card-title'>Didn't find anything</h1><p class='card-text'>Please Search with different perameter, and you can post an ads in this category</p></div></div></div>";
    }
}
