<?php
use \yii\web\Request;
//use Sitemaped\Sitemap;
$baseUrl = str_replace('/frontend/web', '', (new Request)->getBaseUrl());
return [
        'baseUrl' => $baseUrl,
        'enablePrettyUrl' => true,
        'showScriptName' => false,
        // 'enableStrictParsing'=>false,
        'rules' =>[

            ''=>'site/index',
            ['class' => 'frontend\components\GoodUrlRule', 'pattern' => '<cat>-in-<location>', 'route' => 'ads/listing'],
            ['class' => 'frontend\components\GoodUrlRule', 'pattern' => '<cat>/<sub_cat>-in-<location>', 'route' => 'ads/listing'],
            ['class' => 'frontend\components\GoodUrlRule', 'pattern' => '<type>-<sub_cat>/<location>', 'route' => 'ads/listing'],


            '<cat:\w+>-in-<location>'=>'ads/listing',
            '<cat:\w+>/<sub_cat:\w+>-in-<location>'=>'ads/listing',
            '<type:\w+>-<sub_cat:\w+>-in-<location>'=>'ads/listing',

            ['class' => 'frontend\components\PostUrlRule', 'pattern' => 'item/<title>-IID<IID>', 'route' => 'ads/detail'],

            'item/<title>-IID<IID>'=>'ads/detail',
            'item/<title>-IID<IID>'=>'ads/detail',
            'item/<title>/IID<IID>'=>'ads/detail',
            'item/<title>,IID<IID>'=>'ads/detail',


//                [
//                    'pattern'=>'item/<title>-IID<IID>',
//                    'route'=>'ads/detail',
//                    'suffix'=>'.php'
//
//
//                ],

            'user/profile/<username>' => 'user/profile',
            'user/index'=>'user/index',
            'ad/view/<id>' => 'ad/view',
            'ads/edit-ads/<id>' => 'ads/edit-ads',

            'ads/post'=>'ads/post-ads',
            'ads/delete/<id>' => 'ads/delete',
            'ads/delete-ajax/<id>' => 'ads/delete-ajax',

            'ads/category/<cat>' => 'ads/category',

            'ads/category/<cat>/<sub_cat>' => 'ads/category',
            'ads/category/<cat>/<sub_cat>/<type>' => 'ads/category',
            'ads/content-cat/<cat>' => 'ads/content-cat',
            'ads/content-cat/<cat>/<sub_cat>' => 'ads/content-cat',
            'ads/content-cat/<cat>/<sub_cat>/<type>' => 'ads/content-cat',
            'ads/listing' => 'ads/listing',

            'user/u/<user>' => 'user/u',
            'ads/location/<id>' => 'ads/location',
            'ads/final/<ads>' => 'ads/final',

            'search/city/<city>'=> 'search/city',
            'search/set/<country>/<state>/<city>/<type>/<flag>'=> 'search/set',
            'search/sub-category/<id>'=> 'search/sub-category',
            'search/ptype/<id>'=> 'search/ptype',
            'saved/ads/<ref>' => 'saved/ads',
            'saved/remove-ads/<ref>' => 'saved/remove-ads',
            'pages/<id>-<title>'=> 'pages/index',
            'payment/stripe-quick/<type>/<pid>/<price>'=>'payment/stripe-quick',
            'stripe/<type>/<pid>/<price>'=>'stripe/index',
            'paypal/<type>/<pid>/<price>'=>'paypal/index',
            'payment/payu-money/<type>/<pid>/<price>'=>'payment/payu-money',
            'payment/paytm/<type>/<pid>/<price>'=>'payment/paytm',
            'payment/offline/<type>/<pid>/<price>'=>'payment/offline',
            'paystack/<type>/<pid>/<price>'=>'paystack/index',
            'reviews/write/<title>-<token>'=>'reviews/default/write',
            'reviews/index/<token>'=>'reviews/default/index',
        ],
¶];
