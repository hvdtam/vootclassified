<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);
use \yii\web\Request;
use \common\models\SmsTwilio;
$settings = "";
$baseUrl = str_replace('/frontend/web', '', (new Request)->getBaseUrl());
$Keys = \common\models\Plugin::getKeys();
return [
    'id' => 'voot-frontend',
    'basePath' => dirname(__DIR__),
     'bootstrap' => [
         'log',
         [
             'class' => 'frontend\components\SetLanguage',
         ],
         'frontend\base\Settings',
         [
             'class' => 'frontend\base\Settings',
         ],
     ],
   'sourceLanguage' => 'en',
    'modules' => [
        'translate' => 'conquer\i18n\Module',
        'reviews' => [
            'class' => 'frontend\modules\reviews\Module',
        ],
        'blog' => [
            'class' => 'frontend\modules\blog\Module',
        ],
    ],
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
        'Paystack' => [
            'class' => 'smladeoye\paystack\Paystack',
            'environment' => 'test',
            'testPublicKey'=>null,
            'testSecretKey'=>null,
            'livePublicKey'=>'',
            'liveSecretKey'=>'',
        ],
        'request' => [
            'baseUrl' => $baseUrl,
            'cookieValidationKey' => 'QkvfY7uwqQ6pDcfKdDkf',
            'csrfParam' => '_frontendCSRF',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'stripe' => [
            'class' => 'ruskid\stripe\Stripe',
            'publicKey' => "pk_test_04gfgfgfgAvosGzMX5I85V",
            'privateKey' => "pk_test_gfgfgsGzMX5I85V",
        ],

        'session' => [
            'name' => 'PHPFRONTSESSID',
            'savePath' => sys_get_temp_dir(),
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],

        'assetManager'=>[
            'bundles'=>[
                'yii\bootstrap\BootstrapAsset'=>[
                    'css'=>[],
                ]
            ]
        ],

        'sms' => [
            'class' => 'wadeshuler\sms\twilio\Sms',

            // Advanced app use '@common/sms', basic use '@app/sms'
            'viewPath' => '@common/sms',     // Optional: defaults to '@app/sms'

            // send all sms to a file by default. You have to set
            // 'useFileTransport' to false and configure the messageConfig['from'],
            // 'sid', and 'token' to send real messages
            'useFileTransport' => true,

            'messageConfig' => [
                'from' => '+91874587',  // Your Twilio number (full or shortcode)
            ],

            // Find your Account Sid and Auth Token at https://twilio.com/console
            'sid' => 'xxxx',
            'token' => 'ssss',

            // Tell Twilio where to POST information about your message.
            // @see https://www.twilio.com/docs/sms/send-messages#monitor-the-status-of-your-message
            //'statusCallback' => 'https://example.com/path/to/callback',      // optional
        ],
        'authClientCollection' => [

            'class' => 'yii\authclient\Collection',
            'clients' => [
                'facebook' => [
                    'class' =>  yii\authclient\clients\Facebook::class,
                    'authUrl' => 'https://www.facebook.com/dialog/oauth?display=popup',
                    'clientId' => env_value('FACEBOOK_CLIENT_ID'),
                    'clientSecret' => env_value('FACEBOOK_CLIENT_SECRET'),
                    'attributeNames' => ['name', 'email', 'first_name', 'last_name'],
                ],
            ],
        ],
        'urlManager' => [
            'baseUrl' => $baseUrl,
            'enablePrettyUrl' => true,
            'showScriptName' => false,
           // 'enableStrictParsing'=>false,
            'rules' =>[

                ''=>'site/index',
                ['class' => 'frontend\components\GoodUrlRule', 'pattern' => '<cat>-in-<location>', 'route' => 'ads/listing'],
                ['class' => 'frontend\components\GoodUrlRule', 'pattern' => '<cat>/<sub_cat>-in-<location>', 'route' => 'ads/listing'],
                ['class' => 'frontend\components\GoodUrlRule', 'pattern' => '<type>-<sub_cat>/<location>', 'route' => 'ads/listing'],


                '<cat:\w+>-in-<location>'=>'ads/listing',
                '<cat:\w+>/<sub_cat:\w+>-in-<location>'=>'ads/listing',
                '<type:\w+>-<sub_cat:\w+>-in-<location>'=>'ads/listing',

                ['class' => 'frontend\components\PostUrlRule', 'pattern' => 'item/<title>-IID<IID>', 'route' => 'ads/detail'],

                'item/<title>-IID<IID>'=>'ads/detail',
                'item/<title>-IID<IID>'=>'ads/detail',
                'item/<title>/IID<IID>'=>'ads/detail',
                'item/<title>,IID<IID>'=>'ads/detail',


//                [
//                    'pattern'=>'item/<title>-IID<IID>',
//                    'route'=>'ads/detail',
//                    'suffix'=>'.php'
//
//
//                ],

                'user/profile/<username>' => 'user/profile',
                'user/index'=>'user/index',
                'ad/view/<id>' => 'ad/view',
                'ads/edit-ads/<id>' => 'ads/edit-ads',

                'ads/post'=>'ads/post-ads',
                'ads/delete/<id>' => 'ads/delete',
                'ads/delete-ajax/<id>' => 'ads/delete-ajax',

                'ads/category/<cat>' => 'ads/category',

                'ads/category/<cat>/<sub_cat>' => 'ads/category',
                'ads/category/<cat>/<sub_cat>/<type>' => 'ads/category',
                'ads/content-cat/<cat>' => 'ads/content-cat',
                'ads/content-cat/<cat>/<sub_cat>' => 'ads/content-cat',
                'ads/content-cat/<cat>/<sub_cat>/<type>' => 'ads/content-cat',
                'ads/listing' => 'ads/listing',

                'user/u/<user>' => 'user/u',
                'ads/location/<id>' => 'ads/location',
                'ads/final/<ads>' => 'ads/final',

                'search/city/<city>'=> 'search/city',
                'search/set/<country>/<state>/<city>/<type>/<flag>'=> 'search/set',
                'search/sub-category/<id>'=> 'search/sub-category',
                'search/ptype/<id>'=> 'search/ptype',
                'saved/ads/<ref>' => 'saved/ads',
                'saved/remove-ads/<ref>' => 'saved/remove-ads',
                'pages/<id>-<title>'=> 'pages/index',
                'payment/stripe-quick/<type>/<pid>/<price>'=>'payment/stripe-quick',
                'stripe/<type>/<pid>/<price>'=>'stripe/index',
                'paypal/<type>/<pid>/<price>'=>'paypal/index',
                'payment/payu-money/<type>/<pid>/<price>'=>'payment/payu-money',
                'payment/paytm/<type>/<pid>/<price>'=>'payment/paytm',
                'payment/offline/<type>/<pid>/<price>'=>'payment/offline',
                'paystack/<type>/<pid>/<price>'=>'paystack/index',
                'reviews/write/<title>-<token>'=>'reviews/default/write',
                'reviews/index/<token>'=>'reviews/default/index',
            ],

        ],
        'i18n' => [
            'class' => 'yii\i18n\I18N',
            'translations' => $Keys['translationsKeys'],
        ],
        'view' => [
            'class' => 'frontend\components\AppView',
        ],
    ],
    'params' => $params,
];
