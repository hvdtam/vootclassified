<?php

use yii\db\Migration;

/**
 * Class m180702_125835_ads
 */
class m180702_125835_ads extends Migration
{
    /**
     * {@inheritdoc}
     */
//    public function safeUp()
//    {
//
//    }
//
//    /**
//     * {@inheritdoc}
//     */
//    public function safeDown()
//    {
//        echo "m180702_125835_ads cannot be reverted.\n";
//
//        return false;
//    }


    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $faker = Faker\Factory::create("en_US");
        for($i=0;$i<11;$i++)
        {
            $cat = "Real Estate";
            $Subcat = "House";//$faker->randomElement(['Cars','Apartments','House','Shops - Offices - Commercial Space','PG And Roommates','Vacation Rentals - Guest Houses']) ;
            $type = ['Rent','Sale'];
            $tags = 'House, Rent, banglow, PG, Guest House';
            $randType = $faker->randomElement($type);
           \common\models\Category::addCounter($cat);
            \common\models\SubCategory::addCounter($Subcat);
            \common\models\Type::addCounteFakerr($randType,$Subcat);

            //'Tata','Maruti','Volkwagon','Ranult','Nisan','Audi','BMW'
            $this->insert('ads',[
                'user_id'=>'8',
                'category'=> 'Vehicle',
                'price'=> $faker->numberBetween($min = 11000, $max = 9000),
                'sub_category'=> $Subcat,
                'type'=> $randType,
                'ad_type'=> 'Private',
                'ad_title'=> $faker->sentence($nbWords = 6, $variableNbWords = true),
                'ad_description'=> $faker->text( $maxNbChars = 200),
                'image'=> $faker->file($sourceDir = Yii::getAlias('@frontend/web/themes/images/house/'),$targetDir = Yii::getAlias('@frontend/web/images/ads/'), false),
                //'image'=> $faker->image(Yii::getAlias("@frontend/web/themes/images/ads/"), 200, 200, null, false),
                'name'=> $faker->name,
                'mobile'=>$faker->email ,
                'email'=> $faker->email,
                'lat'=> $faker->latitude,
                'lng'=> $faker->longitude,
                'country'=> 'India',
                'city'=> 'Jodhpur',
                'states'=>'Rajasthan',
                'address'=> $faker->address,
                'tags'=> $tags,
                'active'=> 'yes',
                'created_at'=> time(),
            ]);
        }
    }

    public function down()
    {
        echo "m180702_125835_ads cannot be reverted.\n";

        return false;
    }

}
